<!doctype html>
<html lang="en-US" >
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<title>Login </title>
<link rel='stylesheet' href='../css/bootstrap.min.css'  media='all' />
<link rel='stylesheet' href='../css/style.css'  media='all' />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"  media="all">
</head>
<body>
<main class="login">
	<div class="login__wrapper">
		<div class="login__wrapper-logo text-center">
			<img src="../images/logo.png">
		</div>
		<form>
			<div class="form-group">
				<label>Email</label>
				<input type="text" class="form-control" name="">
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" class="form-control" name="">
			</div>
			<div class="d-flex mt-5 align-items-center justify-content-between">
				<a href="#">Forgot Password?</a>
				<a href="companies.php" class="btn btn-primary">Login</a>
			</div>
		</form>
	</div> 
</main>
<?php include("footer.php");?>
</body>
</html>
