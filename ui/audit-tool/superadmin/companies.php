<!doctype html>
<html lang="en-US" >
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<title>Companies </title>
<link rel='stylesheet' href='../css/bootstrap.min.css'  media='all' />
<link rel='stylesheet' href='../css/style.css'  media='all' />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"  media="all">
</head>
<body>
<!-- Add New Company -->
<div class="modal fade" id="newCompany" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">Add New Company</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form>
       		<div class="form-group">
				<label>Company Name</label>
				<input type="text" class="form-control" name="">
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="text" class="form-control" name="">
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" class="form-control" name="">
			</div>
			<div class="form-group">
				<label>Confirm Password</label>
				<input type="password" class="form-control" name="">
			</div>
			<div class="form-group">
				<label>Company Logo</label>
				<input type="file" class="form-control" name="">
			</div>
			
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-inverse" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>
	<nav class="navbar navbar-expand-lg navbar-theme border-bottom fixed-top">
	   <div class="nav__left">
		   <button id="sidebarCollapse" class="hamburger mr-3">
	        <span></span>
	        <span></span>
	        <span></span>
	      </button>
	      <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	        <i class="fas fa-align-justify"></i>
	      </button> 
	      <a class="navbar-brand" href="#">A</a><span class="meta-role">Super Admin</span>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		</div>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">	    
	    <form class="search my-2 my-lg-0">
	      <input class="form-control mr-sm-2" placeholder="Search" aria-label="Search">	     
	    </form>
	    <ul class="navbar-nav ml-auto">	      
	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle my-0 py-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	         <span class="avatar">JD</span> John Doe
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <div class="p-2 d-flex align-items-center"><span class="avatar mr-2">JD</span> <span><strong>John Doe</strong> <br><small>Admin</small></small></div>
	          <div class="dropdown-divider"></div>
	          <a class="dropdown-item" href="#">Setting</a>
	          <a class="dropdown-item" href="#">Accounts</a>
	          <div class="dropdown-divider"></div>	          
	          <a class="dropdown-item" href="#">Logout</a>
	        </div>
	      </li>	      
	    </ul>
	  </div>
	</nav>
	<section class="wrapper">     
        <nav class="wrapper__sidebar border-right" id="sidebar">            
            <ul class="list-unstyled components">
            	<li class="active">
                    <a href="companies.php"><i class="fas fa-building"></i> Companies</a>
                </li>
                <li>
                    <a href="business-units.php"><i class="fas fa-tag"></i> Business Units</a>
                </li>                
                
            </ul>
        </nav>     
        <div class="wrapper__content" id="content">
          <div class="wrapper__content-header">
          	<h2>Companies</h2>
          	<a href="javascript:;" data-toggle="modal" data-target="#newCompany" class="btn btn-primary"><i class="fas fa-plus"></i> New Company</a>
          </div>
          <div class="wrapper__content-body">
          	<div class="main__content-search mb-4">
			 <form class="search-inverse">
			      <input aria-label="Search" placeholder="Search" class="form-control mr-sm-2">	     
			    </form>			      	
	        </div>
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Company</th>	
							<th class="text-right">Actions</th>							
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								1
							</td>
							<td>
								<div class="media align-items-center">
								  <img class="mr-3 rounded" src="../images/placeholder.svg" alt="Generic placeholder image">
								  <div class="media-body">
								    <strong>Amazon Inc</strong><br>
								    johndoe@mailinator.com
								  </div>
								</div>
							</td>
							<td class="text-right action-icons">
								<a href=""><i class="fas fa-pencil-alt"></i></a>
								<a href=""><i class="fas fa-trash"></i></a>
							</td>
						</tr>

						<tr>
							<td>
								1
							</td>
							<td>
								<div class="media align-items-center">
								  <img class="mr-3 rounded" src="../images/placeholder.svg" alt="Generic placeholder image">
								  <div class="media-body">
								    <strong>Amazon Inc</strong><br>
								    johndoe@mailinator.com
								  </div>
								</div>
							</td>
							<td class="text-right action-icons">
								<a href=""><i class="fas fa-pencil-alt"></i></a>
								<a href=""><i class="fas fa-trash"></i></a>
							</td>
						</tr>
						<tr>
							<td>
								1
							</td>
							<td>
								<div class="media align-items-center">
								  <img class="mr-3 rounded" src="../images/placeholder.svg" alt="Generic placeholder image">
								  <div class="media-body">
								    <strong>Amazon Inc</strong><br>
								    johndoe@mailinator.com
								  </div>
								</div>
							</td>
							<td class="text-right action-icons">
								<a href=""><i class="fas fa-pencil-alt"></i></a>
								<a href=""><i class="fas fa-trash"></i></a>
							</td>
						</tr>
						<tr>
							<td>
								1
							</td>
							<td>
								<div class="media align-items-center">
								  <img class="mr-3 rounded" src="../images/placeholder.svg" alt="Generic placeholder image">
								  <div class="media-body">
								    <strong>Amazon Inc</strong><br>
								    johndoe@mailinator.com
								  </div>
								</div>
							</td>
							<td class="text-right action-icons">
								<a href=""><i class="fas fa-pencil-alt"></i></a>
								<a href=""><i class="fas fa-trash"></i></a>
							</td>
						</tr>
						
						
					</tbody>
				</table>
			</div>
			<div class="main__content-data-footer d-flex justify-content-between align-items-center border-top pt-4">
				<small>Showing 25 of 50 records</small>
				<ul role="navigation" class="pagination">        
            		<li aria-label="« Previous" aria-disabled="true" class="page-item disabled">
        			<span aria-hidden="true" class="page-link">‹</span></li>
               		<li aria-current="page" class="page-item active"><span class="page-link">1</span></li>
                    <li class="page-item"><a href="" class="page-link">2</a></li>
                    <li class="page-item"><a href="" class="page-link">3</a></li>
            		<li class="page-item"><a aria-label="Next »" rel="next" href="" class="page-link">›</a>
    				</li>
    			</ul>
    		</div>
          </div>
        </div>
    </section>

<?php include("footer.php");?>

</body>
</html>
