<!doctype html>
<html lang="en-US" >
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<title>Business Units </title>
<link rel='stylesheet' href='../css/bootstrap.min.css'  media='all' />
<link rel='stylesheet' href='../css/style.css'  media='all' />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"  media="all">
</head>
<body>
<!-- Add New Company -->
<div class="modal fade" id="newCompany" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">Add New Business Unit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form>
       		<div class="form-group">
				<label>BU Name</label>
				<input type="text" class="form-control" name="">
			</div>
			<div class="form-group">
				<label>BU Description</label>
				<textarea class="form-control"></textarea>
			</div>
			<div class="form-group">
				<label>Industry</label>
				<select class="chosen-select">
					<option>Select Industry</option>
					<option value="Accounting">Accounting</option>
					<option value="Airlines/Aviation">Airlines/Aviation</option>
					<option value="Alternative Dispute Resolution">Alternative Dispute Resolution</option>
					<option value="Alternative Medicine">Alternative Medicine</option>
					<option value="Animation">Animation</option>
					<option value="Apparel/Fashion">Apparel/Fashion</option>
					<option value="Architecture/Planning">Architecture/Planning</option>
					<option value="Arts/Crafts">Arts/Crafts</option>
					<option value="Automotive">Automotive</option>
					<option value="Aviation/Aerospace">Aviation/Aerospace</option>
					<option value="Banking/Mortgage">Banking/Mortgage</option>
					<option value="Biotechnology/Greentech">Biotechnology/Greentech</option>
					<option value="Broadcast Media">Broadcast Media</option>
					<option value="Building Materials">Building Materials</option>
					<option value="Business Supplies/Equipment">Business Supplies/Equipment</option>
					<option value="Capital Markets/Hedge Fund/Private Equity">Capital Markets/Hedge Fund/Private Equity</option>
					<option value="Chemicals">Chemicals</option>
					<option value="Civic/Social Organization">Civic/Social Organization</option>
					<option value="Civil Engineering">Civil Engineering</option>
					<option value="Commercial Real Estate">Commercial Real Estate</option>
					<option value="Computer Games">Computer Games</option>
					<option value="Computer Hardware">Computer Hardware</option>
					<option value="Computer Networking">Computer Networking</option>
					<option value="Computer Software/Engineering">Computer Software/Engineering</option>
					<option value="Computer/Network Security">Computer/Network Security</option>
					<option value="Construction">Construction</option>
					<option value="Consumer Electronics">Consumer Electronics</option>
					<option value="Consumer Goods">Consumer Goods</option>
					<option value="Consumer Services">Consumer Services</option>
					<option value="Cosmetics">Cosmetics</option>
					<option value="Dairy">Dairy</option>
					<option value="Defense/Space">Defense/Space</option>
					<option value="Design">Design</option>
					<option value="E-Learning">E-Learning</option>
					<option value="Education Management">Education Management</option>
					<option value="Electrical/Electronic Manufacturing">Electrical/Electronic Manufacturing</option>
					<option value="Entertainment/Movie Production">Entertainment/Movie Production</option>
					<option value="Environmental Services">Environmental Services</option>
					<option value="Events Services">Events Services</option>
					<option value="Executive Office">Executive Office</option>
					<option value="Facilities Services">Facilities Services</option>
					<option value="Farming">Farming</option>
					<option value="Financial Services">Financial Services</option>
					<option value="Fine Art">Fine Art</option>
					<option value="Fishery">Fishery</option>
					<option value="Food Production">Food Production</option>
					<option value="Food/Beverages">Food/Beverages</option>
					<option value="Fundraising">Fundraising</option>
					<option value="Furniture">Furniture</option>
					<option value="Gambling/Casinos">Gambling/Casinos</option>
					<option value="Glass/Ceramics/Concrete">Glass/Ceramics/Concrete</option>
					<option value="Government Administration">Government Administration</option>
					<option value="Government Relations">Government Relations</option>
					<option value="Graphic Design/Web Design">Graphic Design/Web Design</option>
					<option value="Health/Fitness">Health/Fitness</option>
					<option value="Higher Education/Acadamia">Higher Education/Acadamia</option>
					<option value="Hospital/Health Care">Hospital/Health Care</option>
					<option value="Hospitality">Hospitality</option>
					<option value="Human Resources/HR">Human Resources/HR</option>
					<option value="Import/Export">Import/Export</option>
					<option value="Individual/Family Services">Individual/Family Services</option>
					<option value="Industrial Automation">Industrial Automation</option>
					<option value="Information Services">Information Services</option>
					<option value="Information Technology/IT">Information Technology/IT</option>
					<option value="Insurance">Insurance</option>
					<option value="International Affairs">International Affairs</option>
					<option value="International Trade/Development">International Trade/Development</option>
					<option value="Internet">Internet</option>
					<option value="Investment Banking/Venture">Investment Banking/Venture</option>
					<option value="Investment Management/Hedge Fund/Private Equity">Investment Management/Hedge Fund/Private Equity</option>
					<option value="Judiciary">Judiciary</option>
					<option value="Law Enforcement">Law Enforcement</option>
					<option value="Law Practice/Law Firms">Law Practice/Law Firms</option>
					<option value="Legal Services">Legal Services</option>
					<option value="Legislative Office">Legislative Office</option>
					<option value="Leisure/Travel">Leisure/Travel</option>
					<option value="Library">Library</option>
					<option value="Logistics/Procurement">Logistics/Procurement</option>
					<option value="Luxury Goods/Jewelry">Luxury Goods/Jewelry</option>
					<option value="Machinery">Machinery</option>
					<option value="Management Consulting">Management Consulting</option>
					<option value="Maritime">Maritime</option>
					<option value="Market Research">Market Research</option>
					<option value="Marketing/Advertising/Sales">Marketing/Advertising/Sales</option>
					<option value="Mechanical or Industrial Engineering">Mechanical or Industrial Engineering</option>
					<option value="Media Production">Media Production</option>
					<option value="Medical Equipment">Medical Equipment</option>
					<option value="Medical Practice">Medical Practice</option>
					<option value="Mental Health Care">Mental Health Care</option>
					<option value="Military Industry">Military Industry</option>
					<option value="Mining/Metals">Mining/Metals</option>
					<option value="Motion Pictures/Film">Motion Pictures/Film</option>
					<option value="Museums/Institutions">Museums/Institutions</option>
					<option value="Music">Music</option>
					<option value="Nanotechnology">Nanotechnology</option>
					<option value="Newspapers/Journalism">Newspapers/Journalism</option>
					<option value="Non-Profit/Volunteering">Non-Profit/Volunteering</option>
					<option value="Oil/Energy/Solar/Greentech">Oil/Energy/Solar/Greentech</option>
					<option value="Online Publishing">Online Publishing</option>
					<option value="Other Industry">Other Industry</option>
					<option value="Outsourcing/Offshoring">Outsourcing/Offshoring</option>
					<option value="Package/Freight Delivery">Package/Freight Delivery</option>
					<option value="Packaging/Containers">Packaging/Containers</option>
					<option value="Paper/Forest Products">Paper/Forest Products</option>
					<option value="Performing Arts">Performing Arts</option>
					<option value="Pharmaceuticals">Pharmaceuticals</option>
					<option value="Philanthropy">Philanthropy</option>
					<option value="Photography">Photography</option>
					<option value="Plastics">Plastics</option>
					<option value="Political Organization">Political Organization</option>
					<option value="Primary/Secondary Education">Primary/Secondary Education</option>
					<option value="Printing">Printing</option>
					<option value="Professional Training">Professional Training</option>
					<option value="Program Development">Program Development</option>
					<option value="Public Relations/PR">Public Relations/PR</option>
					<option value="Public Safety">Public Safety</option>
					<option value="Publishing Industry">Publishing Industry</option>
					<option value="Railroad Manufacture">Railroad Manufacture</option>
					<option value="Ranching">Ranching</option>
					<option value="Real Estate/Mortgage">Real Estate/Mortgage</option>
					<option value="Recreational Facilities/Services">Recreational Facilities/Services</option>
					<option value="Religious Institutions">Religious Institutions</option>
					<option value="Renewables/Environment">Renewables/Environment</option>
					<option value="Research Industry">Research Industry</option>
					<option value="Restaurants">Restaurants</option>
					<option value="Retail Industry">Retail Industry</option>
					<option value="Security/Investigations">Security/Investigations</option>
					<option value="Semiconductors">Semiconductors</option>
					<option value="Shipbuilding">Shipbuilding</option>
					<option value="Sporting Goods">Sporting Goods</option>
					<option value="Sports">Sports</option>
					<option value="Staffing/Recruiting">Staffing/Recruiting</option>
					<option value="Supermarkets">Supermarkets</option>
					<option value="Telecommunications">Telecommunications</option>
					<option value="Textiles">Textiles</option>
					<option value="Think Tanks">Think Tanks</option>
					<option value="Tobacco">Tobacco</option>
					<option value="Translation/Localization">Translation/Localization</option>
					<option value="Transportation">Transportation</option>
					<option value="Utilities">Utilities</option>
					<option value="Venture Capital/VC">Venture Capital/VC</option>
					<option value="Veterinary">Veterinary</option>
					<option value="Warehousing">Warehousing</option>
					<option value="Wholesale">Wholesale</option>
					<option value="Wine/Spirits">Wine/Spirits</option>
					<option value="Wireless">Wireless</option>
					<option value="Writing/Editing">Writing/Editing</option>
				</select>
			</div>
			
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-inverse" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>
	<nav class="navbar navbar-expand-lg navbar-theme border-bottom fixed-top">
	   <div class="nav__left">
		   <button id="sidebarCollapse" class="hamburger mr-3">
	        <span></span>
	        <span></span>
	        <span></span>
	      </button>
	      <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	        <i class="fas fa-align-justify"></i>
	      </button> 
	      <a class="navbar-brand" href="#">A</a><span class="meta-role">Super Admin</span>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		</div>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">	    
	    <form class="search my-2 my-lg-0">
	      <input class="form-control mr-sm-2" placeholder="Search" aria-label="Search">	     
	    </form>
	    <ul class="navbar-nav ml-auto">	      
	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle my-0 py-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	         <span class="avatar">JD</span> John Doe
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <div class="p-2 d-flex align-items-center"><span class="avatar mr-2">JD</span> <span><strong>John Doe</strong> <br><small>Admin</small></small></div>
	          <div class="dropdown-divider"></div>
	          <a class="dropdown-item" href="#">Setting</a>
	          <a class="dropdown-item" href="#">Accounts</a>
	          <div class="dropdown-divider"></div>	          
	          <a class="dropdown-item" href="#">Logout</a>
	        </div>
	      </li>	      
	    </ul>
	  </div>
	</nav>
	<section class="wrapper">     
        <nav class="wrapper__sidebar border-right" id="sidebar">            
            <ul class="list-unstyled components">
            	<li>
                    <a href="companies.php"><i class="fas fa-building"></i> Companies</a>
                </li>
                <li class="active">
                    <a href="business-units.php"><i class="fas fa-tag"></i> Business Units</a>
                </li>                
                
            </ul>
        </nav>     
        <div class="wrapper__content" id="content">
          <div class="wrapper__content-header">
          	<h2>Companies</h2>
          	<a href="javascript:;" data-toggle="modal" data-target="#newCompany" class="btn btn-primary"><i class="fas fa-plus"></i> New Company</a>
          </div>
          <div class="wrapper__content-body">
          	<div class="main__content-search mb-4">
			 <form class="search-inverse">
			      <input aria-label="Search" placeholder="Search" class="form-control mr-sm-2">	     
			    </form>			      	
	        </div>
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Business Unit</th>
							<th>Description</th>	
							<th class="text-right">Actions</th>							
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								1
							</td>
							<td>								
							    <strong>Apparals</strong><br>
							    E-commerce								
							</td>
							<td>								
							  Lorem ipsum doler sit amet							
							</td>
							<td class="text-right action-icons">
								<a href=""><i class="fas fa-pencil-alt"></i></a>
								<a href=""><i class="fas fa-trash"></i></a>
							</td>
						</tr>
						<tr>
							<td>
								1
							</td>
							<td>								
							    <strong>Apparals</strong><br>
							    E-commerce								
							</td>
							<td>								
							  Lorem ipsum doler sit amet							
							</td>
							<td class="text-right action-icons">
								<a href=""><i class="fas fa-pencil-alt"></i></a>
								<a href=""><i class="fas fa-trash"></i></a>
							</td>
						</tr>
						<tr>
							<td>
								1
							</td>
							<td>								
							    <strong>Apparals</strong><br>
							    E-commerce								
							</td>
							<td>								
							  Lorem ipsum doler sit amet							
							</td>
							<td class="text-right action-icons">
								<a href=""><i class="fas fa-pencil-alt"></i></a>
								<a href=""><i class="fas fa-trash"></i></a>
							</td>
						</tr>
						<tr>
							<td>
								1
							</td>
							<td>								
							    <strong>Apparals</strong><br>
							    E-commerce								
							</td>
							<td>								
							  Lorem ipsum doler sit amet							
							</td>
							<td class="text-right action-icons">
								<a href=""><i class="fas fa-pencil-alt"></i></a>
								<a href=""><i class="fas fa-trash"></i></a>
							</td>
						</tr>
						<tr>
							<td>
								1
							</td>
							<td>								
							    <strong>Apparals</strong><br>
							    E-commerce								
							</td>
							<td>								
							  Lorem ipsum doler sit amet							
							</td>
							<td class="text-right action-icons">
								<a href=""><i class="fas fa-pencil-alt"></i></a>
								<a href=""><i class="fas fa-trash"></i></a>
							</td>
						</tr>

						
						
						
					</tbody>
				</table>
			</div>
			<div class="main__content-data-footer d-flex justify-content-between align-items-center border-top pt-4">
				<small>Showing 25 of 50 records</small>
				<ul role="navigation" class="pagination">        
            		<li aria-label="« Previous" aria-disabled="true" class="page-item disabled">
        			<span aria-hidden="true" class="page-link">‹</span></li>
               		<li aria-current="page" class="page-item active"><span class="page-link">1</span></li>
                    <li class="page-item"><a href="" class="page-link">2</a></li>
                    <li class="page-item"><a href="" class="page-link">3</a></li>
            		<li class="page-item"><a aria-label="Next »" rel="next" href="" class="page-link">›</a>
    				</li>
    			</ul>
    		</div>
          </div>
        </div>
    </section>

<?php include("footer.php");?>

</body>
</html>
