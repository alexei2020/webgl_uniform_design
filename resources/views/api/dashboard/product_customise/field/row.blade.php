<div id="Step_<%= stepId %>" data-id="<%= id %>"  data-designkey="<% if(design_key!=undefined){  %><%= design_key %><% } %>" class="ub-tabcontent " style="display: block;">

  <% if(stepId!="players"){ %>

    <!-- <div class="product_mobile_show">
      <div class="d-inline">
        <a href="#" data-show="field_<%= id %>" data-fieldId="<%= id %>" id="field_<%= id %>" ><%= name %></a>
       
      </div>
    </div> -->

  <div>
    <div class="ub-accordion" data-fieldId="<%= id %>" id="field_<%= id %>"><%= name %></div>
      <div class="ub-panel" data-tab="data-tab_show" id="panelField_<%= id %>">
        <div class="my-3">
          <% if(filed_type!=undefined && filed_type=="text") { %>
            <div class="d-flex ub-row">
              <div class="w-50">
                <div class="form-group mb-2 ">
                  <a href="javascript:;" class="ub-eraser" id="textEraser"><img src="{{  asset('public/images/api/eraser.svg')}}" class="img-svg" alt="eraser" /></a>

                  <input type="text" id="ub-fieldtext" class="form-control" placeholder="Enter Text Here" value="<% if(text_value!=null){ %> <%= text_value %> <% } %>" />
                </div>
              </div>
            </div> <!-- Text End  -->

            <% } %>
            <% if(filed_type!=undefined && filed_type=="image"){ %>
            <div class="mb-2">
              <div class="mb-1" > 
                <!-- custom_uploadLogo -->
                <input type="file" class="form-control" id="uploadLogo" />
                <!-- <span>+</span> -->
              </div>
               <% if(image_value!=null){ test="block";  } else{ test="none;" } %> 
              <div class="ub-product_thumb" id="dd_<%= id %>"  style="display: <%= test %>" >
                <a href="javascript:;"> 
                  <img  src="<% if(image_value!=null){ %>{{ url('public/images/design_logo')}}/<%= image_value %> <% } %>" class="w-100" id="drag_image<%= id %>" ondragstart="drag(event)" draggable="true" data-image_value="<%= image_value %>" data-fieldID="<%= id %>">
                </a>
                <span class="ub-delet deleteLogoButton" data-id="<%= image_value %>" data-fieldId="<%= id %>" >+</span>
              </div>
            </div>

            <div class="d-flex ub-row">
              <div class="w-50 mb-2" style="display: none;">
                <div class="form-group ub-slidecontainer  mb-2">     
                  <div class="d-flex justify-content-between  align-items-center">
                      <label>Height</label> 
                      <!-- <div class="custom-control custom-checkbox" > -->
                          <!-- <input type="checkbox" class="custom-control-input " id="height_check_disable" name="height_check_disable"  > -->
                          <!-- <label class="custom-control-label" for="height_check_disable"></label> -->
                      <!-- </div> -->
                  </div>
                  <input type="range"  min="10" max="100" step="5" value="<% if(height_value!=null){ %><%= height_value %><% } else {%>50<% } %>" class="ub-slider" id="height_value" style="width: 100%">
                </div>
              </div>
                    
              <div class="w-50 mb-2">
                <div class="form-group ub-slidecontainer  mb-2">     
                  <!-- <div class="d-flex justify-content-between  align-items-center"> -->
                      <label>Size</label> 
                      <!-- <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="height_check_disable" name="height_check_disable" >
                      </div> -->
                  <!-- </div> -->
                  <input type="range" min="1" max="100" step="1" value="<% if(width_value!=null){ %><%= width_value %><% } else {%>30<% } %>" class="ub-slider" id="width_value" style="width: 100%">
                </div>
              </div>
            </div> <!-- Position End  -->
            <% } %>
            <% if(filed_type!=undefined && (filed_type=="image" || filed_type=="text")){ %>
            <div class="d-flex ub-row">
              <div class="w-50 mb-2">
                <div class="form-group ub-slidecontainer  mb-2">     
                  <label>Horizontal</label>
                  <input type="range" min="0" max="360" step="1" value="<% if(positionX_value!=null){ %><%= positionX_value %><% } else {%>250<% } %>"  class="ub-slider" id="position_x" style="width: 100%" >
                </div>
              </div>
                    
              <div class="w-50 mb-2">
                <div class="form-group ub-slidecontainer  mb-2">     
                  <label>Vertical</label>
                  <input type="range" min="300" max="500" step="1" value="<% if(positionY_value!=null){ %><%= positionY_value %><% } else {%>400<% } %>" class="ub-slider" id="position_y" style="width: 100%" >
                </div>
              </div>
            </div> <!-- Position End  -->
            <% } %>

            <% if(filed_type!=undefined && filed_type=="image" ){ %>
            <div class="text-right mb-2">
              <a href="javascript:;" id="resetLogoValue" ><strong>Reset</strong> </a>
            </div>
            <% } %>
            <!-- <div class="text-right">
              <a href="javascript:;" class="ub-add-row"><span>+</span> Add Row</a>
            </div> -->
            <div class="d-flex ub-row">
              <% if(filed_type!=undefined && filed_type=="text") { %>
              <div class="w-50">
                <div class="form-group ub-slidecontainer mb-3">
                  <label>Font Size</label>
                  <input type="range" min="10" max="36" step="2" value="<% if(fontsize_value!=null){ %><%= fontsize_value %><% } else {%>16<% } %>" class="ub-slider" id="fontSizeId">
                </div>
              </div>
              <% } %>
            </div>

            <div class="d-flex ub-row">
              <% if(filed_type!=undefined && filed_type=="text" && fontGroupId!="") { %>
              <div class="w-50">
                <div class="form-group ub-slidecontainer mb-3">
                  <label>Font Family</label>
                  <select id="fontName" class="chosen-select form-control " >
                    <option>Select Font</option>
                      <% if(fontgroup.length>0){
                        for(k=0;k< fontgroup.length;k++){ %>
                          
                        <% var url="";  if(fontgroup[k].font_file!=null){ 
                          url = "{{ asset('public/images/fonts/')}}/"+fontgroup[k].font_file;  
                          }
                         %>
                        
                        <option value="<%= fontgroup[k].value %>" style="font-family: '<%= fontgroup[k].value %>' !important " data-title="<%= url %>"  <% if(fontgroup[k].value==fontname_value){ %>selected <% } %> ><%= fontgroup[k].value %></option>
                    <% } }  %>
                  </select>
                </div>
              </div>
              <% } %>
            </div>
          <div>  
              <% 
          if(colorgroup.length>0)
          {
            for(k=0;k< colorgroup.length;k++)
            {
              var activeClass="";
              if(color_value!=null){
                if(color_value==colorgroup[k].code){ activeClass="active"; }
              }
          %>
                <div class="ub-color_swatch <%= activeClass %> tooltip"  style="background: <%= colorgroup[k].code %>;" data-value="<%= colorgroup[k].code %>"> 
                  <span class="tooltiptext"><%= colorgroup[k].name %></span> 
                </div>
              <% }} %>
            </div>
        </div> <!-- my-3 class end -->
      </div>
  </div>


  <% }else{ %>
  <div>
    <div class="ub-accordion">Enter Player Details</div>
    <div class="ub-panel" style="display: block;">
      <div class="my-3" >
        <div id="playerrow_result" class="<%= playerCount %>">
           <% 
            if(playerCount>0)
            {
              
              for(i = 0;i < playerList.length; i++) 
              {
               
          %> 
          <div style="display: flex;" class="ub-player_row" id="row_<%= playerList[i].id %>">
            <div class="w-40">
              <div class="form-group mb-1">
                <input type="text" class="form-control ub-player_name"  value="<%= playerList[i]['player_name'] %>" placeholder="Enter Player Name" />
                 <input type="hidden" placeholder="BU" id="ids" value="<%= playerList[i].id %>" class="form-control ids" name="">
              </div>
            </div>
            <div class="w-25">
              <div class="form-group mb-1">
                <input type="text" class="form-control ub-player_number" value="<%= playerList[i]['player_number'] %>" placeholder="Enter Number" onkeypress="return checkOnlyDigits(event)"/>
              </div>
            </div>
            <div class="w-20">
              <div class="form-group mb-1">

                <select class="form-control chosen-select ub-player_size <%= playerList[i].player_size %>" >
                  <option>Select Size</option>
                  <% for(j=0;j< size.length; j++){ %>
                    <option value="<%= size[j] %>" <% if(size[j]==playerList[i].player_size){ %> selected <% } %> ><%= size[j] %></option>
                    <% } %>
                </select>
              </div>
            </div>
            <div class="w-15 ub-delet_nm text-right pull-right">
              <a href="javascript:;" data-id="<%= playerList[i]['id'] %>" class="ub-player_delete"><img src="{{ asset('public/images/api/trash.svg') }}" alt="trash" class="img-svg" /></a>
            </div>
          </div> 
          <% } } else { %>
           <div style="display: flex;" class="player_row" id="row_0">
            <div class="w-40">
              <div class="form-group mb-1">
                <input type="text" class="form-control ub-player_name" placeholder="Enter Player Name" />
                 <input type="hidden" placeholder="BU" id="ids" value="" class="form-control ids" name="">
              </div>
            </div>
            <div class="w-25">
              <div class="form-group mb-1">
                <input type="text" class="form-control ub-player_number" placeholder="Enter Number" />
              </div>
            </div>
            <div class="w-20">
              <div class="form-group mb-1">
                <select class="form-control chosen-select ub-player_size">
                  <option>Select Size</option>
                  <% for(j=0;j< size.length; j++){ %>
                    <option value="<%= size[j] %>"><%= size[j] %></option>
                    <% } %>
                </select>
              </div>
            </div>
            <div class="w-15 ub-delet_nm text-right pull-right">
             <!--  <a href="javascript:;" class="player_view"><img src="{{ asset('public/images/api/view.svg') }}" alt="view" class="img-svg" /></a> -->
              <a href="javascript:;" class="ub-player_delete"><img src="{{ asset('public/images/api/trash.svg') }}" alt="trash" class="img-svg" /></a>
            </div>
          </div> 
          <% } %>
        </div>

      <div class="text-right">
        <a href="javascript:;" class="ub-add-row" id="addplayerrow"><span>+</span> Add Player</a>
      </div>
    </div>
  </div>
  <% } %>
  
</div>


