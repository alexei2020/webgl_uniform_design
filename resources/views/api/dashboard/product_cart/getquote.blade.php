<form id="getQuoteForm" class="edited">
  <div class="text-center">
    <h2 class="my-0">Get A Quote</h2>
</div>
<div>
    <h5>Please fill in all the fields:</h5>
    <hr />
    <div class="mt-3">
      <div class="form-group">
        <label>Name/Team Name/Company Name</label>
        <input type="text" name="company_name" id="company_name" class="form-control" />
      </div>
      <div class="d-flex ub-row">
        <div class="w-50">
          <div class="form-group">
            <label>Email Address</label>
            <input type="email"  name="email" id="email" class="form-control" required/>
          </div>
        </div>
        <div class="w-50">
          <div class="form-group">
            <label>Mobile Number</label>
            <input type="text" name="mobile" id="mobile" class="form-control" />
          </div>
        </div>
      </div>
      <div class="form-group">
        <label>Message</label>
        <textarea type="text" name="message" id="message" rows="5" class="form-control"></textarea>
      </div>
      <div class="form-group">
        <button class="btn btn-primary" id="quoteSubmitButton">Submit</button>
      </div>
    </div>
</div> 
</form>