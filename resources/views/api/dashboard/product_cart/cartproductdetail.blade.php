<div class="ub-final_detail">
    <div class="text-center ub-pro_img mb-3">
      <h2 class="mt-4">Customised Design</h2>
       <!-- images/front.jpg -->
      <% if(image_front!="" && image_front!=null){ %>
        <img src="{{ asset('public/images/front_back') }}/<%= image_front %>" alt="image" datatitle="front" / >
      <% } else { %>
        <img src="{{ asset('public/images/api/front.jpg') }}" alt="image" datatitle="front" />
      <% } %>
      <% if(image_back!="" && image_back!=null){ %>
        <img src="{{ asset('public/images/front_back') }}/<%= image_back %>" alt="image" datatitle="back" / >
      <% } else { %>
        <img src="{{ asset('public/images/api/back.jpg') }}" alt="image" datatitle="back" />
      <% } %>
    </div>
    <div class="w-50 m-auto">
      <h3 class="mb-2 text-center">Field Choice</h3>
      <table class="ub-table table-striped" cellpadding="0" cellspacing="0">
        <tbody>

          <% if(design_data.length>0){ %>
            <% for(m=0;m<design_data.length;m++){ %>
            <tr>
              <td><strong><%= design_data[m].name %></strong></td>
              <td>
              <% if(design_data[m].color!=null){ %> 
                <span class="ub-color-view" style="background-color:<%= design_data[m].color %> "></span> <%= design_data[m].color_name %> (<%= design_data[m].color %>)
                <% } %>
              </td>
            </tr>
            <% } %>
          <% } %>

          <% if(field_data.length>0){ %>
            <% for(m=0;m<field_data.length;m++){ %>
              <% if(field_data[m].type){ %>
                  <!-- <% if(field_data[m].color){ %> -->
                    <tr>
                      <td><strong><%= field_data[m].name %></strong></td>
                      <td>
                      <% if(field_data[m].color!=null){ %> 
                        <span class="ub-color-view" style="background-color:<%= field_data[m].color %> "></span> <% if(field_data[m].color_name!=null){ %> <%= field_data[m].color_name %> <% } else { %> White <% } %> (<%= field_data[m].color %>)
                      <% } %>
                      </td>
                    </tr>
                   
                <!--   <% } else if(field_data[m].fontsize){ %>
                      <tr>
                      <td><strong><%= field_data[m].name %></strong></td>
                      <td>
                        <%= field_data[m].fontsize %>
                      </td>
                    </tr>
                  <% } %> -->

              <% }else{ %>
              <% } %>
            
            <% } %>
          <% } %>




          <!-- <tr>
              <td><strong>Base Colour</strong></td>
              <td> <span class="ub-color-view" style="background-color:#FFcc00 "></span> White (#FFcc00)</td>
          </tr> -->
          <!-- <tr>
              <td><strong>Team Name Font Family</strong></td>
              <td>Athletic</td>
          </tr>
          <tr>
              <td><strong>Team Name Font Size</strong></td>
              <td>150</td>
          </tr>
          <tr>
              <td><strong>Primary Colour</strong></td>
              <td> <span class="ub-color-view" style="background-color: #232156"></span> Dark Royal (#232156)</td>
          </tr>
          <tr>
              <td><strong>Secondary Colour</strong></td>
              <td> <span class="ub-color-view" style="background-color: #C72F1C"></span> Red (#C72F1C)</td>
          </tr>
          <tr>
              <td><strong>Player Name Colour</strong></td>
              <td> <span class="ub-color-view" style="background-color:#232156 "></span> Dark Royal (#232156)</td>
          </tr>
          <tr>
              <td><strong>Player Name Font Size</strong></td>
              <td>130</td>
          </tr>
          <tr>
              <td><strong>Player Number Colour</strong></td>
              <td> <span class="ub-color-view" style="background-color: #A49C86"></span> Taupe (#A49C86)</td>
          </tr>
          <tr>
              <td><strong>Player Number Font Size</strong></td>
              <td>130</td>
          </tr> -->
        </tbody>
      </table>
    </div>

    <div>
      <h3 class="mb-2">Team Member Details</h3>
      <table class="ub-table" cellpadding="0" cellspacing="0">
        <thead>
          <tr>
            <th class="text-left">Player Name</th>
            <th class="text-center">Number</th>
            <th class="text-left">Size</th>
          </tr>
        </thead>
        <tbody>
          <% if(teamMemberDetail.length>0){ %>
         <% for(m=0;m<teamMemberDetail.length;m++){ %>
         <tr>
           <td><%= teamMemberDetail[m].player_name %></td>
           <td class="text-center"><%= teamMemberDetail[m].player_number %></td>
           <td><%= teamMemberDetail[m].player_size %></td>
         </tr>
         <% } %>
         <% } %>
        </tbody>
      </table>
    </div>
    <div>
      <h3 class="mb-2">Sizes</h3>
      <table class="ub-table" cellpadding="0" cellspacing="0">
        <thead>
          <tr>
            <th class="text-left">Size</th>
            <th class="text-center">Quantity</th>
          </tr>
        </thead>
        <tbody>
          <% if(sizeCount.length>0){ %>
          <% for(m=0;m<sizeCount.length;m++){ %>
         <tr>
           <td><%= sizeCount[m].size %></td>
           <td class="text-center"><%= sizeCount[m].count %></td>
           
         </tr>
         <% } %>
         <%  } %>
        </tbody>
      </table>
    </div>
  </div>