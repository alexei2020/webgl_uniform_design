<table class="ub-table w-100" cellspacing="0" cellpadding="0" id="printTable">
  <% if(rows){ %>
  <thead>
  <tr class="not_print">
    <th datatitle="Product" colspan="2" class="text-left">Product</th>
    <!-- <th width="180" datatitle="Price" class="text-right">Quantity</th>                     -->
  </tr>
  </thead>
  <% } %>
  <tbody id="searchResultDiv">
  <%=rows%>
  <% if(!rows){ %>
  <div class="no_audit p-5 text-center mt-3" >
    <p>No Designs Submitted. <a href="" id="norecord_link">Get started.</a> </p>
  </div>
  <!-- @include('common.norecord') -->
  <% } %>
  </tbody>
  <tfoot class="not_print">
  <tr>
    <th colspan="2"></th>
    <!-- <th colspan="2" style="color: #fff" class="text-left" >Total</th> -->
    <!-- <th class="text-right" style="color: #fff" id="totalCountProduct">50</th> -->
  </tr>
  </tfoot>
</table>
