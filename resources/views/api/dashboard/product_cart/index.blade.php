<div id="ub-CartList">
  <div class="ub-product_cart">
    <div class="text-right">
      <div class="ub-txt_action w-auto" id="action_button_tab">
        <a href="javascript:;" class="ub-print" id="printButton"><img src="{{ asset('public/images/api/print.svg') }}" alt="print" class="img-svg"> <span>Print</span></a>
       <!--  <a href="javascript:;" class="ub-form" id="getQuoteButton"><img src="{{ asset('public/images/api/form.svg') }}" alt="form" class="img-svg"> <span>Get a Quote</span></a> -->
        <a href="javascript:;" class="ub-email" id="emailTofriendButton"><img src="{{ asset('public/images/api/email.svg') }}" alt="email" class="img-svg"> <span>Email to Friend</span></a>
        <a href="javascript:;" class="ub-share" id="shareButton"><img src="{{ asset('public/images/api/share.svg') }}" alt="share" class="img-svg"> <span>Share</span></a>
      </div>
    </div>
    <div id="ub-cart_table"></div>
    <div class="ub-txt_action text-right w-auto" id="action_button_tab1" style="display: block;">
     <a href="javascript:;" class="ub-form" id="getQuoteButton"><img src="{{ asset('public/images/api/form.svg') }}" alt="form" class="img-svg"> <span>Get a Quote</span></a>
   </div>
  </div>  
</div>
<div id="ub-CartProductDetail" style="display: block;"></div>

<div id="ub-getAQuotePage" style="display: block;"></div>


  
