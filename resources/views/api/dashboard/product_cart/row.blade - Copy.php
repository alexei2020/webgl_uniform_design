 <tr id="tr_cart_<%= id %>">
        <td class="v_top">
          <div class="ub-product_final">
            <% if(image!="" && image!=null)
            {
                images="";
                type="";
                j=0;
                type=image_type.split('@');
                images=image.split('@');
                for(i=0; i< images.length,j< type.length; i++)
                {
             %>

             
              <a href="javascript:;">
                <img src="{{ asset('public/images/front_back') }}/<%= images[i] %>" alt="product" />
                <div><strong><%= type[j].substr(0,1).toUpperCase() + type[j].substr(1) %></strong></div>
              </a>
              <% j++ %>
              <% } } else{ %>
              <a href="javascript:;">
                <img src="{{ asset('public/images/api/front.jpg') }}" alt="product" />
                <div ><strong>Front</strong></div>
              </a>
              <a href="javascript:;">
                <img src="{{ asset('public/images/api/back.jpg') }}" alt="product" />
                <div><strong>Back</strong></div>
              </a>
            <% } %>
          </div>
        </td>
        <td class="not_print">
          <div class="mb-2 ml-1">
            <h3 class="mb-0 mt-0"><%= product_name %></h3>
            <p class="my-1">Created: <%= createdTime %></p>
            <% if(sizeCount.length>0){ %>
            <p class="my-1">Size: 
              <% for(s=0;s<sizeCount.length;s++){ %>
              <span class="ub-pro_box"><small><%=sizeCount[s].size %></small> <span><%=sizeCount[s].count %></span></span>
              <!-- <span class="ub-pro_box"><small>L</small> <span>13</span></span> -->
              <% } %>
            </p>
            <% } %>
          </div>
          <div class="ub-txt_action w-auto">
            <a href="javascript:;" id="viewDetailButton" data-orderId="<%= id %>" class="mb-1"><img src="{{ asset('public/images/api/view.svg') }}" alt="product" class="img-svg"> <span>View Details</span> </a>
            <!-- <a href="javascript:;" id="changeDesignButton" data-project_slug="<%= productUrl_slug %>" class="mb-1"><img src="{{ asset('public/images/api/folder-file.svg') }}" alt="product" class="img-svg" > <span>Change Design</span> </a> -->
             <% var disabled_class=""; if(pro_deleted==1) { var disabled_class ="isDisabled"; } %> 
            <a href="javascript:;" id="editCustomiseButton" data-orderId="<%= id %>" data-id="<%= id %>" data-project_slug="<%= productUrl_slug %>" data-productDesignId="<%= productDesignId %>" data-design_slug="<%= design_slug %>"  class=" <%= disabled_class %> mb-1">
              <img src="{{ asset('public/images/api/edit.svg') }}" alt="edit" class="img-svg"> <span>Edit</span> 
            </a>

            <a href="javascript:;" id="deletecartpoduct" data-id="<%= id %>" class="mb-1"><img src="{{ asset('public/images/api/trash.svg') }}" alt="trash" class="img-svg"> <span>Delete</span> </a>
          </div>
        </td>
        <!-- <td class="text-right"><%= totalSizeCounteachRow %></td> -->
      </tr>