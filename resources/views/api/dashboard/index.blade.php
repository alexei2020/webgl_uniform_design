
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

<div class="ub-product_container">


  <div id="ub-product_section">
    <ul class="ub-breadcrumb" id="breadcrumb1">
      <li>Product</li>
      <li class="pull-right ub-ViewBasket">
        <a href="javascript:;" class="btn btn-secondary btn-sm" id="ub-viewCartBasket" >
          <img src="{{ asset('public/images/api/basket.svg') }}" alt="basket" class="img-svg" /> <span>View Basket</span></a>
      </li>
    </ul>

    <div class="ub-product_showcase">
      <!-- <div class="ub-categorybar_hideshow"></div> -->
      <div class="ub-product_category">
        <h2>Categories</h2>
        <hr />
        <div id="ub-category_list">
          <!-- Category List -->
        </div>
      </div>

      <div class="ub-product_area">
        <div id="ub-productAreaShow">
            <div class="ub-product_option">
              <div class="ub-product_sorting w-75">
                <select class="float-right mr-3 chosen-select" id="select_sorting_id">
                  <option>Default sorting</option>
                  <option value="sort_by_name">Sort by name</option>
                </select>
              </div>
              <!-- <div class="ub-product_count w-25" id="ub-pagination">  -->
                <!-- Show 01- 09 Of 36 Product -->
              <!-- </div> -->
            </div>

            <div class="my-2 ub-product_list">
              <!-- <div class="mb-4"> -->
                <div id="ub-productList">
                  <!-- Product List -->
                </div>
              <!-- </div> -->
            </div>


              <!-- <div class="ub-pagination"></div> -->



        </div>

        <div id="ub-productDesignAreaShow">
          <div class="my-2 ub-product_list">
              <!-- <div class="mb-4"> -->
                <div id="ub-productDesignList">
                  <!-- Product Design List -->
                </div>
              <!-- </div> -->
            </div>
        </div>
      </div>
    </div>
  </div> <!-- ub-product_section -->

  <div id="ub-ProductCustomiseSection" style="display: none;">
    <ul class="ub-breadcrumb" id="breadcrumb2">
      <li><a id="ub-product_previewlink" href="{{ url('distributor/preview') }}">Product</a></li>
      <li>Product Customize</li>
      <li class="pull-right ub-ViewBasket">
     <a href="javascript:;" class="btn btn-secondary btn-sm" id="ub-viewCartBasket" >
        <img src="{{ asset('public/images/api/basket.svg') }}" alt="basket" class="img-svg" /><span>View Basket</span></a>
      </li>
    </ul>

    <div id="ub-PCustomise_div">
      <div class="ub-product_customize">
        <div class="mb-3">
           <h2 class="mb-0" id="ub-product_name"></h2>
           <h3 class="mb-0" id="ub-product_design_name"></h3>
           <hr>
        </div>

        <div class="w-40">
          <div class="border ub-product_cast">
            <main class="ub-scene" id="webgl" ondrop="drop(event)" ondragover="allowDrop(event)"><img class="webgl_loader" src="{{ asset('public/images/ldr.gif') }}" alt="image"  ></main>

           <div class="ub-product_action top">
              <a href="javascript:;" class="ub-view_orientation">View Back</a>
             <a href="javascript:;" class="ub-change_design"  id="myBtn">Change Design</a>
             <!-- <span id="share_social_media"></span> -->
            </div>
          </div>
          <div class="mt-1" id="ub-share_social_media"></div>
        </div>
        <div class="w-60">
          <div class="pl-3">
            <div class="text-center mb-2">
              <div class="text-center m-auto mb-2">
                <div class="d-inlineb">
                      <div class="ub-product_customize_point mt-3" >
                        <!-- Step Section Here -->
                      </div>
                </div>
              </div>
            </div>
            <div class="ub-product_mobile_show">
              <div class="d-inline">
              </div>
            </div>
            <div class="mb-3 ub-setPrevNext">
              <div id="ub-field_customise">
                <!-- Field Customise List -->
              </div>
              <div class="w-50 ub-next_pre text-left ub-tab-active mt-3" id="ub-previous_button">
                <a href="javascript:;" class="btn btn-primary" dataanchor="Customisation" >Previous</a>
              </div>
              <div class="w-50 ub-next_pre text-right ub-tab-active mt-3" id="ub-next_button">
                <a href="javascript:;" class="btn btn-primary" dataanchor="NameNumber" >Next</a>
              </div>
              <div class="w-50 ub-next_pre text-right mt-3" id="ub-finish_button">
                <a href="javascript:;" class="btn btn-primary">Finish</a>
              </div>
            </div>

            <img src="" id="img_val" />

          </div>
        </div>
      </div>

      <div class="mt-3" id="ub-product_info" style="display: none;">
        <!-- <h2 class="mb-0 text-danger" id="ub-product_name"></h2> -->
        <hr />
        <div id="ub-product_description"></div>
      </div>

    </div>
    <div id="ub-Product_cart_div"> </div>

  </div> <!-- ProductCustomiseSeciton -->




  <div id="ub-texture" class="ub-texture" style="opacity: 0; margin-top: 200px; width: 100%;overflow: hidden; position: relative;" >

  </div>
  <div id="primaryOnSvg"  fill="#CC3762" ></div>
  <div id="secondaryOnSvg"  fill="#CC3762" ></div>
  <div id="tertiaryOnSvg"  fill="#CC3762" ></div>
  <div id="accentOnSvg"  fill="#CC3762" ></div>


  <!-- The Modal -->
<div id="emailtofriendModal" class="ub-modal">
  <!-- Modal content -->
  <div class="ub-modal-content">
    <div class="ub-modal-header">
      <span class="ub-close" id="model_close_button">&times;</span>
      <h3>Please fill in all the fields</h3>
    </div>
    <div class="ub-modal-body">
      <form enctype="multipart/form-data" id="sendemailtofriendform" class="edited">
         <div class="form-group">
          <label for="yourname">Your Name</label>
          <input type="text" class="form-control" id="yourname" placeholder="Enter Name">
        </div>
        <div class="form-group">
          <label for="friendemail">Friend's Email</label>
          <input type="email" class="form-control" id="friendemail" aria-describedby="emailHelp" placeholder="Enter email">
        </div>
      </form>
    </div>
    <div class="ub-modal-footer">
      <div class="form-group text-center">
         <button type='button' id='sendemailbutton' class='ub-button11 btn btn-primary'>Send Email</button>
       </div>
     </div>
  </div>
</div>



<!-- The Modal -->
<div id="sharepopupModal" class="ub-modal">
  <!-- Modal content -->
  <div class="ub-modal-content">
    <div class="ub-modal-header_share">
      <span class="ub-close" id="sharemodel_close_button">&times;</span>
      <h3>Share link</h3>
    </div>
    <div class="ub-modal-body">
      <div class="form-group">
        <label for="copylink" class="mb-2">Copy the link below:</label>
        <input type="text" class="form-control" id="copylink" >
      </div>
    </div>
     <div class="ub-modal-footer">
      <div class="form-group text-center">
        <span id="show_text" style="font-weight: 600;"></span>
        <button class="ub-button11 btn btn-primary" id="copyButton"  onclick="copyContent()">Copy</button>
      </div>
    </div>
  </div>
</div>

<div class="ub-loader"><img class="img-svg" src="{{ asset('public/images/ldr.svg') }}"></div>

</div>


<script src="{{ asset('public/js/api/svg-img.js') }}" type="text/javascript" charset="utf-8"></script>

<script src="{{ asset('public/lib/js/utils/three.js') }}"></script>
<script src="{{ asset('public/lib/js/utils/Tween.min.js') }}"></script>
<script src="{{ asset('public/lib/js/utils/OrbitControls.js') }}"></script>
<script src="{{ asset('public/lib/js/utils/threex.dynamictexture.js') }}"></script>

<script src="{{ asset('public/lib/js/utils/OBJLoader.js') }}"></script>

<script src="{{ asset('public/lib/js/utils/geometry_helper.js') }}"></script>

<!-- <script src="{{ asset('public/lib/js/utils/DragControls.js') }}"></script>
 -->
<script src="{{ asset('public/lib/js/functions/colour.js') }}"></script>
<script src="{{ asset('public/lib/js/functions/getSvg.js') }}"></script>
<script src="{{ asset('public/lib/js/functions/getShapes.js') }}"></script>
<script src="{{ asset('public/lib/js/functions/getLights.js') }}"></script>
<script src="{{ asset('public/lib/js/functions/rotate.js') }}"></script>
<script src="{{ asset('public/lib/js/functions/update.js') }}"></script>
<script src="/360Uniform/public/lib/js/functions/windows.js"></script>
<script src="{{ asset('public/lib/js/functions/windowResize.js') }}"></script>
<script src="/360Uniform/public/lib/js/init.js"></script>

<script type="text/javascript" src="{{ asset('public/js/api/html2canvas.min.js') }}"></script>



