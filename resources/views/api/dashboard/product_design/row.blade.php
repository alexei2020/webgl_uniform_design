<div class="ub-product"> 
  <div class="ub-img-container">
    <div class="ub-img">
        <img src="" id="designImage_<%= id %>" style="display: none;">
        <main class="ub-scene" id="webgl_<%= id %>"><img class="webgl_loader" id="webgl_loader_<%= id %>" src="{{ asset('public/images/ldr.gif') }}" alt="image"  ></main>
        <div id="texture_<%= id %>"  class="ub-texture" style="opacity: 0;  width: 100%;overflow: hidden; position: relative;" >
        <% var viewBox=""; if(svg_viewBox!=null){ viewBox= "viewBox='"+svg_viewBox+"'";}  %>
          <svg data-elems="Body"  xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="<%= svg_width %>" <%= viewBox %> height="<%= svg_height %>"  version="1.0" style="position: absolute;">
            <%= svg_snippet %>
          </svg> 
        </div>
    </div>
  </div>
  <a class="ub-anchor" href="javascript:;" id="ub-productdesignbutton" data-id="<%= id %>" data-design_slug="<%= design_slug %>" data-productId="<%= productId %>" ></a>
  <div class="ub-product_nm" title="<%= name %>"><%= name %></div>
</div>

