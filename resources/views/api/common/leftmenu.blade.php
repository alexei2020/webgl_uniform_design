<div id="ub-category_list">    
  <ul class="list_block ub-main_categories ml-2">
    <li><a href="javascript:;" data-id="<%= categoryId %>" id="a_cate" data-value="<%= url_slug %>" oncontextmenu="replaceLink(event,'{{ url("/distributor/preview/") }}/<%= distributorId %>/#/category/<%= url_slug %>');"  ><%= cname %> </a>
      <% if(subcatList.length>0) { %>
      <ul class="ub-sub_categories">
        <% for(j=0; j< subcatList.length; j++) { %>
        <li><a href="javascript:;" data-id="<%= subcatList[j].categoryId %>" id="a_subcate" data-value="<%= subcatList[j].url_slug %>" class="" ><%= subcatList[j].cname %></a></li>
        <% } %>
      </ul>
      <% } %>
    </li>        
  </ul>
</div>
    
  