@extends('layouts.app')

@section('content')
<main class="login">
    <div class="login__wrapper-logo">
        <img src="{{ asset('public/images/logo.png') }}">
    </div>
    <div class="login__wrapper">
        <!-- <div class="login__wrapper-logo text-center">
            <img src="{{ asset('public/images/logo.png') }}">
        </div> -->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            <!-- <h5 class="mt-5">Forgot Password</h5>
            <hr class="mb-4" /> -->            
            @csrf
            <div class="form-group">
                <label for="email" >Please enter you email to get reset password link</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="d-flex mt-5 align-items-center justify-content-between">
                 <a  href="{{ route('login') }}">
                    {{ __('Back to Login') }}
                </a>
                <button type="submit" class="btn btn-primary">
                    {{ __('Send Link') }}
                </button>
            </div>
        </form>
    </div> 
    <div class="copywrite"> Powered by <a  href="#"><img src="{{ asset('public/images/logo_1.png') }}"></a> </div>
</main>
@endsection
