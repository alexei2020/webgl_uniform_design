@extends('layouts.app')
@section('content')
<div class="container-fluid h-100 login">
    <div class="row align-items-center h-100">
        <div class="col-8 mx-auto">
            <div class="overflow-hidden my-auto p-0">
                <div class="row align-items-center">
                    <div class="col-lg-6 offset-lg-3 col-sm-10 offset-sm-1">
                        <div class="text-center mb-4">
                            <img src="{{ asset('public/images/Uniform-Builder-Logo-white.png') }}" alt="Uniform Builder" class="login_logo" />
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-3 col-sm-10 offset-sm-1  mb-3">
                        <div class="bg_white jumbotron">
                            <div class="text-right">
                                @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            </div>
                            <h5 class="mb-5 txt_black text-center"> <span class="active">Forgot Password</span> </h5>
                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                <div class="form-group">
                                  <label for="email" >Please enter you email to get reset password link</label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                              
                                <!-- <button type="submit" class="btn btn-primary mr-4">Submit</button>  -->
                                 <button type="submit" class="btn btn-primary mr-4">
                                 {{ __('Send Link') }}
                                </button>
                                <a  href="{{ route('login') }}">
                                    {{ __('Back to Login') }}
                                </a>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
