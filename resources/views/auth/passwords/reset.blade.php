@extends('layouts.app')
@section('content')
<div class="container-fluid h-100 login">
    <div class="row align-items-center h-100">
        <div class="col-8 mx-auto">
            <div class="overflow-hidden my-auto p-0">
                <div class="row align-items-center">
                    <div class="col-lg-6 offset-lg-3 col-sm-10 offset-sm-1">
                        <div class="text-center mb-4">
                            <img src="{{ asset('public/images/Uniform-Builder-Logo-white.png') }}" alt="Uniform Builder" class="login_logo" />
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-3 col-sm-10 offset-sm-1  mb-3">
                        <div class="bg_white jumbotron">
                            <div class="text-right">
                                @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                                @endif

                            </div>
                            <h5 class="mb-5 txt_black text-center"> <span class="active">Reset Password</span> </h5>
                            <form method="POST" action="{{ route('password.update') }}">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group">
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group ">
                                    <label for="password" >{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    <a href="javascript:;" class="pass_visible"><i id="pass-status" class="fas fa-eye show_password" onClick="viewPassword()"></i></a>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group ">
                                    <label for="password-confirm" >{{ __('Confirm Password') }}</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                                
                                <div class="form-group mt-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Reset Password') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
