@extends('layouts.app')
@section('content')
<div class="container-fluid h-100 login">
            <div class="row align-items-center h-100">
                <div class="col-sm-8 mx-auto">
                    <div class=" overflow-hidden my-auto p-0">
                        <div class="row align-items-center">
                            <div class="col-lg-6 offset-lg-3 col-sm-10 offset-sm-1">
                                <div class="text-center mb-4">
                                    <img src="{{ asset('public/images/Uniform-Builder-Logo-white.png') }}" alt="Uniform Builder" class="login_logo" />
                                </div>
                            </div>
                            <div class="col-lg-6 offset-lg-3 col-sm-10 offset-sm-1  mb-3">
                                <div class="bg_white jumbotron">
                                    <div class="text-right">
                                    </div>
                                    <h5 class="mb-5 txt_black text-center"> <span class="active">Sign In</span> </h5>
                                    <form method="POST" action="{{ route('login') }}">
                                         @csrf
                                        <div class="form-group">
                                            <input id="email" type="email" placeholder="Enter Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
                                                 @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                        <div class="form-group">
                                            <!-- <label for="pwd">Password:</label> -->
                                            <input id="pwd" type="password" placeholder="Enter Password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password">
                                                <!-- <i id="pass-status" class="fa fa-eye show_password" aria-hidden="true" onClick="viewPassword()"></i> -->
                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                        <div class="form-group form-check mt-4">
                                            <label class="form-check-label"> <input class="form-check-input" type="checkbox" /> Remember me </label>
                                        </div>
                                        <div class="text-center mt-4">
                                            <button type="submit" class="btn btn-block btn-primary"> {{ __('Login') }} </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6 offset-lg-3 col-sm-10 offset-sm-1">
                                <div class="text-right">
                                    @if (Route::has('password.request'))
                                            <a  href="{{ route('password.request') }}" class="forgot_pass">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
