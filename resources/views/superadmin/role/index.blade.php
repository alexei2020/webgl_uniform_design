@extends('layouts.superadmin')

@section('title', 'Roles & Permissions')

@section('content')



    <!-- Modal -->
    <div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="roleModalLabel">
        <div class="modal-dialog" role="document">
            {!! Form::open(['method' => 'post']) !!}

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="roleModalLabel">Role</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- name Form Input -->
                    <div class="form-group @if ($errors->has('name')) has-error @endif">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Role Name']) !!}
                        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <!-- Submit Form Button -->
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


     <div class="wrapper__content" id="content">
          <div class="wrapper__content-header">
            <h2>Roles</h2>
              @can('add_roles')
                <a href="javascript:;"  class="btn btn-primary" data-toggle="modal" data-target="#roleModal"><i class="fas fa-plus"></i>New Role</a>
            @endcan
          
          </div>
          <div class="wrapper__content-body">
            <div class="main__content-search mb-4">
                        
            </div>
            <div class="table-responsive" id="result">
                   
                    @forelse ($roles as $role)
                        {!! Form::model($role, ['method' => 'PUT', 'route' => ['roles.update',  $role->id ], 'class' => 'm-b']) !!}

                        @if($role->name === 'Superadmin')
                            @include('shared._permissions', [
                                          'title' => $role->name .' Permissions',
                                          'options' => ['disabled'] ])
                        @else
                            @include('shared._permissions', [
                                          'title' => $role->name .' Permissions',
                                          'model' => $role ])
                            @can('edit_roles')
                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            @endcan
                        @endif

                        {!! Form::close() !!}

                    @empty
                        <p>No Roles defined, please run <code>php artisan db:seed</code> to seed some dummy data.</p>
                    @endforelse
            </div>
           
          </div>
        </div>





@endsection

@section('script')
<script src="{{ asset('public/js/superadmin/roles.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection