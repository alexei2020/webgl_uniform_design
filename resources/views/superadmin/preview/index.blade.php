<meta name="csrf-token" content="{{ csrf_token() }}">
<input type="hidden" value="{{ csrf_token() }}" id="token">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<div id="UniformBuilder"></div>

<script type="text/javascript" id="distributor-api" data-distributor="{{ $distributorId }}" src="{{ asset('public/js/api/api.js') }}"  ></script>
