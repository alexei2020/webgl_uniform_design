@extends('layouts.superadmin')
@section('content')
         
    <div class="page-body-wrapper animate">
      <div class="main-panel">
        <div class="content-wrapper pb-0">
          <h4>Content Page</h4>

        	 
          	<form id="userform" class="edited mt-4">
              <?php $description=""; $id="";
                if(count($contentpage)>0)
                {
                    $description=html_entity_decode($contentpage[0]->description); $id=$contentpage[0]->id;
                }
               ?>
          		<input type="hidden" name="text" id="id" data-id="{{ $id }}" value="{{ $id }}">

          		<div class="form-group">

          			<textarea name="content_page_editor" id="content_page_editor"><?php echo $description; ?></textarea>
          		</div>
          		<div class="form-group">
           			<button type='button' id='submitcontentpagebutton' class='btn btn-primary'>Save</button>
           		</div>	
          	</form>
              
        </div>

      </div>
    </div>

@endsection

@section('script')


<script src="{{ asset('public/js/superadmin/sitemanage/content_page.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>

    $("#heading").html("Site Manage");

	 CKEDITOR.replace( 'content_page_editor' );
 
 </script>
@endsection