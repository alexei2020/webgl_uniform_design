<form enctype="multipart/form-data" id="companyform" class="edited">
    <input type="hidden" id="id"  value="<%= id %>" class="form-control">
    <div class="form-group">
      <label>Business Name<span class="mandatory">*</span></label>
      <input type="text" id="bu_name" placeholder="Enter Business Name" value="<%= bu_name %>"  class="form-control" >
    </div>
    <div class="form-group">
      <label>Contact Name <span class="mandatory">*</span></label>
      <input type="text" id="name" placeholder="Enter Name" value="<%= name %>" class="form-control" >
    </div>

    <div class="form-group">
      <label>Email <span class="mandatory">*</span></label>
      <input type="email" id="email" autocomplete="false" placeholder="Enter Email" value="<%= email %>" class="form-control" >
    </div>
    
    <div class="form-group">
      <label>Contact Phone </label>
      <input type="text" id="mobile" autocomplete="false" placeholder="Enter Contact Phone" value="<%= mobile %>" class="form-control" onkeypress="return check(event,value)" onchange="return check(event,value)" maxlength="15" >
    </div>
    

  <% if(profile!="" && profile!=null) { %>
    <div class="form-group" id="pID">
      <img  src="{{ url('public/distributor/') }}/<%= profile %>" width="100">
       <a href="javascript:;" id="deleteCompanyLogo" data-value="<%= profile %>" data-id="<%= id %>"><i class="fas fa-trash"></i></a>
    </div>

  <% } %>
      
    <div class="form-group">
      <input type="hidden" id="actionDeleteLogo" >
      <label>Company Logo</label>
      <input type="file" id="profile"  name="profile"  class="form-control">
    </div>


      
        <div class="form-group">
          <label>Password <span class="mandatory">*</span></label>
          <input type="password" autocomplete="new-password" id="password" placeholder="********" class="form-control" value="<%= duplicate_password %>">
          <a href="javascript:;" class="pass_visible"><i id="pass-status" class="fas fa-eye show_password" onClick="viewPassword()"></i></a>
        </div>

        <!-- <div class="form-group">
          <label>Confirm Password <span class="mandatory">*</span></label>
          <input type="password" id="confirm_password" placeholder="********" class="form-control">
        </div> -->

      <!-- <div class="or">
        <span>OR</span>
      </div> -->
      <div class="form-group">
        <!-- <label>Generate Password <span class="mandatory">*</span></label> -->
        <!-- <input type="text" id="password_generate" placeholder="" class="form-control" disabled="disabled"> -->
        <button type="button" id="passwordgenaratebutton" class="mt-3 btn btn-info">Generate Password</button>
      </div>

     
    <% if(id!=""){ %>
      <div class="form-group">

        <label>Feed Code</label>
         
          <textarea class="form-control" rows="10"  id="feed_code_textarea">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<div id="UniformBuilder"></div>
<script type="text/javascript" id="distributor-api" data-distributor="<%= id %>" src="{{ asset('/public/js/api/api.js') }}"  ></script></textarea>
           <!-- 

            <script> 
              document.addEventListener("UniformLoaded", function () {
                  Uniform.init("#360Uniform");
              }); 
            </script>
           <script src="http://dev.imenso.co/360Uniform/api/bootstrapper?distributorId=<%= id %>&key=faf23d89dfcf5dc857c9c2a7b9d4069c" async></script>
            <div id="Uniform"><noscript>UniformBuilder requires JavaScript to be enabled</noscript></div> -->
        </div>    
        <div style="text-align: left;" class="mb-4">
              <button class="text-right btn btn-primary btn-sm" type="button"  onclick="copyContent()">Copy</button>
              <span id="show_text" class="ml-2" style="font-weight: 600; display: none;">Copied!</span>
              <a style="float: right;" target="_blank" class="text-right btn btn-primary btn-sm" href="{{ url('superadmin/preview/') }}?distributorId=<%= id %>">Test Site</a>
          </div >

       

    <% } %>

      <div class="form-group">
        <div class="custom-control custom-checkbox mb-3 mr-3">
          <input type="checkbox" class="custom-control-input" id="status" name="status" <% if(status=="active") { %>checked <% } %> >
          <label class="custom-control-label" for="status">Active</label>
        </div>
      </div>
     
</form>

<script type="text/javascript">
  function copyContent() {
    let textarea = document.getElementById("feed_code_textarea");
    textarea.select();
    document.execCommand("copy");

    $('#show_text').html('The feed code has been copied to your clipboard.');
      $( "#show_text").fadeIn("slow");
      setTimeout(function(){
        $( "#show_text").fadeOut("slow");
      },2000);
  }
</script>