<tr>    
    <td>
      <% if(profile=="" || profile==null) { %>
      <img src="{{ asset('public/images/Uniform-Builder-Logo.png') }}" class="mr-2 img-thumbnail" alt="image">
       <% } else { %>
         <img class="mr-2 img-thumbnail" src="{{ url('public/distributor/') }}/<%= profile %>" alt="image">
       <% } %>
    </td>
    <td><%= bu_name %></td>    
    <td><%= name %></td>
    <td>
      <%= mobile %>
      <div class="d-flex">
        <span class="pr-2 d-flex align-items-center"><%= email %></span>
      </div>
    </td>  
    <!-- <td><% if(userlogId){ %> Yes <% }else{ %> No <% } %></td>  -->
    <td class="text-center">
      <label class="custom_switch">
        <input type="checkbox" id="status_switch" data-id="<%= id %>" <% if(status=="active") { %>checked <% } %> >
        <span class="slider round"></span>
      </label>
    </td>    
    <td class="text-center">
      <a  href="javascript:;" data-id="<%= id %>" id="editdistributorbutton" class="btn btn-sm btn-primary"> <i class="fas fa-pencil-alt"></i> </a>
      <a href="javascript:;" data-id="<%= id %>" id="deletedistributorbutton" class="btn btn-sm btn-default"> <i class="fas fa-trash-alt"></i> </a>
    </td>
</tr>


<!-- Button trigger modal -->
