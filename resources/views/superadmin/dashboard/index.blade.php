@extends('layouts.superadmin')
@section('content')
         
  <div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
          <div class="no_audit p-5 text-center mt-3" >
          <img src="{{ asset('public/images/norecord.png') }}">
          <p class="txt_black" style="font-size: 16px; font-weight: bold;">Coming Soon</p>
        </div>
      </div>
      <?php //include 'copyrighte.php';?>
    </div>
  </div>

@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Dashboard");
</script>
<!-- <script src="{{ asset('public/js/superadmin/dashboard.js') }}" type="text/javascript" charset="utf-8"></script> -->
@endsection