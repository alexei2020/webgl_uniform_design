<div class="table-responsive">
  <table class="table table-hover">
    <% if(rows){ %>
      <thead>
        <tr>          
          <th> Id </th>
          <th class="sorting" data-sorting_type="asc" data-column_name="distributor_name"> Distributor<span class="sort_area" id="distributor_name_icon"></span></th>
          <th> Image </th>
          <th class="sorting" data-sorting_type="asc" data-column_name="name"> Name <span class="sort_area" id="name_icon"></span></th>
          <th class="sorting" data-sorting_type="asc" data-column_name="category"> Category <span class="sort_area" id="category_icon"></span></th>
          <th class="sorting" data-sorting_type="asc" data-column_name="url_slug"> URL Slug <span class="sort_area" id="url_slug_icon"></span></th>
          <th class="text-center sorting" data-sorting_type="asc" data-column_name="position"> Position <span class="sort_area" id="position_icon"></span></th>
          <th class="text-center sorting" data-sorting_type="asc" data-column_name="status"> Active <span class="sort_area" id="status_icon"></span></th>
          <th class="text-center" style="width:120px"> Action </th>          
        </tr>
      </thead>
    <% } %>
    <tbody id="searchResultDiv">
      <%=rows%>
      <% if(!rows){ %>
      <!-- @include('common.norecord') -->
      <div class="no_audit p-5 text-center">
          <!-- <img src="{{ asset('public/images/norecord.png') }}"> -->
          <p>No products found</p>
        </div>
      <% } %>
    </tbody>
  </table>
</div>
<div id="pagination" class="main__content-data-footer d-flex justify-content-between align-items-center border-top pt-4"> </div>
