<form enctype="multipart/form-data" id="stepform">
  <input type="hidden" name="" id="stepid" value="<%= id %>" >
  <input type="hidden" name="" id="productId" value="<%= productId %>" >
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label>Name <span class="mandatory">*</span></label>
          <input type="text" id="stepname" value="<%= name %>" placeholder="" class="form-control" >
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label>Position <span class="mandatory">*</span></label>
          <input type="text" id="stepposition"  placeholder="" class="form-control" value="<%= position %>" onkeypress="return check(event,value)" onchange="return check(event,value)" >
        </div>
      </div>
      <!-- <div class="col-md-12">
        <div class="form-group">
          <label>Status <span class="mandatory">*</span></label>
          <select data-placeholder="" id="stepstatus" class="chosen-select w-100" tabindex="3">
            <option value="active" <% if(status=="active"){ %> selected <% } %> >Active</option>
            <option value="deactive" <% if(status=="deactive"){ %> selected <% } %>>Deactive</option>
          </select>
        </div>
      </div>        -->

        <div class="col-md-12">
          <div class="form-group">
          <div class="custom-control custom-checkbox mb-3 mr-3">
              <input type="checkbox" class="custom-control-input" id="stepstatus" name="stepstatus" <% if(status=="active"){ %> checked <% } %>  >
              <label class="custom-control-label" for="stepstatus">Active</label>
          </div>
        </div>
        </div>

    </div>
</form>
          