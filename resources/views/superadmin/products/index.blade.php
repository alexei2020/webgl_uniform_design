@extends('layouts.superadmin')
@section('content')


<div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
          <div class="search_area mb-5">
              <div class="row">
                 <div class="col-7 col-md-3">
                    {!!Form::select('distributor', $distributor, null, ['id'=>'distributorselectbox','class' => 'chosen-select form-control'])!!}
                  </div>
                  <div class="col-7 col-md-3">
                     <input aria-label="Search" id="searchproductinput" placeholder="Search" class="form-control">  
                  </div>
                 <!-- <div class="col-5 col-md-6 text-right mb-4">
                      <button class="btn btn-primary mb-2 mb-md-0 mr-2 " id="addproductbutton"><i class="fas fa-plus mr-2"></i> New Product</button>
                  </div> -->
              </div>
          </div>
          <div id="edit_product"></div>
          <div id="result"> 
          </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Product");
</script>

<!-- <script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script> -->
<!-- <script src="{{ asset('public/js/ckeditor.js') }}" type="text/javascript"></script> -->

<script src="{{ asset('public/js/superadmin/product.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection