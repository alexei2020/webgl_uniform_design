<object data="" type="image/svg+xml" id="select_svg_object" width="20%" height="20%"  ></object>
<div class="row align-items-center mb-4">  
  <div class="col-md-6 h-100">  
    <h4 id="product_heading"><%= heading %> </h4>
  </div>
  
  <div class="col-md-6">
    <div class="form-group mb-0">
      <label>Select an SVG to load <span class="mandatory">*</span></label>
      <select name="select_svg" id="select_svg" class="chosen-select w-100" >
        <option value="">Select SVG </option>
          <%= svg_image %>
      </select>
    </div>
  </div>
</div>

<hr />
<input type="hidden" id="id" value="<%= id %>" class="form-control">
<nav>
    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
      <a class="nav-item nav-link roottablink" id="Product-tab" data-value="product_tab" role="tab" aria-controls="nav-Product" aria-selected="true">Product</a>
     
      <a class="nav-item nav-link roottablink" id="Steps-tab" data-value="product_step_tab" role="tab" aria-controls="nav-Steps" aria-selected="false">Steps</a>

       <a class="nav-item nav-link roottablink" id="field-tab" data-value="product_field_tab" role="tab" aria-controls="nav-field" aria-selected="false">Field</a>

       <a class="nav-item nav-link roottablink" id="Designs-tab" data-value="product_design_tab" role="tab" aria-controls="nav-Designs" aria-selected="false">Designs</a>
    
    </div>
</nav>

<div class="tab-content mt-4 py-3 px-3 px-sm-0" id="nav-tabContent">
    <div id="product_area">
      <div class="tab-pane fade show active" id="nav-Product" role="tabpanel" aria-labelledby="Product-tab">
            <form enctype="multipart/form-data" id="productform" class="edited mt-4">
              <div class="row">
              <div class="col-md-6">
                 <div class="form-group">
                    <label>Name <span class="mandatory">*</span></label>
                    <input type="text" id="name" placeholder="" value="<%= name %>" class="form-control" >
                 </div>
              </div>
              <div class="col-md-6">
                 <div class="form-group">
                    <label>URL Slug</label>
                    <input type="text" id="url_slug" disabled placeholder="" value="<%= url_slug %>" class="form-control" >
                    <small>#/products/name</small>
                 </div>
              </div>

              <div class="col-md-12 mb-3">
                <div class="form-group">
                  <label>Description <span class="mandatory">*</span></label>
                    <textarea name="extra_description" class="form-control" id="extra_description"><%= description %></textarea>
                </div>
              </div>

              <div class="col-md-12 mb-3">
                <div class="form-group">
                  <label>Category <span class="mandatory">*</span></label>
                  <select id="categoryId" name="category" class="w-100 chosen-select">
                   <option value="">category</option>

                    <%= category %>
                  </select>
                  
              
                </div>
              </div>
             
              <div class="col-md-12 mb-1">
                <div class="d-flex">
                  <div class="custom-control custom-checkbox mb-3 mr-3">
                    <input type="checkbox" class="custom-control-input" id="rotation_disable" name="rotation_disable" <% if(rotation_disable=="true"){ %> checked <% } %>>
                    <label class="custom-control-label" for="rotation_disable">Disable Rotation</label>
                  </div>
                
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <label>List Image<span class="mandatory">*</span></label>
                 <select id="list_ImageId" name="list_ImageId" class="w-100 chosen-select">
                  <option value="">Select image</option>
                  <%= list_image %>
                 </select>

                  <small>Image that's displayed on product listing page</small>
                </div>
              </div>
              <div class="col-md-6">
                <label>3D Model</label>
                 <select id="model_3DId" name="model_3DId" class="w-100 chosen-select">
                  <option value="">Select 3D model </option>
                  <%= model_3D %>
                 </select>
               
                <small>Select .obj file.</small>
              </div>
              </div>
             
              <hr class="mb-4" />
              <div class="row">
               
                <div class="col-md-6">
                  <div class="row">
                  <div class="col-md-12">
                    <label>Position Offset</label>
                  </div>
                    <div class="col-md-4">
                      <div class="form-inline axis">
                        <label>X</label>
                        <input type="number" id="x_axis" placeholder="" value="<%= x_axis %>" class="form-control w-100" >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-inline axis">
                        <label>Y</label>
                        <input type="number" id="y_axis" placeholder="" value="<%= y_axis %>" class="form-control w-100" >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-inline axis">
                        <label>Z</label>
                        <input type="number" id="z_axis" placeholder="" value="<%= z_axis %>" class="form-control w-100" >
                      </div>
                    </div>
                  </div>
                  <!-- <small>1 Unit = 1 Metre. Z is vertical, right-handed coordinate system.</small> -->
                  <small>1 Unit = 1 Metre  (only keep this is 1 Unit = 1 metre.) X = Horizontal Axis, Y = Vertical Axis, Z= Vertical Axis, Right-Handed Coordinate</small>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Scale<span class="mandatory">*</span></label>
                    <input type="number" id="scale" placeholder="" value="<%= scale %>" class="form-control" >
                    <!-- <small>Range between 3 to 6.</small> -->
                  </div>
                </div>                
                <div class="col-md-12 mb-1" style="display: none;">
                  <div class="custom-control custom-checkbox mb-3 mr-3">
                    <input type="checkbox" class="custom-control-input" id="override_model_position_scale" name="override_model_position_scale" <% if(override_model_position_scale=="true"){ %> checked <% } %> >
                    <label class="custom-control-label" for="override_model_position_scale">Override Form Reply Model Position/Scale</label>
                  </div>
                </div>
              </div>
              <hr class="mb-4" />
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group mb-2">
                    <label>Light Brightness<span class="mandatory">*</span></label>
                    <input type="number" id="light_brightness" value="<%= light_brightness %>" placeholder="" class="form-control" >
                    <!-- <small>Default 1. Twice as bright = 2. Basket/form reply images only.</small> -->
                    <small>
                      Default 0. Range between 0 to 1.
                    </small>
                  </div>
                </div>
                 <div class="col-md-6">
                  <div class="form-group">
                    <label>Default Toggle Colour<span class="mandatory">*</span></label>
                    <div class="position-relative">
                      <div class="color-view" id="color_view_default_toggle_color" style="background-color: #fc0"></div>
                      <input type="text" id="default_toggle_color" value="<%= default_toggle_color %>" placeholder="" class="form-control" >
                    </div>
                    <small>The colour of elements that are toggled on/off with a check box  </small>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>SVG Dimensions<span class="mandatory">*</span></label>
                    <div class="d-flex">
                      <input type="text" id="svg_dimension_ht" name="svg_width" value="<%= svg_dimension_ht %>" placeholder="" class="form-control" >
                      <div style="width:50px; line-height: 34px" class="text-center">X</div>
                      <input type="text" id="svg_dimension_vt" name="svg_height" value="<%= svg_dimension_vt %>" placeholder="" class="form-control"  >
                    </div>
                  
                    <small>The width and height of the selected SVG exported from CorelDraw or Illustrator</small>
                    <br>
                    <a href="javascript:;" class="pt-2 text-danger" id="selected_svg_dimension" style="line-height: 34px">Use selected SVG dimensions</a><br />
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Position<span class="mandatory">*</span></label>
                    <input type="text" id="position" placeholder="" value="<%= position %>" class="form-control" onkeypress="return check(event,value)" onchange="return check(event,value)" >
                    <small>Position in the product list.</small>
                  </div>
                </div>
                
              </div>
              <div class="row">
               
                <div class="col-md-12 mb-1 mt-2">
                  <div class="d-flex">
                    <div class="custom-control custom-checkbox mb-3 mr-3">
                      <input type="checkbox" class="custom-control-input" id="status" name="status" <% if(status=="active"){ %> checked <% } %>>
                      <label class="custom-control-label" for="status">Active</label>
                    </div>
                  </div>
                </div>  
                <div class="col-md-12 mb-4 mt-2">
                  <button type="button" id="submitproductbutton" class="btn btn-primary mr-3">Save</button>  
                  <button type="button" class="btn btn-default">Cancel</button>     
                </div>      
              </div>   
            </form>  
      </div>
    </div>
         
    <div id="step_area">
      <div class="tab-pane fade" id="nav-Steps" role="tabpanel" aria-labelledby="Steps-tab">
        <div class="step_search_area">
          <div class="row">
            <div class="col-7 col-md-5">
              <input aria-label="Search" id="searchstepinput" placeholder="Search" class="form-control">  
            </div>
            <div class="col-5 col-md-7 text-right mb-4">
              <button class="btn btn-primary mb-2 mb-md-0 mr-2" data-productId="<%= id %>" id="addstepbutton" ><i class="fas fa-plus mr-2"></i> Add Step</button>
            </div>
          </div>
        </div>
        <div id="step_result"></div>

      </div>
    </div>

    <div id="field_area">
      <div class="tab-pane fade" id="nav-field" role="tabpanel" aria-labelledby="field-tab">
        <div class="field_search_area">
          <div class="row">
            <div class="col-7 col-md-5">
              <input aria-label="Search" id="searchfieldinput" placeholder="Search" class="form-control">  
            </div>
            <div class="col-5 col-md-7 text-right mb-4">
              <button class="btn btn-primary mb-2 mb-md-0 mr-2" data-productId="<%= id %>" id="addfieldbutton" ><i class="fas fa-plus mr-2"></i> Add Field</button>
            </div>
          </div>
        </div>
        <div id="field_result"></div>
      </div>
    </div>

    <div id="design_area">
      <div class="tab-pane fade" id="nav-Designs" role="tabpanel" aria-labelledby="Designs-tab">
        <div class="design_search_area">
          <div class="row">
            <div class="col-7 col-md-5">
              <input aria-label="Search" id="searchdesigninput" placeholder="Search" class="form-control">  
            </div>
            <div class="col-5 col-md-7 text-right mb-4">
              <button class="btn btn-primary mb-2 mb-md-0 mr-2" data-productId="<%= id %>" id="adddesignbutton" ><i class="fas fa-plus mr-2"></i> Add Design</button>
            </div>
          </div>
        </div>
        <div id="design_result"></div>
      </div>
    </div>

    

</div>              


<script>
   CKEDITOR.replace( 'extra_description');
   $('#default_toggle_color').each( function() {
       
        $(this).minicolors({
          change: function(value, opacity) {
            if( !value ) return;
            if( opacity ) value += ', ' + opacity;
            if( typeof console === 'object' ) {
              console.log(value);
            }

          },
          theme: 'bootstrap'
        });

      });

</script>

