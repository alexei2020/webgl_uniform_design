<form enctype="multipart/form-data" id="fieldform">
  <input type="hidden" name="" id="fieldid" value="<%= id %>" >
  <input type="hidden" name="" id="productId" value="<%= productId %>" >
          <div class="row">
             <div class="col-md-12">
              <div class="form-group">
                <label>Name <span class="mandatory">*</span></label>
                <input type="text" id="field_name" placeholder="" value="<%= name %>" class="form-control" >
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label> Step <span class="mandatory">*</span></label>
                <select data-placeholder="" id="field_step" class="chosen-select w-100" tabindex="3">
                  <%= step %>
                </select>
              </div>
            </div>

             <div class="col-md-12">
              <div class="form-group">
                <label>Filed Type <span class="mandatory">*</span></label>
                <select data-placeholder="" id="filed_type" class="chosen-select w-100" tabindex="3">
                  <option >Select Type</option>
                  <option value="text" <% if(filed_type=="text") { %> selected <% } %> >Text</option>
                  <option value="image" <% if(filed_type=="image") { %> selected <% } %>  >Image</option>
                </select>
              </div>
            </div>


            <div class="col-md-12">
              <div class="form-group">
                <label> Color Group <span class="mandatory">*</span></label>
                <select data-placeholder="" id="field_color_group" class="chosen-select w-100" tabindex="3">
                  <%= groups %>
                </select>
              </div>
            </div>

          </div>
        </form>