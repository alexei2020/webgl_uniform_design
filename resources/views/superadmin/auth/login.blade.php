@extends('layouts.superadmin')
@section('content')
<div class="container-fluid h-100 login">
    <div class="row align-items-center h-100">
        <div class="col-8 mx-auto">
            <div class="jumbotron overflow-hidden my-auto p-0">
                <div class="row">
                    <div class="col-6 bg_danger">
                        <div class="p-4 h-100">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div>
                                            <img src="{{ asset('public/images/banner_1.png') }}" class="w-100" alt="image"> <br />
                                            <div class="text-white mt-3 small">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div>
                                            <img src="{{ asset('public/images/banner_2.png') }}" class="w-100" alt="image"><br />
                                            <div class="text-white mt-3 small">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div> 
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div>
                                            <img src="{{ asset('public/images/banner_3.png') }}" class="w-100" alt="image"> <br />
                                            <div class="text-white mt-3 small">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 bg_white">
                        <div class="px-4 py-5">
                            <div class="text-right">
                                <!-- Nav tabs -->
                                <!-- <ul class="nav nav-tabs small justify-content-end">
                                  <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#SignIn">Sign In</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#SignUp">Sign Up</a>
                                  </li>
                                </ul> -->
                            </div>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active container" id="SignIn">
                                    <h5 class="my-5 txt_black"><span class="active">Sign In</span><!--  or <span>Sign Up</span> --></h5>
                                    <form method="POST" action="{{ route('login') }}">
                                         @csrf
                                        <div class="form-group">
                                            <label for="email">Email address:</label>
                                             <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                 @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Password:</label>
                                            <input id="pwd" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                                <!-- <i id="pass-status" class="fa fa-eye show_password" aria-hidden="true" onClick="viewPassword()"></i> -->
                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                        <div class="form-group form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox"> Remember me
                                            </label>
                                        </div>
                                        <!-- <button type="submit" class="btn btn-primary mr-4">Submit</button>  -->
                                         <button type="submit" class="btn btn-primary mr-4">
                                            {{ __('Login') }}
                                        </button>
                                         @if (Route::has('password.request'))
                                            <a  href="{{ route('password.request') }}" class="forgot_pass">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif
                                    </form> 
                                </div>
                                <!-- <div class="tab-pane container" id="SignUp">
                                    <h5 class="my-5 txt_black"><span>Sign In</span> or <span class="active">Sign Up</span></h5>
                                    <form action="/action_page.php">
                                        <div class="form-group">
                                            <label for="email">Email address:</label>
                                            <input type="email" class="form-control" placeholder="Enter email" id="email">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Password:</label>
                                            <input type="password" class="form-control" placeholder="Enter password" id="pwd">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form> 
                                </div> -->
                            </div> 
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
