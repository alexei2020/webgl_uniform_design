<table class="table custom-table text-dark">
       <% if(rows){ %>
      <thead>
        <tr>
          <th width="5%">#</th>
          <th  class="sorting" data-sorting_type="asc" data-column_name="name">Name <span class="sort_area" id="name_icon"></span></th>
          <th  class="sorting" data-sorting_type="asc" data-column_name="code_code">Value <span class="sort_area" id="code_icon"></span></th>  
                              
        </tr>
      </thead>
      <% } %>
      <tbody id="searchResultDiv1">
        <%=rows%>
        <% if(!rows){ %>
      @include('common.norecord')
    <% } %>
  </tbody>
</table>
