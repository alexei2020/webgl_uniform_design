@extends('layouts.app')
@section('content')
         
  <div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
      		<div class="search_area">
          		<div class="container">
            		<div class="row">
                		<div class="col-7 col-md-5">
                  		 <input aria-label="Search" id="searchfontinput" placeholder="Search" class="form-control">  

                		</div>
                		<div class="col-7 text-right mb-4">
                      <!-- <a href="javascript:;" id="font_xls_download" class="mr-3"><i class="fas fa-file-export"></i> Export Font</a> -->

                       <a href="{{ url('distributor/download-font-excel/') }}" ><i class="fas fa-file-export"></i> Export Font</a>

                         <a class="mr-3" href="javascript:;" id="importfontbutton"><i class="fas fa-download"></i> Import Font</a>
                  			<button class="btn btn-primary mb-2 mb-md-0 mr-2 " id="addfontbutton"> New Font</button>
                		</div>
	            	</div>
    		    </div>
        	</div>

            <div class="table-responsive" id="result"> 
            </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Font");
</script>
<script src="{{ asset('public/js/distributor/font.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection