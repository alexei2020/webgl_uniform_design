
<form enctype="multipart/form-data" id="fontform" class="edited">
<input type="hidden" name="" id="id" value="<%= id %>">
        <div class="row">

            <div class="col-md-12">
              <div class="form-group">
                <label>Font <span class="mandatory">*</span></label>
                <input type="text" id="value" autocomplete="false" placeholder="Enter Value" value="<%= value %>" class="form-control" >
              </div>
            </div>
            <div class="col-md-12">
              <div id="test_font" class="form-group">
                <span style="color: black;">
                THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG<br>
                The quick brown fox jumps over the lazy dog. 0123456789<br>
                </span>
              </div>
            </div>
                            

             <div class="col-md-12">
              <div class="form-group">
                <label>Name <span class="mandatory">*</span></label>
                <input type="text" id="name" placeholder="Enter Name" value="<%= name %>" class="form-control" >
              </div>
            </div>


            <div class="col-md-12">
              <div class="form-group">
                <div class="custom-control custom-checkbox mb-3 mr-3">
                  <input type="checkbox" class="custom-control-input" id="status" name="status" <% if(status=="active"){ %> checked <% } %>  >
                  <label class="custom-control-label" for="status">Active</label>
                </div>
              </div>
            </div>

          </div>
        </form>

<script type="text/javascript">
  $('#value')
.fontselect()
.on('change', function() {
  applyFont(this.value);
  setFontToName(this.value);
  setFontToValue(this.value);
});
</script>