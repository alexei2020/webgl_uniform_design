<form enctype="multipart/form-data" id="companyform" class="edited mt-4">
       <input type="hidden" id="id"  value="<%= id %>" class="form-control">
    
   <div class="tab-pane fade show active" id="nav-main" role="tabpanel" aria-labelledby="main-tab">
            <div class="row">
              <div class="col-md-6">
                 <div class="form-group">
                    <label>Name <span class="mandatory">*</span></label>
                    <input type="text" id="company_name" placeholder="Enter Name" value="<%= company_name %>" class="form-control" >
                 </div>
              </div>
            </div>            
            <div class="row">
              <div class="col-md-6">

                <% if(profileName!="") { %>
                 <div class="form-group">
                  <img  src="{{ url('public/images/') }}/<%= profileName %>" class="w-25 mb-2 img-thumbnail">
                 </div>
               <% } %>
                
                  <div class="form-group">
                      <label>Logo</label>

                       <select id="profile" name="profile" class="w-100 chosen-select">
                        <option value="">Select image</option>
                        @foreach($document as $doc)
                         <option value="{{ $doc->id }}">{{ $doc->original_name }}</option>
                         @endforeach
                       </select>

                  </div>
              </div>
            </div>



            <div class="row"> 
              <div class="col-md-6">
                 <div class="form-group">
                    <label>Email <span class="mandatory">*</span></label>
                    <input type="text" id="email" value="<%= email %>" placeholder="Enter Email" class="form-control" >
                    <small>Email address form submissions will be sent to and the address emails are sent from</small>
                 </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Phone <span class="mandatory">*</span></label>
                  <input type="text" id="phone" value="<%= phone %>" placeholder="" class="form-control" >
                </div>
              </div>
              <div class="col-md-6">
                <label>Website URL</label>
                <input type="text" id="website_url" value="<%= website_url %>" placeholder="" class="form-control" >
              </div>
            </div>
            <hr class="mb-4 mt-4" />
          
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Time Zone <span class="mandatory">*</span></label>
                  <select data-placeholder="" id="time_zone" class="chosen-select w-100" tabindex="3">
                    <!-- <option data-tokens="ketchup mustard">-</option> -->
                    <%= timezone %>
                  </select>
                  <small>Display dates using this time zone.</small>
                </div>
              </div>
          
              <div class="col-md-12 mb-1 mt-4">
                <div class="d-flex">
                  <div class="custom-control custom-checkbox mb-3 mr-3">
                    <input type="checkbox" class="custom-control-input" id="status" name="status" <% if(status=="active"){ %> checked <% } %> >
                    <label class="custom-control-label" for="status">Active</label>
                    <small>Allow the public to view the UniformBuilder</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">  
              <div class="col-md-12 mb-4 mt-4">
                <button type="button" id="submitcompanydetailbutton" data-id="<%= id %>" class="btn btn-primary mr-3">Save</button>  
              </div>      
            </div>     
          </div>
       

</form>