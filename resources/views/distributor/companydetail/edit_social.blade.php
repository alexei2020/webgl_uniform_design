
<form enctype="multipart/form-data" id="companySocialform" class="edited mt-4">
       <input type="hidden" id="csmId"  value="<%= id %>" class="form-control">

   <div class="tab-pane fade show active" id="nav-sm" role="tabpanel" aria-labelledby="sm-tab">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>AddThis Social Share Button HTML <span class="mandatory">*</span></label>
                  <textarea id="social_share_html" rows="5" name="social_share_html" placeholder="e.g. <!-- AddThis Button BEGIN -->
<span class='kb-addthis addthis_toolbox addthis_default_style addthis_16x16_style'>
                        <a class='addthis_button_facebook'></a>" class="form-control" ><%= social_share_html %></textarea>
                  <small>By default the AddThis buttons appear on the final page. You can override the default buttons here. Add the class kb-addthis to the span to line up the buttons.</small>
                </div>
              </div>
           
            </div> 
            <div class="row">  
              <div class="col-md-12 mb-4 mt-4">
                <button type="button" id="submitsocialmedialbutton" class="btn btn-primary mr-3">Save</button>  
              </div>      
            </div>  
          </div>
       

</form>