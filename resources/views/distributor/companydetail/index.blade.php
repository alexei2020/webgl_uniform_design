@extends('layouts.app')
@section('content')
         
  <div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
          <h4>Edit Company </h4>
          <hr />
      		
        <nav>
          <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active roottablink" id="main-tab" data-value="companyDetail"  role="tab" aria-controls="nav-main" aria-selected="true">Main</a>
            <a class="nav-item nav-link roottablink" id="sm-tab" data-value="socialMedia" role="tab" aria-controls="nav-sm" aria-selected="false">Social Media</a>
          </div>
        </nav>

        <div class="tab-content mt-4 py-3 px-3 px-sm-0" id="nav-tabContent">
          <div id="companyDetail" ></div>
          <div id="socialMedia" ></div>
        </div>     
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Company Details");
</script>
<script src="{{ asset('public/js/distributor/companydetail.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection