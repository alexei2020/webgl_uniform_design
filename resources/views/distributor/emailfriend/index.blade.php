@extends('layouts.app')
@section('content')
         
  <div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
          <h4>Edit Email Friend Settings </h4>
          <hr />
      		
        <div id="result"> 
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Form");
</script>
<script src="{{ asset('public/js/distributor/emailfrined.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection