<form enctype="multipart/form-data" id="companyform" class="edited mt-4">
       <input type="hidden" id="id"  value="<%= id %>" class="form-control">
    
    <div class="row">
      <div class="col-md-6">
         <div class="form-group">
            <label>From Email <span class="mandatory">*</span></label>
            <input type="email" id="from_email" autocomplete="false" placeholder="Enter Email" value="<%= from_email %>" class="form-control" >
            <small>Email address the friend request email will come from</small>
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label>From Name <span class="mandatory">*</span></label>
            <input type="text" id="from_name" placeholder="Enter Name" value="<%= from_name %>" class="form-control" >
            <small>Name the email will come from</small>
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label>Subject <span class="mandatory">*</span></label>
            <input type="text" id="subject" autocomplete="false" placeholder="eg. Email Subject" value="<%= subject %>" class="form-control" >
            <small>Subject on the Email</small>
         </div>
      </div>
    </div>

    <hr class="mb-4" /> 
    <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label>Email Template <span class="mandatory">*</span></label>
            <textarea type="text" id="email_template" autocomplete="false" placeholder="" class="form-control" ><%= email_template %></textarea>
            <small>The whole email template on the email. Allowed tags that get replaced are: [[friendName]] [[url]] [[message]]</small>
         </div>
      </div>
    </div>

        <div class="row">  
          <div class="col-md-12 mb-4 mt-4">  
            <button type='button' id='submitemailfrinedbutton' class="btn btn-primary mr-3">Save</button>
          </div>      
        </div> 

       

</form>