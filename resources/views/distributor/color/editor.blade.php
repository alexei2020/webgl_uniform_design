
<form enctype="multipart/form-data" id="colorform" class="edited">
<input type="hidden" name="" id="id" value="<%= id %>">
        <div class="row">
             <div class="col-md-12">
              <div class="form-group">
                <label>Name <span class="mandatory">*</span></label>
                <input type="text" id="name" placeholder="Enter Name" value="<%= name %>" class="form-control" >
              </div>
            </div>
           
            <div class="col-md-12">
              <div class="form-group">
                <label>HEX VALUE <span class="mandatory">*</span></label>
                <div class="position-relative">
                  <input type="text" id="code" autocomplete="false" placeholder="Enter Value" value="<%= code %>" class="form-control" >
                  </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <div class="custom-control custom-checkbox mb-3 mr-3">
                  <input type="checkbox" class="custom-control-input" id="status" name="status" <% if(status=="active"){ %> checked <% } %>  >
                  <label class="custom-control-label" for="status">Active</label>
                </div>
              </div>
            </div>

          </div>
        </form>

<script>

  
   $('#code').each( function() {
        $(this).minicolors({
          change: function(value, opacity) {
            if( !value ) return;
            if( opacity ) value += ', ' + opacity;
            if( typeof console === 'object' ) {
              console.log(value);
            }

          },
          theme: 'bootstrap'
        });

      });


</script>