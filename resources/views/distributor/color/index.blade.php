@extends('layouts.app')
@section('content')
         
  <div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
      		<div class="search_area">
          		<div class="container">
            		<div class="row">
                		<div class="col-7 col-md-5">
                  		 <input aria-label="Search" id="searchcolorinput" placeholder="Search" class="form-control"> 
                		</div>
                		<div class="col-7 text-right mb-4">
                      <!-- <a href="javascript:;" id="xls_download" class="mr-3"><i class="fas fa-file-export"></i> Export Color</a> -->

                      <a href="{{ url('distributor/download-color-excel/') }}" ><i class="fas fa-file-export"></i> Export Color</a>

                         <a href="javascript:;" class="mr-3" id="importcolorbutton"><i class="fas fa-download"></i> Import Color</a>
                  			<button class="btn btn-primary mb-2 mb-md-0 mr-2 " id="addcolorbutton"> New Color</button>
                		</div>
	            	</div>
                <div class="row">
                  <div class="col-md-2 col-sm-3 mb-3">
                    <select class="chosen-select" id="limitfilter">
                      <option value="">Select Range</option>
                      <option value="10">1-10</option>
                      <option value="50">1-50</option>
                      <option value="100">1-100</option>
                      <option value="500">1-500</option>
                    </select>
                  </div>
                </div>
    		    </div>
        	</div>

            <div class="table-responsive" id="result"> 
            </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Color");
</script>
<script src="{{ asset('public/js/distributor/color.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection