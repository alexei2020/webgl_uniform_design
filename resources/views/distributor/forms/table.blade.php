<table class="table custom-table text-dark">
       <% if(rows){ %>
      <thead>
        <tr>
          <th width="5%">#</th>
          <th  class="sorting" data-sorting_type="asc" data-column_name="name">Form Name <span class="sort_area" id="name_icon"></span></th>
          <th  class="sorting" data-sorting_type="asc" data-column_name="email">Email <span class="sort_area" id="email_icon"></span></th>
          <th class="text-center">Active</th>
          <th class="text-center" style="width:120px">Action</th>                          
        </tr>
      </thead>
      <% } %>
      <tbody id="searchResultDiv">
        <%=rows%>
        <% if(!rows){ %>
      <!-- @include('common.norecord') -->
      <div class="no_audit p-5 text-center">
          <!-- <img src="{{ asset('public/images/norecord.png') }}"> -->
          <p>No forms found</p>
        </div>
    <% } %>
  </tbody>
</table>

  <div id="pagination" class="main__content-data-footer d-flex justify-content-between align-items-center border-top pt-4">
  </div>
