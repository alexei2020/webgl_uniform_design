<tr>
    <td><%= i %></td>
    <td>
      <strong><%= name %></strong>
    </td>
    <td><%= email %></td>
    <td class="text-center">
      <div class="custom-control custom-checkbox">
        <input type="checkbox" data-id="<%= id %>" class="custom-control-input checkboxoption formcheckboxoption" <% if(status=="active"){ %> checked <% } %> id="status_<%= id %>" name="status_act" value="<%= id %>">
        <label class="custom-control-label" for="status_<%= id %>"></label>
      </div>
    </td>
    <td class="text-center">
      <a  href="javascript:;" data-id="<%= id %>" id="editformbutton" class="btn btn-sm btn-primary"> <i class="fas fa-pencil-alt"></i> </a>
      <a href="javascript:;" data-id="<%= id %>" id="deleteformbutton" class="btn btn-sm btn-default"> <i class="fas fa-trash-alt"></i> </a>
    </td>
</tr>
