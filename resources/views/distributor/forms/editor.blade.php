<form enctype="multipart/form-data" id="companyform" class="edited" autocomplete="off">
        <input type="hidden" id="id"  value="<%= id %>" class="form-control">
        <div class="form-group inputautofill">
           <label>Form Name <span class="mandatory">*</span></label>
           <input type="text" id="name" placeholder="Form Name" value="<%= name %>" class="form-control" >
           <div id="countryList">
        </div>
    
        <div class="form-group mt-2">
            <div class="d-flex">
              <div class="custom-control custom-checkbox mb-3 mr-3">
                <input type="checkbox" class="custom-control-input" id="email_notification" <% if(notification=='true'){ %>  checked <% } %> name="example1">
                <label class="custom-control-label" for="email_notification">Enable Email Notification</label>
              </div>
            </div>
        </div>
        <div class="form-group">
           <label>Email <span class="mandatory">*</span></label>
            <input type="email" id="email" autocomplete="false" placeholder="Email" value="<%= email %>" class="form-control" >
        </div>

      
        <div class="form-group">
            <label>Email Subject</label>
            <input type="text" id="email_subject" autocomplete="false" placeholder="eg. Form Reply" value="<%= email_subject %>" class="form-control" >
        </div>
        <div class="form-group">
            <label>Form Type</label>
            <select id="form_type" class="form-control">
              <option value="Get a quote">Get a quote</option>
            </select>
        </div>
        <div class="form-group" style="display: none;">
            <label>Form Code</label>
            <input type="text" id="form_code" autocomplete="false" placeholder="Form Code" value="" class="form-control">
        </div>

        <div class="form-group">
          <div class="custom-control custom-checkbox mb-3 mr-3">
            <input type="checkbox" class="custom-control-input" id="status" name="status" <% if(status=="active"){ %> checked <% } %>  >
            <label class="custom-control-label" for="status">Active</label>
          </div>
        </div>

</form>