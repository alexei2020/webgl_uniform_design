<form id="documentsform" class="edited mt-4">
       <input type="hidden" id="id" value="<%= id %>" class="form-control">
       <div class="row">
          <div class="col-md-6">
            <div class="form-group" id="categoryParentnotnull">
               <label>Category</label>
               {!!Form::select('category', $category, null, ['class' => 'form-control',"id"=>'categoryId'])!!}
            </div>
            <div class="form-group" id="categoryParentnull" style="display: none">
               <label>Category</label>
               {!!Form::select('category', ["null"=>"Select Category"], null, ['class' => 'form-control'])!!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <label>Name</label>
               <input type="text" id="name" placeholder="Enter Name" value="<%= name %>" class="form-control" name="">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
           	  <label>Url Slug</label>
           			<input type="hidden" value="{{ url('setting/category/') }}" id="base_url"> 
                     <input type="hidden" value="<%= url_slug %>" id="p_name">
               <textarea style="display: none;" id="copy_full_url"><%= url_slug %></textarea>
               <div>{{url('setting/category')}}/</div>
                                     
                {{ Form::text('url_slug',null,array('class' => 'form-control','placeholder'=>'e.g. rugby-kits','id'=>'url_slug',"data-error"=>"This field is required")) }}
                   
                    <a href="javascript:;" class="copy-url" onclick="copyContents()" >Copy</a>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group" >
               <label>Image</label>
               {!!Form::select('image', $category, null, ['class' => 'form-control',"id"=>'image'])!!}
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group" >
               <label>Status</label>
               {!!Form::select('status',["active"=>"Active",'deactive'=>'Deactive'], null, ['class' => 'form-control chosen-select',"id"=>'status', "placeholder"=>"Select Status"])!!}
            </div>
          </div>
        </div>
</form>




