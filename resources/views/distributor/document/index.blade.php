@extends('layouts.app')
@section('content')


<div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
          <div class="search_area">
            <div class="container">
              <div class="row">
                <div class="col-6"> 
                    <input aria-label="Search" id="searchdocumentinput" placeholder="Search" class="form-control">  

                </div>
                <div class="col-6 text-right mb-4">
                  <div class="row">
                    <div class="col-12">
            
                     <form action="" method="post" id="documentform" enctype="multipart/form-data">
                        <div>
                          <div>
                              <div class="d-flex file_uploadarea">
                                <div class="form-group mb-2">
                                  <input type="file" placeholder="Choose file" class="form-control"  id="customFile"  name="name">
                                  <input type="hidden" class="hide_file-btn" id="id" value="">
                                </div>
                                <button type="submit" class="btn btn-primary mb-2 mb-md-0" id="submitdocumentbutton" > Upload</button>
                              </div>
                          </div>
                          <div style="text-align: left;">Files Supported: obj, svg, jpg, png</div>
                        </div>
                      </form>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div id="result"> 
          </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Document");
</script>
<script src="{{ asset('public/js/distributor/document.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection