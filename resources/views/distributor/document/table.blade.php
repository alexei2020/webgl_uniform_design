<div class="table-responsive">
  <table class="table table-hover">
    <% if(rows){ %>
      <thead>
        <tr>
          <th> Id </th>
          <th> Image </th>
          <th  class="sorting" data-sorting_type="asc" data-column_name="original_name"> Name <span class="sort_area" id="original_name_icon"></span></th>
          <th class="sorting" data-sorting_type="asc" data-column_name="file_size"> Size <span class="sort_area" id="file_size_icon"></span></th>
          <th class="sorting" data-sorting_type="asc" data-column_name="createDate"> Date <span class="sort_area" id="createDate_icon"></span></th>
          <th class="text-center" style="width:120px"> Action </th>
        </tr>
      </thead>
    <% } %>
    <tbody id="searchResultDiv">
      <%=rows%>
      <% if(!rows){ %>
      <!-- @include('common.norecord') -->
      <div class="no_audit p-5 text-center">
          <!-- <img src="{{ asset('public/images/norecord.png') }}"> -->
          <p>No files found</p>
        </div>
      <% } %>
    </tbody>
  </table>
</div>
<div id="pagination" class="main__content-data-footer d-flex justify-content-between align-items-center border-top pt-4"> </div>
