<tr>
  <td class="text-center">
    <div class="d-flex">
    
      <div><%= i %></div>
    </div>
  </td>
  <td>
  <% if(name.includes("obj")) { %>
 <img src="{{ url('public/images') }}/3d.png" style="width: 30px;height: 30px" class="mr-2 img-thumbnail" alt="image" />
  <% }else { %>
   <img src="{{ url('public/images') }}/<%= name %>" style="width: 30px;height: 30px" class="mr-2 img-thumbnail" alt="image" />
  <% } %> 

   </td>
   <td><a href="javascript:;"> <%= original_name %> </a></td> 
  <td>
    <a href="javascript:;"><%= file_size %></a>
  </td>
  <td>
    <a href="javascript:;"><%= createDate %></a>
  </td>
  <td class="text-center">
    <!-- <a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#editDistributor"> <i class="fas fa-pencil-alt"></i> </a> -->
    <a href="javascript:;" id="downloaddocumentbutton" data-name="<%= original_name %>" data-file="{{ url('public/images') }}/<%= name %>" class="btn btn-sm btn-primary"> <i class="fas fa-download"></i></a>
    <!-- <a href="#" class="btn btn-sm btn-info"> <i class="far fa-copy"></i> </a> -->
    <a href="javascript:;" id="deletedocumentbutton" data-id="<%= id %>" class="btn btn-sm btn-default"> <i class="fas fa-trash-alt"></i> </a>
  </td>
</tr>
