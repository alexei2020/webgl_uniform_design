<table class="table custom-table text-dark">
       <% if(rows){ %>
      <thead>
        <tr>
          <th width="5%">#</th>
          <th>Company Name </th>
          <th>Date </th>  
          <th>Email</th>
          <th>Distributor</th>
          <th class="text-center" style="width:120px">Action</th>                          
        </tr>
      </thead>
      <% } %>
      <tbody id="searchResultDiv">
        <%=rows%>
        <% if(!rows){ %>
      <!-- @include('common.norecord') -->
      <div class="no_audit p-5 text-center">
          <!-- <img src="{{ asset('public/images/norecord.png') }}"> -->
          <p>No reply found</p>
        </div>
    <% } %>
  </tbody>
</table>

  <div id="pagination" class="main__content-data-footer d-flex justify-content-between align-items-center border-top pt-4">
  </div>
