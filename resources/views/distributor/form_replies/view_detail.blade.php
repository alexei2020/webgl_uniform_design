
    <style type="text/css"> 
      body {margin: 0; padding: 0;font-family: 'Montserrat',sans-serif; font-weight: 400; font-size: 14px; }   
      /*Open Sans*/
      table, td, tr {vertical-align: top; border-collapse: collapse; } 
      * {line-height: inherit; } 
      a[x-apple-data-detectors=true] {color: inherit !important; text-decoration: none !important;  }
      a{display: inline-block; text-decoration: none;}
      hr{ margin: 20px auto; border-top: 1px solid rgba(0,0,0,.5) !important; background: none; border: none }
      .border_less, .border_less td, .border_less th{ border: none !important }
      .table_border, .table_border td, .table_border th{ border:  1px solid rgba(0,0,0,.2) !important }     
      .table_Stirp{ border: 1px solid rgba(0,0,0,.2); padding: 3px }
      .table_Stirp tr:nth-child(even), .table_border th {background: #ddd}
      table td, table th{ padding: 7px 10px }
      /*table.nl-container td, table.nl-container  th{ padding:0 !important}*/

      p{ margin: 0 }

      .txt_normal{font-weight: 300}
      .txt_sb{font-weight: 600}
      .txt_bold{font-weight: 700}
      .txt_black{font-weight: 800}
      .w-100{ width: 100% }

      .p-1{ padding: 10px; }
      .p-2{ padding: 20px; }
      .pt-1{ padding-top: 10px; }
      .pt-2{ padding-top: 20px; }
      .pb-1{ padding-bottom: 10px; }
      .pb-2{ padding-bottom: 20px; }
      .px-2{ padding-left:20px; padding-right: 20px}
      .px-3{ padding-left:30px; padding-right: 30px}
      .py-2{ padding-top:20px; padding-bottom: 20px}

      .m-auto{ margin-left: auto; margin-right: auto; }
      .mx-2{ margin-left: 20px; margin-right: 20px; }
      .my-2{ margin-top: 20px; margin-bottom: 20px; }
      .mb-2{ margin-bottom: 20px }
      .mb-3{ margin-bottom: 30px }

      .gary_dark{background-color:#263339; color: #8c9497}
      .gary_light{background-color:#45555d; color: #8c9497}

      .gary_dark a, .gary_light a{color: #8c9497}

      .font_12{ font-size: 12px }
      .font_14{ font-size: 14px }
      .font_18{ font-size: 18px }
      .font_20{ font-size: 20px }
      .text-center{ text-align: center; }
      .text-right{ text-align: right; }
      .text-left{ text-align: left; }

      .color_box{ display: inline-block; width: 15px; height: 15px; border-radius: 4px; border: 1px solid rgba(0,0,0,.2); margin: 0 5px; vertical-align: -2px;}
      .front_backLogo{width: 100%;  max-width: 300px;}
    </style>
    
    <style type="text/css">
      @media only print {
          body {
            width:100%;
            height:100%;
            overflow-x:visible;
            overflow-y:visible;
            -webkit-print-color-adjust: exact;
          }
          
      }

    </style>

    <button type="button" class="btn btn-primary" id="download_replies">Print Inquiry</button>
   <table id="replies_table" bgcolor="#f6f8f8" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; margin: 0 auto; border-spacing: 0; border-collapse: collapse; background-color: #f6f8f8; width: 100%;" valign="top" width="100%">
        <tbody>
            <tr style="vertical-align: top;" valign="top">
                <td style="word-break: break-word; vertical-align: top;" valign="top">
                 
                    <div class="body-cnt p-2">
                        <section class="mb-5">
                            <h3 class="text-center"><%= distributor_name %> - Quote form</h3>
                            <div class="text-center">
                                <table class="col text-left m-auto table_Stirp" style="max-width: 400px;" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <th>Company name</th>
                                      <td colspan="2"><%= company_name %></td>
                                    </tr>
                                    <tr>
                                      <th>Email</th>
                                      <td colspan="2"><a href=""><%= email %></a></td>
                                    </tr>
                                    <tr>
                                      <th>Telephone</th>
                                      <td colspan="2"><a href=""><%= mobile %></a></td>
                                    </tr>
                                    
                                    <tr>
                                      <th>Message</th>
                                      <td colspan="2"><%= message %></td>
                                    </tr>
                                    <tr>
                                      <th>Date Sent UTC</th>
                                      <td colspan="2"><%= created_at %></td>
                                    </tr>

                                </table>
                            </div>
                        </section>
                    <% for(q=0;q< quotation_detail.length;q++){ %>
                        <section class="mb-3 mt-5">
                          <div class="d-flex justify-content-between mb-3 align-items-center">
                            <h5 ><span ><%= quotation_detail[q].category_name %></span> / <span><%= quotation_detail[q].product_name %></span> / <span><%= quotation_detail[q].design_name %></h5>
                        
                            <% if(quotation_detail[q].doc_name!=null){ %>
                            <a href="{{ asset('public/images') }}/<%= quotation_detail[q].doc_name %>" download class="btn btn-primary">Download Svg</a>
                            <% } else { %>
                            <a href="javascript:;" id="svgDownloadInquiry" data-id="<%= quotation_detail[q].id %>" class="btn btn-primary">Download Svg </a>
                            
                            <div style="display: none;" id="svg_div_<%= quotation_detail[q].id %>">
                              <svg data-elems="Body"  xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="<%= quotation_detail[q].svg_dimension_ht %>" height="<%= quotation_detail[q].svg_dimension_vt %>" version="1.0" >
                                <% if(quotation_detail[q].updated_snippet!=null && quotation_detail[q].updated_snippet!=""){ %>
                                  <%= quotation_detail[q].updated_snippet %>
                                <% } else { %>
                                  <%= quotation_detail[q].svg_snippet %>
                                <% } %>
                              </svg>
                            </div>
                            <% } %>
                          </div>
                          
                          


                          <div class="mb-2">
                            <table class="col text-left m-auto" style="max-width: 600px;" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td class="text-center px-3" style="min-width: 300px">
                                    <% if(quotation_detail[q].image_front!=null && quotation_detail[q].image_front!=""){ %>
                                    <img src="{{ asset('public/images/front_back') }}/<%= quotation_detail[q].image_front %>" class="front_backLogo" />
                                    <% }else{ %>
                                    <div class="imageAdujstRectangle">
                                      
                                      <img src="{{ asset('public/images/api') }}/front.jpg" class="front_backLogo" />
                                    </div>
                                    <% } %>
                                  </td>
                                  <td class="text-center px-3" style="min-width: 300px">
                                    <% if(quotation_detail[q].image_back!=null && quotation_detail[q].image_back!=""){ %>
                                    <img src="{{ asset('public/images/front_back') }}/<%= quotation_detail[q].image_back %>" class="front_backLogo" />
                                    <% }else{ %>
                                    <div class="imageAdujstRectangle">
                                      
                                      <img src="{{ asset('public/images/api') }}/back.jpg" class="front_backLogo" />
                                    </div>
                                    <% } %>
                                  </td>
                                </tr>
                              </table>
                          </div>
                          <div class="mb-2">
                            <table class="col text-left m-auto" style="max-width: 400px;" cellpadding="0" cellspacing="0">
                            <% 
                            var designData= quotation_detail[q].designData;
                             if(designData.length>0){
                            for(d=0;d< designData.length;d++){
                             %>
                                <tr>
                                  <th><%= designData[d].designfield_name %></th>
                                  <td colspan="2"><%= designData[d].color_name %> (<%= designData[d].color %>) <span class="color_box" style="background:<%= designData[d].color %> "></span></td>
                                </tr>
                            <% } } %>
                              </table>
                          </div>
                          <div class="mb-2">
                            <table class="col text-left m-auto table_border" cellpadding="0" cellspacing="0">
                           
                                <tr>
                                    <th>Text</th>
                                    <th>Colour</th>
                                    <th>Font Size</th>
                                    <th>Position X</th>
                                    <th>Position Y</th>
                                </tr>
                                 <% 
                            var fieldData= quotation_detail[q].fieldData;
                            if(fieldData.length>0){
                            for(f=0;f< fieldData.length;f++){
                             %>
                             <% if(fieldData[f].text!=null && fieldData[f].filed_type=="text"){ %>
                                <tr>
                                    <td><%= fieldData[f].text %></td>
                                    <td><%= fieldData[f].color %></td>
                                    <td><%= fieldData[f].fontsize %></td>
                                    <td><%= fieldData[f].position_x %></td>
                                    <td><%= fieldData[f].position_y %></td>
                                </tr>
                               <% } } } %>
                              </table>
                          </div>
                        </section>
                    <% } %>
                    </div>

                  
                </td>
            </tr>
        </tbody>
    </table>
