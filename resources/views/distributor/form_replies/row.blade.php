<tr>
    <td><%= i %></td>
    <td>
        <% if(notification_status=="unread"){ %>
        <strong><%= company_name %></strong>
        <% } else { %>
        <%= company_name %>
        <% } %>
    </td>

    <td>
        <% if(notification_status=="unread"){ %>
        <strong><%= date %></strong>
        <% } else { %>
        <%= date %>
        <% } %>
    </td>

    <td>
        <% if(notification_status=="unread"){ %>
        <strong><%= email %></strong>
        <% } else { %>
        <%= email %>
        <% } %>
    </td>

    <td>
        <% if(notification_status=="unread"){ %>
        <strong><%= distributor_name %></strong>
        <% } else { %>
        <%= distributor_name %>
        <% } %>
    </td>

   
    <td class="text-center">
      <a  href="javascript:;" data-id="<%= id %>" id="viewformreplybutton" class="btn btn-sm btn-primary"> <i class="fas fa-eye"></i> </a>
        <a href="javascript:;" class="btn btn-sm btn-default" data-id="<%= id %>" id="deleteformrepliesbutton"> <i class="fas fa-trash-alt"></i> </a>
    </td>
</tr>
