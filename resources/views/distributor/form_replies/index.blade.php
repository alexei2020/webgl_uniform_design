@extends('layouts.app')
@section('content')
         
  <div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
      		<div class="search_area mb-3">
          		<div class="container">
            		<div class="row">
                		<div class="col-7 col-md-5">
                  		 <input aria-label="Search" id="searchreplyinput" placeholder="Search" class="form-control">  

                		</div>
                    <div class="col-7 text-right mb-4">
                
                      <a href="{{ url('distributor/export-formreply/') }}" ><i class="fas fa-file-export"></i> Export Excel</a>

                    </div>
                		
	            	</div>
    		      </div>
        	</div>

          <div class="table-responsive" id="result"> 
          </div>
          <div id="viewDetail"></div>
          
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Inquiries");
</script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="{{ asset('public/js/distributor/form_replies.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection