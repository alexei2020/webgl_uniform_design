<div class="table-responsive">
  <table class="table table-hover">
    <% if(rows){ %>
      <thead>
        <tr>          
          <th> Id </th>
          <th> Image </th>
          <th>Category Name </th>
          <th> URL Slug </th>
          <!-- <th> URL Override </th> -->
          <th class="text-center"> Position </th>          
          <th class="text-center"> Active </th>
          <th class="text-center" style="width:120px"> Action </th>          
        </tr>
      </thead>
    <% } %>
    <tbody id="searchResultDiv">
      <%=rows%>
      <% if(!rows){ %>
      <div class="no_audit p-5 text-center">
          <!-- <img src="{{ asset('public/images/norecord.png') }}"> -->
          <p>No categories found</p>
        </div>
     <!--  @include('common.norecord') -->
      <% } %>
    </tbody>
  </table>
</div>
<div id="pagination" class="main__content-data-footer d-flex justify-content-between align-items-center border-top pt-4"> </div>
