<form id="categoryform" class="edited" enctype="multipart/form-data">
       <input type="hidden" id="id" value="<%= id %>" class="form-control">
       <div class="row">
          <div class="col-md-6">
            <div class="form-group" id="categoryParentnotnull">
               <label>Category</label>
               {!!Form::select('category', $category, null, ['class' => 'form-control',"id"=>'categoryId'])!!}
            </div>
            <div class="form-group" id="categoryParentnull" style="display: none">
               <label>Category</label>
               {!!Form::select('category', ["null"=>"Select Category"], null, ['class' => 'form-control'])!!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <label>Name</label>
               <input type="text" id="name" placeholder="Enter Name" value="<%= name %>" class="form-control" name="">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
           	  <label>Url Slug</label>
           		
                <input type="text" id="url_slug" placeholder="" disabled value="<%= url_slug %>" class="form-control" >
                <small>Text in the URL e.g. #/category/product</small>
                   
            </div>
          </div>
          <div class="col-md-12">
            
               <% if(image!="" && image!=null) { %>
               <div class="form-group">
                <img  src="{{ url('public/images/') }}/<%= image %>" width="100">
               </div>
             <% } %>

                <div class="form-group">
                    <!-- <label>Image <span class="mandatory">*</span></label> -->
                    <div class="d-flex justify-content-between mb-3 align-items-center">
                      <label>Image<span class="mandatory">*</span></label>      
                      <input type="file" class="text-right uploadCategoryImage" name="name1" />

                      <div class="progress" id="" style="width: 20%; display: block;">
                        <div class="progress-bar progress-bar-success myprogress_uploadCategoryImage" role="progressbar" style="width:0%">0%</div>
                      </div>
                    </div>

                     <select id="image" name="image" class="w-100 chosen-select">
                    <option value="">Select image</option>
                    @foreach($document as $doc)
                     <option value="{{ $doc->id }}">{{ $doc->original_name }}</option>
                     @endforeach
                   </select>
                </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
               <label>Position</label>
               <input type="text" id="position" placeholder="Enter Position" value="<%= position %>" class="form-control" name="">
            </div>
          </div>
          <div class="col-md-12">
            <% var check_status=""; if(status=="active"){check_status="checked";} %>
              <div class="custom-control custom-checkbox mb-3 mr-3">
                <input type="checkbox" class="custom-control-input" <%= check_status %> id="status" name="status" >
                <label class="custom-control-label" for="status">Active</label>
              </div>
            </div>
        </div>
</form>




