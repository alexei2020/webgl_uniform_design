<tr id="row_<%= id %>"  <% if(parentId!=null){ %> class="parent_<%= parentId %>" <% } %>>
  <td> <%= i %> </td>
  <td> 
    <% if(image=="" || image==null) { %>
      <img src="{{ asset('public/images/no-image.png') }}" class="mr-2 img-thumbnail" alt="image">
       <% } else { %>
      <img class="mr-2 img-thumbnail" src="{{ url('public/images/') }}/<%= image %>" alt="image">
    <% } %>
  </td> 
  
  <td> <%= name %> </td>

  <td> <%= url_slug %> </td>
 <!--  <td>
    <a href="#">-</a>
  </td>-->
  <td class="text-center"> <%= position %> </td>

  <td class="text-center">
      <div class="custom-control custom-checkbox">
        <!-- not-allowed-status -->
        <input type="checkbox" data-id="<%= id %>" class="custom-control-input checkboxoption" <% if(status=="active"){ %> checked <% } %>    id="status_<%= id %>" name="status_act" value="<%= id %>">
        <label class="custom-control-label" for="status_<%= id %>"></label>
      </div>
  </td>

   <td class="text-center">   
    <a href="javascript:;" class="btn btn-sm btn-primary" data-id="<%= id %>" id="editcategorybutton"> <i class="fas fa-pencil-alt"></i></a>
    <a href="javascript:;" class="btn btn-sm btn-default" data-id="<%= id %>" id="deletecategorybutton"> <i class="fas fa-trash-alt"></i> </a>
  </td>

</tr>
 
