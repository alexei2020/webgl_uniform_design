@extends('layouts.app')
@section('content')
         
  <div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
          <div class="col-6">
            <form enctype="multipart/form-data" id="companyform" class="edited mt-4">
              <input type="hidden" name="id" id="id" value="{{ $user[0]->id }}">
               <div class="form-group  mb-2">
                   <label>Name <span class="mandatory"></span></label>
                   <input type="text" id="name" placeholder="Enter Name" value="{{ $user[0]->name }}" class="form-control" >
               </div>
               <div class="form-group">
                   <label>Email <span class="mandatory"></span></label>
                   <input type="email" id="email" autocomplete="false" placeholder="Enter Email" value="{{ $user[0]->email }}" class="form-control" >
               </div>

                
              <div class="form-group">
                    <button type='button' id='updateAccountbutton' class='btn btn-primary'>Update</button>
                  </div>  
            </form>
          </div>

      </div>
      <?php //include 'copyrighte.php';?>
    </div>
  </div>

@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Edit Profile");
</script>
<script src="{{ asset('public/js/distributor/account.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection