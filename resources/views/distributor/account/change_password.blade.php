@extends('layouts.app')
@section('content')
         
  <div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
          <div class="col-6">
            <form enctype="multipart/form-data" id="companyform" class="edited mt-4">
               <div class="form-group">
                  <label>Old Password <span class="mandatory">*</span></label>
                  <input type="password" autocomplete="current-password" id="current_password" placeholder="Enter current password" class="form-control">
                </div>

                <div class="form-group">
                  <label>New Password <span class="mandatory">*</span></label>
                  <input type="password" autocomplete="new-password" id="password" placeholder="********" class="form-control">
                   <a href="javascript:;" class="pass_visible"><i id="pass-status" class="fas fa-eye show_password" onclick=" viewPassword()"></i></a>
                </div>
                <div class="form-group ">
                  <label>Confirm Password <span class="mandatory">*</span></label>
                  <input type="password" id="confirm_password" placeholder="********" class="form-control">
                   <a href="javascript:;" class="pass_visible"><i id="conf-pass-status" class="fas fa-eye show_password" onclick=" viewConfirmPassword()"></i></a>
                </div>
              <div class="form-group">
                    <button type='button' id='changePasswordbutton' class='btn btn-primary'>Update</button>
                  </div>  
            </form>
          </div>

      </div>
      <?php //include 'copyrighte.php';?>
    </div>
  </div>

@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Change Password");
</script>
<script src="{{ asset('public/js/distributor/account.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection