
<form enctype="multipart/form-data" id="fontGroupForm" class="edited mt-4">
    <input type="hidden" name="" id="id" value="<%= id %>">
    <div class="row">    
        <div class="col-md-12">
          <div class="form-group">
            <label>Name <span class="mandatory">*</span></label>
            <input type="text" id="name" placeholder="Enter Name" value="<%= name %>" class="form-control" >
          </div>
        </div>
      <!--   <div class="col-md-12">
          <div class="form-group">
            <label>Color <span class="mandatory">*</span></label>
            <select id="colorId" class="chosen-select form-control" multiple="true">
              <%= color %>
            </select>
          </div>
        </div> -->

        <div class="col-md-12">
          <div class="form-group">
            <label>Color <span class="mandatory">*</span></label>
              <select id="colorId" class="form-control" multiple="multiple">
                 <%= color %>
              </select>
            </div>
        </div>

    </div>
</form>
