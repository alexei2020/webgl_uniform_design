@extends('layouts.app')
@section('content')
         
  <div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
      		<p>Paste this HTML code where you want Uniform Builder to appear.</p>
        <div class="form-group">
          
          
          <textarea class="form-control" rows="10" id="feed_code_textarea">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<div id="UniformBuilder"></div>
<script type="text/javascript" id="distributor-api" data-distributor="<?php echo Auth::user()->id ?>" src="{{ asset('/public/js/api/api.js') }}" ></script></textarea>
      </div>    
        <div style="text-align: left;">
              <button class="text-right btn btn-primary "  onclick="copyContent()">Copy</button>
              <span id="show_text" class="ml-2" style="font-weight: 600; display: none;">Copied!</span>
        </div >
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Feed Code");

  function copyContent() {
    let textarea = document.getElementById("feed_code_textarea");
    textarea.select();
    document.execCommand("copy");
    $('#show_text').html('The feed code has been copied to your clipboard.');
      $( "#show_text").fadeIn("slow");
      setTimeout(function(){
        $( "#show_text").fadeOut("slow");
      },2000);
  }
</script>
@endsection