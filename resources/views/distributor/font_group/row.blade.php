<tr>
    <td><%= i %></td>
    <td>
      <strong><%= name %></strong>
    </td>
    <td class="text-center">
      <a  href="javascript:;" data-id="<%= id %>" id="editfontgroupbutton" class="btn btn-sm btn-primary"> <i class="fas fa-pencil-alt"></i> </a>
      <a href="javascript:;" data-id="<%= id %>" id="deletefontgroupbutton" class="btn btn-sm btn-default"> <i class="fas fa-trash-alt"></i> </a>
    </td>
</tr>
