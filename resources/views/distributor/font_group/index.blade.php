@extends('layouts.app')
@section('content')
         
  <div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
      		<div class="search_area">
          		<div class="container">
            		<div class="row">
                		<div class="col-7 col-md-5">
                  		 <input aria-label="Search" id="searchfontgroupinput" placeholder="Search" class="form-control">  

                		</div>
                		<div class="col-7 text-right mb-4">
                  			<button class="btn btn-primary mb-2 mb-md-0 mr-2 " id="addfontgroupbutton"> New Font Group</button>
                		</div>
	            	</div>
    		    </div>
        	</div>

            <div class="table-responsive" id="result"> 
            </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Font Group");
</script>
<script src="{{ asset('public/js/distributor/font_group.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection