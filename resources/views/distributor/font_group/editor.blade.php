
<form enctype="multipart/form-data" id="fontGroupForm" class="edited">
    <input type="hidden" name="" id="id" value="<%= id %>">
    <div class="row">
           
            <div class="col-md-12">
              <div class="form-group">
                <label>Name <span class="mandatory">*</span></label>
                <input type="text" id="name" placeholder="Enter Name" value="<%= name %>" class="form-control" >
              </div>
            </div>
            
            <!-- <div class="col-md-12">
              <div class="form-group">
                <label>Font <span class="mandatory">*</span></label>
              
                {!!Form::select('font', $font, null, ['class' => 'form-control',"id"=>'fontId','multiple'=>true])!!}
              </div>
            </div> -->

            <div class="col-md-12">
              <div class="form-group">
                <label>Font <span class="mandatory">*</span></label>
                <select id="fontId" class="form-control" multiple="multiple">
                 <%= font %>
                </select>
              </div>
            </div>
          
          </div>
        </form>
