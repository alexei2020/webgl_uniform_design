<form enctype="multipart/form-data" id="designForm">
  <input type="hidden" name="" id="designid" value="<%= id %>" >
  <input type="hidden" name="" id="productId" value="<%= productId %>" >
    <div class="row">

      <div class="col-md-6">
        <div class="form-group">
          <label>Name <span class="mandatory">*</span></label>
          <input type="text" id="design_name" value="<%= name %>" placeholder="" class="form-control" >
        </div>
      </div>

      <div class="col-md-6" style="display: none;">
        <div class="form-group">
          <label>SKU <span class="mandatory">*</span></label>
          <input type="text" id="sku_name" value="<%= sku %>" placeholder="" class="form-control" >
        </div>
      </div>
      
      <div class="col-md-12">
         <div class="form-group">
            <label>URL Slug</label>
            <input type="text" id="design_slug" disabled placeholder="" value="<%= design_slug %>" class="form-control" >
            <small>#/products/name</small>
         </div>
      </div>

      <!-- <div class="col-md-12">
        <div class="form-group">
          <label>Override Image <span class="mandatory">*</span></label>
          <select data-placeholder="" id="override_image" class="chosen-select w-100" tabindex="3">
            <option >Select Id</option>
            <option data-tokens="ketchup mustard">Hot Dog, Fries and a Soda</option>
            <option data-tokens="mustard">Burger, Shake and a Smile</option>
            <option data-tokens="frosting">Sugar, Spice and all things nice</option>
          </select>
          <small>Override the generated image displayed on the design listing page.</small>
        </div>
      </div> -->

      <!-- <div class="col-md-12">
          <% if(profile!="" && profile!=null) { %>
            <div class="form-group" id="pID">
              <img  src="{{ url('public/images/') }}/<%= profile %>" width="100">
             
            </div>

            <% } %>
        </div>
          <div class="col-md-12">
            <div class="form-group">
              <input type="hidden" id="actionDeleteLogo" >
              <label>Design Thumbnail</label>
              <input type="file" id="profile"  name="profile"  class="form-control">
            </div>
      </div> -->

      <div class="col-md-12">
        <div class="form-group">
          
          <div class="d-flex justify-content-between mb-3 align-items-center">
            <label> Select SVG File <span class="mandatory">*</span></label>   
              
            <input type="file" class="text-right uploadSvgDesignImage "  name="svg_name" />
            <div class="progress"  style="width: 20%; display: block;">
              <div class="progress-bar progress-bar-success myprogress_uploadSvgDesignImage" role="progressbar" style="width:0%">0%</div>
            </div>
          </div>

          <select data-placeholder="" id="svgFileId" class="chosen-select w-100" tabindex="3">
            <%= svg_doc %>
          </select>
           <span>Files Supported: svg</span>
        </div>

      </div>
     <!--  <object data="" type="image/svg+xml" id="select_svg_object_forsnippet" width="20%" height="20%" style="display: none;" ></object>
      -->
     
      <div class="col-md-12">
        <div class="form-group">
          <label> SVG Snippet <span class="mandatory">*</span></label>
          <textarea class="form-control" rows="7" id="svg_snippet"> <%= svg_snippet %></textarea>
          <small>SVG snippet unique to the design. Usually the fields added below will modify this snippet (e.g. change it's colours). To use, load the SVG in the left pane, select the bit needed and click "Use Selected".</small>
        </div>
        <input type="hidden" name="svg_viewBox" id="svg_viewBox" value="<%= svg_viewBox %>" >
      </div>



       <div style="width: 20%; height: 20%" id="previewSvg" class="p-1 border" style="overflow: hidden;">
        <div id="snippet_show" style="display: none;">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="" x="0px" y="0px" width="100%" height="100px" viewBox="0 0 3685 3685" enable-background="new 0 0 3685 3685" xml:space="preserve"></svg>
        </div>
        <div id="imageshow" style="display: none;">
          <object data="" type="image/svg+xml" id="select_svg_object_forsnippet"  width="100%" height="100%" style="display: block;"  ></object>
     
        </div>
         
      </div>

      

      <!-- <div class="col-md-12">
        <div class="custom-control custom-checkbox mb-3 mr-3">
          <input type="checkbox" class="custom-control-input" id="show_svg_defs" name="show_svg_defs" >
          <label class="custom-control-label" for="show_svg_defs">Show SVG Defs</label>
        </div>
      </div> -->
      <div class="col-md-12">
        <div class="form-group">
          <label>Position <span class="mandatory">*</span></label>
          <input type="text" id="design_position" value="<%= position %>" placeholder="" class="form-control" onkeypress="return check(event,value)" onchange="return check(event,value)"  >
        </div>
      </div>
      <div class="col-md-12">
        <div class="custom-control custom-checkbox mb-3 mr-3">
          <input type="checkbox" class="custom-control-input" id="design_status" name="design_status" <% if(status=="active"){ %> checked <% } %>  >
          <label class="custom-control-label" for="design_status">Active</label>
        </div>
      </div>

      <div class="col-md-12">
        <div class="custom-control custom-checkbox mb-3 mr-3">
          <input type="checkbox" class="custom-control-input" id="player_status" name="player_status" <% if(player_status=="active"){ %> checked <% } %>  >
          <label class="custom-control-label" for="player_status">Player Status</label>
        </div>
      </div>

      <div class="col-md-12">
        <div class="form-group">
          <label>Design Field <span class="mandatory">*</span></label>
           <% 
            if(id!="" && designFiledIdCount.length>0)
            {
              
              for(i = 0;i < designFiled.length; i++) 
              {
               
          %> 
              <div class="designFiled_add_more d-flex" id="df_<%= designFiled[i].id %>"> 
                <div class="d-flex mr-2">
                    <div class="mr-1 mb-4" style="width: 25%">
                      <select class="form-control chosen-select productStepId" id="">
                      <%= step %>
                      </select>
                      <input type="hidden"  value="<%= designFiled[i].id %>" class="form-control ids" name="">
                    </div>

                    <div class="mr-1 mb-4"style="width: 25%">
                       <input type="text "  placeholder="" value="<%= designFiled[i].design_filed_name %>" class="form-control design_filed_name" >
                    </div>

                    <div class="mr-1 mb-4"style="width: 25%">
                      <input type="text"  placeholder="" value="<%= designFiled[i].design_key %>" class="form-control design_key" >
                    </div>

                    <div class="mr-1 mb-4"style="width: 25%">
                       <select class="form-control chosen-select data_group" >
                        <%= group %>
                       </select>
                    </div>
                </div>
                

                <div class="ml-1 mb-4" >
                  <a href="javascript:;" class="deletedesignfiledbutton pt-2 d-block text-muted"  data-id="<%= designFiled[i].id %>"><i class="fas fa-trash"></i></a>
                </div>
             
            </div> <!-- bu_add_more end -->
          <%  
 
              } 
            }
           
          %>
          <div class="" id="designFiledResult"> </div>
        </div>
      </div>

      

      <div class="col-md-12 text-right mt-3">
          <a href="javascript:;" id="adddesignfiledbutton" class="btn btn-primary"><i class="fas fa-plus"></i> Add Design Field</a>
        </div>

    </div>
</form>
