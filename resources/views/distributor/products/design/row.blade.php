<tr id='productDesign_list<%= id %>'>
  <td><%= i %></td>
  <td> 
      <%= name %> 
  </td>
  <td class="text-center">
    <%= position %>
  </td>
  <td class="text-center">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" data-id="<%= id %>" class="custom-control-input checkboxoption designcheckboxoption" <% if(status=="active"){ %> checked <% } %> id="designstatus_<%= id %>" name="status_act" value="<%= id %>">
        <label class="custom-control-label" for="designstatus_<%= id %>"></label>
      </div>
  </td>

  <td class="text-center">
    <!-- <a href="javascript:;" class="btn btn-sm btn-primary" data-id="<%= id %>"  id="adddesignfiledbutton"> Add Design Filed</a> -->
    <a href="javascript:;" class="btn btn-sm btn-primary" data-id="<%= id %>"  id="editdesignbutton"> <i class="fas fa-pencil-alt"></i></a>
    <a href="javascript:;" class="btn btn-sm btn-default" data-id="<%= id %>" id="deletedesignbutton"> <i class="fas fa-trash-alt"></i> </a>
  </td>

</tr>
 
