<div class="table-responsive">
  <table class="table table-hover">
    <% if(rows){ %>
      <thead>
        <tr>          
          <th> Id </th>
          <th> Name </th>
          <th class="text-center"> Position </th>
          <th class="text-center"> Active </th>
          <th class="text-center" style="width:120px"> Action </th>          
        </tr>
      </thead>
    <% } %>
    <tbody id="searchResultDiv">
      <%=rows%>
      <% if(!rows){ %>
       <!-- @include('common.norecord') -->
        <div class="no_audit p-5 text-center">
          <!-- <img src="{{ asset('public/images/norecord.png') }}"> -->
          <p>No design have been created for this product</p>
        </div>
      <% } %>
    </tbody>
  </table>
</div>
