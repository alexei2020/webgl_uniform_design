<tr id='productStep_list<%= id %>'>
  <td> <%= i %> </td>
  <td> <%= name %> </td>
  <td class="text-center"> <%= position %> </td>
  <td class="text-center">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" data-id="<%= id %>" class="custom-control-input checkboxoption stepcheckboxoption" <% if(status=="active"){ %> checked <% } %>   id="stepstatus_<%= id %>" name="status_act" value="<%= id %>">
        <label class="custom-control-label" for="stepstatus_<%= id %>"></label>
      </div>
  </td>
  <td class="text-center">
    <a href="javascript:;" class="btn btn-sm btn-primary" data-id="<%= id %>"  id="editstepbutton"> <i class="fas fa-pencil-alt"></i></a>
    <a href="javascript:;" class="btn btn-sm btn-default" data-id="<%= id %>" id="deletestepbutton"> <i class="fas fa-trash-alt"></i> </a>
  </td>

</tr>
 
