<div class="table-responsive">
  <table class="table table-hover">
    <% if(rows){ %>
      <thead>
        <tr>
          
          <th> Id </th>
          <th> Name </th>
          <th> Step </th>
          <th> Type </th>
          <th class="text-center" style="width:120px"> Action </th>
          
        </tr>
      </thead>
    <% } %>
    <tbody id="searchResultDiv">
      <%=rows%>
      <% if(!rows){ %>
      <!-- @include('common.norecord') -->
        <div class="no_audit p-5 text-center">
          <!-- <img src="{{ asset('public/images/norecord.png') }}"> -->
          <p>No fields have been created for this product</p>
        </div>
      <% } %>
    </tbody>
  </table>
</div>
