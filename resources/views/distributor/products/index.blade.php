@extends('layouts.app')
@section('content')


<div class="page-body-wrapper animate">
    <div class="main-panel">
      <div class="content-wrapper pb-0">
          <div class="search_area">
              <div class="row">
                  <div class="col-7 col-md-5">
                     <input aria-label="Search" id="searchproductinput" placeholder="Search" class="form-control">  
                  </div>
                  <div class="col-5 col-md-7 text-right mb-4">
                      <button class="btn btn-primary mb-2 mb-md-0 mr-2 " id="addproductbutton"><i class="fas fa-plus mr-2"></i> New Product</button>
                  </div>
              </div>

              <div class="row">
                  <div class="col-md-2 col-sm-3 mb-3">
                    <select class="chosen-select" id="productfilterlimit">
                      <option value="">Select Range</option>
                      <option value="10">1-10</option>
                      <option value="50">1-50</option>
                      <option value="100">1-100</option>
                      <option value="500">1-500</option>
                    </select>
                  </div>
              </div>
          </div>
          <div id="edit_product"></div>
          <div id="result"> 
          </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $("#heading").html("Product");
</script>

<script src="{{ asset('public/js/distributor/product.js') }}" type="text/javascript" charset="utf-8"></script>
@endsection