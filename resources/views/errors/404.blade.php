<html lang="en">
<head>
    <meta charset="utf-8"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>360 Uniform</title>    
    <link rel="stylesheet" href="{{ URL::asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/css/style.css') }}">
   
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>    
    <script src="https://use.fontawesome.com/a30863ed31.js"></script>

    
</head>

<body>
<div class="error-page text-center" style="margin-top:5%">
       <!-- <img src="{{url('')}}/public/images/logo.png" width="150"> -->
        <h1 style="font-size:80px;" class="headline text-info">404</h1>

        <div class="error-content">

            <h3> Oops! Page not found.</h3>

            <p>

                We could not find the page you were looking for.
             
            </p>
 		<a href="<?php echo url(''); ?>">Back</a>
        </div>

    </div>
</body>
</html>