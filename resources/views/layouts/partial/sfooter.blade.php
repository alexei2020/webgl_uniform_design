<div id="modalContainer"></div>

<div class="error_section"></div>

    <script src="{{ asset('public/js/jquery.min.js') }}"></script>
    <script src="{{ asset('public/js/popper.min.js') }}"></script>
    <script src="{{ asset('public/js/underscore-min.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/js/custom.js') }}"></script>
    <script src="{{ asset('public/js/swiper.min.js') }}"></script>
    <script src="{{ asset('public/js/superadmin/lib.js') }}" type="text/javascript" charset="utf-8"></script>

    <script src="{{ asset('public/js/underscore-min.js') }}"></script>

    <script src="{{ asset('public/js/prism.js') }}" type="text/javascript" charset="utf-8"></script>

    <script src="{{ asset('public/js/chosen.jquery.js') }}" type="text/javascript"></script>
    <script src="/360Uniform/public/lib/js/init.js"></script>
    <script type="text/javascript" src="{{ asset('public/js/jquery-confirm.min.js') }}"></script>
    <script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>

        <script src="{{ asset('public/js/jquery.minicolors.js') }}" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click','#sidebarCollapse', function () {
            $('#sidebar').toggleClass('active');
            $('#content').toggleClass('offset');
            $('.navbar-theme').toggleClass('offset-nav');
        });
        $(document).on('click','.submenu_a', function () {
        	var ids=$(this).attr('aria-controls');
            //$('.submenu ul.list-unstyled').hide();
            $(this).toggleClass('open minus');
            $('#' + ids).slideToggle();
            $('.wrapper__sidebar.active ul.components').toggleClass('w_195');
        });

            var swiper = new Swiper('.swiper-container', {
              pagination: {
                el: '.swiper-pagination',
                clickable: true,
                renderBullet: function (index, className) {
                  return '<span class="' + className + '">' + (index + 1) + '</span>';
                },
              },
            });
    });
</script>
