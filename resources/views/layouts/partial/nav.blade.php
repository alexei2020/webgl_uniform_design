
<nav class="sidebar animate" id="sidebar" style="overflow-y: auto; overflow-x: hidden;">
    <div class="px-3 pt-3 logo-area">
      <a class="d-block mb-4 navbar-brand text-center" href="{{ url('dashboard') }}">
        
        <img class="sidebar-brand-logo w-75 m-auto" src="{{ asset('public/images/Uniform-Builder-Logo.png') }}" alt="">
      </a>
     
    </div>
    <!-- <hr /> -->
    <ul class="nav">
   
      <li class="pt-2 pb-1">
        <span class="nav-item-head txt_black">Product and Category</span>
      </li>
      <li class="nav-item @if(request()->route()->getName()=='product') active @endif">
        <a class="nav-link" href="{{ url('distributor/product') }}">
          <i class="fas fa-tachometer-alt menu-icon"></i>
          <span class="menu-title">Product</span>
        </a>
      </li>
        
            <li class="nav-item @if(request()->route()->getName()=='category') active @endif"> 
              <a class="nav-link" href="{{ url('distributor/category') }}"><i class="fas fa-user-friends menu-icon"></i> <span class="menu-title">Category</span> </a>
            </li>

      
      <li class="pt-2 pb-1 mt-3">
        <span class="nav-item-head txt_black">Builder Data</span>
      </li>    
      <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#custome-form" aria-expanded="false" aria-controls="custome-form">
          <i class="fas fa-folder-open menu-icon"></i>
          <span class="menu-title">Forms</span>
          <i class="fas fa-angle-right menu-arrow"></i>
        </a>
        <div class="collapse" id="custome-form">
          <ul class="nav flex-column sub-menu">
        
            <li class="nav-item @if(request()->route()->getName()=='form') active @endif"> 
              <a class="nav-link" href="{{ url('distributor/form') }}"><i class="fas fa-user-friends menu-icon"></i> <span class="menu-title">All Forms</span> </a>
            </li>

            <li class="nav-item @if(request()->route()->getName()=='formreplies') active @endif"> 
              <a class="nav-link" href="{{ url('distributor/formreplies') }}"><i class="fas fa-user-friends menu-icon"></i> <span class="menu-title">Inquiries</span> </a>
            </li>
            <li class="nav-item @if(request()->route()->getName()=='emailfriend') active @endif"> 
              <a class="nav-link" href="{{ url('distributor/emailfriend') }}"><i class="fas fa-user-friends menu-icon"></i> <span class="menu-title">Email Friend Settings</span> </a>
            </li>
                    
          </ul>
        </div>
      </li>
  
    
        
            <li class="nav-item @if(request()->route()->getName()=='document') active @endif"> 
              <a class="nav-link" href="{{ url('distributor/document') }}"><i class="fas fa-file menu-icon"></i> <span class="menu-title">Uploaded Files</span> </a>
            </li>
            
                    
    
      <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#colours" aria-expanded="false" aria-controls="colours">
          <i class="fas fa-folder-open menu-icon"></i>
          <span class="menu-title">Colours</span>
          <i class="fas fa-angle-right menu-arrow"></i>
        </a>
        <div class="collapse" id="colours">
          <ul class="nav flex-column sub-menu">
        <!--  fas fa-square menu-icon -->
            <li class="nav-item @if(request()->route()->getName()=='color') active @endif"> 
              <a class="nav-link" href="{{ url('distributor/color') }}"><i class="fas fa-user-friends menu-icon"></i> <span class="menu-title">All Colours</span> </a>
            </li>
            <li class="nav-item @if(request()->route()->getName()=='colorgroup') active @endif"> 
              <a class="nav-link" href="{{ url('distributor/colorgroup') }}"><i class="fas fa-user-friends menu-icon"></i> <span class="menu-title">Colour Groups</span> </a>
            </li>         
          </ul>
        </div>
      </li>

      <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#fonts" aria-expanded="false" aria-controls="fonts">
          <i class="fas fa-folder-open menu-icon"></i>
          <span class="menu-title">Fonts</span>
          <i class="fas fa-angle-right menu-arrow"></i>
        </a>
        <div class="collapse" id="fonts">
          <ul class="nav flex-column sub-menu">
        
            <li class="nav-item @if(request()->route()->getName()=='font') active @endif"> 
              <a class="nav-link" href="{{ url('distributor/font') }}"><i class="fab fa-fonticons-fi menu-icon"></i> <span class="menu-title">All Fonts</span> </a>
            </li>
            <li class="nav-item @if(request()->route()->getName()=='fontgroup') active @endif"> 
              <a class="nav-link" href="{{ url('distributor/fontgroup') }}"><i class="fab fa-fonticons-fi menu-icon"></i> <span class="menu-title">Font Groups</span> </a>
            </li>
          </ul>
        </div>
      </li>



      <li class="pt-2 pb-1 mt-3">
        <span class="nav-item-head txt_black">Company Profile</span>
      </li>       
      <li class="nav-item @if(request()->route()->getName()=='companydetail') active @endif" id="companyDetail_active"> 
        <a class="nav-link" href="{{ url('distributor/companydetail') }}"><i class="fas fa-university menu-icon"></i> <span class="menu-title">Company Details</span> </a>
      </li>      

      <li class="nav-item @if(request()->route()->getName()=='feedcode') active @endif"  id="feedCode_active"> 
        <a class="nav-link" href="{{ url('distributor/feedcode') }}"><i class="fas fa-code menu-icon"></i> <span class="menu-title">Feed Code</span> </a>
      </li> 

      <li class="nav-item  @if(request()->route()->getName()=='preview') active @endif"> 
              <a class="nav-link" href="{{ url('distributor/preview/') }}/{{ Auth::user()->id }}" target="_blank"><i class="fas fa-user-friends menu-icon"></i> <span class="menu-title">Test Site</span> </a>
      </li>

    </ul>
</nav>


