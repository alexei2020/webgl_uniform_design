


<nav class="navbar animate bg_black p-0 fixed-top d-flex flex-row ">
	<div class="navbar-menu-wrapper animate d-flex align-items-stretch">
		<button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
		  <i class="fas fa-angle-double-left"></i>
		</button>
		
		<h2 class="my-3" id="heading"></h2>

		<ul class="navbar-nav navbar-nav-right mt-3">
		
			<li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		         <!-- <span class="avatar">Hi,</span> --> 
		         <span class="nm">{{ Auth::User()->name }}</span>
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		        	<div class="p-2 d-flex align-items-center">
						<!-- <span class="avatar mr-2">{{ substr(ucfirst(Auth::User()->name),0,2) }}</span>  -->
						<span><strong>{{ Auth::User()->name }}</strong> <br><small>{{ ucfirst(Auth::User()->type) }}</small>
					</div>

					<!-- <a class="dropdown-item" href="{{ url('superadmin/account/') }}">Edit Profile</a> -->
					<a class="dropdown-item" href="{{ url('distributor/account/change-password') }}">Change Password</a>
					<!-- <div class="dropdown-divider"></div> -->

						<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> {{ __('Logout') }} </a>    
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> @csrf </form>      
		        </div>
		    </li>
			
        </ul>
	</div>
</nav>