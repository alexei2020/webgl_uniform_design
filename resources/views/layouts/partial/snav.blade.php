<nav class="sidebar animate" id="sidebar">
    <div class="px-3 pt-3 logo-area">
      <a class="d-block mb-4 navbar-brand text-center" href="{{ url('superadmin/dashboard') }}">
        
        <img class="sidebar-brand-logo w-75 m-auto" src="{{ asset('public/images/Uniform-Builder-Logo.png') }}" alt="">
      </a>
     
    </div>
    <!-- <hr /> -->
    <ul class="nav">
      <li class="pt-2 pb-1">
        <span class="nav-item-head txt_black">Distributors and Products</span>
      </li>
     

      <li class="nav-item @if(request()->route()->getName()=='distributor') active @endif"> 
           <a class="nav-link" href="{{ url('superadmin/distributor') }}"><i class="fas fa-user-friends menu-icon"></i> <span class="menu-title">All Distributors</span> </a>
        </a>
       
      </li>

     <li class="nav-item @if(request()->route()->getName()=='product') active @endif"> 
           <a class="nav-link" href="{{ url('superadmin/product') }}"><i class="fas fa-user-friends menu-icon"></i> <span class="menu-title">All Products</span> </a>
        </a>
       
      </li>

    </ul>
</nav>