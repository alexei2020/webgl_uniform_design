<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

  
      <link rel="icon" href="{{ asset('public/images/favicon.ico') }} " />

    <link rel='stylesheet' href="{{ asset('public/css/bootstrap.min.css') }}"  media='all'/> 

    <link rel='stylesheet' href="{{ asset('public/css/swiper.min.css') }}"  media='all' />
    <link rel='stylesheet' href="{{ asset('public/css/fonts/fonts.css') }}"  media='all' />
    <link rel='stylesheet' href="{{ asset('public/css/chosen.min.css') }}"  media='all' />
    <link rel='stylesheet' href="{{ asset('public/css/style.css') }}"  media='all' />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/jquery-confirm.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/jquery.minicolors.css') }}">
</head>
<body>
   @if(!Auth::guest())
   @include('layouts.partial.stop')
   @endif
   @if(Auth::guest())
     @yield('content')
   @else
   <div class="container-scroller">
    @include('layouts.partial.snav')
    @yield('content')
   </div>
   @endif
   @include('layouts.partial.sfooter')
   @yield('script')

</body>
</html>
