<?php
//"http://infooss.com:28934";
//'portUrl'=>'http://192.168.1.35:3000',
//http://18.216.4.219
return [
    "limit"=>20,
    'usertype'=>['1'=>"superadmin",'2'=>"admin",'3'=>"super user",'4'=>"manager",'5'=>"auditor","6"=>"user"],
    'admin_email'=>'mahesh.imenso@gmail.com',
    'added'=>'Added successfully',
    'updated'=>'Updated successfully',
    'notadded'=>'Cannot be added',
    'notupdated'=>'Cannot be updated',
    'deleted'=>'Deleted successfully',
    'notdeleted'=>'There are some data associated with this record. Please remove all the child records before deleting this.',
    'wrong-password'=>'Wrong username and password',
    'not-aproved'=>'This user is not approved',
    'not-user'=>"We can't find a user with this e-mail address",
    'user-disable'=>'User is deactivated. Please contact administrator',
]


?>