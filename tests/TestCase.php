<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    // GET Industry ID
    public function industryId()
    {
        $industryId = \App\Industry::first();
        return $industryId->id;
    }

    // GET Businessunit ID
    public function buId()
    {
        // $buId = \App\Businessunit::first();  
        $buId= \App\CompaniesBusinessunit::join("businessunit",'businessunit.id',"companies_businessunit.businessunitId")->where(array("companies_businessunit.companyId"=>$this->companyId()))->first();
            
        return $buId->id;
    }

    // GET Company ID
    public function companyId()
    {
    	$companyId = \App\Companies::first();        
        return $companyId->id;
    }

    // // GET Unit ID
    public function unitId()
    {
    	$unitId = \App\Units::where(array('companyId'=>$this->companyId()))->first();
    	return $unitId->id;
    }

    // GET Regions ID
    public function regionId()
    {
        $regionId = \App\Regions::where(array('companyId'=>$this->companyId(),'businessunitId'=>$this->buId()))->first();
        return $regionId->id;
    }

    // GET Departments ID
    public function departmentId()
    {
        $departmentId = \App\Departments::where(array('companyId'=>$this->companyId(),'businessunitId'=>$this->buId()))->first();
        return $departmentId->id;
    }

    // GET Questions ID
    public function questionId()
    {
        $questionId = \App\Questions::where(array('companyId'=>$this->companyId()))->first();
        return $questionId->id;
    }

    // GET Audits ID
    public function auditId()
    {
        $auditId = \App\Audits::where(array('status'=>'publish','companyId'=>$this->companyId(),'unitId'=>$this->unitId()))->first();
        return $auditId;
    }
}
