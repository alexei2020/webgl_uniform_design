<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// header('Access-Control-Allow-Origin: *');
// header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );

Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/superadmin', 'Auth\LoginController@superadmin');

Route::resource('/superadmin/roles', 'Superadmin\RoleController');
Route::get('/common/select', 'CommonController@select')->name('select');

Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');

///////////////////// Dashboard Start /////////////////////////////////////////
Route::get('/distributor/dashboard', 'Distributor\DashboardController@index')->name('dashboard');


   ////////// Category ///////
Route::post('/distributor/category/getList', 'Distributor\CategoryController@getList')->name('category');
Route::get('/distributor/category/table', 'Distributor\CategoryController@table')->name('category');
Route::get('/distributor/category/row', 'Distributor\CategoryController@row')->name('category');
Route::post('/distributor/category/getSubCategory', 'Distributor\CategoryController@getSubCategory')->name('category');
Route::post('/distributor/category/update', 'Distributor\CategoryController@update')->name('category');
Route::post('/distributor/category/statusChange', 'Distributor\CategoryController@statusChange')->name('category');
Route::post('/distributor/category/saveuploaddoc', 'Distributor\CategoryController@saveUploadDoc')->name('category');
Route::resource('/distributor/category', 'Distributor\CategoryController', ['names' => ['index' => 'category']]);


 ///////////////////// account  /////////////////////////////////////////
Route::get('/account', 'AccountController@index')->name('account');
Route::get('/account/edit', 'AccountController@edit')->name('account');
Route::post('/account/update', 'AccountController@update')->name('account');


////// Products //// 

Route::post('/distributor/product/getList', 'Distributor\ProductController@getList')->name('product');

Route::get('/distributor/product/select', 'Distributor\ProductController@select')->name('product');
Route::get('/distributor/product/table', 'Distributor\ProductController@table')->name('product');
Route::get('/distributor/product/row', 'Distributor\ProductController@row')->name('product');

Route::post('/distributor/product/getDetailList', 'Distributor\ProductController@getDetailList')->name('product');
Route::post('/distributor/product/productStatusChange', 'Distributor\ProductController@productStatusChange')->name('product');

Route::get('/distributor/product/svgselector', 'Distributor\ProductController@svgSelectorCreate')->name('product');

Route::get('/distributor/product/uploaddoceditor', 'Distributor\ProductController@uploadDocEditor')->name('product');
Route::post('/distributor/product/saveuploaddoc', 'Distributor\ProductController@saveUploadDoc')->name('product');
Route::post('/distributor/product/saveUpload3DImage', 'Distributor\ProductController@saveUpload3DImage')->name('product');
Route::post('/distributor/product/saveUploadSvgImage', 'Distributor\ProductController@saveUploadSvgImage')->name('product');

Route::get('/distributor/product/steptable', 'Distributor\ProductController@stepTable')->name('product');
Route::get('/distributor/product/steprow', 'Distributor\ProductController@stepRow')->name('product');
Route::get('/distributor/product/stepcreate', 'Distributor\ProductController@stepCreate')->name('product');

Route::get('/distributor/product/fieldtable', 'Distributor\ProductController@fieldTable')->name('product');
Route::get('/distributor/product/fieldrow', 'Distributor\ProductController@fieldRow')->name('product');
Route::get('/distributor/product/fieldcreate', 'Distributor\ProductController@fieldCreate')->name('product');

Route::get('/distributor/product/designtable', 'Distributor\ProductController@designTable')->name('product');
Route::get('/distributor/product/designrow', 'Distributor\ProductController@designRow')->name('product');
Route::get('/distributor/product/designcreate', 'Distributor\ProductController@designCreate')->name('product');

Route::get('/distributor/product/designfiledtable', 'Distributor\ProductController@designFiledTable')->name('product');
Route::get('/distributor/product/designfiledrow', 'Distributor\ProductController@designFiledRow')->name('product');
Route::get('/distributor/product/designfiledcreate', 'Distributor\ProductController@designFiledCreate')->name('product');

Route::post('/distributor/product/update', 'Distributor\ProductController@update')->name('product');

Route::post('/distributor/product/getsvgdimension', 'Distributor\ProductController@getSVGDimension')->name('product');

Route::post('/distributor/product/editProduct', 'Distributor\ProductController@editProduct')->name('product');


Route::post('/distributor/product/getProductStep', 'Distributor\ProductController@getProductStep')->name('product');
Route::post('/distributor/product/stepStatusChange', 'Distributor\ProductController@stepStatusChange')->name('product');

Route::post('/distributor/product/getProductField', 'Distributor\ProductController@getProductField')->name('product');

Route::post('/distributor/product/getProductDesign', 'Distributor\ProductController@getProductDesign')->name('product');
Route::post('/distributor/product/designStatusChange', 'Distributor\ProductController@designStatusChange')->name('product');

Route::resource('/distributor/product', 'Distributor\ProductController', ['names' => ['index' => 'product']]);

Route::post('/distributor/product/stepstore', 'Distributor\ProductController@stepStore')->name('product');
Route::post('/distributor/product/stepupdate', 'Distributor\ProductController@stepUpdate')->name('product');
Route::post('/distributor/product/stepdelete', 'Distributor\ProductController@stepDelete')->name('product');

Route::post('/distributor/product/fieldstore', 'Distributor\ProductController@fieldStore')->name('product');
Route::post('/distributor/product/fieldupdate', 'Distributor\ProductController@fieldUpdate')->name('product');
Route::post('/distributor/product/fielddelete', 'Distributor\ProductController@fieldDelete')->name('product');

Route::post('/distributor/product/designstore', 'Distributor\ProductController@designStore')->name('product');
Route::post('/distributor/product/designupdate', 'Distributor\ProductController@designUpdate')->name('product');
Route::post('/distributor/product/designdelete', 'Distributor\ProductController@designDelete')->name('product');

Route::post('/distributor/product/saveUploadSvgInDesign', 'Distributor\ProductController@saveUploadSvgInDesign')->name('product');

// Route::post('/distributor/product/designfiledstore', 'Distributor\ProductController@designFiledStore')->name('product');
Route::post('/distributor/product/designfileddelete', 'Distributor\ProductController@designFiledDelete')->name('product');


Route::post('/distributor/product/filterCategory', 'Distributor\ProductController@filterCategory')->name('product');

////// form //// 

Route::post('/distributor/form/getList', 'Distributor\FormController@getList')->name('form');

Route::get('/distributor/form/select', 'Distributor\FormController@select')->name('form');
Route::get('/distributor/form/table', 'Distributor\FormController@table')->name('form');
Route::get('/distributor/form/row', 'Distributor\FormController@row')->name('form');
Route::post('/distributor/form/update', 'Distributor\FormController@update')->name('form');

Route::post('/distributor/form/autofill', 'Distributor\FormController@autofill')->name('form');

Route::post('/distributor/form/formStatusChange', 'Distributor\FormController@formStatusChange')->name('form');
Route::resource('/distributor/form', 'Distributor\FormController', ['names' => ['index' => 'form']]);


Route::post('/distributor/emailfriend/getList', 'Distributor\EmailFriendController@getList')->name('emailfriend');
Route::post('/distributor/emailfriend/update', 'Distributor\EmailFriendController@update')->name('emailfriend');
Route::resource('/distributor/emailfriend', 'Distributor\EmailFriendController', ['names' => ['index' => 'emailfriend']]);


////// form Replies //// 
Route::post('/distributor/formreplies/getList', 'Distributor\FormRepliesController@getList')->name('formreplies');
Route::get('/distributor/formreplies/select', 'Distributor\FormRepliesController@select')->name('formreplies');
Route::get('/distributor/formreplies/table', 'Distributor\FormRepliesController@table')->name('formreplies');
Route::get('/distributor/formreplies/row', 'Distributor\FormRepliesController@row')->name('formreplies');
Route::get('/distributor/formreplies/viewdetail', 'Distributor\FormRepliesController@viewDetailEdit')->name('formreplies');

Route::get('/distributor/export-formreply/', 'Distributor\FormRepliesController@exportFormReply')->name('formreplies');

Route::post('/distributor/formreplies/getViewDetail/', 'Distributor\FormRepliesController@getViewDetail')->name('formreplies');


Route::resource('/distributor/formreplies', 'Distributor\FormRepliesController', ['names' => ['index' => 'formreplies']]);

/////// Company Detail ///

Route::post('/distributor/companydetail/getList', 'Distributor\CompanyDetailController@getList')->name('companydetail');
Route::get('/distributor/companydetail/createsocialmedia', 'Distributor\CompanyDetailController@createSocialMedia')->name('companydetail');
Route::post('/distributor/companydetail/update', 'Distributor\CompanyDetailController@update')->name('companydetail');
Route::post('/distributor/companydetail/socialStore', 'Distributor\CompanyDetailController@socialStore')->name('companydetail');
Route::post('/distributor/companydetail/socialUpdate', 'Distributor\CompanyDetailController@socialUpdate')->name('companydetail');
Route::resource('/distributor/companydetail', 'Distributor\CompanyDetailController', ['names' => ['index' => 'companydetail']]);

// document 

Route::post('/distributor/document/getList', 'Distributor\DocumentController@getList')->name('document');
Route::get('/distributor/document/table', 'Distributor\DocumentController@table')->name('document');
Route::get('/distributor/document/row', 'Distributor\DocumentController@row')->name('document');
Route::post('/distributor/document/update', 'Distributor\DocumentController@update')->name('document');
Route::resource('/distributor/document', 'Distributor\DocumentController', ['names' => ['index' => 'document']]);

// color 

Route::post('/distributor/color/getList', 'Distributor\ColorController@getList')->name('color');
Route::get('/distributor/color/table', 'Distributor\ColorController@table')->name('color');
Route::get('/distributor/color/row', 'Distributor\ColorController@row')->name('color');

Route::get('/distributor/color/xlstable', 'Distributor\ColorController@xlsTable')->name('color');
Route::get('/distributor/color/xlsrow', 'Distributor\ColorController@xlsRow')->name('color');

Route::get('/distributor/color/import', 'Distributor\ColorController@import')->name('color');
Route::post('/distributor/color/importExcel', 'Distributor\ColorController@importExcel')->name('color');
Route::get('/distributor/color/download', 'Distributor\ColorController@download')->name('color');

Route::post('/distributor/color/update', 'Distributor\ColorController@update')->name('color');
Route::post('/distributor/color/colorStatusChange', 'Distributor\ColorController@colorStatusChange')->name('color');

Route::get('/distributor/download-color-excel/', 'Distributor\ColorController@downloadExcelFile')->name('color');

Route::resource('/distributor/color', 'Distributor\ColorController', ['names' => ['index' => 'color']]);

// font

Route::post('/distributor/font/getList', 'Distributor\FontController@getList')->name('font');
Route::post('/distributor/font/fontStatusChange', 'Distributor\FontController@fontStatusChange')->name('font');
Route::get('/distributor/font/table', 'Distributor\FontController@table')->name('font');
Route::get('/distributor/font/row', 'Distributor\FontController@row')->name('font');

Route::get('/distributor/font/xlstable', 'Distributor\FontController@xlsTable')->name('font');
Route::get('/distributor/font/xlsrow', 'Distributor\FontController@xlsRow')->name('font');

Route::get('/distributor/font/import', 'Distributor\FontController@import')->name('font');
Route::post('/distributor/font/importExcel', 'Distributor\FontController@importExcel')->name('font');
Route::get('/distributor/font/download', 'Distributor\FontController@download')->name('font');

Route::get('/distributor/download-font-excel/', 'Distributor\FontController@downloadExcelFile')->name('font');
Route::post('/distributor/font/update', 'Distributor\FontController@update')->name('font');
Route::resource('/distributor/font', 'Distributor\FontController', ['names' => ['index' => 'font']]);


// Font Group

Route::post('/distributor/fontgroup/getList', 'Distributor\FontGroupController@getList')->name('fontgroup');
Route::get('/distributor/fontgroup/table', 'Distributor\FontGroupController@table')->name('fontgroup');
Route::get('/distributor/fontgroup/row', 'Distributor\FontGroupController@row')->name('fontgroup');
Route::post('/distributor/fontgroup/update', 'Distributor\FontGroupController@update')->name('fontgroup');
Route::resource('/distributor/fontgroup', 'Distributor\FontGroupController', ['names' => ['index' => 'fontgroup']]);

// Color Group

Route::post('/distributor/colorgroup/getList', 'Distributor\ColorGroupController@getList')->name('colorgroup');
Route::get('/distributor/colorgroup/table', 'Distributor\ColorGroupController@table')->name('colorgroup');
Route::get('/distributor/colorgroup/row', 'Distributor\ColorGroupController@row')->name('colorgroup');
Route::post('/distributor/colorgroup/update', 'Distributor\ColorGroupController@update')->name('colorgroup');
Route::resource('/distributor/colorgroup', 'Distributor\ColorGroupController', ['names' => ['index' => 'colorgroup']]);


Route::resource('/distributor/feedcode', 'Distributor\FeedCodeController', ['names' => ['index' => 'feedcode']]);


////////// Account Setting ///////

Route::get('/distributor/account', 'Distributor\AccountController@index')->name('account');
Route::post('/distributor/account/update', 'Distributor\AccountController@update')->name('account');

Route::get('/distributor/account/change-password', 'Distributor\AccountController@changePassword')->name('account');
Route::post('/distributor/account/updatePassword', 'Distributor\AccountController@updatePassword')->name('account');


Route::get('/distributor/preview/{id}','Distributor\PreviewController@index')->name('preview');

 ///////////////////// admin  /////////////////////////////////////////
Route::group(['middleware' => 'is_admin'], function () {
$s="superadmin"; $sb="Superadmin";
 ////////// dashboard ///////
Route::get('/superadmin/dashboard', 'Superadmin\DashboardController@index')->name('dashboard');

 ////////// distributor ///////
Route::post('/superadmin/distributor/getList', 'Superadmin\DistributorController@getList')->name('distributor');
Route::get('/superadmin/distributor/select', 'Superadmin\DistributorController@select')->name('distributor');
Route::get('/superadmin/distributor/table', 'Superadmin\DistributorController@table')->name('distributor');
Route::get('/superadmin/distributor/row', 'Superadmin\DistributorController@row')->name('distributor');
Route::post('/superadmin/distributor/update', 'Superadmin\DistributorController@update')->name('distributor');

Route::post('/superadmin/distributor/changestatus', 'Superadmin\DistributorController@changeStatus')->name('distributor');
Route::post('/superadmin/distributor/deletelogo', 'Superadmin\DistributorController@deleteLogo')->name('distributor');


Route::resource('/superadmin/distributor', 'Superadmin\DistributorController', ['names' => ['index' => 'distributor']]);



////////// Site Manage ///////
// Route::resource('/superadmin/site-manage/content-page', 'Superadmin\SiteManage\ContentPageController', ['names' => ['index' => 'content-page']]);

Route::get('/superadmin/site-manage/content-page', 'Superadmin\SiteManage\ContentPageController@index')->name('content-page');
Route::post('/superadmin/site-manage/content-page/save', 'Superadmin\SiteManage\ContentPageController@store')->name('content-page');
Route::post('/superadmin/site-manage/content-page/update', 'Superadmin\SiteManage\ContentPageController@update')->name('content-page');

Route::get('/superadmin/site-manage/term-condition', 'Superadmin\SiteManage\TermConditionController@index')->name('term-condition');
Route::post('/superadmin/site-manage/term-condition/save', 'Superadmin\SiteManage\TermConditionController@store')->name('term-condition');
Route::post('/superadmin/site-manage/term-condition/update', 'Superadmin\SiteManage\TermConditionController@update')->name('term-condition');


Route::get('/superadmin/site-manage/privacy-policy', 'Superadmin\SiteManage\PrivacyPolicyController@index')->name('privacy-policy');
Route::post('/superadmin/site-manage/privacy-policy/save', 'Superadmin\SiteManage\PrivacyPolicyController@store')->name('privacy-policy');
Route::post('/superadmin/site-manage/privacy-policy/update', 'Superadmin\SiteManage\PrivacyPolicyController@update')->name('privacy-policy');
////////// Account Setting ///////

Route::get('/superadmin/account', 'Superadmin\AccountController@index')->name('account');
Route::post('/superadmin/account/update', 'Superadmin\AccountController@update')->name('account');

Route::get('/superadmin/account/change-password', 'Superadmin\AccountController@changePassword')->name('account');
Route::post('/superadmin/account/updatePassword', 'Superadmin\AccountController@updatePassword')->name('account');



////// Products //// 

Route::post('/superadmin/product/getList', 'Superadmin\ProductController@getList')->name('product');
Route::post('/superadmin/product/productStatusChange','Superadmin\ProductController@productStatusChange')->name('product');

Route::get('/superadmin/product/select', 'Superadmin\ProductController@select')->name('product');
Route::get('/superadmin/product/table', 'Superadmin\ProductController@table')->name('product');
Route::get('/superadmin/product/row', 'Superadmin\ProductController@row')->name('product');

Route::post('/superadmin/product/getDetailList', 'Superadmin\ProductController@getDetailList')->name('product');

Route::get('/superadmin/product/steptable', 'Superadmin\ProductController@stepTable')->name('product');
Route::get('/superadmin/product/steprow', 'Superadmin\ProductController@stepRow')->name('product');
Route::get('/superadmin/product/stepcreate', 'Superadmin\ProductController@stepCreate')->name('product');

Route::get('/superadmin/product/fieldtable', 'Superadmin\ProductController@fieldTable')->name('product');
Route::get('/superadmin/product/fieldrow', 'Superadmin\ProductController@fieldRow')->name('product');
Route::get('/superadmin/product/fieldcreate', 'Superadmin\ProductController@fieldCreate')->name('product');

Route::get('/superadmin/product/designtable', 'Superadmin\ProductController@designTable')->name('product');
Route::get('/superadmin/product/designrow', 'Superadmin\ProductController@designRow')->name('product');
Route::get('/superadmin/product/designcreate', 'Superadmin\ProductController@designCreate')->name('product');

Route::get('/superadmin/product/designfiledtable', 'Superadmin\ProductController@designFiledTable')->name('product');
Route::get('/superadmin/product/designfiledrow', 'Superadmin\ProductController@designFiledRow')->name('product');
Route::get('/superadmin/product/designfiledcreate', 'Superadmin\ProductController@designFiledCreate')->name('product');

Route::get('/superadmin/product/svgselector', 'Superadmin\ProductController@svgSelectorCreate')->name('product');

Route::post('/superadmin/product/update', 'Superadmin\ProductController@update')->name('product');

Route::post('/superadmin/product/getsvgdimension', 'Superadmin\ProductController@getSVGDimension')->name('product');

Route::post('/superadmin/product/editProduct', 'Superadmin\ProductController@editProduct')->name('product');


Route::post('/superadmin/product/getProductStep', 'Superadmin\ProductController@getProductStep')->name('product');

Route::post('/superadmin/product/getProductField', 'Superadmin\ProductController@getProductField')->name('product');

Route::post('/superadmin/product/getProductDesign', 'Superadmin\ProductController@getProductDesign')->name('product');


Route::resource('/superadmin/product', 'Superadmin\ProductController', ['names' => ['index' => 'product']]);

Route::post('/superadmin/product/stepstore', 'Superadmin\ProductController@stepStore')->name('product');
Route::post('/superadmin/product/stepupdate', 'Superadmin\ProductController@stepUpdate')->name('product');
Route::post('/superadmin/product/stepStatusChange', 'Superadmin\ProductController@stepStatusChange')->name('product');

Route::post('/superadmin/product/stepdelete', 'Superadmin\ProductController@stepDelete')->name('product');


Route::post('/superadmin/product/fieldstore', 'Superadmin\ProductController@fieldStore')->name('product');
Route::post('/superadmin/product/fieldupdate', 'Superadmin\ProductController@fieldUpdate')->name('product');
Route::post('/superadmin/product/fielddelete', 'Superadmin\ProductController@fieldDelete')->name('product');


Route::post('/superadmin/product/designstore', 'Superadmin\ProductController@designStore')->name('product');
Route::post('/superadmin/product/designupdate', 'Superadmin\ProductController@designUpdate')->name('product');
Route::post('/superadmin/product/designStatusChange', 'Superadmin\ProductController@designStatusChange')->name('product');
Route::post('/superadmin/product/designdelete', 'Superadmin\ProductController@designDelete')->name('product');

Route::post('/superadmin/product/designfileddelete', 'Superadmin\ProductController@designFiledDelete')->name('product');


Route::post('/superadmin/product/filterCategory', 'Superadmin\ProductController@filterCategory')->name('product');

Route::get('/superadmin/preview','Superadmin\PreviewController@index')->name('preview');

});



Route::get('/api/dashboard', 'Api\DashboardController@index')->name('api');
Route::get('/api/dashboard/row', 'Api\DashboardController@row')->name('api');

Route::get('/api/dashboard/productdesignrow', 'Api\DashboardController@productDesignrow')->name('api');
Route::get('/api/dashboard/stepcustomiserow', 'Api\DashboardController@stepcustomiserow')->name('api');
Route::get('/api/dashboard/fieldcustomiserow', 'Api\DashboardController@fieldcustomiserow')->name('api');
Route::get('/api/dashboard/productcartindex', 'Api\DashboardController@productcartindex')->name('api');
Route::get('/api/dashboard/productcarttable', 'Api\DashboardController@productcarttable')->name('api');
Route::get('/api/dashboard/productcartrow', 'Api\DashboardController@productcartrow')->name('api');
Route::get('/api/dashboard/emailformcreate', 'Api\DashboardController@emailformcreate')->name('api');
Route::get('/api/dashboard/cartproductdetail', 'Api\DashboardController@cartproductdetail')->name('api');
Route::get('/api/dashboard/getquoteeditor', 'Api\DashboardController@getquoteeditor')->name('api');


Route::get('/api/common/leftmenu', 'Api\DashboardController@leftmenu')->name('api');

Route::post('/api/dashboard/getCompanyFeedCodeStatus', 'Api\DashboardController@getCompanyFeedCodeStatus')->name('api');
Route::post('/api/dashboard/getList', 'Api\DashboardController@getList')->name('api');
Route::post('/api/dashboard/getProductList', 'Api\DashboardController@getProductList')->name('api');
Route::post('/api/dashboard/getProductDesignList', 'Api\DashboardController@getProductDesignList')->name('api');
Route::post('/api/dashboard/getProductCustomiseList', 'Api\DashboardController@getProductCustomiseList')->name('api');

Route::post('/api/dashboard/getEditProductCustomiseList', 'Api\DashboardController@getEditProductCustomiseList')->name('api');

Route::post('/api/dashboard/getProductCartList', 'Api\DashboardController@getProductCartList')->name('api');

Route::post('/api/dashboard/addProductOrder', 'Api\DashboardController@addProductOrder')->name('api');
Route::post('/api/dashboard/updateProductOrder', 'Api\DashboardController@updateProductOrder')->name('api');
Route::post('/api/dashboard/deleteCartProduct', 'Api\DashboardController@deleteCartProduct')->name('api');

Route::post('/api/dashboard/captureProductDesignImageUpload', 'Api\DashboardController@captureProductDesignImageUpload')->name('api');

Route::post('/api/dashboard/canvasImageUpload', 'Api\DashboardController@canvasImageUpload')->name('api');

Route::post('/api/dashboard/deletePlayer', 'Api\DashboardController@deletePlayer')->name('api');
Route::post('/api/dashboard/uploadlogo', 'Api\DashboardController@uploadLogo')->name('api');
Route::post('/api/dashboard/deleteProductLogo', 'Api\DashboardController@deleteProductLogo')->name('api');
Route::post('/api/dashboard/emailtofriend', 'Api\DashboardController@emailToFriend')->name('api');

Route::post('/api/dashboard/getCartProductDetail', 'Api\DashboardController@getCartProductDetail')->name('api');

Route::post('/api/dashboard/saveQuote', 'Api\DashboardController@saveQuote')->name('api');

