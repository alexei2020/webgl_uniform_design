<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $table = "category";

    public function children()
    {
       // return $this->hasMany('APP\Category','parentId');   

         return $this->hasMany(\App\Category::class,'parentId','id');

    }
    

    public static function categoryName($id)
    {
    	$data =Category::where(array("id"=>$id))->select("name")->get();
    	return $data[0]->name;
    }
}
