<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $table = "groups";

    public static function getColorGroup($id)
    {
    	$result = \App\Group_fc::leftjoin('color','color.id','group_fc.colorId')->where(array('group_fc.groupId'=>$id))->select('color.code')->get();
    	$data = array();
    	$data1="";
    	foreach ($result as $res) {
    			$data[] = $res->code;
    			// $data1= implode($res->code, ',');
    	}
    	return $data;
    }
}
