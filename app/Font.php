<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Font extends Model
{
    public $table = "font";

    public static function insertFont($arr)
    {
    	
	      $check_exist = Font::where(array('value'=>$arr['value'],'distributorId'=>$arr['distributorId']))->get();
	      if(count($check_exist)<1)
	      { 
	      	Font::insert($arr);
	      }
	     
    	return true;
    }
    
}
