<?php

namespace App\Utils;

use Illuminate\Support\Facades\DB;
use App\CompaniesBusinessunit;
use App\Businessunit;
use Auth;

class DepartmentsUtil extends Util
{
   

   public function getList($search){
      
      $departments=Businessunit::
       leftjoin("departments as d",function($join){
            $join->on("d.businessunitId","=","businessunit.id")
            ->on("d.companyId","=","businessunit.companyId");
        })
      // ->join("businessunit as b","b.id","businessunit.id")
      ->where("businessunit.companyId",Auth::User()->companyId)
      ->when($search,function($q) use ($search)
          {
              if ($search!="" ) {
                $q->where('d.name', 'like', '%' . $search . '%');
              }
            
          })
      ->select("businessunit.code as bcode","businessunit.id as bid","d.name as dname","d.id as did")
      ->orderBy("businessunit.id")
      ->get();
     
      
      return $departments;

   }

    
}
