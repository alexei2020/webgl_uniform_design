<?php

namespace App\Utils;

use Illuminate\Support\Facades\DB;
use App\CompaniesBusinessunit;
use Auth;

class NotificationsUtil extends Util
{

	public function addNotification($created_by,$send_to,$status,$comment)
	{
		$query = array('created_by'=>$created_by,'send_to'=>$send_to,'status'=>$status,'comment'=>$comment,'created_at'=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now());

		\DB::table('notifications')->insert($query);
		return true;
	}

    
}
