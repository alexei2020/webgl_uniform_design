<?php

namespace App\Utils;

use Illuminate\Support\Facades\DB;
use App\CompaniesBusinessunit;
use Auth;

class RegionsUtil extends Util
{
   

   public function getList($search){
      
      	$departments=\App\Departments::
        leftjoin("regions as reg","reg.departmentId","=","departments.id")
      ->where("departments.companyId",Auth::User()->companyId)
       ->when($search,function($q) use ($search)
          {
              if ($search!="" ) {
                $q->where('reg.name', 'like', '%' . $search . '%');
              }
            
          })
      ->select("departments.name as bcode","departments.id as bid","reg.name as dname","reg.id as did")
      ->orderBy("departments.id")
      ->get();


    

    /*$departments=CompaniesBusinessunit::
       leftjoin("regions as d",function($join){
            $join->on("d.businessunitId","=","companies_businessunit.businessunitId")
            ->on("d.companyId","=","companies_businessunit.companyId");
        })
      ->join("businessunit as b","b.id","companies_businessunit.businessunitId")
      ->where("companies_businessunit.companyId",Auth::User()->companyId)
       ->when($search,function($q) use ($search)
          {
              if ($search!="" ) {
                $q->where('d.name', 'like', '%' . $search . '%');
              }
            
          })
      ->select("b.code as bcode","b.id as bid","d.name as dname","d.id as did")
      ->orderBy("b.id")
      ->get();
*/
    

      return $departments;

   }

    
}
