<?php

namespace App\Utils;

use Illuminate\Support\Facades\DB;
use App\CompaniesBusinessunit;
use Auth;

class AuditorsUtil extends Util
{
   

   public function getTimelineHistory($auditId, $created_by, $status="")
   {
   		$query = array('auditId'=>$auditId,'created_by'=>$created_by,'status'=>$status,'created_at'=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now());

        \DB::table('timeline_history')->insert($query);
         return true;
   }

    
}
