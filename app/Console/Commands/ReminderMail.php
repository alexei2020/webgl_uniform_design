<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ReminderMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'command:name';
    protected $signature =  'reminderNotification:ReminderforOverdueAudit'; //'notification:unreadmessages';

    /**
     * The console command description.
     *
     * @var string
     */
    // protected $description = 'Command description';
    protected $description = 'Reminder for the Overdue Audit111';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       

       //  /usr/local/bin/php -q /home/imensosw/public_html/imenso.co/dev/advanchainge/php artisan schedule:run 1>> /dev/null 2>&1
        $audits = \App\Audits::join("audits_assign","audits_assign.auditId","audits.id")
        ->join("units","units.id","audits.unitId")
        ->join("users","users.id","audits_assign.userId")
        ->where(array("audits.deleted"=>0))
        ->whereIn("audits.status",['publish'])
           ->select("audits.title as audit_name","audits.companyId","audits.type","units.name as site_name","users.name as user_name","users.email as user_email",\DB::raw('DATE_FORMAT(audits.auditdate, "%Y-%m-%d") as auditdate'))
        ->get();

        $currentdate = date("Y-m-d");

        foreach ($audits as $audit) 
        {
            if($audit->auditdate < $currentdate && $audit->status!="start" && $audit->status!="submited")
            {   
                $days = $this->calculateDays($currentdate,$audit->auditdate);
                if($days%2!=0)
                {
                    
                    $numberOfDays = $days;
                    $audit_name = $audit->audit_name;
                    $site_name= $audit->site_name;
                    $audit_date=$audit->auditdate;

                    $subject="Reminder for the Overdue Audit" ;
     

                //------------------ Mail Send to Auditors for Overdue Audits ----------------------
                    $from_name='Admin';
                    $from_email=config('constants.admin_email');
                    // $from_email="deepak1gehlot@gmail.com";

                    $to_name = $audit->user_name;  
                    $to_email = $audit->user_email;
                    // $to_email = "deepak.imenso@gmail.com";  

                      $template_id=config("sendgridTemplateIds.Reminder_for_Overdue_Audit_for_Auditor_for_both_planned_surprise");

                      $data = array(':admin_email' => array($from_email),':user_name' => array($to_name),':user_email' => array($to_email),':audit_name' => array($audit_name),':site_name' => array($site_name),':audit_date' => array($audit_date),':number_of_days' => array($numberOfDays));

                    \App\Sendgrid::send_mail( $subject , $template_id , $data , $to_email , $to_name, $from_email ); 

                //------------------ Mail Send to Admin for Overdue Audits ----------------------- 
                    if($audit->companyId!="")
                    {
                        $admin_user = \App\User::where(array("companyId"=>$audit->companyId,"type"=>"admin"))->get();

                        $to_name = $admin_user[0]->name;  
                        $to_email = $audit->user_email;
                    // $to_email = "deepak1gehlot@gmail.com";  

                        $template_id1=config("sendgridTemplateIds.Reminder_for_Overdue_Audit_for_Admin_for_both_planned_surprise");

                        $data1 = array(':admin_email' => array($from_email),':user_name' => array($to_name),':user_email' => array($to_email),':audit_name' => array($audit_name),':site_name' => array($site_name),':audit_date' => array($audit_date),':number_of_days' => array($numberOfDays));

                        \App\Sendgrid::send_mail( $subject , $template_id1 , $data1 , $to_email , $to_name, $from_email ); 
                    }
                    
                }
     
            }
        }
              
    }

   protected function calculateDays($startdate,$enddate)
    {
        $startdate = date_create($startdate); 
        $enddate = date_create($enddate); 
    
        // Calculates the difference between DateTime objects 
        $interval = date_diff($startdate, $enddate); 
          
        // Display the result 
        $days = $interval->format('%a'); 
        if($days==0)
        {
          $days=1;
        }
        return $days;
    }
}
