<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Sendgrid extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];


    public static function send_mail( $subject , $template_id , $sub , $to_email , $to_name,$from_email )
    {
          $js = array(
            'sub' => $sub,
            'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $template_id)))
          );
           
          $params = array(
              'to'        => $to_email,
              'toname'    => $to_name,
              'from'      => $from_email,
              'fromname'  => "Uniform Builder",
              'subject'   => $subject,
              'text'      => "",
              'html'      => " ",
              'x-smtpapi' => json_encode($js),
            );
          
          $session = curl_init('https://api.sendgrid.com/'.'api/mail.send.json');
          curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
          curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.'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'));
          curl_setopt ($session, CURLOPT_POST, true);
          curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
          curl_setopt($session, CURLOPT_HEADER, false);
          curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
          $response = curl_exec($session);
          curl_close($session);
    }

  
}
