<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    public $table = "color";


    public static function insertColor($arr)
    {
    	
	      $check_exist = Color::where(array('code'=>$arr['code'],'distributorId'=>$arr['distributorId']))->get();
	     
	      if(count($check_exist)<1)
	      { 
	      	Color::insert($arr);
	      }
	     
    	return true;
    }
    
}
