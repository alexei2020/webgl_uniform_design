<?php

namespace App;
use Session;
class Role extends \Spatie\Permission\Models\Role
{
    //
    public static function getRoleByNgo($userId)
    {
        
    	$roles = Role::whereIn('id',array(1))->orwhere(array("ngo_id"=>$userId))->get();
    	return $roles;

    }

    public static function getPluckRoleByNgo($ngo_id)
    {
    	$roles = Role::whereIn('id',array(1))->orwhere(array("ngo_id"=>$ngo_id))->get()->pluck('name','id')->prepend('Select','');
    	return $roles;

    }

   
}
