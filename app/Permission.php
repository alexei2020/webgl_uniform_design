<?php

namespace App;

class Permission extends \Spatie\Permission\Models\Permission
{
    public static function defaultPermissions()
    {
        return [
            'view_general',
            'add_general',
            'edit_general',
            'delete_general',

            'view_roles',
            'add_roles',
            'edit_roles',
            'delete_roles',

            'view_departments',
            'add_departments',
            'edit_departments',
            'delete_departments',

            'view_regions',
            'add_regions',
            'edit_regions',
            'delete_regions',

            'view_locations',
            'add_locations',
            'edit_locations',
            'delete_locations',

            'view_checklist',
            'add_checklist',
            'edit_checklist',
            'delete_checklist',

            'view_checklist',
            'add_checklist',
            'edit_checklist',
            'delete_checklist',

            'view_category',
            'add_category',
            'edit_category',
            'delete_category',

            'view_documents',
            'add_documents',
            'edit_documents',
            'delete_documents',

            'view_units',
            'add_units',
            'edit_units',
            'delete_units',

            'view_users',
            'add_users',
            'edit_users',
            'delete_users'

        ];
    }
}
