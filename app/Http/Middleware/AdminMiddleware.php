<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Ngo;
use App\Notification;
use App\Project;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$id="")
    {
      $url = $request->segment(2);
      $cuntrollesr = $request->segment(1);

      Auth::User()->active_action=$url;
      
      if(Auth::user()->type!="admin")
      {
        return redirect('product');
      }


   
      
      return $next($request);
    }
}
