<?php

namespace App\Http\Middleware;


use Closure;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StaffMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next,$id="")
    {
      $url = $request->segment(2);
      $cuntrollesr = $request->segment(1);

      Auth::User()->active_action=$url;

      if(Auth::user()->type=="superadmin")
      {
        return redirect('superadmin/companies');
      }
      else if(Auth::user()->type=="admin" || Auth::user()->type=="super user")
      {
        return redirect('audits');
      }
       else if(Auth::user()->type=="manager")
      {
        return redirect('manager');
      }
      else if(Auth::user()->type=="auditor")
      {
        return redirect('auditor');
      }
      return $next($request);
    }
}
