<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        
        if (Auth::guard($guard)->check()) {
            if(Auth::user()->type=="superadmin")
            {
                return redirect('superadmin/distributor');    
            }
            else if(Auth::user()->type=="distributor")
            {
                return redirect('distributor/product');
            }
            // return redirect('/dashboard');
        }

        return $next($request);
    }
}
