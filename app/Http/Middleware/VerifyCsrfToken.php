<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/api/dashboard',
        '/api/dashboard/row',
        '/api/dashboard/productdesignrow',
        '/api/dashboard/stepcustomiserow',
        '/api/dashboard/fieldcustomiserow',
        '/api/dashboard/productcartindex',
        '/api/dashboard/productcarttable',
        '/api/dashboard/productcartrow',
        '/api/dashboard/emailformcreate',
        '/api/dashboard/cartproductdetail',
        '/api/dashboard/getquoteeditor',
        '/api/common/leftmenu',
        '/api/dashboard/getList',
        '/api/dashboard/getProductList',
        '/api/dashboard/getProductDesignList',
        '/api/dashboard/getProductCustomiseList',
        '/api/dashboard/getEditProductCustomiseList',
        '/api/dashboard/getProductCartList',
        '/api/dashboard/addProductOrder',
        '/api/dashboard/updateProductOrder',
        '/api/dashboard/deleteCartProduct',
        '/api/dashboard/canvasImageUpload',
        '/api/dashboard/deletePlayer',
        '/api/dashboard/uploadlogo',
        '/api/dashboard/deleteProductLogo',
        '/api/dashboard/emailtofriend',
        '/api/dashboard/getCartProductDetail',
        '/api/dashboard/saveQuote',
        '/api/dashboard/captureProductDesignImageUpload',
        '/api/dashboard/getCompanyFeedCodeStatus',
    ];
}
