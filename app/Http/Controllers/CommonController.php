<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;


class CommonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function select()
    {
           
        return view('common.select');
    }
     public function leftmenu()
    {
        return view('api.common.leftmenu');
    }
}
