<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
        return view('api.dashboard.index');
    }

     public function row()
    {
        return view('api.dashboard.row');
    }

     public function leftmenu()
    {
        return view('api.common.leftmenu');
    }
     
    public function productDesignrow()
    {
        return view('api.dashboard.product_design.row');
    }  

    public function stepcustomiserow()
    {
        return view('api.dashboard.product_customise.steprow');
    }  

    public function fieldcustomiserow()
    {
        return view('api.dashboard.product_customise.field.row');
    }  

    public function productcartindex()
    {
        return view('api.dashboard.product_cart.index');
    }  
    public function productcarttable()
    {
        return view('api.dashboard.product_cart.table');
    }  
    public function productcartrow()
    {
        return view('api.dashboard.product_cart.row');
    }  

    public function emailformcreate()
    {
        return view('api.dashboard.product_cart.emailtofriend');
    }  
    
    public function cartproductdetail()
    {
        return view('api.dashboard.product_cart.cartproductdetail');
    }  
    
    public function getquoteeditor()
    {
        return view('api.dashboard.product_cart.getquote');
    }

    public function getCompanyFeedCodeStatus(Request $request)
    {
        $distributorId = $request->distributorId;

        $checkFeedCodeStatus=\App\Company::where(array('distributorId'=>$distributorId))->select('id','status')->get();
       
       
         return json_encode(array("checkFeedCodeStatus"=>$checkFeedCodeStatus));
    }

    public function getList(Request $request)
    {
        $distributorId = $request->distributorId;

        $category=\App\Category::where(array("userId"=>$distributorId,'parentId'=>null,'category.status'=>'active'))
                ->select("category.name as cname" ,"category.userId as distributorId","category.id as categoryId","category.parentId","category.url_slug",'category.position')
                ->orderByRaw('-category.position DESC')
                ->get();

        $totalCategory =\App\Category::where(array("userId"=>$distributorId,'category.status'=>'active'))
                ->whereNotNull('parentId')
                ->select("category.name as cname","category.userId as distributorId",'category.position',"category.id as categoryId","category.parentId",DB::raw("(select name from category as cat where cat.id=category.parentId) as parentCategoryName"),"category.url_slug")
                ->orderByRaw('-category.position DESC')
                ->get(); 
        
         return json_encode(array("category"=>$category,"totalCategory"=>$totalCategory));
    }

    public function getProductList(Request $request)
    {   
       
       if($request->categoryUrl_slug!="" && $request->categoryId==""){
            $catId= \App\Category::where(array('url_slug'=>$request->categoryUrl_slug))->select('id')->first();
            $categoryId= $catId->id;
       }
       else{
        $categoryId = $request->categoryId;
       }
        $distributorId = $request->distributorId;
        $categoryUrl_slug = $request->categoryUrl_slug;
        
        $subcategoryUrl_slug = $request->subcategoryUrl_slug;
        $subcategoryId = $request->subcategoryId;
        $data=[
            "categoryId"=>$categoryId,
            "categoryUrl_slug"=>$categoryUrl_slug,
            "subcategoryId"=>$subcategoryId,
            "subcategoryUrl_slug"=>$subcategoryUrl_slug
        ];
        $sortBy = $request->sortBy;
        $sortName = $request->sortName;
        if($sortName=="sort_by_name")
        {
            $sortName="products.name";
            $orderRaw='products.name ASC';
        }
        else
        {
            $sortName="products.id";
            $orderRaw='-products.position DESC';
        }
        $products=\App\Product::
                join('documents','documents.id','products.list_ImageId')->join('category','category.id','products.categoryId')
                ->where(array("products.distributorId"=>$distributorId,'products.status'=>'active','products.deleted'=>0))
                ->when($data,function($q) use ($data)
                {
                   
                    if ($data['subcategoryId']=="" && $data['categoryId']!="") {
                        $q->where('category.parentId',$data['categoryId']);
                        $q->orwhere('category.id',$data['categoryId']);
                    }
                    else if($data['subcategoryId']!="")
                    {
                        $q->Where('category.id',$data['subcategoryId']);
                        $q->Where('category.parentId',$data['categoryId']);
                    }
                    else if($data['categoryUrl_slug']!="" && $data['subcategoryUrl_slug']=="")
                    {
                       $q->where('category.url_slug',$data['categoryUrl_slug']);
                    }
                    else if($data['subcategoryUrl_slug']!="")
                    {
                         $q->where('category.url_slug',$data['subcategoryUrl_slug']);
                    }

                })
                ->select("products.id","products.deleted","products.distributorId","products.url_slug","products.name",'documents.name as doc_name','products.status as product_status')

                ->orderByRaw($orderRaw)
                ->orderBy($sortName,$sortBy)
                ->paginate(config('constants.limit'));
                
         return json_encode(array("products"=>$products,"pagination"=>str_replace('/?','?',$products->render('common.pagination'))));


    }

    public function getProductDesignList(Request $request)
    {   

        $distributorId = $request->distributorId;
        $productId = $request->productId;
        $productUrl_slug = $request->productUrl_slug;
        $data=[
            "productId"=>$productId,
            "productUrl_slug"=>$productUrl_slug
        ];

        $product_designRs=\App\Product_design::join("products","product_design.productId","products.id")
        ->leftjoin('documents','documents.id','products.model_3DId')
                ->where(array('product_design.status'=>'active'))
                ->when($data,function($q) use ($data)
                {

                    if ($data['productId']!="" ) {
                      $q->where('product_design.productId',$data['productId']);                      
                    }
                    else if($data['productUrl_slug']!="")
                    {
                        $q->where('products.url_slug',$data['productUrl_slug']);
                    }

                })
                ->select("product_design.id","product_design.design_slug","product_design.name","product_design.profile","product_design.svg_snippet","product_design.productId",'products.svg_dimension_ht as svg_width','products.svg_dimension_vt as svg_height',"documents.name as obj_doc","products.scale as pro_scale","products.x_axis as prox_axis","products.y_axis as proy_axis","products.z_axis as proz_axis",'product_design.svg_viewBox','product_design.profile')

                ->groupBy('product_design.id')
                ->orderByRaw('-product_design.position DESC')
                ->get();

                $product_design=array();
            foreach ($product_designRs as $quest) {
                $quest->id=intval($quest->id);
                $quest->distributorId=intval($quest->distributorId);
                $quest->productId=intval($quest->productId);
                $product_design[]=$quest;
            }


         return json_encode(array("product_design"=>$product_design));
    }

    public function getProductCustomiseList(Request $request)
    {   
        $distributorId=$request->distributorId;
        $designId = $request->productDesignId;
        $productUrl_slug = $request->productUrl_slug;
        $design_slug=$request->design_slug;
        $productId = $request->productId;
        $data=[
            "productId"=>$productId,
            "productUrl_slug"=>$productUrl_slug,
            "designId"=>$designId,
            'design_slug'=>$design_slug
        ];
        $product_stepRs=\App\Product_step::join("products","product_step.productId","products.id")
            ->where(array('products.distributorId'=>$distributorId))
            ->leftjoin('documents','documents.id','products.model_3DId')
            ->where(array('product_step.status'=>'active'))
            ->when($data,function($q) use ($data)
                {
                    if ($data['productId']!="" ) {
                      $q->where('product_step.productId',$data['productId']);                      
                    }
                    else if($data['productUrl_slug']!="")
                    {
                        $q->where('products.url_slug',$data['productUrl_slug']);
                    }

                })
         
            ->select("product_step.id","product_step.name","product_step.productId","products.name as product_name","products.description","documents.name as obj_doc")

            ->groupBy('product_step.id')
            ->get();

            $product_step=array();
            foreach ($product_stepRs as $quest) {
                $quest->id=intval($quest->id);
                $quest->distributorId=intval($quest->distributorId);
                $quest->productId=intval($quest->productId);
                $quest->description=htmlspecialchars_decode($quest->description, ENT_QUOTES);
                $product_step[]=$quest;
            }

        $product_fieldRs=\App\Product_field::join("products","product_field.productId","products.id")
            ->leftjoin('product_step','product_step.id','product_field.stepId')
            ->when($data,function($q) use ($data)
                {
                    if ($data['productId']!="" ) {
                      $q->where('product_field.productId',$data['productId']);                      
                    }
                    else if($data['productUrl_slug']!="")
                    {
                        $q->where('products.url_slug',$data['productUrl_slug']);
                    }

                })
         
            ->select("product_field.id","product_field.name","product_field.groupId","product_field.fontGroupId","product_field.stepId","product_field.filed_type","product_step.status",
                DB::raw('null as text_value'),
                DB::raw('null as color_value'),
                DB::raw('null as image_value'),
                DB::raw('null as fontsize_value'),
                DB::raw('null as positionX_value'),
                DB::raw('null as positionY_value'),
                DB::raw('null as height_value'),
                DB::raw('null as width_value'),
                DB::raw('null as fontname_value'),
                DB::raw('null as height_check_disable')
           )
            ->get();    

               $product_field=array();
            foreach ($product_fieldRs as $quest) {
                $quest->id=intval($quest->id);
                $quest->productId=intval($quest->productId);
                $quest->distributorId=intval($quest->distributorId);
                $quest->groupId=intval($quest->groupId);
                $quest->fontGroupId=intval($quest->fontGroupId);
                $quest->stepId=intval($quest->stepId);
                $quest->colorGup=$this->getColorGroup($quest->groupId);
                $quest->fontGup=$this->getFontGroup($quest->fontGroupId);
                $quest->height_check_disable="true";
                $product_field[]=$quest;
            }

        $design_filedRS = \App\Design_filed::leftjoin('product_design','product_design.id','design_filed.designId')

            ->leftjoin('products','products.id','product_design.productId')
           
            ->where(array('product_design.status'=>'active'))

                ->when($data,function($q) use ($data)
                {
                    if ($data['designId']!="" && $data['productUrl_slug']!="") {
                      $q->where('design_filed.designId',$data['designId']);    
                      $q->where('products.url_slug',$data['productUrl_slug']);                     
                    }
                    if($data['design_slug']!="" && $data['productUrl_slug']!="")
                    {
                        $q->where('product_design.design_slug',$data['design_slug']);
                        $q->where('products.url_slug',$data['productUrl_slug']);   
                    }
                 
                })
          
                ->select('design_filed.id','design_filed.name','design_filed.design_key','design_filed.groupId','design_filed.stepId','design_filed.designId','product_design.name as pd_name','product_design.productId',
                    DB::raw('null as color_value')
                    )
            ->groupBy('design_filed.id')
            ->get();    

            $design_filed=array();
            foreach ($design_filedRS as $quest) {
                $quest->id=intval($quest->id);
                $quest->stepId=intval($quest->stepId);
                $quest->groupId=intval($quest->groupId);
                $quest->colorGup=$this->getColorGroup($quest->groupId);
                $quest->fontGup=$this->getFontGroup($quest->groupId);
                $design_filed[]=$quest;
            }
          
        $proDesign =\App\Product_design::leftjoin('products','products.id','product_design.productId')
                        ->where(array('product_design.design_slug'=>$design_slug,'product_design.status'=>'active','products.url_slug'=>$productUrl_slug))

            ->select('product_design.id','product_design.name','product_design.design_slug','product_design.svg_snippet',"products.x_axis",'products.y_axis','products.z_axis','products.svg_dimension_ht as svg_width','products.svg_dimension_vt as svg_height','products.scale','products.light_brightness','product_design.player_status','products.rotation_disable','products.default_toggle_color','product_design.svg_viewBox')
            ->get();


        $colorsRS = \App\Color::join('group_fc','group_fc.colorId','color.id')
                ->where(array('color.status'=>'active','color.distributorId'=>$distributorId))
                ->select('color.*','group_fc.groupId')
               ->groupBy('color.id')
                ->get();

            $colors=array();
            foreach ($colorsRS as $quest) {
                $quest->id=intval($quest->id);
                $quest->groupId=intval($quest->groupId);
                $colors[]=$quest;
            }

        $fontRS = \App\Font::join('group_fc','group_fc.fontId','font.id')
            ->where(array('font.status'=>'active','font.distributorId'=>$distributorId))
            ->select('font.*','group_fc.groupId')
            ->groupBy('font.id')
            ->get();

            $fonts=array();
            foreach ($fontRS as $quest) {
                $quest->id=intval($quest->id);
                $quest->groupId=intval($quest->groupId);
                
                $fonts[]=$quest;
            }

        $size_list=array('Mens - XS'=>'Mens - XS','Mens - S'=>'Mens - S','Mens - M'=>'Mens - M','Mens - L'=>'Mens - L','Mens - XL'=>'Mens - XL','Mens - 2XL'=>'Mens - 2XL','Mens - 3XL'=>'Mens - 3XL','Mens - 4XL'=>'Mens - 4XL','Mens - 5XL'=>'Mens - 5XL');

        $playerList=array();

        $social_media=\App\Company_social::where(array('distributorId'=>$distributorId))->get();

     
         return json_encode(array("product_step"=>$product_step,"product_field"=>$product_field,"design_filed"=>$design_filed,'colors'=>$colors,'fonts'=>$fonts,"proDesign"=>$proDesign,'size_list'=>$size_list,'playerList'=>$playerList,'social_media'=>$social_media));
    }


     public function getEditProductCustomiseList(Request $request)
    {   

        $distributorId=$request->distributorId;
        $designId = $request->productDesignId;
        $productUrl_slug = $request->productUrl_slug;
        $productId = $request->productId;

        $design_slug=$request->design_slug;
        $data=[
            "productId"=>$productId,
            "productUrl_slug"=>$productUrl_slug,
            "designId"=>$designId,
            "design_slug"=>$design_slug,
        ];
        $product_stepRs=\App\Product_step::join("products","product_step.productId","products.id")
        ->where(array('products.distributorId'=>$distributorId))
            ->leftjoin('documents','documents.id','products.model_3DId')
            ->where(array('product_step.status'=>'active'))
            ->when($data,function($q) use ($data)
                {
                    if($data['productUrl_slug']!="")
                    {
                        $q->where('products.url_slug',$data['productUrl_slug']);
                    }
                    else if ($data['productId']!="" ) {
                      $q->where('product_step.productId',$data['productId']);                      
                    }

                })
            ->select("product_step.id","product_step.name","product_step.productId","products.name as product_name","products.description","documents.name as obj_doc")
            ->groupBy('product_step.id')
            ->get();

            $product_step=array();
            foreach ($product_stepRs as $quest) {
                $quest->id=intval($quest->id);
                $quest->distributorId=intval($quest->distributorId);
                $quest->productId=intval($quest->productId);
                $quest->description=htmlspecialchars_decode($quest->description, ENT_QUOTES);
                $product_step[]=$quest;
            }

        $product_fieldRs=\App\Product_field::join("products","product_field.productId","products.id")
                        ->leftjoin('product_order','product_order.productId','products.id')
                        ->leftjoin('product_order_field','product_order_field.fieldId','product_field.id') 
                        ->leftjoin('product_step','product_step.id','product_field.stepId')
           ->where(array("product_order_field.productOrderId"=>$request->orderId))
                        ->whereNotNull('product_order_field.fieldId')
            ->when($data,function($q) use ($data)
                {
                    if($data['productUrl_slug']!="")
                    {
                        $q->where('products.url_slug',$data['productUrl_slug']);
                    }
                    else if ($data['productId']!="" ) {
                      $q->where('product_field.productId',$data['productId']);                      
                    }

                })
            ->select("product_field.id","product_field.name","product_field.stepId","product_field.filed_type","product_step.status","product_field.groupId","product_field.fontGroupId",
                DB::raw('product_order_field.text as text_value'),
                DB::raw('product_order_field.color as color_value'),
                DB::raw('product_order_field.image as image_value'),
                DB::raw('product_order_field.fontsize as fontsize_value'),
                DB::raw('product_order_field.position_x as positionX_value'),
                DB::raw('product_order_field.position_y as positionY_value'),
                DB::raw('product_order_field.height_value as height_value'),
                DB::raw('product_order_field.width_value as width_value'),
                DB::raw('product_order_field.fontname as fontname_value'),
                DB::raw('product_order_field.height_check_disable')
                // DB::raw('product_order_field.width_check_disable')
            )
            ->groupBy('product_field.id')
            ->get();    


               $product_field=array();
            foreach ($product_fieldRs as $quest) {
                $quest->id=intval($quest->id);
                $quest->productId=intval($quest->productId);
                $quest->distributorId=intval($quest->distributorId);
                $quest->colorGup=$this->getColorGroup($quest->groupId);
                $quest->fontGup=$this->getFontGroup($quest->fontGroupId);
                $quest->stepId=intval($quest->stepId);
                $product_field[]=$quest;
            }


        $design_filedRS = \App\Design_filed::leftjoin('product_design','product_design.id','design_filed.designId')
        ->join('products','products.id','product_design.productId')
                        ->leftjoin('product_order','product_order.productId','products.id')
                        ->leftjoin('product_order_field','product_order_field.designfieldId','design_filed.id') 
                ->whereNotNull('product_order_field.designfieldId')
                ->where(array("product_order_field.productOrderId"=>$request->orderId,'product_design.status'=>'active'))
                ->when($data,function($q) use ($data)
                {
                    if ($data['designId']!="" && $data['productUrl_slug']!="") {
                      $q->where('design_filed.designId',$data['designId']);  
                      $q->where('products.url_slug',$data['productUrl_slug']);                       
                    }
                    if($data['design_slug']!="" && $data['productUrl_slug']!="")
                    {
                        $q->where('product_design.design_slug',$data['design_slug']);  
                        $q->where('products.url_slug',$data['productUrl_slug']);    
                    }
         
                })
          
                ->select('design_filed.id','design_filed.name','design_filed.design_key','design_filed.groupId','design_filed.stepId','design_filed.designId',DB::raw('product_order_field.color as color_value'),'product_design.name as pd_name','product_design.productId')
            ->groupBy('design_filed.id')
            ->get();    

            $design_filed=array();
            foreach ($design_filedRS as $quest) {
                $quest->id=intval($quest->id);
                $quest->stepId=intval($quest->stepId);
               $quest->colorGup=$this->getColorGroup($quest->groupId);
                $quest->fontGup=$this->getFontGroup($quest->groupId);
                $design_filed[]=$quest;
            }



        $proDesign =\App\Product_design::leftjoin('products','products.id','product_design.productId')
        ->where(array('product_design.design_slug'=>$design_slug,'product_design.status'=>'active','products.url_slug'=>$productUrl_slug))
      
        ->select('product_design.id','product_design.name','product_design.design_slug','product_design.svg_snippet',"products.x_axis",'products.y_axis','products.z_axis','products.svg_dimension_ht as svg_width','products.svg_dimension_vt as svg_height','products.scale','products.light_brightness','product_design.player_status','products.rotation_disable','products.default_toggle_color','product_design.svg_viewBox')
        ->get();
                        
        $colorsRS = \App\Color::join('group_fc','group_fc.colorId','color.id')
                ->where(array('color.status'=>'active','color.distributorId'=>$distributorId))
               ->select('color.id','color.name','color.code','group_fc.groupId')
                ->groupBy('color.id')
                ->get();

                $colors=array();
            foreach ($colorsRS as $quest) {
                $quest->id=intval($quest->id);
                $quest->groupId=intval($quest->groupId);
                $colors[]=$quest;
            }

        $fontRS = \App\Font::join('group_fc','group_fc.fontId','font.id')
            ->where(array('font.status'=>'active','font.distributorId'=>$distributorId))   
            ->select('font.*','group_fc.groupId')
            ->groupBy('font.id')
            ->get();

            $fonts=array();
            foreach ($fontRS as $quest) {
                $quest->id=intval($quest->id);
                $quest->groupId=intval($quest->groupId);
                $fonts[]=$quest;
            }

        $size_list=array('Mens - XS'=>'Mens - XS','Mens - S'=>'Mens - S','Mens - M'=>'Mens - M','Mens - L'=>'Mens - L','Mens - XL'=>'Mens - XL','Mens - 2XL'=>'Mens - 2XL','Mens - 3XL'=>'Mens - 3XL','Mens - 4XL'=>'Mens - 4XL','Mens - 5XL'=>'Mens - 5XL');

        $playerList=\App\Player_setting::where(array('player_setting.productOrderId'=>$request->orderId))
            ->orderBy('player_setting.id','asc')
            ->get();

            $social_media=\App\Company_social::where(array('distributorId'=>$distributorId))->get();

         return json_encode(array("product_step"=>$product_step,"product_field"=>$product_field,"design_filed"=>$design_filed,'colors'=>$colors,'fonts'=>$fonts,"proDesign"=>$proDesign,'size_list'=>$size_list,'playerList'=>$playerList,'social_media'=>$social_media));
    }

    public function getColorGroup ($groupId){
        $colorsRS = \App\Color::join('group_fc','group_fc.colorId','color.id')
                ->where(array('color.status'=>'active','group_fc.type'=>"color",'group_fc.groupId'=>$groupId))
                ->select('color.*','group_fc.groupId')
                ->groupBy('color.id')
                ->get();

            
            return $colorsRS;
    }
    public function getFontGroup($groupId){
         $fontRS = \App\Font::join('group_fc','group_fc.fontId','font.id')
            ->where(array('font.status'=>'active','group_fc.type'=>"font",'group_fc.groupId'=>$groupId))
            ->select('font.*','group_fc.groupId')
           ->groupBy('font.id')
            ->get();
            return $fontRS;
    }

    public function addProductOrder(Request $request)
    {  
      
        if($request->post())
        {
            DB::beginTransaction();
            try {
                    $distributorId = $request->distributorId;
                    $productUrl_slug = $request->productUrl_slug;
                    $design_slug = $request->design_slug;
                    $productId = \App\Product::where(array('distributorId'=>$distributorId,"url_slug"=>$productUrl_slug))->first();

                    $productDesign = \App\Product_design::where(array('productId'=>$productId->id,"design_slug"=>$design_slug))->first();
                   
                    $product_order = new \App\Product_order();
                    $product_order->distributorId = $distributorId;
                    $product_order->productId=$productId->id;
                    $product_order->productDesignId =$productDesign->id;
                    $product_order->updated_snippet = $request->updated_snippet;
                    $product_order->orderId = mt_rand(100000,999999);
                    if($product_order->save())
                    {
                        $design_filed = $request->design_filed;
                        if(!empty($design_filed))
                        {

                            foreach ($design_filed as $df) {
                                $product_order_field = new \App\Product_order_field();
                                $product_order_field->productOrderId = $product_order->id;
                                $product_order_field->designfieldId =$df['id'];
                                $product_order_field->stepId =$df['stepId'];
                                $product_order_field->designId =$df['designId'];
                                $product_order_field->color =$df['color_value'];
                                $product_order_field->save();
                            }
                        }

                        $product_field = $request->product_field;
                        if(!empty($product_field))
                        {
                            foreach ($product_field as $pf) {
                                $product_order_field = new \App\Product_order_field();
                                $product_order_field->productOrderId = $product_order->id;
                                $product_order_field->stepId =$pf['stepId'];
                                $product_order_field->fieldId =$pf['id'];
                                $product_order_field->color =$pf['color_value'];
                                $product_order_field->fontsize =$pf['fontsize_value'];
                                $product_order_field->fontname =$pf['fontname_value'];
                                $product_order_field->image =$pf['image_value'];
                                $product_order_field->text =$pf['text_value'];
                                $product_order_field->height_value =$pf['height_value'];
                                $product_order_field->width_value =$pf['width_value'];
                                $product_order_field->height_check_disable=$pf['height_check_disable'];
                                // $product_order_field->width_check_disable=$pf['width_check_disable'];

                                $product_order_field->position_x =$pf['positionX_value'];
                                $product_order_field->position_y =$pf['positionY_value'];
                                $product_order_field->save();
                            }
                        }


                        if($request->player_name!="" || $request->player_number!="" || $request->player_size!="")
                        {
                            $player_name=explode(",",$request->player_name);
                            $player_number=explode(",",$request->player_number);
                            $player_size=explode(",",$request->player_size);
                            $data = array();
                            for($i=0;$i<count($player_name);$i++)
                             {
                                $data[] =[
                                          'productOrderId' =>$product_order->id,
                                          'player_name' =>$player_name[$i],
                                          'player_number'=>$player_number[$i],
                                          'player_size'=>$player_size[$i],
                                          'created_at' => date('Y-m-d H:i:s'),
                                          'updated_at' => date('Y-m-d H:i:s')
                                         ];        
                             }

                             \App\Player_setting::insert($data);
                        }
                    }
                    DB::commit();
                    return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$product_order->id));
                }catch (\Throwable $e) {
                    DB::rollback();
                    return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
            }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }



    public function updateProductOrder(Request $request)
    {
        
        $product_field_db = \App\Product_order_field::join('product_order','product_order.id','product_order_field.productOrderId')->where(array('product_order.id'=>$request->id))->whereNotNull('product_order_field.fieldId')->select('product_order_field.*')->get();
        $design_field_db = \App\Product_order_field::join('product_order','product_order.id','product_order_field.productOrderId')->where(array('product_order.id'=>$request->id))->whereNotNull('product_order_field.designfieldId')->select('product_order_field.*')->get();

        if($request->post())
        {
            DB::beginTransaction();
            try {
                    $distributorId = $request->distributorId;
                    $productUrl_slug = $request->productUrl_slug;
                    $design_slug = $request->design_slug;
                    $productId = \App\Product::where(array('distributorId'=>$distributorId,"url_slug"=>$productUrl_slug))->first();
                   
                    $productDesign = \App\Product_design::where(array('productId'=>$productId->id,"design_slug"=>$design_slug))->first();
                    $product_order = \App\Product_order::find($request->id);
                    $product_order->distributorId = $distributorId;
                    $product_order->productDesignId =$productDesign->id;
                    $product_order->productId=$productId->id;
                    if($product_order->save())
                    {
                        $design_filed = $request->design_filed;

                        if(!empty($design_filed))
                        {
                            foreach ($design_filed as $res_design) 
                            { 
                                if(!empty($design_field_db))
                                {
                                    foreach ($design_field_db as $df) 
                                    {
                                        if($df->designfieldId==$res_design['id'])
                                        {
                                            $product_order_field = \App\Product_order_field::find($df->id);
                                            // $product_order_field->productOrderId = $product_order->id;
                                            $product_order_field->designfieldId =$res_design['id'];
                                            $product_order_field->stepId =$res_design['stepId'];
                                            $product_order_field->designId =$res_design['designId'];
                                            $product_order_field->color =$res_design['color_value'];
                                            $product_order_field->save();
                                        }
                                    }
                                }
                                else{
                                    $product_order_field =new \App\Product_order_field;
                                            $product_order_field->productOrderId = $request->id;
                                            $product_order_field->designfieldId =$res_design['id'];
                                            $product_order_field->stepId =$res_design['stepId'];
                                            $product_order_field->designId =$res_design['designId'];
                                            $product_order_field->color =$res_design['color_value'];
                                            $product_order_field->save();
                                }
                            }
                        } // end IF design_field

                        $product_field = $request->product_field;
                        if(!empty($product_field))
                        {
                            foreach ($product_field as $res_pro_field) 
                            { 
                                if(!empty($product_field_db))
                                {
                                    foreach ($product_field_db as $pf) 
                                    {
                                        if($pf->fieldId==$res_pro_field['id'])
                                        {
                                            $product_order_field = \App\Product_order_field::find($pf->id);
                                            $product_order_field->stepId =$res_pro_field['stepId'];
                                            $product_order_field->fieldId =$res_pro_field['id'];
                                            $product_order_field->color =$res_pro_field['color_value'];
                                            $product_order_field->fontsize =$res_pro_field['fontsize_value'];
                                            $product_order_field->fontname =$res_pro_field['fontname_value'];
                                            $product_order_field->image =$res_pro_field['image_value'];
                                            $product_order_field->text =$res_pro_field['text_value'];
                                            $product_order_field->position_x =$res_pro_field['positionX_value'];
                                            $product_order_field->position_y =$res_pro_field['positionY_value'];
                                            $product_order_field->width_value =$res_pro_field['width_value'];
                                            $product_order_field->height_value =$res_pro_field['height_value'];
                                            $product_order_field->height_check_disable=$res_pro_field['height_check_disable'];
                                            // $product_order_field->width_check_disable=$res_pro_field['width_check_disable'];
                                            $product_order_field->save();
                                        }
                                    }
                                }
                                else{
                                    $product_order_field = new \App\Product_order_field();
                                    $product_order_field->productOrderId = $request->id;
                                    $product_order_field->stepId =$res_pro_field['stepId'];
                                    $product_order_field->fieldId =$res_pro_field['id'];
                                    $product_order_field->color =$res_pro_field['color_value'];
                                    $product_order_field->fontsize =$res_pro_field['fontsize_value'];
                                    $product_order_field->fontname =$res_pro_field['fontname_value'];
                                    $product_order_field->image =$res_pro_field['image_value'];
                                    $product_order_field->text =$res_pro_field['text_value'];
                                    $product_order_field->position_x =$res_pro_field['positionX_value'];
                                    $product_order_field->position_y =$res_pro_field['positionY_value'];
                                    $product_order_field->width_value =$res_pro_field['width_value'];
                                    $product_order_field->height_value =$res_pro_field['height_value'];
                                    $product_order_field->height_check_disable=$res_pro_field['height_check_disable'];
                                            // $product_order_field->width_check_disable=$res_pro_field['width_check_disable'];
                                    $product_order_field->save();
                                }
                            }
                        } // end IF product_field


                        if($request->player_name!="" || $request->player_number!="" || $request->player_size!="")
                        {
                            $player_name=explode(",",$request->player_name);
                            $player_number=explode(",",$request->player_number);
                            $player_size=explode(",",$request->player_size);
                             $ids=explode(",",$request->ids);
                            $data = array();
                            for($i=0;$i<count($player_name);$i++)
                             {
                                $flag=0;
                                if(isset($ids[$i])){
                                    $checkUp=\App\Player_setting::where(array("id"=>$ids[$i]))->get();
                                    if(count($checkUp)>0)
                                    {
                                      $flag=1;
                                    } 
                                }

                                if($flag==1)
                                {
                                    $playerupdate=\App\Player_setting::find($ids[$i]);
                                    $playerupdate->player_name=$player_name[$i];
                                    $playerupdate->player_number=$player_number[$i];
                                    $playerupdate->player_size=$player_size[$i];
                                    $playerupdate->save();
                                }
                                else{
                                    $playerupdate=new \App\Player_setting();
                                    $playerupdate->productOrderId=$request->id;
                                    $playerupdate->player_name=$player_name[$i];
                                    $playerupdate->player_number=$player_number[$i];
                                    $playerupdate->player_size=$player_size[$i];
                                    $playerupdate->save();
                                }

                             }
                        }

                    }
                    DB::commit();
                    return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
                }catch (\Throwable $e) {
                    DB::rollback();
                    return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
            }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }

    public function getProductCartList(Request $request)
    {

        $distributorId = $request->distributorId;
        $product_orderRs = \App\Product_order::
                    join('products','products.id','product_order.productId')
                    ->leftjoin('product_order_field','product_order_field.productOrderId','product_order.id')
                    ->leftjoin('order_front_back','order_front_back.productOrderId','product_order.id')
                    // ->leftjoin('product_design','product_design.id','product_order_field.designId')
                    ->leftJoin('product_design', function ($join) {
                            $join->on('product_order_field.designId', '=', 'product_design.id');
                            $join->orOn('product_order.productDesignId', '=', 'product_design.id');
                        })
                    ->where(array('product_order.distributorId'=>$distributorId))
                    ->select('product_order.id','products.name as product_name',"product_order.orderId","products.url_slug as productUrl_slug",'product_order.productDesignId as productDesignId','products.deleted as pro_deleted','product_design.design_slug',
                        DB::raw("DATE_FORMAT(product_order.created_at,'%h:%i %p') as createdTime"),
                       /* DB::raw('(select GROUP_CONCAT(ofb.type SEPARATOR "@") from order_front_back as ofb where product_order.id=ofb.productOrderId ) as image_type'),
                         DB::raw('(select GROUP_CONCAT(ofb1.image SEPARATOR "@") from order_front_back as ofb1 where product_order.id=ofb1.productOrderId ) as image')*/
                         DB::raw('(select ofb.image from order_front_back as ofb where product_order.id=ofb.productOrderId and ofb.type="front" ) as image_front'),
                        DB::raw('(select ofb1.image from order_front_back as ofb1 where product_order.id=ofb1.productOrderId and ofb1.type="back" ) as image_back')
                    )
                    ->orderBy('product_order.id','desc')
                    ->groupBy('product_order.id')
                    ->get();
                    
            $product_order=array();
            foreach ($product_orderRs as $quest) {
                $product_order[]=$quest;
            }

            $player_setting = \App\Player_setting::get();

        return json_encode(array("product_order"=>$product_order,"player_setting"=>$player_setting));
    }


   


    public function deleteCartProduct(Request $request)
    {
         DB::beginTransaction();
     try {
            \App\Player_setting::where(array('productOrderId'=>$request->id))->delete();
            \App\Order_front_back::where(array('order_front_back.productOrderId'=>$request->id))->delete();
            \App\Quotation_order::where(array('productOrderId'=>$request->id))->delete();
            \App\Product_order_field::join('product_order','product_order.id','product_order_field.productOrderId')
            ->where(array('product_order.id'=>$request->id))
            ->delete();

            \App\Product_order::where(array('product_order.id'=>$request->id))
            ->delete();
          
            DB::commit();
              return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
         } 
      catch (\Throwable $e) {
        DB::rollback();
        return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

    public function canvasImageUpload(Request $request)
    {
        $dataArray=array();
        $data = $request->imgBase64;

        // $data = 'data:image/png;base64,AAAFBfj42Pj4';

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);

        $rand=rand(1111,9999);
        $filename = $rand.time(). '.png';
        file_put_contents('public/images/front_back/' . $filename, $data);
      
                $result=\App\Order_front_back::where(array("productOrderId"=>$request->productOrderId,"type"=>$request->type))->get();

            if(count($result)>0)
            {   
                  if($result[0]->image!="" && file_exists(public_path('images/front_back/'.$result[0]->image)))
                  {
                        unlink(public_path('images/front_back/'.$result[0]->image));
                  }  
                  $Order_front_back=\App\Order_front_back::find($result[0]->id);
                  $Order_front_back->image=$filename;
                  $Order_front_back->save();
            }
            else
            {
                $dataArray[] = [
                    'image'=>$filename,
                    'type'=>$request->type,
                    'productOrderId'=>$request->productOrderId,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                \App\Order_front_back::insert($dataArray);

            }


        /*if(preg_match('/data:image\/(gif|jpeg|png);base64,(.*)/i', $data, $matches)) 
        {
            $imageType = $matches[1];
            $imageData = base64_decode($matches[2]);
            $image = imagecreatefromstring($imageData);
            $rand=rand(1111,9999);
            $filename = $rand.time().md5($imageData) . '.png';
            imagepng($image,'public/images/front_back/' . $filename);
            
            $result=\App\Order_front_back::where(array("productOrderId"=>$request->productOrderId,"type"=>$request->type))->get();

            if(count($result)>0)
            {   
                  if($result[0]->image!="" && file_exists(public_path('images/front_back/'.$result[0]->image)))
                  {
                        unlink(public_path('images/front_back/'.$result[0]->image));
                  }  
                  $Order_front_back=\App\Order_front_back::find($result[0]->id);
                  $Order_front_back->image=$filename;
                  $Order_front_back->save();
            }
            else
            {
                $dataArray[] = [
                    'image'=>$filename,
                    'type'=>$request->type,
                    'productOrderId'=>$request->productOrderId,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                \App\Order_front_back::insert($dataArray);

            }

        }*/
    
    }

    public function captureProductDesignImageUpload(Request $request)
    {

        $data = $request->imgBase64;
        if(preg_match('/data:image\/(gif|jpeg|png);base64,(.*)/i', $data, $matches)) 
        {
            $imageType = $matches[1];
            $imageData = base64_decode($matches[2]);
            $image = imagecreatefromstring($imageData);
            $rand=rand(1111,9999);
            $filename = $rand.time().md5($imageData) . '.png';
            imagepng($image,'public/images/' . $filename);
                
            $update= \App\Product_design::find($request->id);
            $update->profile=$filename;
            $update->save();

        
        }
    
    }

    public function deletePlayer(Request $request)
    {
         DB::beginTransaction();
     try {

            \App\Player_setting::where(array('id'=>$request->id))
            ->delete();
          
            DB::commit();
              return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
         } 
      catch (\Throwable $e) {
        DB::rollback();
        return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

    public function uploadLogo(Request $request)
    {

        $imageName="";
        if($request->hasFile('file')) 
        {
            $image = $request->file;
            $extension = $image->getClientOriginalExtension();
            $imageName = rand(111,999).time().".".$extension;
            $image->move(public_path('images/design_logo'),$imageName); 
            $data = ['src'=> url('/')."/public/images/design_logo/$imageName",'imageName'=>$imageName];
        }
        return response()->json(array('resp' => 'ok','success' => true,"msg"=>'','result'=>$data)); 
    }

    public function deleteProductLogo(Request $request)
    {
         DB::beginTransaction();
     try {

            if(file_exists(public_path('images/design_logo/'.$request->imageName)))
            {
                unlink(public_path('images/design_logo/'.$request->imageName));
            }  

            $check = DB::table('product_order_field')
                ->where('image', $request->imageName)->get();
            if(!empty($check))
            {
                DB::table('product_order_field')
                ->where('image', $request->imageName)
                ->update(['image' => null]);    
            }
            
            \App\Player_setting::where(array('id'=>$request->id))
            ->delete();
          
            DB::commit();
              return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
         } 
      catch (\Throwable $e) {
        DB::rollback();
        return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

    public function emailToFriend(Request $request)
    {

        if($request->post())
        {
             DB::beginTransaction();
            try {
                $template_id= '9492d0a9-3315-46bf-980d-253a9e0d23de'; //config("sendgridTemplateIds.Email_to_friend");
                $to_name =  $request->name;  
                $to_email =  $request->email;  
                $main_url = url('/');
                $send_url=$request->send_url;

                $from_name='Admin';
                $from_email= config('constants.admin_email');
                
                $subject= $to_name." shared a uniform they've designed with you!";  //"Welcome to the Uniform Builder" ;
                
                $data = array(':admin_email' => array($from_email),':user_name' => array($to_name),':user_email' => array($to_email),':send_url'=>array($send_url),':main_url'=>array($main_url));

                \App\Sendgrid::send_mail( $subject , $template_id , $data , $to_email , $to_name, $from_email ); 

                DB::commit();

            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }

    public function getCartProductDetail(Request $request)
    {

        $product_order_detail = \App\Product_order::leftjoin('product_order_field','product_order_field.productOrderId','product_order.id')
            ->leftjoin('player_setting','player_setting.productOrderId','product_order.id')
            ->leftjoin('order_front_back','order_front_back.productOrderId','product_order.id')
            ->where(array('product_order.id'=>$request->id))
            ->select('product_order.id',
                DB::raw('(select ofb.image from order_front_back as ofb where product_order.id=ofb.productOrderId and ofb.type="front" ) as image_front'),
                DB::raw('(select ofb1.image from order_front_back as ofb1 where product_order.id=ofb1.productOrderId and ofb1.type="back" ) as image_back')
            )
            ->groupBy('product_order.id')
            ->get();

        $field_basic_dtl=\App\Product_order_field::leftjoin('product_field','product_field.id','product_order_field.fieldId')
            ->join('product_order','product_order.id','product_order_field.productOrderId')
            ->where(array('product_order_field.productOrderId'=>$request->id))
            ->whereNotNull('product_order_field.fieldId')
            ->select('product_order_field.*','product_field.name as field_name','product_field.filed_type',
                DB::raw('(select color.name from color where color.code=product_order_field.color and color.distributorId=product_order.distributorId) as color_name')
                )
            ->get();

        $design_basic_dtl=\App\Product_order_field::leftjoin('design_filed','design_filed.id','product_order_field.designfieldId')
        ->join('product_order','product_order.id','product_order_field.productOrderId')
            ->where(array('product_order_field.productOrderId'=>$request->id))
            ->whereNotNull('product_order_field.designfieldId')
            ->select('product_order_field.*','design_filed.name as design_name',
            DB::raw('(select color.name from color where color.code=product_order_field.color and color.distributorId=product_order.distributorId) as color_name')    
            
           )
            ->get();

        $player_setting_Detail=\App\Player_setting::where(array('productOrderId'=>$request->id))->get();    

        return json_encode(array("product_order_detail"=>$product_order_detail,'player_setting_Detail'=>$player_setting_Detail,'field_basic_dtl'=>$field_basic_dtl,'design_basic_dtl'=>$design_basic_dtl));
    }

    public function saveQuote(Request $request)
    {
        if($request->post())
        {
           
            DB::beginTransaction();
            try {
                    $getquote= new \App\GetQuote();
                    $getquote->distributorId=$request->distributorId;
                    $getquote->company_name=$request->company_name;
                    $getquote->email=$request->email;
                    $getquote->mobile=$request->mobile;
                    $getquote->message=$request->message;
                    $getquote->notification_status= "unread";
                   if($getquote->save())
                    {
                        $product_order1 = \App\Product_order::where(array('product_order.distributorId'=>$request->distributorId))->select('product_order.id','product_order.orderId')->get(); 
                        
                        foreach ($product_order1 as $res) {
                            $quotation_order = new \App\Quotation_order();
                            $quotation_order->quoteId = $getquote->id;
                            $quotation_order->productOrderId = $res->id;
                            $quotation_order->save();
                        }
                        $quotation_detail = $this->quoteDetail($getquote->id);



                        $quotation_template = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="width=device-width" name="viewport" />
    <!--[if !mso]><!-->
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <!--<![endif]-->
    <title></title>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,400&display=swap" rel="stylesheet">
    <!--[if !mso]><!-->
    <!--<![endif]-->
    <style type="text/css"> 
        body {margin: 0; padding: 0;font-family: "Montserrat-Regular,arial", sans-serif; font-size: 14px; } 
        table, td, tr {vertical-align: top; border-collapse: collapse; } 
        * {line-height: inherit; } 
        a[x-apple-data-detectors=true] {color: inherit !important; text-decoration: none !important;  }
        a{display: inline-block; text-decoration: none;}
        hr{ margin: 20px auto; border-top: 1px solid rgba(0,0,0,.5) !important; background: none; border: none }
        .border_less, .border_less td, .border_less th{ border: none !important }
        .table_border, .table_border td, .table_border th{ border:  1px solid rgba(0,0,0,.2) !important }   
        .message_table_border, .message_table_border td, .message_table_border th{ border:  1px solid rgba(0,0,0,.2) !important }       
        .table_Stirp{ border: 1px solid rgba(0,0,0,.2); padding: 3px }
        .table_Stirp tr:nth-child(even), .table_border th {background: #ddd}
        table td, table th{ padding: 7px 10px }
        table.nl-container td, table.nl-container  th{ padding:5px !important}

        p{ margin: 0 }

        .txt_normal{font-weight: 300}
        .txt_sb{font-weight: 600}
        .txt_bold{font-weight: 700}
        .txt_black{font-weight: 800}
        .w-100{ width: 100% }

        .p-1{ padding: 10px; }
        .p-2{ padding: 20px; }
        .pt-1{ padding-top: 10px; }
        .pt-2{ padding-top: 20px; }
        .pb-1{ padding-bottom: 10px; }
        .pb-2{ padding-bottom: 20px; }
        .px-2{ padding-left:20px; padding-right: 20px}
        .px-3{ padding-left:30px; padding-right: 30px}
        .py-2{ padding-top:20px; padding-bottom: 20px}

        .m-auto{ margin-left: auto; margin-right: auto; }
        .mx-2{ margin-left: 20px; margin-right: 20px; }
        .my-2{ margin-top: 20px; margin-bottom: 20px; }
        .mb-2{ margin-bottom: 20px }
        .mb-3{ margin-bottom: 30px }

        .gary_dark{background-color:#263339; color: #8c9497}
        .gary_light{background-color:#45555d; color: #8c9497}

        .gary_dark a, .gary_light a{color: #8c9497}

        .font_12{ font-size: 12px }
        .font_14{ font-size: 14px }
        .font_18{ font-size: 18px }
        .font_20{ font-size: 20px }
        .text-center{ text-align: center; }
        .text-right{ text-align: right; }
        .text-left{ text-align: left; }

        .color_box{ display: inline-block; width: 15px; height: 15px; border-radius: 4px; border: 1px solid rgba(0,0,0,.2); margin: 0 5px; vertical-align: -2px;}
    </style>
    <style id="media-query" type="text/css">
        @media (max-width: 660px) {
            .col {min-width: 320px !important; width: 100% !important; display: block !important; } 
            .col>div {margin: 0 auto; } 
            img.fullwidth, img.fullwidthOnMobile {max-width: 100% !important; } 
            .no-stack .col {min-width: 0 !important; display: table-cell !important; } 
            .no-stack.two-up .col {width: 50% !important; } 
            .no-stack .col.num4 {width: 33% !important; } 
            .no-stack .col.num8 {width: 66% !important; } 
            .no-stack .col.num4 {width: 33% !important; } 
            .no-stack .col.num3 {width: 25% !important; } 
            .no-stack .col.num6 {width: 50% !important; } 
            .no-stack .col.num9 {width: 75% !important; } 
            .video-block {max-width: none !important; } 
            .mobile_hide {min-height: 0px; max-height: 0px; max-width: 0px; display: none; overflow: hidden; font-size: 0px; } 
            .desktop_hide {display: block !important; max-height: none !important; }
        }
    </style>
</head>

<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #f6f8f8;">
    <!--[if IE]><div class="ie-browser"><![endif]-->
    <table bgcolor="#f6f8f8" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; margin: 0 auto; border-spacing: 0; border-collapse: collapse; background-color: #f6f8f8; width: 100%;" valign="top" width="100%">
        <tbody>
            <tr style="vertical-align: top;" valign="top">
                <td style="word-break: break-word; vertical-align: top;" valign="top">
                    <div class="top">
                        <table class="border_less col gary_light w-100" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="text-center">
                                        <a href="#" class="py-2"><img alt="Image" border="0" class="left autowidth" src="'.asset('public/images/Logo.png').'" title="Image" width="200"></a>
                                    </div>
                                </td>
                            </tr> 
                        </table>
                    </div>

                    <div class="body-cnt p-2">
                        <section class="mb-3">
                            <h3 class="text-center">Testing - Quote form</h3>
                            <div class="text-center">
                                <table class="col text-left m-auto table_Stirp message_table_border" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Company name</th>
                                        <td colspan="2">'.$request->company_name.'</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td colspan="2"><a href="">'.$request->email.'</a></td>
                                    </tr>
                                    <tr>
                                        <th>Telephone</th>
                                        <td colspan="2"><a href="">'.$request->mobile.'</a></td>
                                    </tr>
                                    
                                    <tr>
                                        <th>Message</th>
                                        <td colspan="2">'.$request->message.'</td>
                                    </tr>
                                    <tr>
                                        <th>Date Sent UTC</th>
                                        <td colspan="2">'.$request->send_date.'</td>
                                    </tr>
                                   
                                </table>
                            </div>
                        </section>';

        foreach ($quotation_detail as $res) {
                         
            $quotation_template .= ' <section class="mb-3">
                            <h5><span class="txt_normal">'.$res->category_name.'</span> / <span>'.$res->product_name.'</span> / <span>'.$res->design_name.'</h5>
                            <div class="mb-2">
                                <table class="col text-left m-auto" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="text-center px-3">
                                            <img src="'.asset('/public/images/front_back/').'/'.$res->image_front.'" class="w-100" />
                                        </td>
                                        <td class="text-center px-3">
                                            <img src="'.asset('/public/images/front_back/').'/'.$res->image_back.'" class="w-100" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="mb-2">
                                <table class="col text-left m-auto" cellpadding="0" cellspacing="0">';
                                foreach ($res->designData as $design_res) {
                    $quotation_template .='<tr>
                                        <th>'.$design_res->design_name.'</th>
                                        <td colspan="2">'.$design_res->color_name.' ('.$design_res->color.') <span class="color_box" style="background:'.$design_res->color.' "></span></td>
                                    </tr>';
                                  }
                    $quotation_template .='</table>
                            </div>';
                
                
                    $quotation_template .='<div class="mb-2">
                                <table class="col text-left m-auto table_border" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Text</th>
                                        <th>Colour</th>
                                        <th>Font Size</th>
                                        <th>Position X</th>
                                        <th>Position Y</th>
                                  
                                    </tr>';
                                    foreach ($res->fieldData as $field_res) {
                                  if($field_res->text!="" && $field_res->text!=null){
                         $quotation_template  .=          ' <tr>
                                        <td>'.$field_res->text.'</td>
                                        <td>'.$field_res->color.'</td>
                                        <td>'.$field_res->fontsize.'</td>
                                        <td>'.$field_res->position_x.'</td>
                                        <td>'.$field_res->position_y.'</td>
                                       
                                    </tr>';
                                    }
                                 }  
                          $quotation_template .='</table>
                            </div>';
                
                    $quotation_template .='</section>';
        }
                $quotation_template .='</div>

                    <div class="footer">
                        <table class="border_less col gary_light w-100" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2">
                                    <div class="text-center pt-2">
                                        <a href="#"> <img alt="Image" border="0" class="left autowidth" src="'.asset('public/images/Logo.png').'" title="Image" width="126"> </a>
                                    </div>
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="" class="px-2 pb-2">+1 0000 000000</a>
                                </td>
                                <td class="text-right">
                                    <a href="" class="px-2 pb-2">support@domain.com</a>
                                </td>
                            </tr>
                        </table>
                        <table class="border_less col gary_dark w-100" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <p class="font_12 p-2">Etiam quis tempus ex. Sed vitae ipsum suscipit, ultricies odio vitae, suscipit massa. Sed tempus ipsum eget diam aliquam maximus. Cras accumsan urna vel rutrum lobortis. Maecenas tristique purus vel ex tempor consequat. Curabitur dui massa, congue sed sem at, rhoncus imperdiet sem. Fusce ac orci fermentum, malesuada dolor a, cursus augue. Quisque porttitor sapien arcu, quis iaculis nisi faucibus eget. Vestibulum eu velit rhoncus, aliquam ante eget, tristique diam.</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <!--[if (IE)]></div><![endif]-->
</body>

</html>';

     
                        $template_id= '5f6c1bb4-cc3b-493a-97c7-904edfe5667d'; 
                        $to_name =  "";  
                        $to_email =  $request->email; 

                        $from_name='Admin';
                        $from_email= config('constants.admin_email');
                        $mobile= $request->mobile;
                        $message=$request->message;
                        $company_name=$request->company_name;
                        $send_date=$getquote->created_at;

                        $subject= " Quote form Reply";  //"Welcome to the Uniform Builder" ;
                        
                        $data = array(':admin_email' => array($from_email),':mobile' => array($mobile),':user_email' => array($to_email),':quotation_template' => array($quotation_template));

                        \App\Sendgrid::send_mail( $subject , $template_id , $data , $to_email , $to_name, $from_email ); 
                    }

                    DB::commit();
                    return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
                }catch (\Throwable $e) {
                    DB::rollback();
                    return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
            }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }

    
    public function quoteDetail($quoteId)
    {   
        $quotation_detail=array();
        $quote_order = \App\Quotation_order::where(array('quotation_order.quoteId'=>$quoteId))->select('*')->get(); 
        foreach ($quote_order as $res) {
            $quotation_detailRs = \App\Product_order::leftjoin('product_order_field','product_order_field.productOrderId','product_order.id')

            ->leftjoin('products','products.id','product_order.productId')
            ->leftjoin('category','category.id','products.categoryId')
            ->leftJoin('product_design', function ($join) {
                $join->on('product_order_field.designId', '=', 'product_design.id');
                $join->orOn('product_order.productDesignId', '=', 'product_design.id');
            })

            ->leftjoin('order_front_back','order_front_back.productOrderId','product_order.id')
            ->where(array('product_order.id'=>$res->productOrderId))
            ->select('product_order.*','products.name as product_name','category.name as category_name','product_design.name as design_name',
                DB::raw('(select ofb.image from order_front_back as ofb where product_order.id=ofb.productOrderId and ofb.type="front" ) as image_front'),
                DB::raw('(select ofb1.image from order_front_back as ofb1 where product_order.id=ofb1.productOrderId and ofb1.type="back" ) as image_back')
            )

            ->groupBy('product_order.id')
            ->get();

            foreach ($quotation_detailRs as $res_field) {
                $res_field->designData=$this->designData($res_field->id);
                $res_field->fieldData=$this->fieldData($res_field->id);
                $quotation_detail[]=$res_field;
            }
        }

        return $quotation_detail;
    }

    public function fieldData($productOrderId)
    {
        $field_basic_dtl=\App\Product_order_field::leftjoin('product_field','product_field.id','product_order_field.fieldId')
            ->join('product_order','product_order.id','product_order_field.productOrderId')
            ->where(array('product_order_field.productOrderId'=>$productOrderId))
            ->whereNotNull('product_order_field.fieldId')
            ->select('product_order_field.id','product_order_field.color','product_order_field.text','product_order_field.fontsize','product_order_field.position_x','product_order_field.position_y','product_order_field.image','product_field.name as field_name','product_field.filed_type',
                DB::raw('(select color.name from color where color.code=product_order_field.color and color.distributorId=product_order.distributorId) as color_name')
                )
            ->get();

            return $field_basic_dtl;
    }

     public function designData($productOrderId)
    {
    $design_basic_dtl=\App\Product_order_field::leftjoin('design_filed','design_filed.id','product_order_field.designfieldId')
            ->join('product_order','product_order.id','product_order_field.productOrderId')
            ->where(array('product_order_field.productOrderId'=>$productOrderId))
            ->whereNotNull('product_order_field.designfieldId')
            ->select('product_order_field.id','product_order_field.color','design_filed.name as design_name',
            DB::raw('(select color.name from color where color.code=product_order_field.color and color.distributorId=product_order.distributorId) as color_name')    
           )
            ->get();
             return $design_basic_dtl;
    }
}
