<?php
namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Category;
use DB;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('App\Http\Middleware\UserMiddleware');

    }
    public function index()
    {
         
      $category=Category::where(array("userId"=>Auth::user()->id,"parentId"=>null))->pluck("name","id")->prepend('All Categories', '');
      return view('setting.category.index',array("category"=>$category));

    } //index

      public function select()
    {
        return view('common.select');
    }

    public function table()
    {
        return view('setting.category.table');

    }//table

    public function row()
    {

        return view('setting.category.row');

    } //row
    public function getSubCategory(Request $request){
      $id=$request->id;
      if($id=="")
      {
        $id=0;
      }

      $result=Category::where("parentId",$id)->get();

      return json_encode(array("subcategory"=>$result));

    }
    public function getList(Request $request)
    {
      
       $category=$request->category;
        $subcategory=$request->subcategory;

         $data=array("category"=>$category,"subcategory"=>$subcategory);
      $categorys=Category::
    //  leftjoin("category as parentcategory",'parentcategory.id','category.parentId')
       leftjoin("category as childcategory",'childcategory.parentId','category.id')
      ->Where('category.userId', Auth::user()->id)
      ->Where('category.name', 'like', '%' . $request->search.'%')
      ->when($data,function($q) use ($data)
          {
              if ($data['subcategory']=="" && $data['category']!="") {
                $q->where('category.parentId',$data['category']);
                $q->orwhere('category.id',$data['category']);
              }
              else if($data['subcategory']!="")
              {
               // $q->orwhere('category.id',$data['category']);
                $q->whereIn('category.id',array($data['subcategory'],$data['category']));
              }
            
        })
      ->select("category.name",'category.id','category.parentId','childcategory.id as childcategoryid','category.url_slug','category.image','category.status')
      ->orderBy("category.id","desc")
      ->groupBy("category.id")
      ->paginate(config('constants.categorylimit'));
     	return json_encode(array("category"=>$categorys,"pagination"=>str_replace('/?','?',$categorys->render('common.pagination')) ));
    	
 
    } // getCategory

    public function create()
    {
    	$category=Category::where(array("userId"=>Auth::user()->id,"parentId"=>null))->pluck("name","id")->prepend('Select Category', '');
    	return view('setting.category.editor',array("category"=>$category));
    } //create

    public function store(Request $request)
    { 

        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'url_slug' => 'regex:/^[a-z0-9\-]+$/u|unique:category,url_slug',
                 'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
           
             DB::beginTransaction();
          try {
              
                $imageName="";
               if($request->hasFile('image')) 
               {
                  $image = $request->profile;
                  $extension = $image->getClientOriginalExtension();
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data = url('/')."/public/images/$imageName";
               }
              $category=new Category();
              $category->userId = Auth::user()->id;
              $category->parentId = $request->categoryId;
              $category->url_slug = $request->url_slug;
              $category->image = $imageName;
              if($request->status==true)
              {
                $category->status = "active";  
              }
              else{
                $category->status = "deactive";  
              }
              $category->name=$request->name;
              $category->save();
             
              DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function update(Request $request)
    {
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'url_slug' => 'regex:/^[a-z0-9\-]+$/u|unique:category,url_slug,'.$request->id,
                 'image' => 'image|mimes:jpeg,png,jpg|max:2048|nullable',
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }

             DB::beginTransaction();
          try {
          
               
              $category=Category::find($request->id);
              $category->userId = Auth::user()->id;
              $category->parentId = $request->categoryId;
              $category->url_slug = $request->url_slug;
              if($request->hasFile('image')) {
                  if($category->image!="" && file_exists(public_path('images/'.$category->image)))
                  {
                        unlink(public_path('images/'.$category->image));
                  }
                  $image = $request->image;
                  $extension = $image->getClientOriginalExtension();
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data = url('/')."/public/images/$imageName";
                  $category->image=$imageName;
              }
              if($request->status==true)
              {
                $category->status = "active";  
              }
              else{
                $category->status = "deactive";  
              }
              $category->name=$request->name;
              $category->save();
             
            DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }
    
     public function destroy($id,Request $request)
    {
      if($request->post())
      {
          $ids = explode(",",$request->id);
            
            DB::beginTransaction();
         try {
                Category::whereIn("id",$ids)->delete();

                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }
}