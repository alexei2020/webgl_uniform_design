<?php
namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Businessunit;
use App\User;
use App\Units;
use DB;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
use App\UserUnits;
use App\Departments;
use App\UserDepartments;
use Mail;


class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('App\Http\Middleware\UserMiddleware');
    }
    public function index()
    {
      
       /*$businessunit=
       \App\CompaniesBusinessunit::join("businessunit",'businessunit.id',"companies_businessunit.businessunitId")
      ->where(array("companyId"=>Auth::user()->companyId))
      ->pluck("businessunit.code","businessunit.id")
      ->prepend('All BUs', '');*/

      $businessunit=
       Businessunit::where(array("companyId"=>Auth::user()->companyId))
      ->pluck("businessunit.code","businessunit.id")
      ->prepend('All BUs', '');

     
        return view('setting.users.index',array("businessunit"=>$businessunit));
    }
    public function table()
    {
        return view('setting.users.table');
    }
    public function row()
    {
        return view('setting.users.row');
    }
    public function getUnit()
    {
      $data=array();
       $units=Units::
        join('businessunit','businessunit.id','units.businessunitId')
       ->Where('businessunit.companyId', Auth::user()->companyId)
       ->select("units.id","units.name","units.businessunitId")->get();
       foreach ($units as $unit) {
        $data[]=array("id"=>$unit->id,"name"=>$unit->name,"bid"=>intval($unit->businessunitId));
       }

      return response()->json(array('resp'=>'ok','data'=>$data,'success'=>true,'msg'=>''));

    }
    public function getDepartment()
    {
      $data=array();
       $departments=Departments::
       join('businessunit','businessunit.id','departments.businessunitId')
       ->Where('businessunit.companyId', Auth::user()->companyId)
       ->select("departments.id","departments.name","departments.businessunitId")->get();
       foreach ($departments as $department) {
        $data[]=array("id"=>$department->id,"name"=>$department->name,"bid"=>intval($department->businessunitId));
       }

      return response()->json(array('resp'=>'ok','data'=>$data,'success'=>true,'msg'=>''));

    }

    public function getList(Request $request)
    {
      $businessunit=$request->businessunit;
      $unit=$request->unit;
      $search = $request->search;
    	$usersUnit=User::
      leftjoin("model_has_roles",'users.id','model_has_roles.model_id')
      ->leftjoin("businessunit",'businessunit.id','users.businessunitId')
      ->leftjoin("user_departments",'user_departments.userId','users.id')
      ->leftjoin("departments",'departments.id','user_departments.departmentId')
      ->leftjoin("user_units",'user_units.userId','users.id')
      ->leftjoin("units",'units.id','user_units.unitId')
      ->leftjoin("departments as dept","dept.id","units.departmentId")

      // ->Where('users.name', 'like', '%' . $request->search . '%')

      ->Where(function($q) use ($search)
        {
            $q->orWhere('users.name', 'like', '%' . $search . '%');
            $q->orWhere('units.name', 'like', '%' . $search . '%');
            $q->orWhere('users.email', 'like', '%' . $search . '%');
        })

      ->Where('businessunit.companyId', Auth::user()->companyId)
      ->WherenotIn('users.id', array(Auth::user()->id))
         ->when($businessunit,function($q) use ($businessunit)
        {
            if ($businessunit!="" ) {
              $q->where('businessunit.id',$businessunit);
            }
        })
        ->when($unit,function($q) use ($unit)
        {
            if ($unit!="" ) {
              $q->where('units.id',$unit);
            }
        })
    	->select("users.name","users.type","users.email",'users.id','users.unitId','users.status','model_has_roles.role_id as role','businessunit.code as bname','businessunit.id as bid',DB::raw("(GROUP_CONCAT(departments.name SEPARATOR ', ')) as dname"),DB::raw("(GROUP_CONCAT(departments.id SEPARATOR ',')) as departmentId"),DB::raw("(GROUP_CONCAT(units.name SEPARATOR ', ')) as uname"),DB::raw("(GROUP_CONCAT(units.id SEPARATOR '@')) as unitId"),DB::raw("(GROUP_CONCAT(dept.name SEPARATOR ', ')) as dept_name"))
    	->orderBy("id","desc")
      ->groupBy("users.id")
    	->paginate(config('constants.limit'));

      

    	return json_encode(array("users"=>$usersUnit,"pagination"=>str_replace('/?','?',$usersUnit->render('common.pagination')) ));
    	
    }
    public function create()
    {
 
       $businessunit=
       Businessunit::where(array("companyId"=>Auth::user()->companyId))
      ->pluck("businessunit.code","businessunit.id")
      ->prepend('Select BU', '');

      $role=\App\Role::WherenotIn('id',array(1,2))->pluck("name","id")->prepend('Select Role', '');
    	return view('setting.users.editor',array("businessunit"=>$businessunit,"role"=>$role));
    }

    public function store(Request $request)
    { 

        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'email' =>'required|string|max:255|unique:users,email',
                 // 'unit' => 'required',
                 // 'department' => 'required',
                 'bu'=>'required',
                 'password' => 'required|same:password|min:8',
                 'confirmpassword' => 'required|same:password|min:8',
                 'role'=>'required'
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
              
            }

             DB::beginTransaction();
          try {
                
              $type=config('constants.usertype');

              $user=new User();
              $user->companyId=Auth::user()->companyId;
              $user->name=$request->name;
              $user->email=$request->email;
              $user->type=$type[$request->role];
              $user->businessunitId=$request->bu;
              $user->password=Hash::make($request->password);
              // $user->save();
              if($user->save())
              {
                if(!empty($request['unit']))
                {
                  foreach ($request->unit as $unit)
                  {
                      
                    $user_units =new UserUnits();
                    $user_units->unitId=$unit;
                    $user_units->userId=$user->id;
                    $user_units->save();
                        
                  }
                }
                if(!empty($request['department']))
                {
                  foreach ($request->department as $department)
                  {
                      
                    $user_departments =new UserDepartments();
                    $user_departments->departmentId=$department;
                    $user_departments->userId=$user->id;
                    $user_departments->save();
                        
                  }
                }
              }
             
              $query = array('role_id'=>$request->role,'model_type'=>'App\User','model_id'=>$user->id);
                DB::table('model_has_roles')->insert($query);

               
                /*$data = array('password' =>$request->password,'name' =>$request->name,'email' =>$request->email,'from' => $request->email);

                Mail::send('emails.companyRegister', $data, function($message) use ($data)
              {
                  $message->from(config('constants.admin_email'), "Audit Tool");
                  $message->subject("Welcome to Audit Tool");
                  $message->to($data['email']);
              });*/

              $company_name = \App\Companies::where(array('id'=>Auth::user()->companyId))->first();

                 $role=ucfirst($type[$request->role]);
                 if($role=="Manager")
                 {
                    $template_id = config("sendgridTemplateIds.Welcome_mail_on_Manager_creation");
                 } 
                 else if($role=="Auditor")
                 {
                    $template_id = config("sendgridTemplateIds.Welcome_mail_on_Auditor_creation");
                 }  
                 else if($role=="User")
                 {
                    $template_id = config("sendgridTemplateIds.Welcome_mail_on_User_creation");
                 } 
                 else if($role=="Super user")
                 {
                    $template_id = config("sendgridTemplateIds.Welcome_mail_on_SuperUser_creation");
                 } 

                $to_name = $request->name;  
                $to_email = $request->email;  
                $url_link = url('/')."/account";
                
                $company_name=$company_name->name;
                $from_name='Admin';
                $from_email=config('constants.admin_email');
                $user_password = $request->password;
                
                $subject="Welcome to the Audit Platform" ;
                
                $data = array(':admin_email' => array($from_email),':user_name' => array($to_name),':user_email' => array($to_email),':role'=>array($role),':company_name'=>array($company_name),':user_password'=>array($user_password),':url_link'=>array($url_link));

                \App\Sendgrid::send_mail( $subject , $template_id , $data , $to_email , $to_name, $from_email );  
               


            DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
   
     public function update($id,Request $request)
    {

         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'email' => 'required|string|max:255|unique:users,email,'.$request->id,
                 'name' =>'required|string|max:255',
                // 'unit' => 'required',
                 'bu'=>'required',
                 'role'=>'required'
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }

            DB::beginTransaction();
         try {

            $type=config('constants.usertype');
            $user=User::find($request->id);
            $user->name=$request->name;
            $user->email=$request->email;
            $user->businessunitId=$request->bu;
            $user->type=$type[$request->role];
            $user->save();
           
              $query = array('role_id'=>$request->role);
               DB::table('model_has_roles')->where(array('model_id'=>$user->id))->update($query);
             UserDepartments::where(array('userId'=>$request->id))->delete();
             UserUnits::where(array('userId'=>$request->id))->delete();
             if(!empty($request['unit']))
                {
                  
                  foreach ($request->unit as $unit)
                  {
                      
                    $user_units =new UserUnits();
                    $user_units->unitId=$unit;
                    $user_units->userId=$request->id;
                    $user_units->save();
                        
                  }
                }
                if(!empty($request['department']))
                {
                  
                  foreach ($request->department as $department)
                  {
                      
                    $user_departments =new UserDepartments();
                    $user_departments->departmentId=$department;
                    $user_departments->userId=$user->id;
                    $user_departments->save();
                        
                  }
                }


            DB::commit();
           
           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
             DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }


    public function changeUserStatus(Request $request)
    {
        if($request->post())
        {
            DB::beginTransaction();
            try {
                  $user = User::find($request->id);
                  if($user->status=="active")
                  {
                    $user->status = "deactive";
                  }
                  else if($user->status=="deactive")
                  {
                    $user->status = "active";
                  }
                  if($user->status=="deactive") 
                  {
                      $this->sendMailToChangeUserStatus($request->id);
                  } 
                  $user->save();
                    
                  DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>"Status change successfully")); 
            } 
            catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp' => 'ok','success' => false,"msg"=>"Status not change"),404); 
            }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>"Status not change"),404); 
        }
    }

     public function destroy($id,Request $request)
    {

       if($request->post())
        {
        
            DB::beginTransaction();
         try {
                $user = User::where(array("id"=>$request->id))->first();
                $user_unit =  UserUnits::where(array("userId"=>$request->id))->get();
               
               $user_departments = UserDepartments::where(array("userId"=>$request->id))->get();

                if(!empty($user_unit))
                {
                  UserUnits::where(array("userId"=>$request->id))->delete();
                }
                if(!empty($user_departments))
                {
                    UserDepartments::where(array("userId"=>$request->id))->delete();
                }
                User::where(array("id"=>$request->id))->delete();

                /* Send mail if user is deleted */
                
                $template_id=config("sendgridTemplateIds.Mail_if_User_deleted");;
                $to_name = $user->name;  
                $to_email = $user->email;  
                
                $from_name='Admin';
                $from_email=config('constants.admin_email');
                
                $subject="Deletion of User ID" ;
                
                $data = array(':admin_email' => array($from_email),':user_name' => array($to_name),':user_email' => array($to_email));

                \App\Sendgrid::send_mail( $subject , $template_id , $data , $to_email , $to_name, $from_email ); 


                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')), 404); 
          }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')), 404); 
        }

    } //- destroy


    public function sendMailToChangeUserStatus($id)
    {
      $user = User::find($id);
      $to_name = $user->name;  
      $to_email = $user->email;  
      $from_name='Admin';
      $from_email=config('constants.admin_email');
      $template_id = config("sendgridTemplateIds.Mail_send_if_user_deactivate");
      $subject="Deactivation of User ID" ;
      
      $data = array(':admin_email' => array($from_email),':user_name' => array($to_name),':user_email' => array($to_email));
      \App\Sendgrid::send_mail( $subject , $template_id , $data , $to_email , $to_name, $from_email );  
    }
}