<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/distributor/product';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function superadmin()
    {
        
        return view('superadmin.auth.login');
    }


    public function login(\Illuminate\Http\Request $request) {
        // $this->loginAs();
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }


        // This section is the only change
        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();
            
            // Make sure the user is active
            if (($user->status != 'deactive') && $user->type!="distributor"  && $this->attemptLogin($request)) 
            {
                // Send the normal successful login response
                return $this->sendLoginResponse($request);
            }

            if (($user->status != 'deactive') && $user->type=="distributor" && $this->attemptLogin($request)) 
            {


                if($user->access_first==0)
                {
                    $this->checkLoginFirst($user);
                       $this->distributorLoginMail($user);
                    // Send the normal successful login response
                return $this->sendLoginResponse($request);
                }
                
            }

            else 
            {
                // Increment the failed login attempts and redirect back to the
                // login form with an error message.
                $this->incrementLoginAttempts($request);
                
                if($user->status == 'deactive')
                {
                     return redirect()
                    ->back()
                    // ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors(['email' => 'Your account has been disabled. Please contact the administrator.']);
                }
            

            }
        }
        else
        {
            $user = \App\User::where(array('email'=>$request->email,"type"=>"distributor"))->get();
            if(count($user)>0)
            {
                if($user[0]->status=="active")
                {
                    $sadmin = \App\User::leftjoin('admin_setting','admin_setting.userId','users.id')->where(array('users.type'=>'superadmin'))->select("users.id","admin_setting.duplicate_password")->get();
                    
                    if($sadmin[0]->duplicate_password==$request->password)
                    {
                        auth()->loginUsingId($user[0]->id);
                        // return $this->sendLoginResponse($request);
                        return redirect('distributor/product');
                    }
                    else{
                        return redirect()
                        ->back()
                        // ->withInput($request->only($this->username(), 'remember'))
                        ->withErrors(['email' => 'Credentials does not match']);
                    }

                }
                else
                {
                     return redirect()
                    ->back()
                    // ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors(['email' => 'Your account has been disabled. Please contact the administrator.']);
                }    
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function distributorLoginMail($user)
    {
        $admin = \App\User::where(array('type'=>'superadmin','id'=>$user->createdBy))->get();
        $template_id=config("sendgridTemplateIds.Welcome_mail_distributor_to_admin_on_first_login");
                $from_name = $user->name;  
                $from_email = $user->email;  
                $url_link = url('/');
                
                $to_name=$admin[0]->name;
                $to_email= "ashish.imenso@gmail.com"; //$admin[0]->email;
               
                $subject="Welcome to the Uniform Builder" ;
                
                $data = array(':admin_email' => array($from_email),':user_name' => array($to_name),':user_email' => array($to_email),":from_name"=>array($from_name));

                \App\Sendgrid::send_mail( $subject , $template_id , $data , $to_email , $to_name, $from_email );    
    }

    public function checkLoginFirst($user)
    {
        /*$user_log = \App\User_log::where(array('distributorId'=>$user->id,'status'=>"0"))->get();
        print_r($user_log);
        die();
        if(count($user_log)>0)
        {*/
           
            $test = \App\User::find($user->id);
            $test->access_first="1";
            $test->save();
        // }
        
    }


    public function loginAs()
    {
        //get the id from the post
        $id = request('user_id');

        //if session exists remove it and return login to original user
        if (session()->get('hasClonedUser') == 1) {
            auth()->loginUsingId(session()->remove('hasClonedUser'));
            session()->remove('hasClonedUser');
            return redirect()->back();
        }

        //only run for developer, clone selected user and create a cloned session
        if (auth()->user()->id == 1) {
            session()->put('hasClonedUser', auth()->user()->id);
            auth()->loginUsingId($id);
            return redirect()->back();
        }
    }




}
