<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use Session;
use DB;
use Mail;
use Auth;
class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
   public function index()
    {
      return view('account.index');
    }
    public function edit()
    {
    
    	return view('account.editor');
    }

     public function update(Request $request)
    {
         if($request->post())
        {
        	  $getUserId=Auth::user()->id;
             $validator = Validator::make($request->all(), [
            	   'name' => 'required|string|max:255',
                 'email' =>'required|string|max:255|unique:users,email,'.$getUserId
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
               }
            if($request->passwordcheck=="true")
            {     
                $validator = Validator::make($request->all(), [
                 'current_password' => 'required',
                 'password' => 'required|same:password|min:8',
                 'confirmpassword' => 'required|same:password|min:8'
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
               }
            
              $current_password = Auth::User()->password;     
             if(!Hash::check($request->current_password, $current_password))
                {
                    return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("current_password"=>"current password is wrong")),400);
                      
                }
            }

             DB::beginTransaction();
          try {
          
             ///////// User /////
             
              $user=User::find($getUserId);
              $user->name=$request->name;
              $user->email=$request->email;
              if($request->passwordcheck=="true")
              {
                $user->password=Hash::make($request->password);
                $template_id = config("sendgridTemplateIds.Change_Password_mail");
                $to_name = $request->name;  
                $to_email = $request->email;  
                $from_email=config('constants.admin_email');
                
                $subject="Change of Password" ;
                
                $data = array(':admin_email' => array($from_email),':user_name' => array($to_name),':user_email' => array($to_email));

                \App\Sendgrid::send_mail( $subject , $template_id , $data , $to_email , $to_name, $from_email );  
              }
              $user->save();


            DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated'),404));
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated'),404));
        }
    }

}