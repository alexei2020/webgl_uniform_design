<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\QuoteForm;
use Session;
use DB;
use Rule;
use Mail;
use Auth;

class FormController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     public function index()
    {
        return view('distributor.forms.index');
    }
    public function table()
    {
        return view('distributor.forms.table');
    }
    public function select()
    {
        return view('common.select');
    }
    public function row()
    {
        return view('distributor.forms.row');
    }
    public function getList(Request $request)
    {

         $search = $request->search;

        $sortBy = $request->sortBy;
        $sortName = $request->sortName;

        if($sortName=="name")
        {
          $sortName="quote_form.name";
        }
        else if($sortName=="email")
        {
            $sortName="quote_form.email"; 
        }
        else
        {
          $sortName="quote_form.".$sortName;
        }



    	$companies=QuoteForm::
        where(function($q) use ($search)
            {
                  $q->orWhere('quote_form.name', 'like', '%' . $search . '%');
                  $q->orWhere('quote_form.email', 'like', '%' . $search . '%');
            })
    	->select("quote_form.*")
        ->orderBy($sortName,$sortBy)
        ->groupBy('quote_form.id')
    	->paginate(config('constants.limit'));

     

    	return json_encode(array("companies"=>$companies,"pagination"=>str_replace('/?','?',$companies->render('common.pagination'))));
    	
    }
    public function create()
    {
        $distributor=User::where(array("type"=>'distributor'))->orderBy('name','asc')->pluck("name","id")->prepend('All', '');

    	return view('distributor.forms.editor',compact('distributor'));
    }

    public function store(Request $request)
    {

        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'email' =>'required|string|max:255',
                 // 'distributorId' => 'required',
                 'email_subject' =>'required|string|max:255',
                 // 'form_code' =>'required|string|max:255',
                 // 'submit_button_code' =>'required|string|max:255',
                 // 'intro_text' =>'required|string|max:1000',
                 // 'footer_text' =>'required|string|max:1000',
                 'form_type' => 'required',
                 'status' => 'required'

                ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }

            DB::beginTransaction();
          try {
               

             
           
	            $quoteForm=new QuoteForm();

	            $quoteForm->name=$request->name;
	            $quoteForm->email=$request->email;
	            $quoteForm->distributorId=Auth::user()->id;
                $quoteForm->email_subject = $request->email_subject;
                // $quoteForm->form_code = $request->form_code;
                // $quoteForm->submit_button_code =$request->submit_button_code;
                // $quoteForm->intro_text = $request->intro_text;
                // $quoteForm->footer_text = $request->footer_text;
                $quoteForm->notification = $request->notification;
                $quoteForm->type = $request->form_type;
                
                // $quoteForm->completion_page = $request->completion_page;
                if($request->status=="true")
                {
                    $quoteForm->status="active";
                }
                else
                {
                    $quoteForm->status="deactive";
                }
	            $quoteForm->save();

              DB::commit();

           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function update(Request $request)
    {
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'email' =>'required|string|max:255',
                 // 'distributorId' => 'required',
                 'email_subject' =>'required|string|max:255',
                 // 'form_code' =>'required|string|max:255',
                 // 'submit_button_code' =>'required|string|max:255',
                 // 'intro_text' =>'required|string|max:1000',
                 // 'footer_text' =>'required|string|max:1000',
                 'form_type' => 'required',
                 'status' => 'required'

                ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            

         DB::beginTransaction();
         try {           
                    $quoteForm=QuoteForm::find($request->id);
                   
                    $quoteForm->name=$request->name;
                    $quoteForm->email=$request->email;
                    $quoteForm->distributorId=Auth::user()->id;
                    $quoteForm->email_subject = $request->email_subject;
                    // $quoteForm->form_code = $request->form_code;
                    // $quoteForm->submit_button_code = $request->submit_button_code;
                    // $quoteForm->intro_text = $request->intro_text;
                    // $quoteForm->footer_text = $request->footer_text;
                    $quoteForm->notification = $request->notification;
                    $quoteForm->type = $request->form_type;
                    // $quoteForm->completion_page = $request->completion_page;
                    if($request->status=="true")
                    {
                        $quoteForm->status="active";
                    }
                    else
                    {
                        $quoteForm->status="deactive";
                    }
                    $quoteForm->save();

            DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

     public function destroy($id,Request $request)
    {
        DB::beginTransaction();
     try {

            QuoteForm::where("id",$request->id)->delete();

            DB::commit();
            return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
         } 
      catch (\Throwable $e) {
        DB::rollback();
        return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')), 404); 
      }
    }

     public function formStatusChange(Request $request)
    {
      
        if($request->post())
        {              
              DB::beginTransaction();
           try {
                  $formStatus= QuoteForm::find($request->id);
                  if($formStatus->status=="active")
                  {
                    $formStatus->status="deactive";  
                  }
                  else{
                    $formStatus->status="active";
                  }
                  $formStatus->save();
                  

                  DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
               } 
            catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
            }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
        }
    }


    public function autofill(Request $request){
/*
        $query = $request->get('query','');        

        $posts = Post::where('name','LIKE','%'.$query.'%')->get();        

        return response()->json($posts);*/


        if($request->get('query'))
         {
          $query = $request->get('query');
          $data = DB::table('quote_form')
            ->where('name', 'LIKE', "%{$query}%")
            ->get();
          $output = '<ul class="dropdown-menu" >';
          foreach($data as $row)
          {
           $output .= '
           <li style="margin:0 0 2px 0"><a href="javascript:;">'.$row->name.'</a></li>
           ';
          }
          $output .= '</ul>';
          if(count($data)>0)
          {
            $output=$output;
          }
          else {
            $output="";
          }

          // echo $output;
          return response()->json($output);
         }

    }

   
}