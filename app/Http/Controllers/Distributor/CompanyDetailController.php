<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\EmailFriend;
use App\Company;
use App\Company_social;
use App\Document;
use Session;
use DB;
use Rule;
use Mail;
use Auth;
class CompanyDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     public function index()
    {
        return view('distributor.companydetail.index');
    }
   
    public function getList(Request $request)
    {
        $timezone = DB::table('timezones')->get();

    	$company_details=Company::
        leftjoin("documents",'documents.id','company.profile')
        ->where(array("company.distributorId"=>Auth::user()->id))
    	->select("company.*",'documents.name as profileName','documents.id as profileId')
        ->groupBy('company.id')
    	->get();

        $company_social_detail=Company_social::where(array('company_social.distributorId'=>Auth::user()->id))
                ->select('company_social.*')
                ->groupBy('company_social.id')
                ->get();
         
    	return json_encode(array("company_details"=>$company_details,'company_social_detail'=>$company_social_detail,'timezone'=>$timezone));
    	
    }

     public function create()
    {
        $document=Document::where(array("distributorId"=>Auth::user()->id))->whereNotIn("type",['svg','obj'])->select('name','id','original_name')->get();
        return view('distributor.companydetail.editor',array("document"=>$document));
    }
     public function createSocialMedia()
    {
        return view('distributor.companydetail.edit_social');
    }
    public function store(Request $request)
    {

        if($request->post())
        {

            $validator = Validator::make($request->all(), [
                 'company_name' => 'required|string|max:255',
                 'email' =>'required|string|max:255',
                 'profile' => 'required',
                 'phone' => 'required|digits:10',
                 // 'website_url'=>['regex' => '/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/'],
                ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }

            DB::beginTransaction();
          try {
              
	            $company=new Company();
	            $company->company_name=$request->company_name;
	            $company->email=$request->email;
	            $company->distributorId=Auth::user()->id;
                $company->phone = $request->phone;
                $company->website_url = $request->website_url;
                // $company->default_lang = $request->default_lang;
                $company->time_zone = $request->time_zone;
                // $company->locale = $request->locale;
                $company->profile = $request->profile;
                if($request->status=="true")
                {
                    $company->status = "active";  
                }
                else{
                    $company->status = "deactive";  
                }
               	$company->save();

              DB::commit();

           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function update(Request $request)
    {
         if($request->post())
        {


            $validator = Validator::make($request->all(), [
                 'company_name' => 'required|string|max:255',
                 'email' =>'required|string|max:255|unique:company,email,'.$request->id,
                 'profile' => 'required',
                 'phone' => 'required|digits:10',
                 // 'website_url'=>['regex' => '/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/'],
                ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            

         DB::beginTransaction();
         try {           
                    $company=Company::find($request->id);
                    $company->company_name = $request->company_name;
                    $company->email=$request->email;
                    $company->distributorId=Auth::user()->id;
                    $company->phone = $request->phone;
                    $company->website_url = $request->website_url;
                    // $company->default_lang = $request->default_lang;
                    $company->time_zone = $request->time_zone;
                    // $company->locale = $request->locale;
                    $company->profile = $request->profile;

                    if($request->status=="true")
                    {
                        $company->status = "active";  
                    }
                    else{
                        $company->status = "deactive";  
                    }
                    $company->save();


            DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

    public function socialStore(Request $request)
    {
        if($request->post())
        {

            $validator = Validator::make($request->all(), [
                /* 'company_name' => 'required|string|max:255',
                 'email' =>'required|string|max:255',
                 'profile' => 'image|mimes:jpeg,png,jpg|max:2048',
                 'phone' => 'required|digits:10',*/
                ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }

            DB::beginTransaction();
          try {
              
                $company_social=new Company_social();
                $company_social->social_share_html=$request->social_share_html;
                $company_social->distributorId =Auth::user()->id;
                // $company_social->pubId = $request->pubId;
                // $company_social->status_add_script=$request->status_add_script;
                $company_social->save();

              DB::commit();

           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }

     public function socialUpdate(Request $request)
    {
         if($request->post())
        {

            $validator = Validator::make($request->all(), [
                 /*'company_name' => 'required|string|max:255',
                 'email' =>'required|string|max:255|unique:company,email,'.$request->id,
                 'profile' => 'image|mimes:jpeg,png,jpg|max:2048',
                 'phone' => 'required|digits:10',*/
                ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            

         DB::beginTransaction();
         try {           
                    $company_social=Company_social::find($request->id);
                    $company_social->social_share_html = $request->social_share_html;
                    // $company_social->pubId=$request->pubId;
                    // $company_social->status_add_script=$request->status_add_script;
                    $company_social->distributorId =Auth::user()->id;
                  
                    $company_social->save();


            DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }
 
   
}