<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Category;
use App\Product;
use App\Document;
use App\Product_step;
use App\Product_field;
use App\Product_design;
use App\Design_filed;
use DB;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('App\Http\Middleware\UserMiddleware');
    }
    public function index()
    {
      return view('distributor.products.index');
    } //index

    public function table()
    {
        return view('distributor.products.table');
    }//table

    public function row()
    {
        return view('distributor.products.row');
    } //row
   
    public function getList(Request $request)
    {
      $search = $request->search;
      $limit = $request->limit;
      $products=Product::join('category','category.id','products.categoryId')
          ->leftjoin('documents','documents.id','products.list_ImageId')
          ->where(array('products.distributorId'=>Auth::user()->id,'products.deleted'=>0))
          ->where(function($q) use ($search)
            {
                  $q->orWhere('products.name', 'like', '%' . $search . '%');
                  $q->orWhere('category.name', 'like', '%' . $search . '%');
                  $q->orWhere('products.url_slug', 'like', '%' . $search . '%');
            })
      ->select('products.*','category.name as category_name','documents.name as image')
      // ->select('products.id','products.name','products.url_slug','products.position','products.status','products.description','products.rotation_disable','products.cameras','category.name as category_name','documents.name as image')
      ->orderBy("products.id","desc")
      ->groupBy("products.id")
      ->paginate($limit);
     	return json_encode(array("product"=>$products,"pagination"=>str_replace('/?','?',$products->render('common.pagination')) ));
    } 

    public function editProduct(Request $request)
    {
      $distributorId=0;
      $fc_groups= array();
      if($request->productId!=0)
      {
        $product=Product::where("id",$request->productId)->select("distributorId")->get();
        if(count($product)>0)
        {
          $distributorId=$product[0]['distributorId'];
        }
        $fc_groups= \App\Group::where(array('distributorId'=>$distributorId))->select('groups.*')->get();

        $product_step = Product_step::where(array('product_step.distributorId'=>$distributorId, 'product_step.productId'=>$request->productId))
            ->select('product_step.*')
            ->orderBy("product_step.productId","desc")
            ->groupBy("product_step.id")
            ->get();

        $product_field = Product_field::join('product_step','product_step.id','product_field.stepId')
            ->where(array('product_field.distributorId'=>$distributorId, 'product_field.productId'=>$request->productId))
            ->select('product_field.*','product_step.name as step_name')
            ->orderBy("product_field.productId","desc")
            ->groupBy("product_field.id")
            ->get();


        $product_design = Product_design::leftjoin('design_filed','design_filed.designId','product_design.id')->where(array('product_design.distributorId'=>$distributorId, 'product_design.productId'=>$request->productId))
            ->select('product_design.*',DB::raw("(GROUP_CONCAT(design_filed.id SEPARATOR '@')) as designFiledID"))
            ->orderBy("product_design.productId","desc")
            ->groupBy("product_design.id")
            ->get();
            
        $design_filed = Design_filed::join('product_step','product_step.id','design_filed.stepId')->select('design_filed.design_key','design_filed.groupId','design_filed.stepId','design_filed.designId','design_filed.name as design_filed_name','design_filed.id','product_step.name as step_name')->get();


        $list_image = Document::where('distributorId',$distributorId)->whereIn("type",['png','svg','jpeg','jpg'])
              ->orderBy('documents.id','desc')->get('documents.*');
           
        $model_3D = Document::where(array('distributorId'=>$distributorId,"type"=>"obj"))->select("original_name","name","id")->orderBy('documents.id','desc')->get();

        $category=Category::where(array("userId"=>$distributorId,'status'=>'active'))->select("name","id")->get();

        $document=Document::where(array("distributorId"=>$distributorId))->select('original_name as name','id')->orderBy('documents.id','desc')->get();
      }
      else{
          $product_step=array();
          $product_field=array();
          $product_design=array();
          $design_filed=array();
          // $list_image=array();
          $list_image = Document::where('distributorId',Auth::user()->id)->whereIn("type",['png','svg','jpeg','jpg'])
              ->orderBy('documents.id','desc')->get('documents.*');
          $fc_groups= \App\Group::where(array('distributorId'=>Auth::user()->id))->select('groups.*')->get();
          $model_3D=array();
          $category=array();
          $document=array();

      }

      return json_encode(array("product_step"=>$product_step,"product_field"=>$product_field,"product_design"=>$product_design,'fc_groups'=>$fc_groups,"design_filed"=>$design_filed,"document"=>$document,"category"=>$category,"model_3D"=>$model_3D,"list_image"=>$list_image));
    }


    public function getProductStep(Request $request){

       $search = $request->search; $distributorId=0;
      $product_step= array(); 

      if($request->productId!=0)
      {
        $product=Product::where("id",$request->productId)->select("distributorId")->get();
        if(count($product)>0)
        {
          $distributorId=$product[0]['distributorId'];
        }

         $product_step = Product_step::where(array('product_step.distributorId'=>$distributorId, 'product_step.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_step.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_step.position', 'like', '%' . $search . '%');
              })
            ->select('product_step.*')
            ->orderBy("product_step.productId","desc")
            ->groupBy("product_step.id")
            ->get();

      }
        return json_encode(array("product_step"=>$product_step));
    }


  public function getProductField(Request $request){
      
      $search = $request->search; $distributorId=0;
      $product_field= array(); 

      if($request->productId!=0)
      {
        $product=Product::where("id",$request->productId)->select("distributorId")->get();
        if(count($product)>0)
        {
          $distributorId=$product[0]['distributorId'];
        }

        $product_field = Product_field::join('product_step','product_step.id','product_field.stepId')
            ->where(array('product_field.distributorId'=>$distributorId, 'product_field.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_field.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_step.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_field.filed_type', 'like', '%' . $search . '%');
              })
            ->select('product_field.*','product_step.name as step_name')
            ->orderBy("product_field.productId","desc")
            ->groupBy("product_field.id")
            ->get();

      }
        return json_encode(array("product_field"=>$product_field));
    }

    public function getProductDesign(Request $request){

      $search = $request->search; $distributorId=0;
      $product_design= array(); 
      $limit = $request->limit;

      if($request->productId!=0)
      {
        $product=Product::where("id",$request->productId)->select("distributorId")->get();
        if(count($product)>0)
        {
          $distributorId=$product[0]['distributorId'];
        }

        
        $product_design = Product_design::leftjoin('design_filed','design_filed.designId','product_design.id')->where(array('product_design.distributorId'=>$distributorId, 'product_design.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_design.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_design.sku', 'like', '%' . $search . '%');
              })
            ->select('product_design.*',DB::raw("(GROUP_CONCAT(design_filed.id SEPARATOR '@')) as designFiledID"))
            ->orderBy("product_design.productId","desc")
            ->groupBy("product_design.id")
            ->take($limit)
            ->get();

        $design_filed = Design_filed::join('product_step','product_step.id','design_filed.stepId')->select('design_filed.design_key','design_filed.groupId','design_filed.stepId','design_filed.designId','design_filed.name as design_filed_name','design_filed.id','product_step.name as step_name')->get();

         // $list_image=array();
          $list_image = Document::where('distributorId',Auth::user()->id)->whereIn("type",['png','svg','jpeg','jpg'])
              ->orderBy('documents.id','desc')->get('documents.*');

      }
        return json_encode(array("product_design"=>$product_design,'design_filed'=>$design_filed,'list_image'=>$list_image));
    }


    public function getDetailList(Request $request)
    {
      $search = $request->search;
      $fc_groups= \App\Group::where(array('distributorId'=>Auth::user()->id))->select('groups.*')->get(); 

      if($request->productId!=0)
      {
        $product_step = Product_step::where(array('product_step.distributorId'=>Auth::user()->id, 'product_step.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_step.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_step.position', 'like', '%' . $search . '%');
              })
            ->select('product_step.*')
            ->orderBy("product_step.productId","desc")
            ->groupBy("product_step.id")
            ->get();

        $product_field = Product_field::join('product_step','product_step.id','product_field.stepId')
            ->where(array('product_field.distributorId'=>Auth::user()->id, 'product_field.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_field.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_step.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_field.filed_type', 'like', '%' . $search . '%');
              })
            ->select('product_field.*','product_step.name as step_name')
            ->orderBy("product_field.productId","desc")
            ->groupBy("product_field.id")
            ->get();


        $product_design = Product_design::leftjoin('design_filed','design_filed.designId','product_design.id')->where(array('product_design.distributorId'=>Auth::user()->id, 'product_design.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_design.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_design.sku', 'like', '%' . $search . '%');
              })
            ->select('product_design.*',DB::raw("(GROUP_CONCAT(design_filed.id SEPARATOR '@')) as designFiledID"))
            ->orderBy("product_design.productId","desc")
            ->groupBy("product_design.id")
            ->get();
            
            $design_filed = Design_filed::join('product_step','product_step.id','design_filed.stepId')->select('design_filed.design_key','design_filed.groupId','design_filed.stepId','design_filed.designId','design_filed.name as design_filed_name','design_filed.id','product_step.name as step_name')->get();
      }
      else{
          $product_step=array();
          $product_field=array();
          $product_design=array();
          $design_filed=array();
      }

      return json_encode(array("product_step"=>$product_step,"product_field"=>$product_field,"product_design"=>$product_design,'fc_groups'=>$fc_groups,"design_filed"=>$design_filed));
    }

 

    public function create()
    { 
      $list_image = Document::where('distributorId',Auth::user()->id)->whereIn("type",['png','svg','jpeg','jpg'])->orderBy('documents.id','desc')->get('documents.*');
        $model_3D = Document::where(array('distributorId'=>Auth::user()->id,"type"=>"obj"))->orderBy('documents.id','desc')->get('documents.*');

      $svg_images = Document::where(array('distributorId'=>Auth::user()->id,"type"=>"svg"))->orderBy('documents.id','desc')->get("documents.*");  

       $categoryRs=Category::where(array("userId"=>Auth::user()->id,'status'=>"active"))
       ->select("category.name as cname","category.id as categoryId","category.parentId",DB::raw("(select name from category as cat where cat.id=category.parentId) as parentCategoryName"))
       ->get();

        $category=array();
            foreach ($categoryRs as $quest) {
              if($quest->parentId!=null)
              {
                $quest->category=$quest->parentCategoryName;
                $quest->subcategory = $quest->cname;
              }
              else
              {
                $quest->category=$quest->cname;
                $quest->subcategory = "";
              }
              $category[]=$quest;
            }

          $columns_1 = array_column($category, 'category');
          $columns_2 = array_column($category, 'subcategory');
         array_multisort($columns_1, SORT_ASC, $columns_2, SORT_ASC, $category);
      $document=Document::where(array("distributorId"=>Auth::user()->id))->select('name','id','original_name')->orderBy('documents.id','desc')->get();
      
    	return view('distributor.products.editor',array("category"=>$category,'document'=>$document,'list_image'=>$list_image,'model_3D'=>$model_3D,"svg_images"=>$svg_images));
    } //create

    public function svgSelectorCreate()
    {
        return view('distributor.products.editorsvgselector');
    }
    public function store(Request $request)
    { 
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'url_slug' => 'required|regex:/^[a-z0-9\-]+$/u',
                 'category'=>'required',
                 'list_ImageId'=>'required',
                 'model_3DId'=>'required',
                 'svg_width'=>'required',
                 'svg_height'=>'required',
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }

            $flag=0;
            $checkurl=Product::where(array('distributorId'=>Auth::user()->id,'deleted'=>0))->select('url_slug','id')->get();
            foreach ($checkurl as $check) 
            {
              if($request->url_slug==$check->url_slug)
              {
                $flag=1;
              }  
            }
            if($flag==1)
            {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The product slug has already been taken."),400);
            }
           
             DB::beginTransaction();
          try {
              
              $product=new Product();
              $product->distributorId = Auth::user()->id;
              $product->categoryId = $request->categoryId;
              $product->list_ImageId = $request->list_ImageId;
              $product->model_3DId = $request->model_3DId;
              $product->name=$request->name;
              $product->url_slug = $request->url_slug;
              $product->description = htmlentities(($request->description), ENT_QUOTES);
              $product->rotation_disable=$request->rotation_disable;
              $product->cameras=$request->cameras;
              $product->camera_override=$request->camera_override;
              if(isset($request->x_axis))
              {
                $product->x_axis=$request->x_axis;
              }
              else{
                $product->x_axis=3; 
              }
              if(isset($request->y_axis))
              {
                $product->y_axis=$request->y_axis;
              }
              else{
                $product->y_axis=0; 
              }
              if(isset($request->z_axis))
              {
                $product->z_axis=$request->z_axis;
              }
              else{
                $product->z_axis=3; 
              }
              if(isset($request->scale))
              {
                $product->scale=$request->scale;
              }
              else{
                $product->scale=5.2; 
              }
              if(isset($request->light_brightness))
              {
                $product->light_brightness=$request->light_brightness;
              }
              else{
                $product->light_brightness=0; 
              }

              $product->override_model_position_scale=$request->override_model_position_scale;
              // $product->light_brightness=$request->light_brightness;
              $product->svg_dimension_ht=$request->svg_dimension_ht;
              $product->svg_dimension_vt=$request->svg_dimension_vt;
              $product->hight_on_basker_page=$request->hight_on_basker_page;
              $product->default_toggle_color=$request->default_toggle_color;
              $product->position=$request->position;
             
              if($request->status=='true')
              {
                $product->status = "active";  
              }
              else{
                $product->status = "deactive";  
              }
             
              $product->save();
             
              DB::commit();
            $data=[
              'id'=>$product->id,
              'x_axis'=>$product->x_axis,
              'y_axis'=>$product->y_axis,
              'z_axis'=>$product->z_axis,
              'scale'=>$product->scale,
              'light_brightness'=>$product->light_brightness,
            ];
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$data));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }

    public function getSVGDimension(Request $request)
    {
        $list_image = Document::find($request['list_imageId']);
        $imagedetails = getimagesize(url('public/images/')."/".$list_image->name);
        return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$imagedetails));
    }
    
    public function update(Request $request)
    {
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'url_slug' => 'regex:/^[a-z0-9\-]+$/u',
                 'category'=>'required',
                 'list_ImageId'=>'required',
                 'model_3DId'=>'required',
                 'svg_width'=>'required',
                 'svg_height'=>'required',
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }
            $flag=0;
            $checkurl=Product::where(array('distributorId'=>Auth::user()->id))->select('url_slug','id')->get();
            foreach ($checkurl as $check) {
              if($check->id!=$request->id)
              {
                if($request->url_slug==$check->url_slug)
                {
                  $flag=1;
                }  
              }
            }
            if($flag==1)
            {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The product slug has already been taken."),400);
            }

             DB::beginTransaction();
          try {
               
              $product=Product::find($request->id);
              $product->distributorId = Auth::user()->id;
              $product->categoryId = $request->categoryId;
              $product->list_ImageId = $request->list_ImageId;
              $product->model_3DId = $request->model_3DId;
              $product->name=$request->name;
              $product->url_slug = $request->url_slug;
              $product->description= htmlentities(($request->description), ENT_QUOTES); 
              $product->rotation_disable=$request->rotation_disable;
              $product->cameras=$request->cameras;
              $product->camera_override=$request->camera_override;
              $product->x_axis=$request->x_axis;
              $product->y_axis=$request->y_axis;
              $product->z_axis=$request->z_axis;
              $product->scale=$request->scale;
              $product->override_model_position_scale=$request->override_model_position_scale;
              $product->light_brightness=$request->light_brightness;
              $product->svg_dimension_ht=$request->svg_dimension_ht;
              $product->svg_dimension_vt=$request->svg_dimension_vt;
              $product->hight_on_basker_page=$request->hight_on_basker_page;
              $product->default_toggle_color=$request->default_toggle_color;
              $product->position=$request->position;

             
              if($request->status=='true')
              {
                $product->status = "active";  
              }
              else {
                $product->status = "deactive";  
              }
             
              $product->save();
             
            DB::commit();
             $data=[
              'id'=>$product->id,
              'x_axis'=>$product->x_axis,
              'y_axis'=>$product->y_axis,
              'z_axis'=>$product->z_axis,
              'scale'=>$product->scale,
              "light_brightness"=>$product->light_brightness,
            ];
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$data));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }
    
    public function destroy($id,Request $request)
    {
      if($request->post())
      {

            DB::beginTransaction();
         try {
              
                /*$product = Product::find($request->id);
                $product->deleted = 1;
                $product->status="deactive";
                $product->save();

                 DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); */
                \App\Quotation_order::join('product_order','product_order.id','quotation_order.productOrderId')->where(array('product_order.productId'=>$request->id))->delete();

                \App\Player_setting::join('product_order','product_order.id','player_setting.productOrderId')->where(array('product_order.productId'=>$request->id))->delete();

                $imagefrontBack = \App\Order_front_back::join('product_order','product_order.id','order_front_back.productOrderId')->where(array('product_order.productId'=>$request->id))->get();
                  foreach ($imagefrontBack as $value) {
                      if($value->image!="" && file_exists(public_path('images/front_back/'.$value->image)))
                      {
                          unlink(public_path('images/front_back/'.$value->image));
                      }
                  }

                \App\Order_front_back::join('product_order','product_order.id','order_front_back.productOrderId')->where(array('product_order.productId'=>$request->id))->delete();

                \App\Product_order_field::join('product_order','product_order.id','product_order_field.productOrderId')->where(array('product_order.productId'=>$request->id))->delete();
                \App\Product_order::where(array('product_order.productId'=>$request->id))->delete();

                Design_filed::join('product_design','product_design.id','design_filed.designId')->where(array('product_design.productId'=>$request->id))->delete();

                $productdesignthumb=Product_design::where(array('product_design.productId'=>$request->id))->get();

                foreach ($productdesignthumb as $value) {
                      if($value->image!="" && file_exists(public_path('images/'.$value->image)))
                      {
                          unlink(public_path('images/'.$value->image));
                      }
                  }

                Product_design::where(array('product_design.productId'=>$request->id))->delete();
                Product_field::where(array('product_field.productId'=>$request->id))->delete();
                Product_step::where(array('product_step.productId'=>$request->id))->delete();
                $result = Product::where(array('products.id'=>$request->id))->delete();

                if($result == 1)
                { 
                    DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
                }
                else
                {
                    DB::rollBack();
                    return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
                }
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }


    public function stepTable()
    {
        return view('distributor.products.step.table');

    }//table

    public function stepRow()
    {
        return view('distributor.products.step.row');

    } //row

     public function stepCreate()
    {
      return view('distributor.products.step.editor');
    } //create


    public function stepStore(Request $request)
    { 
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
           
             DB::beginTransaction();
          try {
              
              $step=new Product_step();
              $step->distributorId = Auth::user()->id;
              $step->productId = $request->productId;
              $step->name = $request->name;
              $step->position = $request->position;
             
              if($request->status=='true')
              {
                $step->status = "active";  
              }
              else {
                $step->status = "deactive";  
              }
              $step->save();
              
               $product_step = Product_step::where(array('product_step.distributorId'=>Auth::user()->id, 'product_step.productId'=>$request->productId))
             
              ->select('product_step.*')
              ->orderBy("product_step.productId","desc")
              ->groupBy("product_step.id")
              ->get();

              DB::commit();
            
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$product_step));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function stepUpdate(Request $request)
    {
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
             DB::beginTransaction();
          try {
               
              $step=Product_step::find($request->id);
              $step->distributorId = Auth::user()->id;
              $step->productId = $request->productId;
              $step->name = $request->name;
              $step->position = $request->position;
              if($request->status=='true')
              {
                $step->status = "active";  
              }
              else {
                $step->status = "deactive";  
              }
              $step->save();
            
             $product_step = Product_step::where(array('product_step.distributorId'=>Auth::user()->id, 'product_step.productId'=>$request->productId))
             
                  ->select('product_step.*')
                  ->orderBy("product_step.productId","desc")
                  ->groupBy("product_step.id")
                  ->get();   
            DB::commit();
          
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$product_step));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

    public function stepDelete(Request $request)
    {

      if($request->post())
      {
            DB::beginTransaction();
         try {
                Product_field::where(array('stepId'=>$request->id))->delete();  
                Product_step::where(array("id"=>$request->id))->delete();

                DB::commit();

                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

    public function fieldTable()
    {
        return view('distributor.products.field.table');
    }//table

    public function fieldRow()
    {
        return view('distributor.products.field.row');
    } //row

     public function fieldCreate()
    {
      return view('distributor.products.field.editor');
    } //create

    public function fieldStore(Request $request)
    { 
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
            ]);
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
           
             DB::beginTransaction();
          try {
              $field=new Product_field();
              $field->distributorId = Auth::user()->id;
              $field->productId = $request->productId;
              $field->stepId = $request->field_step;
              $field->name = $request->name;
              $field->filed_type = $request->filed_type;
              $field->groupId = $request->field_color_group;
              $field->fontGroupId = $request->field_font_group;
              $field->save();
             
              DB::commit();

              $product_field = Product_field::join('product_step','product_step.id','product_field.stepId')
              ->where(array('product_field.distributorId'=>Auth::user()->id, 'product_field.productId'=>$request->productId))
              ->select('product_field.*','product_step.name as step_name')
              ->orderBy("product_field.productId","desc")
              ->groupBy("product_field.id")
              ->get();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$product_field));
            }catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function fieldUpdate(Request $request)
    {
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
             DB::beginTransaction();
          try {
               
              $field=Product_field::find($request->id);
              $field->distributorId = Auth::user()->id;
              $field->productId = $request->productId;
              $field->stepId = $request->field_step;
              $field->name = $request->name;
              $field->filed_type = $request->filed_type;
              $field->groupId = $request->field_color_group;  
              $field->fontGroupId = $request->field_font_group;
              $field->save();
          
            DB::commit();
           
              $product_field = Product_field::join('product_step','product_step.id','product_field.stepId')
              ->where(array('product_field.distributorId'=>Auth::user()->id, 'product_field.productId'=>$request->productId))
              ->select('product_field.*','product_step.name as step_name')
              ->orderBy("product_field.productId","desc")
              ->groupBy("product_field.id")
              ->get();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$product_field));
            }catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

    public function fieldDelete(Request $request)
    {
      if($request->post())
      {
            DB::beginTransaction();
         try {
                Product_field::where(array("id"=>$request->id))->delete();
                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

    public function designTable()
    {
        return view('distributor.products.design.table');

    }//table

    public function designRow()
    {
        return view('distributor.products.design.row');
    } //row

     public function designCreate()
    {
      return view('distributor.products.design.editor');
    } //create

    public function designStore(Request $request)
    { 
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'profile' => 'image|mimes:jpeg,png,jpg|max:2048',
                 'design_slug' => 'required|regex:/^[a-z0-9\-]+$/u',
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }

            $flag=0;
            $checkurl=Product_design::where(array('productId'=>$request->productId))->select('design_slug')->get();
            foreach ($checkurl as $check) {
              if($request->design_slug==$check->design_slug)
              {
                $flag=1;
              }
            }
            if($flag==1)
            {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The design slug has already been taken."),400);
            }

             DB::beginTransaction();
          try {
            $imageName="";
               if($request->hasFile('profile')) 
               {
                  $image = $request->profile;
                  $extension = $image->getClientOriginalExtension();
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data = url('/')."/public/images/$imageName";
               }
              
              $design=new Product_design();
              $design->distributorId = Auth::user()->id;
              $design->productId = $request->productId;
              $design->name = $request->name;
              $design->sku = $request->sku;
              $design->design_slug = $request->design_slug;
              $design->svgFileId=$request->svgFileId;
              $design->svg_snippet = $request->svg_snippet;
              $design->svg_viewBox = $request->svg_viewBox;  
              $design->position = $request->position;  
              $design->profile=$imageName;
              if($request->status=='true')
              {
                $design->status = 'active';   
              }
              else
              {
                  $design->status = 'deactive';
              }
              if($request->player_status=='true')
              {
                $design->player_status = 'active';   
              }
              else
              {
                  $design->player_status = 'deactive';
              }
              $design->save();

              if($request->design_filed_name!="")
              {
                $design_filed_name=explode(",",$request->design_filed_name);
                $design_key=explode(",",$request->design_key);
                $stepId=explode(",",$request->stepId);
                $data_group=explode(",",$request->data_group);
                $ids=explode(",",$request->ids);
                $data = array();
                for($i=0;$i<count($design_filed_name);$i++)
                {
                  if($data_group[$i]!="")
                  {
                    $groupId=$data_group[$i];
                  }
                  else{
                    $groupId=null;
                  }
                  $design_filed=  new Design_filed();
                  $design_filed->groupId=$groupId;
                  $design_filed->name =$design_filed_name[$i];
                  $design_filed->design_key =$design_key[$i];
                  $design_filed->stepId =$stepId[$i];
                  $design_filed->designId =$design->id;
                  $design_filed->save();
                }
              }
             
              DB::commit();

              $product_design = Product_design::leftjoin('design_filed','design_filed.designId','product_design.id')->where(array('product_design.distributorId'=>Auth::user()->id, 'product_design.productId'=>$request->productId))
           
              ->select('product_design.*',DB::raw("(GROUP_CONCAT(design_filed.id SEPARATOR '@')) as designFiledID"))
              ->orderBy("product_design.productId","desc")
              ->groupBy("product_design.id")
              ->get();

              $design_filed = Design_filed::join('product_step','product_step.id','design_filed.stepId')->select('design_filed.design_key','design_filed.groupId','design_filed.stepId','design_filed.designId','design_filed.name as design_filed_name','design_filed.id','product_step.name as step_name')->get();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$product_design,'res_design_filed'=>$design_filed));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function designUpdate(Request $request)
    {
          
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'profile' => 'image|mimes:jpeg,png,jpg|max:2048',
                 'design_slug' => 'required|regex:/^[a-z0-9\-]+$/u',
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }

            $flag=0;
            $checkurl=Product_design::where(array('productId'=>$request->productId))->select('design_slug','id')->get();
            foreach ($checkurl as $check) {
              if($check->id!=$request->id)
              {
                if($request->design_slug==$check->design_slug)
                {
                  $flag=1;
                }  
              }
            }
            if($flag==1)
            {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The design slug has already been taken."),400);
            }

             DB::beginTransaction();
          try {
               
             $design=Product_design::find($request->id);
              $design->distributorId = Auth::user()->id;
              $design->productId = $request->productId;
              $design->name = $request->name;
              $design->sku = $request->sku;
              $design->design_slug = $request->design_slug;
              $design->svgFileId=$request->svgFileId;
              $design->svg_snippet = $request->svg_snippet;
              $design->svg_viewBox = $request->svg_viewBox;  
              $design->position = $request->position;  


              if($request->hasFile('profile')) {
                  if($design->profile!="" && file_exists(public_path('images/'.$design->profile)))
                  {
                        unlink(public_path('images/'.$design->profile));
                  }
                  $image = $request->profile;
                  $extension = $image->getClientOriginalExtension();
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data = url('/')."/public/images/$imageName";
                  $design->profile=$imageName;
              }


              if($request->status=='true')
              {
                $design->status = 'active';   
              }
              else
              {
                  $design->status = 'deactive';
              }
              if($request->player_status=='true')
              {
                $design->player_status = 'active';   
              }
              else
              {
                  $design->player_status = 'deactive';
              }
              
              $design->save();


              if($request->design_filed_name!="")
              {
                $design_filed_name=explode(",",$request->design_filed_name);
                $design_key=explode(",",$request->design_key);
                $stepId=explode(",",$request->stepId);
                $data_group=explode(",",$request->data_group);
                $ids=explode(",",$request->ids);


                $data = array();
                for($i=0;$i<count($design_filed_name);$i++)
                {
                  if($data_group[$i]!="")
                  {
                    $groupId=$data_group[$i];
                  }
                  else{
                    $groupId=null;
                  }
                    $flag=0;
                    if($ids[$i]!="")
                    {
                      $checkUp=Design_filed::where(array("id"=>$ids[$i]))->get();
                      if(count($checkUp)>0)
                      {
                        $flag=1;
                      } 
                    }
                    
                    if($flag==1)
                    {
                        $design_filed=Design_filed::find($ids[$i]);  
                        $design_filed->groupId= $groupId;
                        $design_filed->name =$design_filed_name[$i];
                        $design_filed->design_key =$design_key[$i];
                        $design_filed->stepId =$stepId[$i];
                        $design_filed->designId =$design->id;
                        $design_filed->updated_at =date('Y-m-d H:i:s');
                        $design_filed->save();
                    }
                    else{
                        $design_filed=  new Design_filed();
                        $design_filed->groupId=$groupId;
                        $design_filed->name =$design_filed_name[$i];
                        $design_filed->design_key =$design_key[$i];
                        $design_filed->stepId =$stepId[$i];
                        $design_filed->designId =$design->id;
                        $design_filed->updated_at =date('Y-m-d H:i:s');
                        $design_filed->save();
                    }
                }
              }

               $product_design = Product_design::leftjoin('design_filed','design_filed.designId','product_design.id')->where(array('product_design.distributorId'=>Auth::user()->id, 'product_design.productId'=>$request->productId))
           
              ->select('product_design.*',DB::raw("(GROUP_CONCAT(design_filed.id SEPARATOR '@')) as designFiledID"))
              ->orderBy("product_design.productId","desc")
              ->groupBy("product_design.id")
              ->get();

              $design_filed = Design_filed::join('product_step','product_step.id','design_filed.stepId')->select('design_filed.design_key','design_filed.groupId','design_filed.stepId','design_filed.designId','design_filed.name as design_filed_name','design_filed.id','product_step.name as step_name')->get();

            DB::commit();

            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$product_design,'res_design_filed'=>$design_filed));
            }catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

    public function designDelete(Request $request)
    {

      if($request->post())
      {
         
            DB::beginTransaction();
         try {
                Design_filed::where(array("designId"=>$request->id))->delete();
                $productdesignthumb=Product_design::where(array('id'=>$request->id))->get();

                foreach ($productdesignthumb as $value) {
                      if($value->image!="" && file_exists(public_path('images/'.$value->image)))
                      {
                          unlink(public_path('images/'.$value->image));
                      }
                  }
                Product_design::where(array("id"=>$request->id))->delete();

                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

  
    public function designFiledDelete(Request $request)
    {
      
      if($request->post())
      {
         
            DB::beginTransaction();
         try {
                Design_filed::where(array("id"=>$request->designFiledId))->delete();

                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

    public function productStatusChange(Request $request)
    {
      if($request->post())
        {              
              DB::beginTransaction();
           try {
                  $productStatus= Product::find($request->id);
                  if($productStatus->status=="active")
                  {
                    $productStatus->status="deactive";  
                  }
                  else{
                    $productStatus->status="active";
                  }
                  $productStatus->save();
                  DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
               } 
            catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
            }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
        }
    }


    public function stepStatusChange(Request $request)
    {
      if($request->post())
        {              
              DB::beginTransaction();
           try {
                  $stepStatus= Product_step::find($request->id);
                  if($stepStatus->status=="active")
                  {
                    $stepStatus->status="deactive";  
                  }
                  else{
                    $stepStatus->status="active";
                  }
                  $stepStatus->save();
                  DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
               } 
            catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
            }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
        }
    }

    public function designStatusChange(Request $request)
    {

      if($request->post())
        {              
              DB::beginTransaction();
           try {
                  $designStatus= Product_design::find($request->id);
                  if($designStatus->status=="active")
                  {
                    $designStatus->status="deactive";  
                  }
                  else{
                    $designStatus->status="active";
                  }
                  $designStatus->save();
                  DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
               } 
            catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
            }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
        }
    }

    public function uploadDocEditor(Request $request)
    {
        return view('distributor.products.uploaddoceditor');
    }

    public function saveUploadDoc(Request $request)
    {
        if($request->post())
        {
          $allow_image=array("jpeg","jpg","png","svg","obj");
            $validator = Validator::make($request->all(), [
                 'file' => 'required|max:30048'
            ]);
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
             DB::beginTransaction();
          try {
                $imageName=""; $image_name=""; $sizefile=""; $extension="";
               if($request->hasFile('file')) 
               {
                  $image = $request->file('file');
                  $sizefile=$image->getClientSize();
                  $image_name=$image->getClientOriginalName();
                  $extension = $image->getClientOriginalExtension();
                  if(!in_array($extension,$allow_image))
                  {
                     return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("name"=>["The name must be a file of type: jpg, jpeg, png."])),400);
                  }
                  if($extension=="obj" || $extension=="svg")
                  {
                    return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("name"=>["The name must be a file of type:  jpg, jpeg, png."])),400);
                  }

                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data = url('/')."/public/images/$imageName";

               }
              $document=new Document();
              $document->original_name = $image_name;
              $document->name = $imageName;
              $document->type = $extension;
              $document->distributorId = Auth::user()->id;
              $document->file_size = $sizefile;
              $document->createDate = date('Y-m-d');
              $document->save();  
              
              $value =  asset("public/images/").'/'.$document->name;
              $option ='<option value="'.$document->id.'" style=" background-repeat: no-repeat;background-size: 20px 20px;background-image:url('.$value.')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$document->original_name.'</option>';
             
             $data=[
              'option'=>$option,
              'type'=>$document->type,
              'id'=>$document->id,
             ];
              DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$data));
            }catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }

    public function saveUpload3DImage(Request $request)
    {
        if($request->post())
        {
          $allow_image=array("jpeg","jpg","png","svg","obj");
            $validator = Validator::make($request->all(), [
                 'file' => 'required|max:30048'
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
             DB::beginTransaction();
          try {
                $imageName=""; $image_name=""; $sizefile=""; $extension="";
               if($request->hasFile('file')) 
               {
                  $image = $request->file('file');
                  $sizefile=$image->getClientSize();
                  $image_name=$image->getClientOriginalName();
                  $extension = $image->getClientOriginalExtension();
                  if(!in_array($extension,$allow_image))
                  {
                     return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("name"=>["The name must be a file of type: obj."])),400);
                  }
                  else if($extension!="obj")
                  {
                    return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("name"=>["The name must be a file of type:  obj."])),400);
                  }
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data = url('/')."/public/images/$imageName";

               }
              $document=new Document();
              $document->original_name = $image_name;
              $document->name = $imageName;
               $document->type = $extension;
              $document->distributorId = Auth::user()->id;
              $document->file_size = $sizefile;
              $document->createDate = date('Y-m-d');
              $document->save();  
              
              $value =  asset("public/images/").'/'.$document->name;
              $option ='<option value="'.$document->id.'" data-title="'.$document->name.'" style=" background-repeat: no-repeat;background-size: 20px 20px;background-image:url('.$value.')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$document->original_name.'</option>';
             $data=[
              'option'=>$option,
              'type'=>$document->type,
              'id'=>$document->id,
             ];
              DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$data));
            }catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }


    public function saveUploadSvgImage(Request $request)
    {
      // Svg Upload from Product 
        if($request->post())
        {
          $allow_image=array("jpeg","jpg","png","svg","obj");
            $validator = Validator::make($request->all(), [
                 'file' => 'required|max:30048'
            ]);
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
             DB::beginTransaction();
          try {
                $imageName=""; $image_name=""; $sizefile=""; $extension="";
               if($request->hasFile('file')) 
               {
                  $image = $request->file('file');
                  $sizefile=$image->getClientSize();
                  $image_name=$image->getClientOriginalName();
                  $extension = $image->getClientOriginalExtension();
                  
                  if(!in_array($extension,$allow_image))
                  {
                     return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("name"=>["The name must be a file of type: svg."])),400);
                  }
                  else if($extension!="svg")
                  {
                    return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("name"=>["The name must be a file of type:  svg."])),400);
                  }
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data = url('/')."/public/images/$imageName";

               }
              $document=new Document();
              $document->original_name = $image_name;
              $document->name = $imageName;
               $document->type = $extension;
              $document->distributorId = Auth::user()->id;
              $document->file_size = $sizefile;
              $document->createDate = date('Y-m-d');
              $document->save();  
              
              $value =  asset("public/images/").'/'.$document->name;
              $option ='<option value="'.$document->name.'" style=" background-repeat: no-repeat;background-size: 20px 20px;background-image:url('.$value.')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$document->original_name.'</option>';
              $test = $document;
             $data=[
              'docname'=>$document->name,
              'option'=>$option,
              'type'=>$document->type,
              'id'=>$document->id,
             ];
              DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$data,'test'=>$test));
            }catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }

    public function saveUploadSvgInDesign(Request $request)
    {
      // Svg Upload from Product Design
        if($request->post())
        {
          $allow_image=array("jpeg","jpg","png","svg","obj");
            $validator = Validator::make($request->all(), [
                 'file' => 'required|max:30048'
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
             DB::beginTransaction();
          try {
              
                $imageName=""; $image_name=""; $sizefile=""; $extension="";
               if($request->hasFile('file')) 
               {
                  $image = $request->file('file');
                  $sizefile=$image->getClientSize();
                  $image_name=$image->getClientOriginalName();
                  $extension = $image->getClientOriginalExtension();
                  
                  if(!in_array($extension,$allow_image))
                  {
                     return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("name"=>["The name must be a file of type: jpg, jpeg, obj, svg."])),400);
                  }
                  else if($extension!="svg")
                  {
                    return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("name"=>["The name must be a file of type:  svg."])),400);
                  }
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data1 = url('/')."/public/images/$imageName";

               }
              $document=new Document();
              $document->original_name = $image_name;
              $document->name = $imageName;
               $document->type = $extension;
              $document->distributorId = Auth::user()->id;
              $document->file_size = $sizefile;
              $document->createDate = date('Y-m-d');
              $document->save();  
              
              $value =  asset("public/images/").'/'.$document->name;
              $option ='<option value="'.$document->id.'"data-title="'.$document->name.'" style=" background-repeat: no-repeat;background-size: 20px 20px;background-image:url('.$value.')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$document->original_name.'</option>';
             
             $data=[
              'docname'=>$document->name,
              'option'=>$option,
              'type'=>$document->type,
              'id'=>$document->id,
             ];
              DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$data));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
}