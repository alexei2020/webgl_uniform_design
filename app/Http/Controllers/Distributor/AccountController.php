<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Companies;
use App\User;
use Session;
use DB;
use Rule;
use Mail;
use Auth;
class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
    public function index()
    {
    	$user = User::where(array('id'=>Auth::user()->id,"type"=>"distributor"))->get();
        return view('distributor.account.index',array("user"=>$user));
    }

    public function update(Request $request)
    {
    	if($request->post())
        {
            $validator = Validator::make($request->all(), [
            	  	'name' => 'required|string|max:255',
                	'email' =>'required|string|max:255|unique:users,email,'.$request->id,
               ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            
         DB::beginTransaction();
         try {           
                    $user=User::find($request->id);
                    $user->name=$request->name;
                    $user->email=$request->email;
                    $user->save();

            DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

    public function changePassword()
    {
    	return view('distributor.account.change_password');
    }

	public function updatePassword(Request $request)
    {
    	
    	if($request->post())
        {
            $validator = Validator::make($request->all(), [
            	  	'current_password'=>'required',
	                'password' => 'required|same:password|min:6',
	                'confirm_password' => 'required|same:password|min:6',
               ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            
         DB::beginTransaction();
         try {           

         		$current_password = \Auth::user()->password;
                if(Hash::check($request['current_password'],$current_password))
                {
                    $user=User::find(Auth::user()->id);
                    $user->password = Hash::make($request->password);
                    $user->save();
                    DB::commit();
            		return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
                }
                else{
                	return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The current password does not match"),404);
                }

            
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }    
    
   
}