<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\EmailFriend;
use Session;
use DB;
use Rule;
use Mail;
use Auth;
class EmailFriendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     public function index()
    {
        return view('distributor.emailfriend.index');
    }
   
    public function getList(Request $request)
    {
    	$email_friend=EmailFriend::
        where(array("distributorId"=>Auth::user()->id))
    	->select("email_friend.*")
        ->groupBy('email_friend.id')
    	->get();
    	return json_encode(array("email_friend"=>$email_friend));
    	
    }
     public function create()
    {
        return view('distributor.emailfriend.editor');
    }
    public function store(Request $request)
    {
       
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'from_name' => 'required|string|max:255',
                 'from_email' =>'required|string|max:255',
                 'subject' =>'required|string|max:255',
                 'email_template' =>'required|string|max:1000'
               
                ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }

            DB::beginTransaction();
          try {
               
	            $emailFriend=new EmailFriend();
	            $emailFriend->from_name=$request->from_name;
	            $emailFriend->from_email=$request->from_email;
	            $emailFriend->distributorId=Auth::user()->id;
                $emailFriend->subject = $request->subject;
                $emailFriend->email_template = $request->email_template;
               	$emailFriend->save();

              DB::commit();

           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function update(Request $request)
    {
         if($request->post())
        {
           
            $validator = Validator::make($request->all(), [
                 'from_name' => 'required|string|max:255',
                 'from_email' =>'required|string|max:255',
                 'subject' =>'required|string|max:255',
                 'email_template' =>'required|string|max:1000'
               
                ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            

         DB::beginTransaction();
         try {           
                    $emailFriend=EmailFriend::find($request->id);
                    $emailFriend->from_name=$request->from_name;
                    $emailFriend->from_email=$request->from_email;
                    $emailFriend->distributorId=Auth::user()->id;
                    $emailFriend->subject = $request->subject;
                    $emailFriend->email_template = $request->email_template;
                    $emailFriend->save();


            DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

 
   
}