<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Document;
use App\Category;
use DB;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('App\Http\Middleware\UserMiddleware');

    }
    public function index()
    {
         
      return view('distributor.document.index');

    } //index

      public function select()
    {
        return view('common.select');
    }

    public function table()
    {
        return view('distributor.document.table');

    }//table

    public function row()
    {

        return view('distributor.document.row');

    } //row
  
    public function getList(Request $request)
    {
      
        $search = $request->search;

        $sortBy = $request->sortBy;
        $sortName = $request->sortName;

        if($sortName=="original_name")
        {
            $sortName="documents.original_name"; 
        }
        else if($sortName=="file_size")
        {
            $sortName="documents.file_size"; 
        }
        else if($sortName=="createDate")
        {
            $sortName="documents.createDate"; 
        }
        else
        {
          $sortName="documents.".$sortName;
        }



      $documents=Document::
      where(function($q) use ($search)
            {
                  $q->orWhere('documents.original_name', 'like', '%' . $search . '%');
                 
            })
      ->where(array("distributorId"=> Auth::user()->id))
      ->select("documents.*")
        ->orderBy($sortName,$sortBy)
        ->groupBy('documents.id')
      ->paginate(config('constants.limit'));

    
      return json_encode(array("documents"=>$documents,"pagination"=>str_replace('/?','?',$documents->render('common.pagination'))));
      
    	
 
    } // getCategory

  
    public function store(Request $request)
    { 

        if($request->post())
        {
          $allow_image=array("jpeg","jpg","png","svg","obj");
            $validator = Validator::make($request->all(), [
                 'name' => 'required|max:30048'
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
           
             DB::beginTransaction();
          try {
              
                $imageName=""; $image_name=""; $sizefile=""; $extension="";
               if($request->hasFile('name')) 
               {
                  $image = $request->name;
                  $sizefile=$image->getClientSize();
                  $image_name=$image->getClientOriginalName();
                  $extension = $image->getClientOriginalExtension();
                  
                  if(!in_array($extension,$allow_image))
                  {
                     return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("name"=>["The name must be a file of type: jpg, jpeg, obj, svg."])),400);
                  }
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data = url('/')."/public/images/$imageName";

                


               }
              $document=new Document();
              $document->original_name = $image_name;
              $document->name = $imageName;
               $document->type = $extension;
              $document->distributorId = Auth::user()->id;
              $document->file_size = $sizefile;
              $document->createDate = date('Y-m-d');

                 
              $document->save();
             
              DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
    
     public function destroy($id,Request $request)
    {
      if($request->post())
      {
          $ids = explode(",",$request->id);
            
            DB::beginTransaction();
         try {
                Document::whereIn("id",$ids)->delete();

                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }
}