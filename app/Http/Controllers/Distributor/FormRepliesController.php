<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Category;
use App\Product;
use App\Document;
use App\Product_step;
use App\Product_field;
use App\Product_design;
use App\Design_filed;
use App\GetQuote;
use DB;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
use Response;
use Excel;
use File;

class FormRepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('App\Http\Middleware\UserMiddleware');

    }
    public function index()
    {
      return view('distributor.form_replies.index');

    } //index

    public function table()
    {
        return view('distributor.form_replies.table');

    }//table

    public function row()
    {
        return view('distributor.form_replies.row');

    } //row
   
    public function getList(Request $request)
    {
      $search = $request->search;
      $form_replies=GetQuote::leftjoin('users','users.id','get_quote.distributorId')
        ->where(array('get_quote.distributorId'=>Auth::user()->id))
          ->where(function($q) use ($search)
            {
                  $q->orWhere('users.name', 'like', '%' . $search . '%');
            })
        ->select('get_quote.id','get_quote.company_name','get_quote.email','get_quote.created_at as date','users.name as distributor_name','get_quote.notification_status')
        ->orderBy("get_quote.id","desc")
        ->groupBy("get_quote.id")
        ->paginate(config('constants.categorylimit'));

     	return json_encode(array("form_replies"=>$form_replies,"pagination"=>str_replace('/?','?',$form_replies->render('common.pagination')) ));
    	
    } 

    public function viewDetailEdit()
    {
      return view('distributor.form_replies.view_detail');
    }

    public function getViewDetail(Request $request)
    {

        $notify = \App\GetQuote::find($request->id);
        if($notify->notification_status=="unread")
        {
            $notify->notification_status="read";
        }
        $notify->save();


      $quotation_detail=array();
      $quote_mail = \App\GetQuote::join('users','users.id','get_quote.distributorId')
              ->where(array('get_quote.id'=>$request->id))
              ->select('get_quote.*','users.name as distributor_name')->get();

      $quote_order = \App\Quotation_order::where(array('quotation_order.quoteId'=>$request->id))->select('*')->get(); 
        foreach ($quote_order as $res) {
            $quotation_detailRs = \App\Product_order::leftjoin('product_order_field','product_order_field.productOrderId','product_order.id')

            ->leftjoin('products','products.id','product_order.productId')
            ->leftjoin('category','category.id','products.categoryId')
            // ->leftjoin('product_design','product_design.id','product_order_field.designId')
            ->leftJoin('product_design', function ($join) {
                $join->on('product_order_field.designId', '=', 'product_design.id');
                $join->orOn('product_order.productDesignId', '=', 'product_design.id');
            })
            
            ->leftjoin('documents','documents.id','product_design.svgFileId')
            ->leftjoin('order_front_back','order_front_back.productOrderId','product_order.id')
            ->where(array('product_order.id'=>$res->productOrderId))
            ->select('product_order.*','products.name as product_name','category.name as category_name','product_design.name as design_name','product_design.svg_snippet','products.svg_dimension_ht','products.svg_dimension_vt','documents.name as doc_name',
                DB::raw('(select ofb.image from order_front_back as ofb where product_order.id=ofb.productOrderId and ofb.type="front" ) as image_front'),
                DB::raw('(select ofb1.image from order_front_back as ofb1 where product_order.id=ofb1.productOrderId and ofb1.type="back" ) as image_back')
            )

            ->groupBy('product_order.id')
            // ->orderBy('product_order.orderId','asc')
            ->get();

            foreach ($quotation_detailRs as $res_field) {
                $res_field->quoteId=intval($res->quoteId);
                $res_field->designData=$this->designData($res_field->id);
                $res_field->fieldData=$this->fieldData($res_field->id);
                $quotation_detail[]=$res_field;
            }
        }
     return json_encode(array("quotation_detail"=>$quotation_detail,'quote_mail'=>$quote_mail));
    }

    public function fieldData($productOrderId)
    {
        $field_basic_dtl=\App\Product_order_field::leftjoin('product_field','product_field.id','product_order_field.fieldId')
            ->join('product_order','product_order.id','product_order_field.productOrderId')
            ->where(array('product_order_field.productOrderId'=>$productOrderId))
            ->whereNotNull('product_order_field.fieldId')
            ->select('product_order_field.id','product_order_field.color','product_order_field.text','product_order_field.fontsize','product_order_field.position_x','product_order_field.position_y','product_order_field.image','product_field.name as field_name','product_field.filed_type',
                DB::raw('(select color.name from color where color.code=product_order_field.color and color.distributorId=product_order.distributorId) as color_name')
                )
            ->get();
        return $field_basic_dtl;
    }

    public function designData($productOrderId)
    {
        $design_basic_dtl=\App\Product_order_field::leftjoin('design_filed','design_filed.id','product_order_field.designfieldId')
            ->join('product_order','product_order.id','product_order_field.productOrderId')
            ->where(array('product_order_field.productOrderId'=>$productOrderId))
            ->whereNotNull('product_order_field.designfieldId')
            ->select('product_order_field.id','product_order_field.color','design_filed.name as designfield_name',
            DB::raw('(select color.name from color where color.code=product_order_field.color and color.distributorId=product_order.distributorId) as color_name')    
           )
            ->get();
        return $design_basic_dtl;
    }


    public function destroy($id,Request $request)
    {
        DB::beginTransaction();
     try {
            \App\Quotation_order::where(array('quoteId'=>$request->id))->delete();

            \App\GetQuote::where("id",$request->id)->delete();
            DB::commit();
            return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
         } 
      catch (\Throwable $e) {
        DB::rollback();
        return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')), 404); 
      }
    }


    public function exportFormReply(){
       
        $reply_data = DB::table('get_quote')->join('users','users.id','get_quote.distributorId')->where(array('get_quote.distributorId'=>Auth::user()->id))->select('get_quote.company_name','get_quote.email','get_quote.mobile','get_quote.message','users.name as distributor_name')->get()->toArray();
        $reply_array[] = array('Company Name', 'Email','Mobile','Message','Distributor Name');
        foreach($reply_data as $reply)
        {
          $reply_array[] = array(
          'Company Name'  => $reply->company_name,
          'Email'   => $reply->email,
          'Mobile'   => $reply->mobile,
          'Message'   => $reply->message,
          'Distributor Name'   => $reply->distributor_name,

          );
        }
        
        Excel::create('FormReply', function($excel) use ($reply_array){
            $excel->setTitle('FormReply');
            $excel->sheet('FormReply', function($sheet) use ($reply_array){
                $sheet->fromArray($reply_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }      

 


}