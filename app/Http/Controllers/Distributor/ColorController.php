<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\QuoteForm;
use App\Color;
use Session;
use DB;
use Rule;
use Mail;
use Auth;
use Excel;
use File;
use Response;
class ColorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     public function index()
    {
        return view('distributor.color.index');
    }
    public function table()
    {
        return view('distributor.color.table');
    }
    public function select()
    {
        return view('common.select');
    }
    public function row()
    {
        return view('distributor.color.row');
    }

    public function xlsTable()
    {
        return view('distributor.color.xls_download.table');
    }
    public function xlsRow()
    {
        return view('distributor.color.xls_download.row');
    }

    public function import()
    {
        return view('distributor.color.import');
    }

    public function getList(Request $request)
    {

        $search = $request->search;

        $sortBy = $request->sortBy;

        $sortName = $request->sortName;
      

      /*  if($sortName=="name")
        {
          $sortName="color.name";
        }
        else if($sortName=="code")
        {
            $sortName="color.code"; 
        }
        else
        {
          $sortName="color.".$sortName;
        }*/

      if($request->download_status=="xls_download")
      {
        $limit= 10000000000;
      }
      else{
        $limit= $request->limit; //config('constants.limit');
      }

    	$colors=Color::
        where(function($q) use ($search)
            {
                  $q->orWhere('color.name', 'like', '%' . $search . '%');
                  $q->orWhere('color.code', 'like', '%' . $search . '%');
            })
      ->where("distributorId",Auth::user()->id)
    	->select("color.*")
        // ->orderBy($sortName,$sortBy)
        ->groupBy('color.id')
    	->paginate($limit);

    
    	return json_encode(array("colors"=>$colors,"pagination"=>str_replace('/?','?',$colors->render('common.pagination'))));
    	
    }
    public function create()
    {
        $distributor=User::where(array("type"=>'distributor'))->orderBy('name','asc')->pluck("name","id")->prepend('All', '');

    	return view('distributor.color.editor',compact('distributor'));
    }

    public function store(Request $request)
    {
       
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 // 'name' => 'required|string|max:255',
                 'code' =>'required|',
                 // 'status' => 'required'

                ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }

            $check = Color::where(array('distributorId'=>Auth::user()->id,'code'=>$request->code))->get();

            if (count($check)>0) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>'Color already exist'),400);
            }


            DB::beginTransaction();
          try {
               
	            $color=new Color();
              $color->distributorId=Auth::user()->id; 
	            $color->name=$request->name;
	            $color->code=$request->code;
              $color->thumb=$request->code;
              if($request->status=="true")
              {
                $color->status="active";
              }
              else
              {
                $color->status="deactive";
              }
	            $color->save();

                DB::commit();
 
           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function update(Request $request)
    {
      
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 // 'name' => 'required|string|max:255',
                 'code' =>'required',
                 // 'status' => 'required'

                ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            

         DB::beginTransaction();
         try {           
                    $color=Color::find($request->id);
                    $color->distributorId=Auth::user()->id; 
                    $color->name=$request->name;
                    $color->code=$request->code;
                    $color->thumb=$request->code;
                    if($request->status=="true")
                    {
                      $color->status="active";
                    }
                    else
                    {
                      $color->status="deactive";
                    }
                    $color->save();

            DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

    public function downloadExcelFile(){
       
        $color_data = DB::table('color')->where(array('distributorId'=>Auth::user()->id))->get()->toArray();
        $color_array[] = array('name', 'code');
        foreach($color_data as $color)
        {
          $color_array[] = array(
          'name'  => $color->name,
          'code'   => $color->code,
          );
        }
        
        Excel::create('Color', function($excel) use ($color_array){
            $excel->setTitle('Colors');
            $excel->sheet('Colors', function($sheet) use ($color_array){
                $sheet->fromArray($color_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }      


    public function importExcel(Request $request)
    {
      
        $path = $request->file('import_file')->getRealPath();

        $data = Excel::load($path)->get();
        $flag=0;  
        $extension = File::extension($request->import_file->getClientOriginalName());   

        if($data->count())
        {
          foreach ($data as $value) 
          {
            if($extension!="csv")
            {
              $word_count=0;
              if(!isset($value['name'])){
                $flag=1;
              }
            }
            else 
            {
              $word_count=0;
              if(!isset($value->name)){
                $flag=1;
              }
            }

          }
        }
        if($flag==1)
        {
          return response()->json(array('resp'=>'ok','success'=>false,'msg'=>'Column does not match. Please download sample file.'),404);
        }
        

      $validator = Validator::make($request->all(), [
            'import_file' => 'required'
          ],
          [ 'import_file.required' => 'Please choose a file!' ]);

        if($validator->fails()) {
          return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
        }

       DB::beginTransaction();
       
       try {

          $extension = File::extension($request->import_file->getClientOriginalName());
          if ($extension != "csv" && $extension != "xls" && $extension != "xlsx") {
             return response()->json(array('resp'=>'ok','success'=>false,'msg'=>'The file type must be a csv or xlsx.'),400);
          }

          // $path = $request->file('import_file')->getRealPath();

          // $data = Excel::load($path)->get();
          
          // if($data->count()){
           
              /*foreach ($data as $key => $value) 
              {

                     if($extension!="csv")
                     {
                         foreach($value as $row)
                         {
                            if($row['name']!="")
                            {
                              $arr[] = [
                                  'distributorId' => Auth::user()->id,
                                  'name' =>$row['name'],
                                  'code'=> $row['code'],
                                  'thumb'=> $row['code']
                              ];
                            }
                         }
                      }
                      else
                      {

                        if($value->name!="")
                        {
           
                        $arr[] = [
                                  'distributorId' => Auth::user()->id,
                                  'name' =>$value->name,
                                  'code'=> $value->code,
                                  'thumb'=> $value->code
                              ];
                        }
                      }
              }
              if(!empty($arr))
              {
                for ($i=0; $i <count($arr) ; $i++) 
                { 
                  Color::insertColor($arr[$i]);
                }
              }*/

                if($request->hasFile('import_file')){
                    $path = $request->file('import_file')->getRealPath();
                    $data = Excel::load($path)->get();
                    if($data->count()){
                        foreach ($data as $key => $value) {
                            $arr[] = ['distributorId' => Auth::user()->id, 'name' => $value->name, 'code' => $value->code, 'thumb' => $value->code];

                        }
                        if(!empty($arr)){
                           for ($i=0; $i <count($arr) ; $i++) 
                            { 
                              Color::insertColor($arr[$i]);
                            }
                            
                        }
                    }
                }
           
          // }
             DB::commit();
           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
        }catch (\Throwable $e) {
            DB::rollback();
         return response()->json(array('resp'=>'ok','success'=>false,'msg'=>'Please check format'),404);
    }
  }


    public function download()
    {
      $filepath = public_path('/images/sample.csv');
      return Response::download($filepath);
    }

     public function destroy($id,Request $request)
    {
        DB::beginTransaction();
     try {

            Color::where("id",$request->id)->delete();

            DB::commit();
            return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
         } 
      catch (\Throwable $e) {
        DB::rollback();
        return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')), 404); 
      }
    }


    public function colorStatusChange(Request $request)
    {
      
        if($request->post())
        {              
              DB::beginTransaction();
           try {
                  $colorStatus= Color::find($request->id);
                  if($colorStatus->status=="active")
                  {
                    $colorStatus->status="deactive";  
                  }
                  else{
                    $colorStatus->status="active";
                  }
                  $colorStatus->save();
                  

                  DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
               } 
            catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
            }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
        }
    }

   
}