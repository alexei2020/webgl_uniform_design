<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Category;
use DB;
use App\Document;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
use App\Product;
class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('App\Http\Middleware\UserMiddleware');

    }
    public function index()
    {
         
      $category=Category::where(array("userId"=>Auth::user()->id,"parentId"=>null))->pluck("name","id")->prepend('All Categories', '');
      return view('distributor.category.index',array("category"=>$category));

    } //index

    public function table()
    {
        return view('distributor.category.table');

    }//table

    public function row()
    {

        return view('distributor.category.row');

    } //row
    public function getSubCategory(Request $request){
      $id=$request->id;
      if($id=="")
      {
        $id=0;
      }

      $result=Category::where("parentId",$id)->get();

      return json_encode(array("subcategory"=>$result));

    }
    public function getList(Request $request)
    {
      
       $category=$request->category;
        $subcategory=$request->subcategory;
        $search = $request->search;

        $limit=$request->limit;

         $data=array("category"=>$category,"subcategory"=>$subcategory);
      $categorys=Category::
    //  leftjoin("category as parentcategory",'parentcategory.id','category.parentId')
       leftjoin("category as childcategory",'childcategory.parentId','category.id')
      ->leftjoin("documents",'documents.id','category.image')
      ->Where('category.userId', Auth::user()->id)
      ->Where('category.name', 'like', '%' . $search.'%')
      ->when($data,function($q) use ($data)
          {
              if ($data['subcategory']=="" && $data['category']!="") {
                $q->where('category.parentId',$data['category']);
                $q->orwhere('category.id',$data['category']);
              }
              else if($data['subcategory']!="")
              {
               // $q->orwhere('category.id',$data['category']);
                $q->whereIn('category.id',array($data['subcategory'],$data['category']));
              }
            
        })
      ->select("category.name",'category.id','category.parentId','childcategory.id as childcategoryid','category.url_slug','documents.name as image','documents.id as imageId','category.status','category.position')
      ->orderBy("category.id","desc")
      ->groupBy("category.id")
      ->paginate($limit);
      // ->paginate(config('constants.categorylimit'));
     	return json_encode(array("category"=>$categorys,"pagination"=>str_replace('/?','?',$categorys->render('common.pagination')) ));
    	
 
    } // getCategory

    public function create()
    {
    	$category=Category::where(array("userId"=>Auth::user()->id,"parentId"=>null))->pluck("name","id")->prepend('Select Category', '');

      $document=Document::where(array("distributorId"=>Auth::user()->id))->whereNotIn("type",['svg','obj'])->select('name','id','original_name')->get();
    	return view('distributor.category.editor',array("category"=>$category,'document'=>$document));
    } //create

    public function store(Request $request)
    { 
     
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'url_slug' => 'regex:/^[a-z0-9\-]+$/u',
                 'image' => 'required',
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }

            $flag=0;
            $checkurl=Category::where(array('userId'=>Auth::user()->id))->select('url_slug')->get();
            foreach ($checkurl as $check) {
              if($request->url_slug==$check->url_slug)
              {
                $flag=1;
              }
            }
            if($flag==1)
            {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The url slug has already been taken."),400);
            }
           
             DB::beginTransaction();
          try {
              
              
              $category=new Category();
              $category->userId = Auth::user()->id;
              $category->parentId = $request->categoryId;
              $category->url_slug = $request->url_slug;
              $category->image =$request->image;
              $category->position=$request->position;
              if($request->status=='true')
              {
                $category->status = "active";  
              }
              else{
                $category->status = "deactive";  
              }
              $category->name=$request->name;
              $category->save();
             
              DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function update(Request $request)
    {

         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'url_slug' => 'regex:/^[a-z0-9\-]+$/u',
                 'image' => 'required',
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }

            $flag=0;
            $checkurl=Category::where(array('userId'=>Auth::user()->id))->select('url_slug','id')->get();
            foreach ($checkurl as $check) {
              if($check->id!=$request->id)
              {
                if($request->url_slug==$check->url_slug)
                {
                  $flag=1;
                }  
              }
            }
            if($flag==1)
            {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The url slug has already been taken."),400);
            }

             DB::beginTransaction();
          try {
          
               
              $category=Category::find($request->id);
              $category->userId = Auth::user()->id;
              $category->parentId = $request->categoryId;
              $category->url_slug = $request->url_slug;
              $category->image =$request->image;
              $category->position=$request->position;
              if($request->status=="true")
              {
                $category->status = "active";  
              }
              else{
                $category->status = "deactive";  
              }
              $category->name=$request->name;
              $category->save();
             
            DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }
    
     public function destroy($id,Request $request)
    {
      
          
      if($request->post())
      {

            DB::beginTransaction();
         try {
              $ids = explode(",",$request->id);
                /*Category::whereIn("id",$ids)->delete();

                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); */

                  $checkProduct = Product::where(array('products.categoryId'=>$ids,"products.deleted"=>1))->get();
                  if(count($checkProduct)>0)
                  {
                      foreach ($checkProduct as $res) {
                          \App\Quotation_order::join('product_order','product_order.id','quotation_order.productOrderId')->where(array('product_order.productId'=>$res->id))->delete();

                          \App\Player_setting::join('product_order','product_order.id','player_setting.productOrderId')->where(array('product_order.productId'=>$res->id))->delete();

                          $imagefrontBack = \App\Order_front_back::join('product_order','product_order.id','order_front_back.productOrderId')->where(array('product_order.productId'=>$res->id))->get();
                            foreach ($imagefrontBack as $value) {
                                if($value->image!="" && file_exists(public_path('images/front_back/'.$value->image)))
                                {
                                    unlink(public_path('images/front_back/'.$value->image));
                                }
                            }

                          \App\Order_front_back::join('product_order','product_order.id','order_front_back.productOrderId')->where(array('product_order.productId'=>$res->id))->delete();

                          \App\Product_order_field::join('product_order','product_order.id','product_order_field.productOrderId')->where(array('product_order.productId'=>$res->id))->delete();
                          \App\Product_order::where(array('product_order.productId'=>$res->id))->delete();

                          \App\Design_filed::join('product_design','product_design.id','design_filed.designId')->where(array('product_design.productId'=>$res->id))->delete();

                          \App\Product_design::where(array('product_design.productId'=>$res->id))->delete();
                          \App\Product_field::where(array('product_field.productId'=>$res->id))->delete();
                          \App\Product_step::where(array('product_step.productId'=>$res->id))->delete();
                          Product::where(array('products.id'=>$res->id))->delete();
                      }
                  }
                

                 $result = Category::whereIn("id",$ids)->delete();
                if($result == 1)
                { 
                    DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
                }
                else
                {
                    DB::rollBack();
                    return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
                }

             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

    public function statusChange(Request $request)
    {
      
        if($request->post())
        {              
              DB::beginTransaction();
           try {
                  $categoryStatus= Category::find($request->id);
                  if($categoryStatus->status=="active")
                  {
                    $categoryStatus->status="deactive";  
                  }
                  else{
                    $categoryStatus->status="active";
                  }
                  $categoryStatus->save();
                  

                  DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
               } 
            catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
            }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
        }
    }


    public function saveUploadDoc(Request $request)
    {

        if($request->post())
        {
          $allow_image=array("jpeg","jpg","png");
            $validator = Validator::make($request->all(), [
                 'file' => 'required|max:30048'
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }

             DB::beginTransaction();
          try {
              
                $imageName=""; $image_name=""; $sizefile=""; $extension="";
               if($request->hasFile('file')) 
               {
                  $image = $request->file('file');
                  $sizefile=$image->getClientSize();
                  $image_name=$image->getClientOriginalName();
                  $extension = $image->getClientOriginalExtension();
                  
                  if(!in_array($extension,$allow_image))
                  {
                     return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("name"=>["The name must be a file of type: jpg, jpeg, png."])),400);
                  }

                  if($extension=="obj" || $extension=="svg")
                  {
                    return response()->json(array('resp'=>'ok','success'=>false,'msg'=>array("name"=>["The name must be a file of type:  jpg, jpeg, png."])),400);
                  }

                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data = url('/')."/public/images/$imageName";

                


               }
              $document=new Document();
              $document->original_name = $image_name;
              $document->name = $imageName;
               $document->type = $extension;
              $document->distributorId = Auth::user()->id;
              $document->file_size = $sizefile;
              $document->createDate = date('Y-m-d');
              $document->save();  
              
              $value =  asset("public/images/").'/'.$document->name;
              $option ='<option value="'.$document->id.'" style=" background-repeat: no-repeat;background-size: 20px 20px;background-image:url('.$value.')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$document->original_name.'</option>';
             
             $data=[
              'option'=>$option,
              'type'=>$document->type,
              'id'=>$document->id,
             ];
              DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$data));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
}