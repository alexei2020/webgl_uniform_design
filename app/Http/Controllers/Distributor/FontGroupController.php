<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\QuoteForm;
use App\Group;
use App\Font;
use Session;
use DB;
use Rule;
use Mail;
use Auth;
class FontGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     public function index()
    {
        return view('distributor.font_group.index');
    }
    public function table()
    {
        return view('distributor.font_group.table');
    }
    public function select()
    {
        return view('common.select');
    }
    public function row()
    {
        return view('distributor.font_group.row');
    }
    public function getList(Request $request)
    {

        $search = $request->search;

        $sortBy = $request->sortBy;

        $sortName = $request->sortName;

      /*  if($sortName=="name")
        {
          $sortName="color.name";
        }
        else if($sortName=="code")
        {
            $sortName="color.code"; 
        }
        else
        {
          $sortName="color.".$sortName;
        }*/


    	$fontgroup=Group::
        join('group_fc','group_fc.groupId','groups.id')
        ->where(array('groups.type'=>"font",'groups.distributorId'=>Auth::user()->id))
        ->where(function($q) use ($search)
            {
                  $q->orWhere('groups.name', 'like', '%' . $search . '%');
            })
    	->select("groups.*",DB::raw("(GROUP_CONCAT(group_fc.fontId SEPARATOR ',')) as fId"))
        // ->orderBy($sortName,$sortBy)
        ->groupBy('groups.id')
    	->paginate(config('constants.limit'));

      $font=Font::where(array("distributorId"=>Auth::user()->id))->orderBy('name','desc')->get();
    
    	return json_encode(array("fontgroup"=>$fontgroup,"font"=>$font,"pagination"=>str_replace('/?','?',$fontgroup->render('common.pagination'))));
    	
    }
    public function create()
    {
        $font=Font::where(array("distributorId"=>Auth::user()->id))->orderBy('name','asc')->pluck("name","id");

    	return view('distributor.font_group.editor',compact('font'));
    }

    public function store(Request $request)
    {
       
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',

                ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }

            DB::beginTransaction();
          try {
               
	            $font=new Group();
              $font->distributorId=Auth::user()->id; 
              $font->name=$request->name;
              // $font->fontId=$request->fontId;
              $font->type = "font";
              $font->save();
              if($request->fontId!="")
                {
                  $fontId=explode(",",$request->fontId);
                  // \App\Group_fc::where(array('groupId'=>$font->id,'type'=>'font'))->delete();
                  for($i=0;$i<count($fontId);$i++)
                  {
                      $fc =  new \App\Group_fc();
                      $fc->groupId = $font->id;
                      $fc->fontId = $fontId[$i];
                      $fc->type = 'font';
                      $fc->save();
                  }
                }

                DB::commit();
 
           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function update(Request $request)
    {
      
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            

         DB::beginTransaction();
         try {           
                    $font=Group::find($request->id);
                    $font->distributorId=Auth::user()->id; 
                    $font->name=$request->name;
                    // $font->fontId=$request->fontId;
                    $font->type = "font";
                    $font->save();
                    if($request->fontId!="")
                    {
                      $fontId=explode(",",$request->fontId);
                      \App\Group_fc::where(array('groupId'=>$font->id,'type'=>'font'))->delete();
                      for($i=0;$i<count($fontId);$i++)
                      {
                          $fc =  new \App\Group_fc();
                          $fc->groupId = $font->id;
                          $fc->fontId = $fontId[$i];
                          $fc->type = 'font';
                          $fc->save();
                      }
                    }

            DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

     public function destroy($id,Request $request)
    {
        DB::beginTransaction();
     try {
            \App\Group_fc::where(array('groupId'=>$request->id,'type'=>'font'))->delete();
            Group::where(array("id"=>$request->id, "type"=>"font"))->delete();

            DB::commit();
            return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
         } 
      catch (\Throwable $e) {
        DB::rollback();
        return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')), 404); 
      }
    }

   
}