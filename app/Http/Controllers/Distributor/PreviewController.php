<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;

use App\QuoteForm;
use App\Color;
use Session;
use DB;
use Rule;
use Mail;
use Auth;

class PreviewController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
   
    public function index($id)
    {
        // $checkuser = User::where(array('id'=>$id))->get();
        if(User::where('id', $id)->exists())
        {
            return view('distributor.preview.index',array("id"=>$id));    
        }
        else{
            return view('errors.404');
        }
    		
    	    
    }

   
}