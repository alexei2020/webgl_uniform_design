<?php
namespace App\Http\Controllers\Distributor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\QuoteForm;
use App\Font;
use Session;
use DB;
use Rule;
use Mail;
use Auth;
use Excel;
use File;
use Response;

class FontController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     public function index()
    {
        return view('distributor.font.index');
    }
    public function table()
    {
        return view('distributor.font.table');
    }
    public function select()
    {
        return view('common.select');
    }
    public function row()
    {
        return view('distributor.font.row');
    }

    public function xlsTable()
    {
        return view('distributor.font.xls_download.table');
    }
    public function xlsRow()
    {
        return view('distributor.font.xls_download.row');
    }

    public function import()
    {
        return view('distributor.font.import');
    }

    public function getList(Request $request)
    {

        $search = $request->search;

        $sortBy = $request->sortBy;

        $sortName = $request->sortName;

      if($request->download_status=="xls_download")
      {
        $limit= 10000000000;
      }
      else{
        $limit= config('constants.limit');
      }

    	 $font=Font::
        where(function($q) use ($search)
            {
                  $q->orWhere('font.name', 'like', '%' . $search . '%');
            })
      ->where("distributorId",Auth::user()->id)
    	->select("font.*")
        // ->orderBy($sortName,$sortBy)
        ->groupBy('font.id')
    	->paginate($limit);

    
    	return json_encode(array("font"=>$font,"pagination"=>str_replace('/?','?',$font->render('common.pagination'))));
    	
    }
    public function create()
    {
        $distributor=User::where(array("type"=>'distributor'))->orderBy('name','asc')->pluck("name","id")->prepend('All', '');

    	return view('distributor.font.editor',compact('distributor'));
    }

    public function store(Request $request)
    {
       
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 // 'name' => 'required|string|max:255',
                 'value' =>'required',
                 // 'status' => 'required'

                ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }
            $check = Font::where(array('distributorId'=>Auth::user()->id,'value'=>$request->value))->get();

            if (count($check)>0) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>'Font already exist'),400);
            }

            DB::beginTransaction();
          try {
               
	            $font=new Font();
              $font->distributorId=Auth::user()->id; 
	            $font->name=$request->name;
	            $font->value=$request->value;
              if($request->status=="true")
              {
                $font->status="active";
              }
              else
              {
                $font->status="deactive";
              }
	            $font->save();

                DB::commit();
 
           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function update(Request $request)
    {
      
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 // 'name' => 'required|string|max:255',
                 'value' =>'required',
                
                 // 'status' => 'required'

                ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            

         DB::beginTransaction();
         try {           
                    $font=Font::find($request->id);
                    $font->distributorId=Auth::user()->id; 
                    $font->name=$request->name;
                    $font->value=$request->value;
                    if($request->status=="true")
                    {
                      $font->status="active";
                    }
                    else
                    {
                      $font->status="deactive";
                    }
                    $font->save();

            DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

     public function destroy($id,Request $request)
    {
        DB::beginTransaction();
     try {
            // $font = Font::where('id',$request->id)->first();
            
            Font::where("id",$request->id)->delete();

            DB::commit();
            return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
         } 
      catch (\Throwable $e) {
        DB::rollback();
        return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')), 404); 
      }
    }


    public function fontStatusChange(Request $request)
    {
      
        if($request->post())
        {              
              DB::beginTransaction();
           try {
                  $fontStatus= Font::find($request->id);
                  if($fontStatus->status=="active")
                  {
                    $fontStatus->status="deactive";  
                  }
                  else{
                    $fontStatus->status="active";
                  }
                  $fontStatus->save();
                  

                  DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
               } 
            catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
            }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
        }
    }


    public function download()
    {
      $filepath = public_path('/images/sample_font.csv');
      return Response::download($filepath);
    }


    public function downloadExcelFile(){
       
        $font_data = DB::table('font')->where(array('distributorId'=>Auth::user()->id))->get()->toArray();
        $font_array[] = array('name', 'value');
        foreach($font_data as $font)
        {
          $font_array[] = array(
          'name'  => $font->name,
          'value'   => $font->value,
          );
        }
        
        Excel::create('Font', function($excel) use ($font_array){
            $excel->setTitle('Fonts');
            $excel->sheet('Fonts', function($sheet) use ($font_array){
                $sheet->fromArray($font_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }   


    public function importExcel(Request $request)
    {

        $path = $request->file('import_file')->getRealPath();

        $data = Excel::load($path)->get();
        $flag=0;  
        $extension = File::extension($request->file('import_file')->getClientOriginalName()); 
        if($extension=="ttf" || $extension=="TTF"){
          $flag=0;
        }
        else
        {
          if($data->count()){
            foreach ($data as $value) {
              if($extension!="csv"){
                $word_count=0;
                if(!isset($value['name'])){
                  $flag=1;
                }
              }
              else {
                $word_count=0;
                if(!isset($value->name)){
                  $flag=1;
                }
              }
            }
          }
        }

        if($flag==1)
        {
          return response()->json(array('resp'=>'ok','success'=>false,'msg'=>'Column does not match. Please download sample file.'),404);
        }


      $validator = Validator::make($request->all(), [
            'import_file' => 'required'
          ],
          [ 'import_file.required' => 'Please choose a file!' ]);

        if($validator->fails()) {
          return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
        }

       DB::beginTransaction();
       
       try {

          $extension = File::extension($request->file('import_file')->getClientOriginalName());
          if ($extension != "csv" && $extension != "xls" && $extension != "xlsx" && $extension!="ttf" && $extension!="TTF") {
             return response()->json(array('resp'=>'ok','success'=>false,'msg'=>'The file type must be a csv or xlsx or ttf.'),400);
          }

          $path = $request->file('import_file')->getRealPath();
          if($extension=="ttf" || $extension=="TTF")
          {
              /*if($request->hasFile('import_file')) 
              {*/
                  $image = $request->file('import_file');
                  $sizefile=$image->getClientSize();
                  $image_name=$image->getClientOriginalName();
                  $extension = $image->getClientOriginalExtension();
                  
                  
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images/fonts'),$imageName); 
                  $data = url('/')."/public/images/fonts/$imageName";
                  $font = new Font();
                  $font->distributorId=Auth::user()->id;
                  $font->name=pathinfo($image_name, PATHINFO_FILENAME);
                  $font->value=pathinfo($image_name, PATHINFO_FILENAME);
                  $font->font_file=$imageName;
                  $font->font_file_original_name=$image_name;
                  $font->save();
              // }
          }
          else{
              /*$data = Excel::load($path)->get();
              if($data->count()){
           
                  foreach ($data as $key => $value) 
                  {

                          if($extension!="csv")
                          {
                             foreach($value as $row)
                             {
                                if($row['name']!="")
                                {
                                  $arr[] = [
                                      'distributorId' => Auth::user()->id,
                                      'name' =>$row['name'],
                                      'value'=> $row['value'],
                                  ];
                                }
                             }
                          }
                          else
                          {
                            if($value->name!="")
                            {
               
                            $arr[] = [
                                  'distributorId' => Auth::user()->id,
                                  'name' =>$value->name,
                                  'value'=> $value->value,
                              ];
                            }
                          }
                  }
                  if(!empty($arr))
                  {
                    for ($i=0; $i <count($arr) ; $i++) 
                    { 
                      Font::insertFont($arr[$i]);
                    }
                  }
               
              }*/

              if($request->hasFile('import_file')){
                    $path = $request->file('import_file')->getRealPath();
                    $data = Excel::load($path)->get();
                    if($data->count()){
                        foreach ($data as $key => $value) {
                            $arr[] = ['distributorId' => Auth::user()->id, 'name' => $value->name, 'value' => $value->value];

                        }
                        if(!empty($arr)){
                           for ($i=0; $i <count($arr) ; $i++) 
                            { 
                              Font::insertFont($arr[$i]);
                            }
                            
                        }
                    }
                }
          }
          
          DB::commit();
          return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
        }catch (\Throwable $e) {
            DB::rollback();
         return response()->json(array('resp'=>'ok','success'=>false,'msg'=>'Please check format'),404);
    }
  }
   
}