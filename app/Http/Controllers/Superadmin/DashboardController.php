<?php
namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Companies;
use App\User;
use App\Businessunit;
use App\CompaniesBusinessunit;
use Session;
use DB;
use Rule;
use Mail;
class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     public function index()
    {
    	
        return view('superadmin.dashboard.index');
    }
    
   
}