<?php
namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Category;
use App\Product;
use App\Document;
use App\Product_step;
use App\Product_field;
use App\Product_design;
use App\Design_filed;
use App\User;
use DB;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    public function index()
    {

     $distributor=User::where("type","distributor")->pluck("name","id")->prepend('Select Distributor', '');
      return view('superadmin.products.index',array("distributor"=>$distributor));

    } //index

    public function table()
    {
        return view('superadmin.products.table');

    }//table

    public function row()
    {
        return view('superadmin.products.row');

    } //row
   
    public function getList(Request $request)
    {
      $search = $request->search;
       $distributorId =  $request->distributor;

        $sortBy = $request->sortBy;
        $sortName = $request->sortName;

        if($sortName=="distributor_name")
        {
          $sortName="users.name";
        }
        else if($sortName=="name")
        {
            $sortName="products.name"; 
        }
        else if($sortName=="category")
        {
            $sortName="category.name"; 
        }
        else if($sortName=="bu_name")
        {
            $sortName="products.url_slug"; 
        }
        else if($sortName=="position")
        {
            $sortName="products.position"; 
        }
        else if($sortName=="status")
        {
            $sortName="products.status"; 
        }
        else
        {
          $sortName="products.".$sortName;
        }



      $products=Product::join('category','category.id','products.categoryId')
          ->leftjoin('documents','documents.id','products.list_ImageId')
          ->join('users','users.id','products.distributorId')
          ->where('products.deleted',0)
           ->when($distributorId,function($q) use ($distributorId)
            {
               if($distributorId!="")
               {
                $q->Where('products.distributorId',$distributorId);
               }
             
            })
          ->where(function($q) use ($search)
            {
            	  $q->orWhere('users.name', 'like', '%' . $search . '%');
                  $q->orWhere('products.name', 'like', '%' . $search . '%');
                  $q->orWhere('category.name', 'like', '%' . $search . '%');
                  $q->orWhere('products.url_slug', 'like', '%' . $search . '%');
            })
        ->select('users.name as distributorName','products.*','category.name as category_name','documents.name as image')
        // ->orderBy("products.id","desc")
        ->orderBy($sortName,$sortBy)
        ->groupBy("products.id")
        ->paginate(config('constants.categorylimit'));

     	return json_encode(array("product"=>$products,"pagination"=>str_replace('/?','?',$products->render('common.pagination')) ));
    	
    } 

    public function editProduct(Request $request)
    {
      $distributorId=0;
      $fc_groups= array(); //->pluck('name','id');

     

      if($request->productId!=0)
      {
        $product=Product::where("id",$request->productId)->select("distributorId")->get();
        if(count($product)>0)
        {
          $distributorId=$product[0]['distributorId'];
        }
       
         $fc_groups= \App\Group::where(array('distributorId'=>$distributorId))->select('groups.*')->get(); //->pluck('name','id');

        $product_step = Product_step::where(array('product_step.distributorId'=>$distributorId, 'product_step.productId'=>$request->productId))
            ->select('product_step.*')
            ->orderBy("product_step.productId","desc")
            ->groupBy("product_step.id")
            ->get();

        $product_field = Product_field::join('product_step','product_step.id','product_field.stepId')
            ->where(array('product_field.distributorId'=>$distributorId, 'product_field.productId'=>$request->productId))
            ->select('product_field.*','product_step.name as step_name')
            ->orderBy("product_field.productId","desc")
            ->groupBy("product_field.id")
            ->get();


        $product_design = Product_design::leftjoin('design_filed','design_filed.designId','product_design.id')->where(array('product_design.distributorId'=>$distributorId, 'product_design.productId'=>$request->productId))
            ->select('product_design.*',DB::raw("(GROUP_CONCAT(design_filed.id SEPARATOR '@')) as designFiledID"))
            ->orderBy("product_design.productId","desc")
            ->groupBy("product_design.id")
            ->get();
            
            $design_filed = Design_filed::join('product_step','product_step.id','design_filed.stepId')->select('design_filed.design_key','design_filed.groupId','design_filed.stepId','design_filed.designId','design_filed.name as design_filed_name','design_filed.id','product_step.name as step_name')->get();


            $list_image = Document::where('distributorId',$distributorId)->whereIn("type",['png','svg','jpeg','jpg'])->orderBy('documents.id','desc')->get('documents.*');
           
            $model_3D = Document::where(array('distributorId'=>$distributorId,"type"=>"obj"))->select("name","id")->orderBy('documents.id','desc')->get();

          $category=Category::where(array("userId"=>$distributorId,"parentId"=>null))->select("name","id")->get();

          $document=Document::where(array("distributorId"=>$distributorId))->select('original_name as name','id')->orderBy('documents.id','desc')->get();


      }
      else{
          $product_step=array();
          $product_field=array();
          $product_design=array();
          $design_filed=array();
          $list_image=array();
          $model_3D=array();
          $category=array();
          $document=array();


      }

      return json_encode(array("product_step"=>$product_step,"product_field"=>$product_field,"product_design"=>$product_design,'fc_groups'=>$fc_groups,"design_filed"=>$design_filed,"document"=>$document,"category"=>$category,"model_3D"=>$model_3D,"list_image"=>$list_image));
    }


    public function getProductStep(Request $request){

       $search = $request->search; $distributorId=0;
      $product_step= array(); //->pluck('name','id');

      

      if($request->productId!=0)
      {
        $product=Product::where("id",$request->productId)->select("distributorId")->get();
        if(count($product)>0)
        {
          $distributorId=$product[0]['distributorId'];
        }

         $product_step = Product_step::where(array('product_step.distributorId'=>$distributorId, 'product_step.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_step.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_step.position', 'like', '%' . $search . '%');
              })
            ->select('product_step.*')
            ->orderBy("product_step.productId","desc")
            ->groupBy("product_step.id")
            ->get();

      }
        return json_encode(array("product_step"=>$product_step));
    }


  public function getProductField(Request $request){

       $search = $request->search; $distributorId=0;
      $product_field= array(); //->pluck('name','id');

      

      if($request->productId!=0)
      {
        $product=Product::where("id",$request->productId)->select("distributorId")->get();
        if(count($product)>0)
        {
          $distributorId=$product[0]['distributorId'];
        }


        $product_field = Product_field::join('product_step','product_step.id','product_field.stepId')
            ->where(array('product_field.distributorId'=>$distributorId, 'product_field.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_field.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_step.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_field.filed_type', 'like', '%' . $search . '%');
              })
            ->select('product_field.*','product_step.name as step_name')
            ->orderBy("product_field.productId","desc")
            ->groupBy("product_field.id")
            ->get();

      }
        return json_encode(array("product_field"=>$product_field));
    }



    public function getProductDesign(Request $request){

        $search = $request->search; $distributorId=0;
        $product_design= array(); //->pluck('name','id');

      

      if($request->productId!=0)
      {
        $product=Product::where("id",$request->productId)->select("distributorId")->get();
        if(count($product)>0)
        {
          $distributorId=$product[0]['distributorId'];
        }

        
        $product_design = Product_design::leftjoin('design_filed','design_filed.designId','product_design.id')->where(array('product_design.distributorId'=>$distributorId, 'product_design.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_design.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_design.position', 'like', '%' . $search . '%');
              })
            ->select('product_design.*',DB::raw("(GROUP_CONCAT(design_filed.id SEPARATOR '@')) as designFiledID"))
            ->orderBy("product_design.productId","desc")
            ->groupBy("product_design.id")
            ->get();


            $design_filed = Design_filed::join('product_step','product_step.id','design_filed.stepId')->select('design_filed.design_key','design_filed.groupId','design_filed.stepId','design_filed.designId','design_filed.name as design_filed_name','design_filed.id','product_step.name as step_name')->get();

      }
        return json_encode(array("product_design"=>$product_design,'design_filed'=>$design_filed));
    }


     public function getDetailList(Request $request)
    {
      $search = $request->search; $distributorId=0;
      $fc_groups= array(); //->pluck('name','id');

     

      if($request->productId!=0)
      {
        $product=Product::where("id",$request->productId)->select("distributorId")->get();
        if(count($product)>0)
        {
          $distributorId=$product[0]['distributorId'];
        }
       
         $fc_groups= \App\Group::where(array('distributorId'=>$distributorId))->select('groups.*')->get(); //->pluck('name','id');

        $product_step = Product_step::where(array('product_step.distributorId'=>$distributorId, 'product_step.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_step.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_step.position', 'like', '%' . $search . '%');
              })
            ->select('product_step.*')
            ->orderBy("product_step.productId","desc")
            ->groupBy("product_step.id")
            ->get();

        $product_field = Product_field::join('product_step','product_step.id','product_field.stepId')
            ->where(array('product_field.distributorId'=>$distributorId, 'product_field.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_field.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_step.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_field.filed_type', 'like', '%' . $search . '%');
              })
            ->select('product_field.*','product_step.name as step_name')
            ->orderBy("product_field.productId","desc")
            ->groupBy("product_field.id")
            ->get();


        $product_design = Product_design::leftjoin('design_filed','design_filed.designId','product_design.id')->where(array('product_design.distributorId'=>$distributorId, 'product_design.productId'=>$request->productId))
            ->where(function($q) use ($search)
              {
                  $q->orWhere('product_design.name', 'like', '%' . $search . '%');
                  $q->orWhere('product_design.sku', 'like', '%' . $search . '%');
              })
            ->select('product_design.*',DB::raw("(GROUP_CONCAT(design_filed.id SEPARATOR '@')) as designFiledID"))
            ->orderBy("product_design.productId","desc")
            ->groupBy("product_design.id")
            ->get();
            
            $design_filed = Design_filed::join('product_step','product_step.id','design_filed.stepId')->select('design_filed.design_key','design_filed.groupId','design_filed.stepId','design_filed.designId','design_filed.name as design_filed_name','design_filed.id','product_step.name as step_name')->get();


      }
      else{
          $product_step=array();
          $product_field=array();
          $product_design=array();
          $design_filed=array();



      }

      return json_encode(array("product_step"=>$product_step,"product_field"=>$product_field,"product_design"=>$product_design,'fc_groups'=>$fc_groups,"design_filed"=>$design_filed));
    }



    public function filterCategory(Request $request)
    {
      $data=DB::table('category')->where(array('userId'=>Auth::user()->id))
      ->where('category.name', 'like', '%' . $request->search . '%')
        ->select("category.name","category.id")
        ->orderBy("category.name","asc")
        ->get();
        
      return response()->json(array('resp'=>'ok',"data"=>$data,'success'=>true,'msg'=>''));
    }


    public function create()
    { 
    	return view('superadmin.products.editor',array("category"=>array(),'document'=>array(),'list_image'=>array(),'model_3D'=>array()));
    } //create


    public function svgSelectorCreate()
    {
        return view('superadmin.products.editorsvgselector');
    }

    public function store(Request $request)
    { 
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 // 'url_slug' => 'required|regex:/^[a-z0-9\-]+$/u|unique:products,url_slug',
                 'url_slug' => 'required|regex:/^[a-z0-9\-]+$/u',
                 'category'=>'required',
                 'list_ImageId'=>'required',
                 'model_3DId'=>'required',
                 'svg_width'=>'required',
                 'svg_height'=>'required',
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }

            $flag=0;
            $checkurl=Product::where(array('distributorId'=>Auth::user()->id,'deleted'=>0))->select('url_slug','id')->get();
            foreach ($checkurl as $check) {
              
                if($request->url_slug==$check->url_slug)
                {
                  $flag=1;
                }  
              
            }
            if($flag==1)
            {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The product slug has already been taken."),400);
            }
           
             DB::beginTransaction();
          try {
              

              $product=new Product();
              $product->distributorId = Auth::user()->id;
              $product->categoryId = $request->categoryId;
              $product->list_ImageId = $request->list_ImageId;
              $product->model_3DId = $request->model_3DId;
              $product->name=$request->name;
              $product->url_slug = $request->url_slug;
              $product->description = htmlentities(($request->description), ENT_QUOTES);
              $product->rotation_disable=$request->rotation_disable;
              $product->cameras=$request->cameras;
              $product->camera_override=$request->camera_override;
              $product->x_axis=$request->x_axis;
              $product->y_axis=$request->y_axis;
              $product->z_axis=$request->z_axis;
              $product->scale=$request->scale;
              $product->override_model_position_scale=$request->override_model_position_scale;
              $product->light_brightness=$request->light_brightness;
              $product->svg_dimension_ht=$request->svg_dimension_ht;
              $product->svg_dimension_vt=$request->svg_dimension_vt;
              $product->hight_on_basker_page=$request->hight_on_basker_page;
              $product->default_toggle_color=$request->default_toggle_color;
              $product->position=$request->position;
             
              if($request->status=='true')
              {
                $product->status = "active";  
              }
              else{
                $product->status = "deactive";  
              }
             
              $product->save();
             
              DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }

    public function getSVGDimension(Request $request)
    {
        $list_image = Document::find($request['list_imageId']);
        $imagedetails = getimagesize(url('public/images/')."/".$list_image->name);
        return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$imagedetails));
    }
    
     public function update(Request $request)
    {
      
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 // 'url_slug' => 'regex:/^[a-z0-9\-]+$/u|unique:products,url_slug,'.$request->id,
                 'url_slug' => 'regex:/^[a-z0-9\-]+$/u',
                 'category'=>'required',
                 'list_ImageId'=>'required',
                 'model_3DId'=>'required',
                 'svg_width'=>'required',
                 'svg_height'=>'required',
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }

             $flag=0;
            $checkurl=Product::where(array('distributorId'=>Auth::user()->id))->select('url_slug','id')->get();
            foreach ($checkurl as $check) {
              if($check->id!=$request->id)
              {
                if($request->url_slug==$check->url_slug)
                {
                  $flag=1;
                }  
              }
            }
            if($flag==1)
            {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The product slug has already been taken."),400);
            }

             DB::beginTransaction();
          try {
               
              $product=Product::find($request->id);
              $product->categoryId = $request->categoryId;
              $product->list_ImageId = $request->list_ImageId;
              $product->model_3DId = $request->model_3DId;
              $product->name=$request->name;
              $product->url_slug = $request->url_slug;
              $product->description= htmlentities(($request->description), ENT_QUOTES); 
              $product->rotation_disable=$request->rotation_disable;
              $product->cameras=$request->cameras;
              $product->camera_override=$request->camera_override;
              $product->x_axis=$request->x_axis;
              $product->y_axis=$request->y_axis;
              $product->z_axis=$request->z_axis;
              $product->scale=$request->scale;
              $product->override_model_position_scale=$request->override_model_position_scale;
              $product->light_brightness=$request->light_brightness;
              $product->svg_dimension_ht=$request->svg_dimension_ht;
              $product->svg_dimension_vt=$request->svg_dimension_vt;
              $product->hight_on_basker_page=$request->hight_on_basker_page;
              $product->default_toggle_color=$request->default_toggle_color;
              $product->position=$request->position;

             
              if($request->status=='true')
              {
                $product->status = "active";  
              }
              else {
                $product->status = "deactive";  
              }
             
              $product->save();
             
            DB::commit();

            $data=[
              'id'=>$product->id,
              'x_axis'=>$product->x_axis,
              'y_axis'=>$product->y_axis,
              'z_axis'=>$product->z_axis,
              'scale'=>$product->scale,
            ];
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>'','result'=>$data));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }
    
     public function destroy($id,Request $request)
    {
      if($request->post())
      {
         
            DB::beginTransaction();
         try {
                // Product::whereIn("id",$id)->delete();
                 $product = Product::find($request->id);
                $product->deleted = 1;
                $product->save();
                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }


    public function stepTable()
    {
        return view('superadmin.products.step.table');

    }//table

    public function stepRow()
    {

        return view('superadmin.products.step.row');

    } //row

     public function stepCreate()
    {
      return view('superadmin.products.step.editor');
    } //create


    public function stepStore(Request $request)
    { 
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
           
             DB::beginTransaction();
          try {
            
             $product=Product::where("id",$request->productId)->select("distributorId")->get();

              $step=new Product_step();
              $step->distributorId =$product[0]['distributorId'];
              $step->productId = $request->productId;
              $step->name = $request->name;
              $step->position = $request->position;
               if($request->status=='true')
              {
                $step->status = "active";  
              }
              else {
                $step->status = "deactive";  
              }
              
              $step->save();

            
             
              DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function stepUpdate(Request $request)
    {

         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }

             DB::beginTransaction();
          try {

               
              $step=Product_step::find($request->id);
              $step->name = $request->name;
              $step->position = $request->position;
               if($request->status=='true')
              {
                $step->status = "active";  
              }
              else {
                $step->status = "deactive";  
              }
              $step->save();

            DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

    public function stepDelete(Request $request)
    {

      if($request->post())
      {
         
            DB::beginTransaction();
         try {
                Product_step::where(array("id"=>$request->id))->delete();


              

                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

    public function fieldTable()
    {
        return view('superadmin.products.field.table');

    }//table

    public function fieldRow()
    {

        return view('superadmin.products.field.row');

    } //row

     public function fieldCreate()
    {
      return view('superadmin.products.field.editor');
    } //create

    public function fieldStore(Request $request)
    { 
      
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
           
             DB::beginTransaction();
          try {

              $product=Product::where("id",$request->productId)->select("distributorId")->get();
              $field=new Product_field();
              $field->distributorId =$product[0]['distributorId'];
              $field->productId = $request->productId;
              $field->stepId = $request->field_step;
              $field->name = $request->name;
              $field->filed_type = $request->filed_type;
              $field->groupId = $request->field_color_group;  
              
              $field->save();
             
              DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function fieldUpdate(Request $request)
    {
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
             DB::beginTransaction();
          try {
               
              $field=Product_field::find($request->id);
              $field->stepId = $request->field_step;
              $field->name = $request->name;
              $field->filed_type = $request->filed_type;
              $field->groupId = $request->field_color_group;  
              $field->save();
             
            DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

    public function fieldDelete(Request $request)
    {

      if($request->post())
      {
         
            DB::beginTransaction();
         try {
                Product_field::where(array("id"=>$request->id))->delete();

                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

    public function designTable()
    {
        return view('superadmin.products.design.table');

    }//table

    public function designRow()
    {

        return view('superadmin.products.design.row');

    } //row

     public function designCreate()
    {
      return view('superadmin.products.design.editor');
    } //create

    public function designStore(Request $request)
    { 
      

        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'profile' => 'image|mimes:jpeg,png,jpg|max:2048',
                 // 'design_slug' => 'required|regex:/^[a-z0-9\-]+$/u|unique:product_design,design_slug',
                 'design_slug' => 'required|regex:/^[a-z0-9\-]+$/u',
            ]);
            
            if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            
            $flag=0;
            $checkurl=Product_design::where(array('productId'=>$request->productId))->select('design_slug')->get();
            foreach ($checkurl as $check) {
              if($request->design_slug==$check->design_slug)
              {
                $flag=1;
              }
            }
            if($flag==1)
            {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The design slug has already been taken."),400);
            }

             DB::beginTransaction();
          try {

              $imageName="";
               if($request->hasFile('profile')) 
               {
                  $image = $request->profile;
                  $extension = $image->getClientOriginalExtension();
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data = url('/')."/public/images/$imageName";
               }
              
              $product=Product::where("id",$request->productId)->select("distributorId")->get();
              $design=new Product_design();
              $design->distributorId  =$product[0]['distributorId'];
              $design->productId = $request->productId;
              $design->name = $request->name;
              $design->design_slug = $request->design_slug;
              $design->sku = $request->sku;
              $design->svg_snippet = $request->svg_snippet;
              $design->show_svg_defs = $request->show_svg_defs;  
              $design->position = $request->position;  
               $design->profile=$imageName;
              if($request->status=='true')
              {
                $design->status = 'active';   
              }
              else
              {
                  $design->status = 'deactive';
              }

               if($request->player_status=='true')
              {
                $design->player_status = 'active';   
              }
              else
              {
                  $design->player_status = 'deactive';
              }
              $design->save();

              if($request->design_filed_name!="")
              {
                $design_filed_name=explode(",",$request->design_filed_name);
                $design_key=explode(",",$request->design_key);
                $stepId=explode(",",$request->stepId);
                $data_group=explode(",",$request->data_group);
                $ids=explode(",",$request->ids);

                $data = array();
                for($i=0;$i<count($design_filed_name);$i++)
                {
                  if($data_group[$i]!="")
                  {
                    $groupId=$data_group[$i];
                  }
                  else{
                    $groupId=null;
                  }
                  $design_filed=  new Design_filed();
                  $design_filed->groupId=$groupId;
                  $design_filed->name =$design_filed_name[$i];
                  $design_filed->design_key =$design_key[$i];
                  $design_filed->stepId =$stepId[$i];
                  $design_filed->designId =$design->id;
                  $design_filed->save();
                }

              
              }
             
              DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function designUpdate(Request $request)
    {
    
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 'profile' => 'image|mimes:jpeg,png,jpg|max:2048',
                 // 'design_slug' => 'regex:/^[a-z0-9\-]+$/u|unique:product_design,design_slug,'.$request->id,
                 // 'design_slug' => 'regex:/^[a-z0-9\-]+$/u|unique:product_design,design_slug,'.$request->id,
                 'design_slug' => 'required|regex:/^[a-z0-9\-]+$/u',
               ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }

            $flag=0;
            $checkurl=Product_design::where(array('productId'=>$request->productId))->select('design_slug','id')->get();
            foreach ($checkurl as $check) {
              if($check->id!=$request->id)
              {
                if($request->design_slug==$check->design_slug)
                {
                  $flag=1;
                }  
              }
            }
            if($flag==1)
            {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The design slug has already been taken."),400);
            }

             DB::beginTransaction();
          try {
               
             $design=Product_design::find($request->id);
              $design->name = $request->name;
              $design->sku = $request->sku;
              $design->svg_snippet = $request->svg_snippet;
              $design->show_svg_defs = $request->show_svg_defs;  
              $design->position = $request->position;  
              $design->design_slug = $request->design_slug;

              if($request->hasFile('profile')) {
                  if($design->profile!="" && file_exists(public_path('images/'.$design->profile)))
                  {
                        unlink(public_path('images/'.$design->profile));
                  }
                  $image = $request->profile;
                  $extension = $image->getClientOriginalExtension();
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('images'),$imageName); 
                  $data = url('/')."/public/images/$imageName";
                  $design->profile=$imageName;
              }
              if($request->status=='true')
              {
                $design->status = 'active';   
              }
              else
              {
                  $design->status = 'deactive';
              }
               if($request->player_status=='true')
              {
                $design->player_status = 'active';   
              }
              else
              {
                  $design->player_status = 'deactive';
              }
              $design->save();


              if($request->design_filed_name!="")
              {
                $design_filed_name=explode(",",$request->design_filed_name);
                $design_key=explode(",",$request->design_key);
                $stepId=explode(",",$request->stepId);
                $data_group=explode(",",$request->data_group);
                $ids=explode(",",$request->ids);


                $data = array();
                for($i=0;$i<count($design_filed_name);$i++)
                {
                    $flag=0;
                    if($ids[$i]!="")
                    {
                      $checkUp=Design_filed::where(array("id"=>$ids[$i]))->get();
                      if(count($checkUp)>0)
                      {
                        $flag=1;
                      } 
                    }
                    
                    if($flag==1)
                    {
                        $design_filed=Design_filed::find($ids[$i]);  
                        $design_filed->groupId= $data_group[$i];
                        $design_filed->name =$design_filed_name[$i];
                        $design_filed->design_key =$design_key[$i];
                        $design_filed->stepId =$stepId[$i];
                        $design_filed->designId =$design->id;
                        $design_filed->updated_at =date('Y-m-d H:i:s');
                        $design_filed->save();
                    }
                    else{
                        $design_filed=  new Design_filed();
                        $design_filed->groupId=$data_group[$i];
                        $design_filed->name =$design_filed_name[$i];
                        $design_filed->design_key =$design_key[$i];
                        $design_filed->stepId =$stepId[$i];
                        $design_filed->designId =$design->id;
                        $design_filed->updated_at =date('Y-m-d H:i:s');
                        $design_filed->save();
                    }
                    
                }

              }

            DB::commit();
           
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

    public function designDelete(Request $request)
    {

      if($request->post())
      {
         
            DB::beginTransaction();
         try {
                Design_filed::where(array("designId"=>$request->id))->delete();
                Product_design::where(array("id"=>$request->id))->delete();

                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

  
    public function designFiledDelete(Request $request)
    {
      
      if($request->post())
      {
         
            DB::beginTransaction();
         try {
                Design_filed::where(array("id"=>$request->designFiledId))->delete();

                DB::commit();
                  return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
             } 
          catch (\Throwable $e) {
            DB::rollback();
            return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
          }
      }
      else
      {
         return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
      }
    }

    public function productStatusChange(Request $request)
    {
      if($request->post())
        {              
              DB::beginTransaction();
           try {
                  $productStatus= Product::find($request->id);
                  if($productStatus->status=="active")
                  {
                    $productStatus->status="deactive";  
                  }
                  else{
                    $productStatus->status="active";
                  }
                  $productStatus->save();
                  

                  DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
               } 
            catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
            }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
        }
    }


    public function stepStatusChange(Request $request)
    {

      if($request->post())
        {              
              DB::beginTransaction();
           try {
                  $stepStatus= Product_step::find($request->id);
                  if($stepStatus->status=="active")
                  {
                    $stepStatus->status="deactive";  
                  }
                  else{
                    $stepStatus->status="active";
                  }
                  $stepStatus->save();
                  

                  DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
               } 
            catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
            }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
        }
    }

    public function designStatusChange(Request $request)
    {

      if($request->post())
        {              
              DB::beginTransaction();
           try {
                  $designStatus= Product_design::find($request->id);
                  if($designStatus->status=="active")
                  {
                    $designStatus->status="deactive";  
                  }
                  else{
                    $designStatus->status="active";
                  }
                  $designStatus->save();
                  

                  DB::commit();
                    return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
               } 
            catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
            }
        }
        else
        {
           return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')),404); 
        }
    }
}