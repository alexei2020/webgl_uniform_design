<?php
namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Admin_setting;;
use Session;
use DB;
use Rule;
use Mail;
use Auth;
class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
    public function index()
    {
    	$user = User::
        leftjoin('admin_setting','admin_setting.userId','users.id') 
        ->where(array('users.id'=>Auth::user()->id))->select('users.*','admin_setting.password as admin_password','admin_setting.duplicate_password as dup_pass')->get();
        return view('superadmin.account.index',array("user"=>$user));
    }

    public function update(Request $request)
    {
    	if($request->post())
        {
            $validator = Validator::make($request->all(), [
            	  	'name' => 'required|string|max:255',
                	'email' =>'required|string|max:255|unique:users,email,'.$request->id,
                  'password' => 'required|min:8',
               ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            
         DB::beginTransaction();
         try {           
                    $user=User::find($request->id);
                    $user->name=$request->name;
                    $user->email=$request->email;
                    $user->save();

                    $check_setting = Admin_setting::where(array('userId'=>$request->id))->get();
                    if(count($check_setting)>0)
                    {
                        $setting = Admin_setting::find($check_setting[0]->id);
                        $setting->userId = $user->id;
                        $setting->password = Hash::make($request->password);
                        $setting->duplicate_password = $request->password;
                        $setting->save();
                    }
                    else 
                    {
                        $setting = new Admin_setting();
                        $setting->userId = $user->id;
                        $setting->password = Hash::make($request->password);
                        $setting->duplicate_password = $request->password;
                        $setting->save();
                    }

            DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

    public function changePassword()
    {
    	return view('superadmin.account.change_password');
    }

	public function updatePassword(Request $request)
    {
    	
    	if($request->post())
        {
            $validator = Validator::make($request->all(), [
            	  	'current_password'=>'required',
	                'password' => 'required|same:password|min:6',
	                'confirm_password' => 'required|same:password|min:6',
               ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            
         DB::beginTransaction();
         try {           

         		$current_password = \Auth::user()->password;
                if(Hash::check($request['current_password'],$current_password))
                {
                    $user=User::find(Auth::user()->id);
                    $user->password = Hash::make($request->password);
                    $user->save();
                    DB::commit();
            		return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
                }
                else{
                	return response()->json(array('resp'=>'ok','success'=>false,'msg'=>"The current password does not match"),404);
                }

            
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }    
    
   
}