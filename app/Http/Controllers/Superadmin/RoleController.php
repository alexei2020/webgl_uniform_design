<?php

namespace App\Http\Controllers\Superadmin;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;
use DB;


class RoleController extends Controller
{
    use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
       // $this->middleware('App\Http\Middleware\UserMiddleware');
    }
     public function edit()
    {
    }
    public function index()
    {

        $active_menu = "roles";
        $roles = Role::all();
        $permissions = Permission::all();

        return view('superadmin.role.index', compact('roles', 'permissions', 'active_menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $ngo_id=Session::get('ngo_id');
        $this->validate($request, ['name' => 'required']);
        $checkUnique=DB::table('roles')->where(array("name"=>$request->name))->whereIn("ngo_id",array(0,$ngo_id))->get();
        if(count($checkUnique)>0)
        {
        	Session::flash('error', 'name already exist'); 
             return redirect('roles');

        }
    
        if($request->post())
        {
            
            $role = new Role();
            $role->name = $request->name;
            $role->ngo_id = Session::get('ngo_id');
            $role->save();
            Session::flash('success', 'Role Added'); 

        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($role = Role::findOrFail($id)) {
            // admin role has everything
            if($role->name === 'Superadmin') {
                $role->syncPermissions(Permission::all());
                return redirect()->route('roles.index');
            }

            $permissions = $request->get('permissions', []);

            $role->syncPermissions($permissions);

            Session::flash( $role->name . ' permissions has been updated.');
        } else {
            Session::flash()->error( 'Role with id '. $id .' note found.');
        }

        return redirect()->route('roles.index');
    }


    public function getModelHasRole()
    {
        $users = User::where(array('type'=>'default'))->get();     
        foreach ($users as $user) {
            $query = array('role_id'=>1,'model_type'=>'App\User','model_id'=>$user->id);
                \DB::table('model_has_roles')->insert($query);    
        }
        echo "Success";
    }
}
