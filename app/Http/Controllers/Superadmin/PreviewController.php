<?php
namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;

use App\QuoteForm;
use App\Color;
use Session;
use DB;
use Rule;
use Mail;
use Auth;

class PreviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     public function index(Request $request)
    {
        return view('superadmin.preview.index',array('distributorId'=>$request->distributorId));
    }

   
}