<?php
namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Companies;
use App\User;
use App\Businessunit;
use App\CompaniesBusinessunit;
use Session;
use DB;
use Rule;
use Mail;
use Auth;
class DistributorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     public function index()
    {
        return view('superadmin.distributor.index');
    }
    public function table()
    {
        return view('superadmin.distributor.table');
    }
    public function select()
    {
        return view('common.select');
    }
    public function row()
    {
        return view('superadmin.distributor.row');
    }
    
    public function getList(Request $request)
    {
      
        $search = $request->search;

        $sortBy = $request->sortBy;
        $sortName = $request->sortName;

        if($sortName=="name")
        {
          $sortName="users.name";
        }
        else if($sortName=="email")
        {
            $sortName="users.email"; 
        }
        else if($sortName=="mobile")
        {
            $sortName="users.mobile"; 
        }
        else if($sortName=="bu_name")
        {
            $sortName="users.bu_name"; 
        }
        else if($sortName=="status")
        {
            $sortName="users.status"; 
        }
        else
        {
          $sortName="users.".$sortName;
        }

    	  $companies=User::Where('users.type', 'distributor')
    	  ->leftjoin('user_log','user_log.distributorId','users.id')
          ->where(function($q) use ($search)
            {
                  $q->orWhere('users.name', 'like', '%' . $search . '%');
                  $q->orWhere('users.email', 'like', '%' . $search . '%');
                  $q->orWhere('users.mobile', 'like', '%' . $search . '%');
                  $q->orWhere('users.bu_name', 'like', '%' . $search . '%');
            })
        	->select("users.*",'user_log.id as userlogId')
        	// ->orderBy("users.id","desc")
            ->orderBy($sortName,$sortBy)
            ->groupBy('users.id')
        	->paginate(config('constants.limit'));

    	return json_encode(array("companies"=>$companies,"pagination"=>str_replace('/?','?',$companies->render('common.pagination'))));
    	
    }
    public function create()
    {
    	return view('superadmin.distributor.editor');
    }

    public function store(Request $request)
    {
       
        if($request->post())
        {
            $validator = Validator::make($request->all(), [
                 'name' => 'required|string|max:255',
                 // 'bu_name' => 'required|string|max:255',
                  'email' =>'required|string|max:255|unique:users,email',
                 // 'password' => 'required|same:password|min:8',
                 // 'confirmpassword' => 'required|same:password|min:8',
                 'profile' => 'image|mimes:jpeg,png,jpg|max:2048',
                 // 'mobile' => 'required|between:10,15',
                ]);
             if($validator->fails()) {
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
       
            }
            

            DB::beginTransaction();
          try {
               
                $imageName="";
               if($request->hasFile('profile')) 
               {
                  $image = $request->profile;
                  $extension = $image->getClientOriginalExtension();
                  $imageName = rand(111,999).time().".".$extension;
                  $image->move(public_path('distributor'),$imageName); 
                  $data = url('/')."/public/distributor/$imageName";
               }
           
	            $user=new User();
/*
              if($request->password!="" && $request->password_generate=="")
              {
                $password=$request->password;
              }
              else if($request->password=="" && $request->password_generate!="")
              {
                $password=$request->password_generate;
              }
              else if($request->password!="" && $request->password_generate!="")
              {
                $password=$request->password;
              }*/

              if($request->password!="")
              {
                $password=$request->password;
              }

	            $user->name=$request->name;
              $user->bu_name=$request->bu_name;
              $user->createdBy=Auth::user()->id;
	            $user->email=$request->email;
	            $user->type="distributor";
              $user->profile=$imageName;
              $user->mobile = $request->mobile;
	            $user->password=Hash::make($password);
              $user->duplicate_password=$password;
              if($request->status=="true")
              {
                $status="active";
              }
              else{
                $status="deactive";
              }
              $user->status=$status;
	            $user->save();

              $this->addDefaultFont($user->id);

	            $query = array('role_id'=>1,'model_type'=>'App\User','model_id'=>$user->id);
                DB::table('model_has_roles')->insert($query);

                $template_id=config("sendgridTemplateIds.Welcome_mail_on_distributor_creation");
                $to_name = $request->name;  
                $to_email = $request->email;  
                $url_link = url('/');
                
                $from_name='Admin';
                $from_email= config('constants.admin_email');
                $user_password = $password;
                
                $subject="Welcome to the Uniform Builder" ;
                
                $data = array(':admin_email' => array($from_email),':user_name' => array($to_name),':user_email' => array($to_email),':user_password'=>array($user_password),':url_link'=>array($url_link));

                \App\Sendgrid::send_mail( $subject , $template_id , $data , $to_email , $to_name, $from_email ); 

	      

            DB::commit();

           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
     public function update(Request $request)
    {
    
         if($request->post())
        {
            $validator = Validator::make($request->all(), [
            	  'name' => 'required|string|max:255',
                // 'bu_name' => 'required|string|max:255',
                'email' =>'required|string|max:255|unique:users,email,'.$request->id,
                'profile' => 'image|mimes:jpeg,png,jpg|max:2048|nullable',
                // 'mobile' => 'required|between:10,15',
               ]);

             if($validator->fails()) {
               
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>$validator->errors()),400);
            }
            

         DB::beginTransaction();
         try {           
                    $user=User::find($request->id);
                    $user->name=$request->name;
                    $user->bu_name=$request->bu_name;
                    $user->email=$request->email;
                    if($request->action=="logoDelete")
                    {
                        if($user->profile!="" && file_exists(public_path('distributor/'.$user->profile)))
                        {
                              unlink(public_path('distributor/'.$user->profile));
                        }
                        $user->profile=null;
                    }
                    if($request->hasFile('profile')) 
                    {
                        if($user->profile!="" && file_exists(public_path('distributor/'.$user->profile)))
                        {
                              unlink(public_path('distributor/'.$user->profile));
                        }
                        $image = $request->profile;
                        $extension = $image->getClientOriginalExtension();
                        $imageName = rand(111,999).time().".".$extension;
                        $image->move(public_path('distributor'),$imageName); 
                        $data = url('/')."/public/distributor/$imageName";
                        $user->profile=$imageName;
                    }
                    if($request->status=="true")
                    {
                      $status="active";
                    }
                    else{
                      $status="deactive";
                    }

                    $user->status=$status;
                    $user->mobile = $request->mobile;
                    $user->password=Hash::make($request->password);
                    $user->duplicate_password=$request->password;
                    $user->save();

            DB::commit();
            return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }

     public function destroy($id,Request $request)
    {
        DB::beginTransaction();
     try {

     	    DB::table("model_has_roles")->where("model_id",$request->id)->delete();
          \App\Font::where(array('distributorId'=>$request->id))->delete();

            User::where("id",$request->id)->delete();

            DB::commit();
            return response()->json(array('resp' => 'ok','success' => true,"msg"=>config('constants.deleted'))); 
         } 
      catch (\Throwable $e) {
        DB::rollback();
        return response()->json(array('resp' => 'ok','success' => false,"msg"=>config('constants.notdeleted')), 404); 
      }
    }

    public function changeStatus(Request $request)
    {
        if($request->post())
        {
         DB::beginTransaction();
         try {           
                $user=User::find($request->id);
                $user->status=$request->status;
                $user->save();

              DB::commit();
              return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
              return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
          }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }


    public function deleteLogo(Request $request)
    {
        if($request->post())
        {
         DB::beginTransaction();
         try {           
                $user = User::find($request->id);

                if($user->profile!="" && file_exists(public_path('distributor/'.$user->profile)))
                {
                      unlink(public_path('distributor/'.$user->profile));
                }

                $user->profile=null;
                $user->save();

                DB::commit();
                return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
                DB::rollback();
                return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
            }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notupdated')),404);
        }
    }
   

    public function addDefaultFont($distributorId)
    {
        $fontList = ['Arial', 'Arial Bold', 'Helvetica Neue', 'Helvetica Neue Bold', 'Impact', 'Athletic', 'Jersey M54'];
        foreach ($fontList as $fonts) {
            $font=new \App\Font();
            $font->distributorId=$distributorId; 
            $font->name=$fonts;
            $font->value=$fonts;
            $font->status="active";
            
            $font->save();
        }
      
        
    }

}