<?php
namespace App\Http\Controllers\Superadmin\SiteManage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\SadminSiteManage;
use Session;
use DB;
use Rule;
use Mail;
use Auth;
class PrivacyPolicyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     public function index()
    {
    	$contentpage = SadminSiteManage::where(array('userId'=>Auth::user()->id,'type'=>"privacy-policy"))->get();

        return view('superadmin.sitemanage.privacy_policy.index',array('contentpage'=>$contentpage));
    }
    
    public function store(Request $request)
    {	
      	
    	if($request->post())
        {
           
            DB::beginTransaction();
          try {

                $privacy_policy=new SadminSiteManage();
                $privacy_policy->userId=Auth::user()->id;
                $privacy_policy->type = "privacy-policy";
                $privacy_policy->description= htmlentities($request->description, ENT_QUOTES) ;
                $privacy_policy->save();
                
                DB::commit();

                return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
            }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }


    public function update(Request $request)
    {

        if($request->post())
        {

            $check_page = SadminSiteManage::where(array("userId"=>Auth::user()->id,'type'=>"privacy-policy"))->get();
            $flag=0;

            if(count($check_page)>0)
            {
                $flag=1;
            }

            DB::beginTransaction();
          try {
         if($flag==1)
                {
                    $privacy_policy=SadminSiteManage::find($check_page[0]->id);
                    $privacy_policy->description=htmlentities($request->description, ENT_QUOTES);
                    $privacy_policy->save();
                }
                 DB::commit();

           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
   
}