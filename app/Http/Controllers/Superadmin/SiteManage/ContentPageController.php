<?php
namespace App\Http\Controllers\Superadmin\SiteManage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\SadminSiteManage;
use Session;
use DB;
use Rule;
use Mail;
use Auth;
class ContentPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
    public function index()
    {
    	$contentpage = SadminSiteManage::where(array('userId'=>Auth::user()->id,'type'=>"content-page"))->get();
        
        return view('superadmin.sitemanage.content_page.index',array('contentpage'=>$contentpage));
    }

    public function store(Request $request)
    {	
      
    	if($request->post())
        {
           
            DB::beginTransaction();
          try {

                $content_page=new SadminSiteManage();
                $content_page->userId=Auth::user()->id;
                $content_page->type = "content-page";
                $content_page->description= htmlentities($request->description, ENT_QUOTES) ;
                $content_page->save();
                
                DB::commit();

                return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
            }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }


    public function update(Request $request)
    {

        if($request->post())
        {

            $check_page = SadminSiteManage::where(array("userId"=>Auth::user()->id,'type'=>"content-page"))->get();
            $flag=0;

            if(count($check_page)>0)
            {
                $flag=1;
            }

            DB::beginTransaction();
          try {
         if($flag==1)
                {
                    $content_page=SadminSiteManage::find($check_page[0]->id);
                    $content_page->description=htmlentities($request->description, ENT_QUOTES);
                    $content_page->save();
                }
                 DB::commit();

           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
}