<?php
namespace App\Http\Controllers\Superadmin\SiteManage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use App\User;
use App\SadminSiteManage;
use DB;
use Rule;
use Mail;
use Auth;
class TermConditionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     public function index()
    {
    	$contentpage = SadminSiteManage::where(array('userId'=>Auth::user()->id,'type'=>"term-condition"))->get();
        return view('superadmin.sitemanage.term_condition.index',array("contentpage"=>$contentpage));
    }
    
    public function store(Request $request)
    {	
      	
    	if($request->post())
        {
           
            DB::beginTransaction();
          try {

                $term_condition=new SadminSiteManage();
                $term_condition->userId=Auth::user()->id;
                $term_condition->type = "term-condition";
                $term_condition->description= htmlentities($request->description, ENT_QUOTES) ;
                $term_condition->save();
                
                DB::commit();

                return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
            }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
            }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }


    public function update(Request $request)
    {

        if($request->post())
        {

            $check_page = SadminSiteManage::where(array("userId"=>Auth::user()->id,'type'=>"term-condition"))->get();
            $flag=0;

            if(count($check_page)>0)
            {
                $flag=1;
            }

            DB::beginTransaction();
          try {
         if($flag==1)
                {
                    $term_condition=SadminSiteManage::find($check_page[0]->id);
                    $term_condition->description=htmlentities($request->description, ENT_QUOTES);
                    $term_condition->save();
                }
                 DB::commit();

           return response()->json(array('resp'=>'ok','success'=>true,'msg'=>''));
           }catch (\Throwable $e) {
              DB::rollback();
            
              return response()->json(array('resp'=>'ok','success'=>false,'msg'=>config('constants.notadded')),404);
         }
        }
        else
        {
            return response()->json(array('resp'=>'no','success'=>false,'msg'=>config('constants.notadded')),404);
        }
    }
   
}