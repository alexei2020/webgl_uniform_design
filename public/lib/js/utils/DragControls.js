/*
 * @author zz85 / https://github.com/zz85
 * @author mrdoob / http://mrdoob.com
 * Running this will allow you to drag three.js objects around the screen.
 */

THREE.DragControls = function ( _objects, _camera, _domElement ) {

	if ( _objects instanceof THREE.Camera ) {

		console.warn( 'THREE.DragControls: Constructor now expects ( objects, camera, domElement )' );
		var temp = _objects; _objects = _camera; _camera = temp;

	}

	var _plane = new THREE.Plane();
	var _raycaster = new THREE.Raycaster();

	var _mouse = new THREE.Vector2();
	var _offset = new THREE.Vector3();
	var _intersection = new THREE.Vector3();

	var _selected = null, _hovered = null;
	
	//

	var scope = this;

	function activate() {

		_domElement.addEventListener( 'mousemove', onDocumentMouseMove, false );
		_domElement.addEventListener( 'mousedown', onDocumentMouseDown, false );
		_domElement.addEventListener( 'mouseup', onDocumentMouseUp, false );
		// _domElement.addEventListener( 'click', onDocumentClick, false);

		_domElement.addEventListener( 'touchmove', onDocumentTouchmove, false );
		_domElement.addEventListener( 'touchstart', onDocumentTouchstart, false );
		_domElement.addEventListener( 'touchend', onDocumentTouchend, false );

	}

	function deactivate() {

		_domElement.removeEventListener( 'mousemove', onDocumentMouseMove, false );
		_domElement.removeEventListener( 'mousedown', onDocumentMouseDown, false );
		_domElement.removeEventListener( 'mouseup', onDocumentMouseUp, false );

		_domElement.removeEventListener( 'touchmove', onDocumentTouchmove, false );
		_domElement.removeEventListener( 'touchstart', onDocumentTouchstart, false );
		_domElement.removeEventListener( 'touchend', onDocumentTouchend, false );

	}

	function dispose() {

		deactivate();

	}

	function onDocumentMouseMove( event ) {
		event.preventDefault();
		
		var rect = _domElement.getBoundingClientRect();
		_mouse.x = ( (event.clientX - rect.left) / rect.width ) * 2 - 1;
		_mouse.y = - ( (event.clientY - rect.top) / rect.height ) * 2 + 1;
		// console.log(event.clientX+" - "+event.clientY);


		if(_mouse.x>200 || _mouse.y>300 || _mouse.x<0 || _mouse.y<0)
		{
			// return false;
		}
		_raycaster.setFromCamera( _mouse, _camera );

		if ( _selected && scope.enabled ) {

			if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

				_selected.position.copy( _intersection.sub( _offset ) );


				/*if(_selected.event_type=="text"){

                	var  dynamictexture = new THREEx.DynamicTexture(512, 512);
				     dynamictexture.context.font = "bolder 20px red";
				     dynamictexture.texture.needsUpdate = true;
				     console.log(controls.object.position.x);
				     if(controls.object.position.x>0)
				     {
				     	var clientX = event.clientX + 20;	
				     }
				     else{
				     	var clientX = event.clientX - 190;
				     }

				     var clientY = event.clientY + 250;
				     console.log(clientX);
				    
				    for (var i = 0; i < app360.drawArray.length; i++) {
						if(app360.drawArray[i]['id']==_selected.event_id)
						{
							app360.drawArray[i]['positionX_value']=clientX;
							app360.drawArray[i]['positionY_value']=clientY;
						}
					}

			         dynamictexture.drawText(_selected.event_imageText,clientX,clientY,_selected.event_colorValue,_selected.event_fontsize);
			       	if(_selected.event_variable=="dynamictexture1")
	              	{
	              		$('#panelField_'+_selected.event_id).find('#position_x').val(clientX);
	              		$('#panelField_'+_selected.event_id).find('#position_y').val(clientY);
	              	  	
	              	  	loadedMesh1[0].children[1].material = new THREE.MeshLambertMaterial({ map: dynamictexture.texture });
	                  	loadedMesh1[0].children[1].material.transparent = true;
	              	}
	              	else if(_selected.event_variable=="dynamictexture2")
	              	{
	              		$('#panelField_'+_selected.event_id).find('#position_x').val(clientX);
	              		$('#panelField_'+_selected.event_id).find('#position_y').val(clientY);
	              	  	
	              	  loadedMesh1[0].children[2].material = new THREE.MeshLambertMaterial({ map: dynamictexture.texture });
	                  loadedMesh1[0].children[2].material.transparent = true;	
	              	}
	              	else if(_selected.event_variable=="dynamictexture3")
	              	{
	              		$('#panelField_'+_selected.event_id).find('#position_x').val(clientX);
	              		$('#panelField_'+_selected.event_id).find('#position_y').val(clientY);
	              	  loadedMesh1[0].children[3].material = new THREE.MeshLambertMaterial({ map: dynamictexture.texture });
	                  loadedMesh1[0].children[3].material.transparent = true;	
	              	}

              	}

              	else if(_selected.event_type=="image")
              	{
              		var dynamictextureImage = new THREEx.DynamicTexture(512, 512);
				    dynamictextureImage.context.font = "bolder 20px red";
				    dynamictextureImage.texture.needsUpdate = true;

				     var clientY = event.clientY + 250;
				     var clientX = event.clientX + 20;	
				     console.log(clientX+" - "+clientY);

				     for (var i = 0; i < app360.drawArray.length; i++) {
						if(app360.drawArray[i]['id']==_selected.event_id)
						{
							app360.drawArray[i]['positionX_value']=clientX;
							app360.drawArray[i]['positionY_value']=clientY;
						}
					}
				  
	                var _image = document.createElement('img');
		            _image.src = app360.location+'public/images/design_logo/'+_selected.event_imageText;
		            if(_image.width<=300)
	                  {
	                    _image.width=_image.width*_selected.event_imageSize/100;
	                    _image.height=_image.height*_selected.event_imageSize/100;
	                  }
	                  else if(_image.width<=600){
	                    _image.width=_image.width*_selected.event_imageSize/200;
	                    _image.height=_image.height*_selected.event_imageSize/200;
	                  }
	                  else if(e.target.width<=900){
	                    _image.width=_image.width*_selected.event_imageSize/300;
	                    _image.height=_image.height*_selected.event_imageSize/300; 
	                  }
	                  else {
	                    _image.width=_image.width*_selected.event_imageSize/400;
	                    _image.height=_image.height*_selected.event_imageSize/400;
	                  }
		           	dynamictextureImage.drawImage(_image, clientX,clientY,_image.width,_image.height);  
					

	        		if(_selected.event_variable=="dynamictextureImage1")
	              	{
	              		$('#panelField_'+_selected.event_id).find('#position_x').val(clientX);
	              		$('#panelField_'+_selected.event_id).find('#position_y').val(clientY);
	              		loadedMesh1[0].children[4].material = new THREE.MeshLambertMaterial({ map: dynamictextureImage.texture });
	                  	loadedMesh1[0].children[4].material.transparent = true;	
	              	}
	              	else if(_selected.event_variable=="dynamictextureImage2")
	              	{
	              		$('#panelField_'+_selected.event_id).find('#position_x').val(clientX);
	              		$('#panelField_'+_selected.event_id).find('#position_y').val(clientY);
	              		loadedMesh1[0].children[5].material = new THREE.MeshLambertMaterial({ map: dynamictextureImage.texture });
	                  	loadedMesh1[0].children[5].material.transparent = true;	
	              	}
	              	else if(_selected.event_variable=="dynamictextureImage3")
	              	{
	              		$('#panelField_'+_selected.event_id).find('#position_x').val(clientX);
	              		$('#panelField_'+_selected.event_id).find('#position_y').val(clientY);
	              		loadedMesh1[0].children[6].material = new THREE.MeshLambertMaterial({ map: dynamictextureImage.texture });
	                  	loadedMesh1[0].children[6].material.transparent = true;	
	              	}
              	}
*/

			}

			scope.dispatchEvent( { type: 'drag', object: _selected } );

			return;

		}

		_raycaster.setFromCamera( _mouse, _camera );

		var intersects = _raycaster.intersectObjects( _objects );

		if ( intersects.length > 0 ) {

			var object = intersects[ 0 ].object;

			_plane.setFromNormalAndCoplanarPoint( _camera.getWorldDirection( _plane.normal ), object.position );

			if ( _hovered !== object ) {

				scope.dispatchEvent( { type: 'hoveron', object: object } );

				_domElement.style.cursor = 'pointer';
				_hovered = object;

			}

		} else {

			if ( _hovered !== null ) {

				scope.dispatchEvent( { type: 'hoveroff', object: _hovered } );

				_domElement.style.cursor = 'auto';
				_hovered = null;

			}

		}

	}

	function onDocumentTouchmove( event ) {

		event.preventDefault();

		var rect = _domElement.getBoundingClientRect();

		_mouse.x = + ( (event.targetTouches[ 0 ].pageX - rect.left) / rect.width ) * 2 - 1;
		_mouse.y = - ( (event.targetTouches[ 0 ].pageY - rect.top) / rect.height ) * 2 + 1;
		
		_raycaster.setFromCamera( _mouse, _camera );

		if ( _selected && scope.enabled ) {

			if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

				_selected.position.copy( _intersection.sub( _offset ) );

			}

			scope.dispatchEvent( { type: 'drag', object: _selected } );

			return;

		}

	}




	function onDocumentMouseDown( event ) {
		event.preventDefault();
		var clientX = event.clientX+20;
		var clientY= event.clientY+230;

		var rect = _domElement.getBoundingClientRect();
		// console.log(clientX+" - "+clientY);
		_raycaster.setFromCamera( _mouse, _camera );

		var intersects = _raycaster.intersectObjects( _objects );
	
		if ( intersects.length > 0 ) {
/*
			var textLength=0;
			var textFlag=0;
			var flag=0;
			var z=0
			var imageFlag=0;
			var imageLengt=0;
			for (var i = 0; i < app360.drawArray.length; i++) {
				if(app360.drawArray[i]['filed_type']=="text"  && flag==0)
				{		
					if(app360.drawArray[i]['positionY_value']!=null && app360.drawArray[i]['positionX_value']!=null)
					{
						var charLength = app360.drawArray[i]['text_value'].length;
						var fontsize=app360.drawArray[i]['fontsize_value'];
						// console.log(app360.drawArray[i]['positionX_value']+" - "+app360.drawArray[i]['positionY_value']);
						var x_add=clientX+20;
						var x_minus=clientX-20;
						var y_add=clientY+10;
						var y_minus=clientY-10;
						
						if(clientY>=470)
						{
							y_add=clientY+5;
							y_minus=clientY-20;
						}
						else if(clientY<=300)
						{
							y_add=clientY+20;
							y_minus=clientY-5;	
						}
						else if(clientY>500)
						{
							clientY=500;
						}

					
						// console.log("X = "+app360.drawArray[i]['positionX_value']+" >= "+ x_minus +" && "+ app360.drawArray[i]['positionX_value']+" <= "+x_add);
						console.log("Y = "+app360.drawArray[i]['positionY_value']+" >= "+ y_minus +" && "+ app360.drawArray[i]['positionY_value']+" <= "+y_add);
						// && (app360.drawArray[i]['positionX_value']>=x_minus && app360.drawArray[i]['positionX_value']<=x_add ) 
						if( (app360.drawArray[i]['positionY_value']>=y_minus && app360.drawArray[i]['positionY_value']<=y_add)  )
						{	
							textFlag=1;
							flag=1;
						}
					}
					
					textLength++;
				}
			}

			if(flag==1 && textFlag==1)
			{

				z=textLength-1;
			}
			else if(flag==1 && imageFlag==1){
				z=imageLength;
			}
			else{
				_selected=null;
				_selectedTo=null;
				$('.buttonGroupDrag').hide();
				// console.log("_selected null");
				return false;
			}
			if(intersects[ z ].object.event_id==undefined)
			{
				$('.buttonGroupDrag').hide();
				console.log(false);
				// return false;
			}
			else{
				console.log("hi");
				$('.buttonGroupDrag').show();

			}
			_selected = intersects[ z ].object;
			_selectedTo= intersects[ z ].object;*/
			

			// console.log(_objects);
			// console.log(intersects[ 0 ].object.event_id);
		

			if(intersects[ 0 ].object.event_id==undefined)
			{
				$('.buttonGroupDrag').hide();
				console.log(false);
				// return false;
			}
			else{
				console.log("hi");
				$('.buttonGroupDrag').show();

			}
			_selected = intersects[ 0 ].object;
			_selectedTo= intersects[ 0 ].object;
						console.log(_selected);
			


			// console.log(_selectedTo);
			// console.log(event);
			if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

				_offset.copy( _intersection ).sub( _selected.position );

			}
			_domElement.style.cursor = 'move';

			scope.dispatchEvent( { type: 'dragstart', object: _selected } );

		}
		
	}

	function onDocumentTouchstart( event ) {

		event.preventDefault();

		var rect = _domElement.getBoundingClientRect();

		_mouse.x = + ( (event.targetTouches[ 0 ].pageX - rect.left) / rect.width ) * 2 - 1;
		_mouse.y = - ( (event.targetTouches[ 0 ].pageY - rect.top) / rect.height ) * 2 + 1;

		_raycaster.setFromCamera( _mouse, _camera );

		var intersects = _raycaster.intersectObjects( _objects );

		if ( intersects.length > 0 ) {

			var object = intersects[ 0 ].object;


			_plane.setFromNormalAndCoplanarPoint( _camera.getWorldDirection( _plane.normal ), object.position );

			if ( _hovered !== object ) {

				scope.dispatchEvent( { type: 'hoveron', object: object } );

				_domElement.style.cursor = 'pointer';
				_hovered = object;

			}

		} else {

			if ( _hovered !== null ) {

				scope.dispatchEvent( { type: 'hoveroff', object: _hovered } );

				_domElement.style.cursor = 'auto';
				_hovered = null;

			}

		}

		if ( intersects.length > 0 ) {

			_selected = intersects[ 0 ].object;

			if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

				_offset.copy( _intersection ).sub( _selected.position );

			}

			_domElement.style.cursor = 'move';

			scope.dispatchEvent( { type: 'dragstart', object: _selected } );

		}

	}

	function onDocumentMouseUp( event ) {

		event.preventDefault();

		if ( _selected ) {

			scope.dispatchEvent( { type: 'dragend', object: _selected } );

			_selected = null;

		}

		_domElement.style.cursor = 'auto';

	}

	function onDocumentTouchend( event ) {

		event.preventDefault();

		if ( _selected ) {

			scope.dispatchEvent( { type: 'dragend', object: _selected } );

			_selected = null;

		}

		_domElement.style.cursor = 'auto';

	}

	activate();

	// API

	this.enabled = true;

	this.activate = activate;
	this.deactivate = deactivate;
	this.dispose = dispose;

	// Backward compatibility

	this.setObjects = function () {

		console.error( 'THREE.DragControls: setObjects() has been removed.' );

	};

	this.on = function ( type, listener ) {

		console.warn( 'THREE.DragControls: on() has been deprecated. Use addEventListener() instead.' );
		scope.addEventListener( type, listener );

	};

	this.off = function ( type, listener ) {

		console.warn( 'THREE.DragControls: off() has been deprecated. Use removeEventListener() instead.' );
		scope.removeEventListener( type, listener );

	};

	this.notify = function ( type ) {

		console.error( 'THREE.DragControls: notify() has been deprecated. Use dispatchEvent() instead.' );
		scope.dispatchEvent( { type: type } );

	};

};

THREE.DragControls.prototype = Object.create( THREE.EventDispatcher.prototype );
THREE.DragControls.prototype.constructor = THREE.DragControls;