
// HELP WINDOW

$('#helpButton').click(function() {
    $('#helpWindow').addClass('helpActive');
  });

  $('#helpWindowClose').click(function() {
    $('#helpWindow').removeClass('helpActive');
  });

  // PALETTE WINDOW

  var paletteToggle = false;

  $('#paletteWindowToggle').click(function() {
    if (paletteToggle === false) {
      $('#palette').addClass('paletteActive');
      $('#paletteWindowToggle').removeClass('paletteChevronClosed');
      $('#paletteWindowToggle').addClass('paletteChevronOpen');
      paletteToggle = true;
    } else {
      $('#palette').removeClass('paletteActive');
      $('#paletteWindowToggle').removeClass('paletteChevronOpen');
      $('#paletteWindowToggle').addClass('paletteChevronClosed');
      paletteToggle = false;
    }
  });

  // SELECTOR WINDOW

  var selector = 'primary';

  function grabCurrentSelector(id) {
    selector = id;
    // console.log('Current selector: ' + selector);
    $('#palette').addClass('paletteActive');
    $('#paletteWindowToggle').removeClass('paletteChevronClosed');
    $('#paletteWindowToggle').addClass('paletteChevronOpen');
    paletteToggle = true;
  }

  $('#primary, #secondary, #tertiary, #accent').click(function() {
    var id = $(this).attr('id');
    grabCurrentSelector(id);
  });

  // All colour boxes
  $('#white, #lightGrey, #midGrey, #darkGrey, #black, #azure, #sky, #royal, #navy, #darkNavy, #darkGreen, #forest, #emerald, #jade, #lime, #maroon, #scarlet, #brightRed, #lightRed, #tangerine, #orange, #amber, #gold, #yellow, #lemon, #lichePurple, #emperorsChildren, #lavender, #lilac, #velvet, #burgundy, #raspberry, #grape, #magenta, #rhubarb')
  .click(function() {
    var colour = $(this).attr('fill');
    // console.log(colour);
    $('#' + selector + 'SelectorSvg').attr('fill', colour);
    // console.log(selector);
    $('#' + selector + 'OnSvg').attr('fill', colour);
    if($("#webgl").length > 1) {
      $(this).empty();
    }
    getSvg();


   scene = updatedscreen();


    //jsonloader.load('assets/models/jersey.json', getJersey);
  })

  function agenInit(){
      console.log('ready again');
      camera="", scene="" , renderer="";
   jsonloader="",objectobj="";
   controls="";
   loadedMesh1=[];
      $("#webgl").find('canvas').remove();
       setTimeout(function(){
        getSvg();
         scene = init2(app360.location+'public/images/'+app360.obj_doc.filename);
      },1000);
  }

  function updatedscreen()
  {

      if($("#webgl").length>1) {

      $(this).empty();
    }

    ctx.drawImage(img, 0, 0);
    var texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;
    // MeshStandardMaterial
    // MeshPhongMaterial
      var material = new THREE.MeshLambertMaterial({ map: texture });

      // material.shininess=1;
      var dynamictexture = new THREEx.DynamicTexture(512, 512);
      dynamictexture.context.font = "bolder 20px red";
      dynamictexture.texture.needsUpdate = true;

      var _image = {};

      for (var i = 0; i < app360.drawArray.length; i++) {
        if(app360.drawArray[i]['filed_type']=="text")
        {
          if(app360.drawArray[i]['text_value']!=null && app360.drawArray[i]['positionX_value']!=null && app360.drawArray[i]['positionY_value']!=null && app360.drawArray[i]['color_value']!=null)
          {
            var fontsize_value = 'bold '+app360.drawArray[i]['fontsize_value']+'px'+' '+app360.drawArray[i]['fontname_value'];
            dynamictexture.drawText(app360.drawArray[i]['text_value'], app360.drawArray[i]['positionX_value'], app360.drawArray[i]['positionY_value'],app360.drawArray[i]['color_value'],fontsize_value);
          }
        }
        else if(app360.drawArray[i]['filed_type']=="image")
        {

          if(app360.drawArray[i]['image_value']!="" && app360.drawArray[i]['image_value']!=null && app360.drawArray[i]['positionX_value']!=null && app360.drawArray[i]['positionY_value']!=null && app360.drawArray[i]['height_value']!=null && app360.drawArray[i]['width_value']!=null )
          {
            var name = app360.drawArray[i]['image_value'];
              _image[name] = new Image();
               _image[name].crossOrigin = "anonymous";
              _image[name].src = app360.location+'public/images/design_logo/'+app360.drawArray[i]['image_value'];
              _image[name].positionX_value=app360.drawArray[i]['positionX_value'];
              _image[name].positionY_value=app360.drawArray[i]['positionY_value'];
              _image[name].size=app360.drawArray[i]['width_value'];
              // _image[name].height=app360.drawArray[i]['height_value'];
              _image[name].addEventListener('load',function (e) {
                // alert(e.target.width + " " + e.target.height);
                if(e.target.width<=300)
                {
                  e.target.width=e.target.width*e.target.size/100;
                  e.target.height=e.target.height*e.target.size/100;
                }
                else if(e.target.width<=600){

                   e.target.width=e.target.width*e.target.size/200;
                  e.target.height=e.target.height*e.target.size/200;
                }
                else if(e.target.width<=900){
                  e.target.width=e.target.width*e.target.size/300;
                  e.target.height=e.target.height*e.target.size/300;
                }
                else {
                  e.target.width=e.target.width*e.target.size/400;
                  e.target.height=e.target.height*e.target.size/400;
                }



                // alert(e.target.width + " " + e.target.height);
                // console.log(e.target);
                dynamictexture.drawImage(e.target,e.target.positionX_value,e.target.positionY_value,e.target.width,e.target.height);
              // dynamictexture.rotate( Math.PI / 4 );
              });

          }
        }

      }

      for (var i = 0; i < loadedMesh1.length; i++) {


      loadedMesh1[i].children[0].material = new THREE.MeshLambertMaterial({ map: texture });
      loadedMesh1[i].children[1].material = new THREE.MeshLambertMaterial({ map: dynamictexture.texture });
      loadedMesh1[i].rotation.y = app360.common.viewValue;
      loadedMesh1[i].children[1].material.transparent = true;

    }
   //return scene;
  }

