function update(e, t, n, u) {
    e.render(t, n), u.update(), requestAnimationFrame(function() {
        update(e, t, n, u)
    }), TWEEN.update()
}