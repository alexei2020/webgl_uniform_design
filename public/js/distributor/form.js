// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "formeditor",
  file: "distributor/form/create"
  },
  {
  name: "formtable",
  file: "distributor/form/table"
  },
  {
  name: "formrow",
  file: "distributor/form/row"
  },
 /* {
  name: "select",
  file: "distributor/companies/select"
  }*/
];

app.companiesForm={
  id:""
};
app.respSelect={};
app.industrySelected=[];

app.filter={
  search:"",
  sortName:"id",
  sortBy:"asc",
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};

app.bu={};




$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

 $(document).on('click touch', '#addformbutton', function(e) {
    e.preventDefault();
     var id = 0;
     app.editForm(id);
  });
  $(document).on('click touch', '#editformbutton', function(e) {
    e.preventDefault();
     var id = $(this).attr("data-id");

     app.editForm(id);
  });

  $(document).on('click touch', '#submitformbutton', function(e) {
    e.preventDefault();
     app.handleForm();
  });
 

  $(document).on('click touch', '#deleteformbutton', function(e) {
    e.preventDefault();
     app.companiesForm.id=$(this).attr("data-id");
     app.deleteForm();
  });

  $(document).on('change','.formcheckboxoption',function(e){
    e.preventDefault();
    app.companiesForm.id=$(this).attr('data-id');
    app.formStatusChange();
  });

  $(document).on('keyup','#name',function(){
    var query = $(this).val();
      app.autofillName(query);
  });

  $(document).on('click', '#countryList>ul>li', function(){  
        $('#name').val($(this).text());  
        $('#countryList').fadeOut();  
    });  
 
  
   $(document).on('keyup touch', '#searchforminput', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getForm();
  });

   $(document).on('click', '.sorting', function(){
        app.filter.sortName=$(this).data('column_name')
        app.filter.sortBy=$(this).data('sorting_type');
       
          app.getForm();  
  
  });


   $(document).on('click','.pagination a', function (event) {
        event.preventDefault();
        app.filter.page=$(this).attr('href').match(/page=([0-9]+)/)[1];
        app.getForm();
    });

  
} //setListeners

app.launchUser = function() {

  app.getForm();
  // app.getBU();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchUser();
} //- startApp

app.handleForm =function(){

 app.makeForm();
     var errors=app.validateForm();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveForm();

}

app.makeForm=function(){
   var checked = $('#email_notification').is(":checked");
   var editId=$('#id').val().trim();
    app.companiesForm={
    name:$('#name').val().trim(),
    email:$('#email').val().trim(),
    // distributorId: $('#distributorId').val(),
    email_subject: $('#email_subject').val(),
    // completion_page:$('#completion_page').val(),
    // form_code: $('#form_code').val(),
    // submit_button_code: $('#submit_button_code').val(),
    // intro_text: $('#intro_text').val(),
    // footer_text: $('#footer_text').val(),
    notification:( checked==true ?'true':'false'),
    status: $('#status').prop('checked'),
    form_type: $('#form_type').val(),
    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
 



}




app.validateForm=function(){
  var error=[];

 

   if(app.companiesForm.name=="")
  {
      error.push("Please enter form name");
  }

  // else if(app.companiesForm.distributorId=="")
  // {
  //     error.push("Please select distributor");
  // }
  else if(app.companiesForm.email=="")
  {
      error.push("Please enter form email");
  }
  else if(!app.validateEmail(app.companiesForm.email))
  {
      error.push("E-mail address is not valid");
  }
  else if(app.companiesForm.email_subject=="")
  {
      error.push("Please enter email subject");
  }
 /* else if(app.companiesForm.form_code=="")
  {
      error.push("Please enter form code");
  }*/
  /*else if(app.companiesForm.submit_button_code=="")
  {
      error.push("Please enter submit button code");
  }
  else if(app.companiesForm.intro_text=="")
  {
      error.push("Please enter intro text");
  }
  else if(app.companiesForm.footer_text=="")
  {
      error.push("Please enter footer text");
  }*/
  else if(app.companiesForm.form_type=="")
  {
      error.push("Please slect form type");
  }
 /* else if(app.companiesForm.status=="")
  {
      error.push("Please select status");
  }*/

  

  


  return error;
}

app.saveForm=function(){

$('#submitformbutton').prop("disabled",true);
$('#submitformbutton').html("Saving....");
 var button="Save";
if(app.companiesForm.id<1)
{
  var url=app.location+"distributor/form";
}
else
{
  button="Save Changes";
  var url=app.location+"distributor/form/update";
}

 var formData = new FormData($('#companyform')[0]);
 formData.append("_token", app.companiesForm._token);
        formData.append("id", app.companiesForm.id);
        formData.append("name", app.companiesForm.name);
        formData.append("email",app.companiesForm.email);
        // formData.append("distributorId",app.companiesForm.distributorId);
        formData.append("email_subject",app.companiesForm.email_subject);

        // formData.append("completion_page",app.companiesForm.completion_page);

        // formData.append("form_code",app.companiesForm.form_code);
        // formData.append("submit_button_code",app.companiesForm.submit_button_code);
        // formData.append("intro_text",app.companiesForm.intro_text);
        // formData.append("footer_text",app.companiesForm.footer_text);
        formData.append("notification",app.companiesForm.notification);
        formData.append("form_type",app.companiesForm.form_type);
        formData.append("status",app.companiesForm.status);


        
       
  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
          /*if(response.msg.profile.length>0)
          {
            var message =["Company Logo must be an image type as jpg/jpeg or png."]; 
            response.msg.profile = message;
          }*/

         app.validationMessage(response.msg,"warning");
        $('#submitdistributorbutton').prop("disabled",false);
        $('#submitdistributorbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {


           // $('#modal').modal('hide');
           location.reload();
            // app.getForm();
       }
      
    }); //- ajax

}
app.editForm=function(editId){
    var edit = {};
    var heading="Add New Form";
     var button="Save";
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
        edit = _.findWhere(app.resp.companies.data, {
            id:parseInt(editId)
        });

        button="Save Changes";
        heading="Edit Form";
    } else {
        edit = {
            id: "",
            name: "",
            email: "",
            // distibutorId:"",
            email_subject:"",

            completion_page:"",

            // form_code:"",
            submit_button_code:"",
            intro_text:"",
            footer_text:"",
            notification:""
        };
    }

    edit.heading=heading;

    app.log("Got click: ", edit);
   
  $('#modalContainer').html(app.templates.modal({
    'title': edit.heading,
    'content': app.templates.formeditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary' data-dismiss='modal'>Close</button><button type='button' id='submitformbutton' class='btn btn-primary'>"+button+"</button></div>"
  }));

  if(edit.id)
  {
    // $('#distributorId').val(edit.distributorId);
    $('#form_type').val(edit.form_type);
    $('#status').val(edit.status);
  }


 $("select").chosen();
  $('#modal').modal('show');
}



app.autofillName=function(query){
  if(query != '')
        {
         var _token = $('meta[name="csrf-token"]').attr('content');
         $.ajax({
          url:app.location+"distributor/form/autofill",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
            console.log(data);
            if(data!="")
            {
              $('#countryList').fadeIn();  
              $('#countryList').html(data);  
            }
            else{
              $('#countryList').fadeOut();
            }
          }
         });
        }
}

app.deleteForm=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete the record',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteForm();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.companiesForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}

app.dodeleteForm=function(){

     app.companiesForm = {
            id: app.companiesForm.id,
            _token:$('meta[name="csrf-token"]').attr('content')
        };

  $.ajax({
      method: "DELETE",
      url: app.location+"distributor/form/1",
      dataType: "json",
      data:app.companiesForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
            app.getForm();
       }
      
    }); //- ajax

}//-ef deleteUser




app.getForm=function(pages = 1) {
     
        // $('.loading').show();
        var token = $('meta[name="csrf-token"]').attr('content');
       
       // formData.append("sortby",reverse_order);
       // formData.append("sorttype",column_name);
       
        $.ajax({
            type: "POST",
            url: app.location+'distributor/form/getList',
            data : app.filter,
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
              //  $("#searchResultDiv").html(data);
              // $('.loading').hide();

                app.showDistributorList();
                
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
    }




app.showDistributorList=function(){

    var rows = [];
    var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }
   _.each(app.resp.companies.data, function(content) {
  
        content.i=i++;
        rows.push(app.templates.formrow(content));
    }); //-

    $('#result').html(app.templates.formtable({
         rows: rows.join(""),
         filter:app.filter
    }));
    $('#pagination').html(app.resp.pagination);
   app.sorting();

}

app.sorting=function(){
     app.clear_icon();
    if(app.filter.sortBy == 'asc')
      {
       $('#'+app.filter.sortName+'_icon').closest('.sorting').data('sorting_type', 'desc');
       $('#'+app.filter.sortName+'_icon').html('<span class="opacity_5 fa fa-caret-up"></span><span class="fa fa-caret-down"></span>');
      
      }
      if(app.filter.sortBy == 'desc')
      {
        $('#'+app.filter.sortName+'_icon').closest('.sorting').data('sorting_type', 'asc');
        $('#'+app.filter.sortName+'_icon').html(' <span class="fa fa-caret-up"></span><span class="fa fa-caret-down opacity_5"></span>');
      }
  


}
app.clear_icon=function(){
  $('#name_icon,#email_icon,#mobile_icon').html('<span class="fa fa-caret-up opacity_5"></span><span class="fa fa-caret-down opacity_5"></span>');
}

app.formStatusChange=function(){
    app.companiesForm = {
        id: app.companiesForm.id,
        // status:app.categoryForm.status,
        _token:$('meta[name="csrf-token"]').attr('content')
    };

  $.ajax({
      method: "POST",
      url: app.location+"distributor/form/formStatusChange",
      dataType: "json",
      data:app.companiesForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
            // app.getDistributor();
            // app.alertMessage("Status change successfully","success");
       }
    }); //- ajax
}


app.loadTemplates = function() {

	app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    
  app.templates.norecord = _.template('<div class="col-sm-12 text-center"><img src="'+app.location+'public/images/norecord.png" width="300"><h4 class="mt-4">No partner found</h4></div>');

} //- loadTemplates
