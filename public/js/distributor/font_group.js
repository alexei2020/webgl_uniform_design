// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "fontgroupeditor",
  file: "distributor/fontgroup/create"
  },
  {
  name: "fontgrouptable",
  file: "distributor/fontgroup/table"
  },
  {
  name: "fontgrouprow",
  file: "distributor/fontgroup/row"
  },
  {
  name: "select",
  file: "common/select"
  }
];

app.fontGroupForm={
  id:""
};

app.filter={
  search:"",
 
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};





$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

  $(document).on('click touch', '#addfontgroupbutton', function(e) {
    e.preventDefault();
     var id = 0;
     app.editFontGroup(id);
  });
 
  $(document).on('click touch', '#editfontgroupbutton', function(e) {
    e.preventDefault();
     var id = $(this).attr("data-id");
     app.editFontGroup(id);
  });

  $(document).on('click touch', '#submitfontgroupbutton', function(e) {
    e.preventDefault();
     app.handleFontGroup();
  });



    $(document).on("click touch","#deletefontgroupbutton",function() {
      app.fontGroupForm.id=$(this).attr('data-id');
      app.deleteFontGrouphandle();
   });
   

   $(document).on('keyup touch', '#searchfontgroupinput', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getFontGroup();
  });
    

   $(document).on('click','.pagination a', function (event) {
        event.preventDefault();
        app.filter.page=$(this).attr('href').match(/page=([0-9]+)/)[1];
        app.getFont();
    });
 
} //setListeners

app.launchFontGroup = function() {
  app.getFontGroup();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchFontGroup();
} //- startApp


app.handleFontGroup =function(){

 app.makeFontGroup();
     var errors=app.validateFontGroup();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveFontGroup();

}

app.makeFontGroup=function(){
  var editId=$('#id').val().trim();
   app.fontGroupForm={
    name:$('#name').val().trim(),
    fontId:$('#fontId').val(),
    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
 

}
app.validateFontGroup=function(){
  var error=[];
  if(app.fontGroupForm.id=="")
  {
    if(app.fontGroupForm.name==""){
      error.push("Please enter name");
    }
   
    
  }
  return error;
}

app.saveFontGroup=function(){
  console.log(app.fontGroupForm);
$('#submitfontgroupbutton').prop("disabled",true);
$('#submitfontgroupbutton').html("Saving....");

var button="Save";
// var method="POST";
if(app.fontGroupForm.id<1)
{
  var url=app.location+"distributor/fontgroup";
}
else
{
  // method="PUT";
  button="Save Changes";
  var url=app.location+"distributor/fontgroup/update";
}
      
    var formData = new FormData($('#fontGroupForm')[0]);
 formData.append("_token", app.fontGroupForm._token);
        formData.append("id", app.fontGroupForm.id);
        formData.append("name", app.fontGroupForm.name);
        formData.append("fontId",app.fontGroupForm.fontId);


  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
        
        $('#submitfontgroupbutton').prop("disabled",false);
        $('#submitfontgroupbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           $('#modal').modal('hide');
           window.location.reload();
  
       }
      
    }); //- ajax

}
app.editFontGroup=function(editId){
    var edit = {};
    var heading="Add Font Group";
    var button="Save";

    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
         edit = _.findWhere(app.resp.fontgroup.data, {
            id:parseInt(editId)
        });

         button="Save Changes";
        heading="Edit Font Group";

    } else {
        edit = {
            id: "",
            name: "",
            fontId:""
        };
    }

     var font=[]; 
    
    var sortedAsc = _.sortBy(app.resp.font, 'name');

    _.each(sortedAsc, function(content) {
          font.push('<option value="'+content.id+'">'+content.name+'</option>');
    }); 
    console.log(font);
    /*_.each(app.resp.color, function(content) {
          color.push('<option value="'+content.id+'" >'+content.name+'</option>');
    }); */

    edit.font=font;
    edit.heading=heading;
   app.log("Got click: ", edit);
  

  $('#modalContainer').html(app.templates.modal({
    'title': edit.heading,
    'content': app.templates.fontgroupeditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary mr-3' data-dismiss='modal'>Close</button>  <button type='button' id='submitfontgroupbutton' class='btn btn-primary'>"+button+"</button>"
  }));

  if(edit.id>0)
  {
    // $("#fontId").val(edit.fontId);
    $.each(edit.fId.split(","), function(i,e){
          $("#fontId option[value='" + e + "']").prop("selected", true);
      });
  }

  $('#fontId').multiselect({
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true
    });

  // $("select").chosen();
  $('#modal').modal('show');
}


app.deleteFontGroupbuttonCheck=function(){
 
  var flag=0;
  $('.checkboxoption').each(function() {
       if($(this).is(":checked"))
       {
           flag++;
       }
  });
  
  if(flag!=0)
  {
     if(flag>1)
      {
         $('#deletefontbutton').html("Delete "+flag+" items");
      }
      else
      {
         $('#deletefontbutton').html("Delete "+flag+" item");
      }
      $('#multiOperate').show();
  }
  else
  {
     $('#multiOperate').hide();
  }
}

app.deleteFontGrouphandle=function(){
   
   app.deleteFontGroup();
}

app.deleteFontGroup=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete the record',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteFontGroup();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.fontGroupForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}


app.dodeleteFontGroup=function(){
   
     app.fontGroupForm = {
            id: app.fontGroupForm.id,
            _token:$('meta[name="csrf-token"]').attr('content')
        };

  $.ajax({
      method: "DELETE",
      url: app.location+"distributor/fontgroup/1",
      dataType: "json",
      data:app.fontGroupForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           /// app.getCategory();
           window.location.reload();
       }
        $('#multiOperate').hide();
      
    }); //- ajax

}//-ef deleteUser

app.getFontGroup=function(pages = 1) {

        var token = $('meta[name="csrf-token"]').attr('content');
 
        $.ajax({
            type: "POST",
            url: app.location+'distributor/fontgroup/getList',
            data : app.filter,
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
                app.showFontGroupList();
                
                
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
}



app.showFontGroupList=function(){

    var rows = [];
    var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }
   _.each(app.resp.fontgroup.data, function(content) {
  
        content.i=i++;
        rows.push(app.templates.fontgrouprow(content));
    }); //-

    $('#result').html(app.templates.fontgrouptable({
         rows: rows.join(""),
         filter:app.filter
    }));
    $('#pagination').html(app.resp.pagination);
}


app.loadTemplates = function() {

  app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    

} //- loadTemplates

