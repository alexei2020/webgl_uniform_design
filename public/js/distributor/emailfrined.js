// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "emailfriendeditor",
  file: "distributor/emailfriend/create"
  }
 /* {
  name: "select",
  file: "superadmin/companies/select"
  }*/
];

app.companiesForm={
  id:""
};
app.respSelect={};
app.industrySelected=[];

app.filter={
  search:"",
  sortName:"id",
  sortBy:"asc",
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};

app.bu={};




$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();


  $(document).on('click touch', '#submitemailfrinedbutton', function(e) {
    e.preventDefault();
     app.handleEmailFrined();
  });


  
} //setListeners

app.launchUser = function() {

  app.getEmailFriend();
  // app.getBU();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchUser();
} //- startApp

app.handleEmailFrined =function(){

 app.makeEmailFrined();
     var errors=app.validateEmailFrined();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveEmailFrined();

}

app.makeEmailFrined=function(){
   var editId=$('#id').val().trim();
    app.companiesForm={
    from_name:$('#from_name').val().trim(),
    from_email:$('#from_email').val().trim(),
    subject: $('#subject').val(),
    email_template: $('#email_template').val(),
    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
 



}




app.validateEmailFrined =function(){
  var error=[];

 

   if(app.companiesForm.from_name=="")
  {
      error.push("Please enter name");
  }

  else if(app.companiesForm.from_email=="")
  {
      error.push("Please enter email");
  }
  else if(!app.validateEmail(app.companiesForm.from_email))
  {
      error.push("E-mail address is not valid");
  }
  else if(app.companiesForm.subject=="")
  {
      error.push("Please enter subject");
  }
  else if(app.companiesForm.email_template=="")
  {
      error.push("Please enter email template");
  }
 

  

  return error;
}

app.saveEmailFrined=function(){

$('#submitemailfrinedbutton').prop("disabled",true);
$('#submitemailfrinedbutton').html("Saving....");
 var button="Save";
if(app.companiesForm.id<1)
{
  var url=app.location+"distributor/emailfriend";
}
else
{
  button="Save Changes";
  var url=app.location+"distributor/emailfriend/update";
}

 var formData = new FormData($('#companyform')[0]);
 formData.append("_token", app.companiesForm._token);
        formData.append("id", app.companiesForm.id);
        formData.append("from_name", app.companiesForm.from_name);
        formData.append("from_email",app.companiesForm.from_email);
        formData.append("subject",app.companiesForm.subject);
        formData.append("email_template",app.companiesForm.email_template);


       
  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
          /*if(response.msg.profile.length>0)
          {
            var message =["Company Logo must be an image type as jpg/jpeg or png."]; 
            response.msg.profile = message;
          }*/

         app.validationMessage(response.msg,"warning");
        $('#submitemailfrinedbutton').prop("disabled",false);
        $('#submitemailfrinedbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {


           // $('#modal').modal('hide');
          // location.reload();
            app.getForm();
       }
      
    }); //- ajax

}
app.editEmailFrined=function(editId){
    var edit = {};
    var heading="Add New Form";
     var button="Save";
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
        edit = _.findWhere(app.resp.email_friend, {
            id:parseInt(editId)
        });

        button="Save Changes";
        heading="Edit Form";
    } else {
        edit = {
            id: "",
            from_name: "",
            from_email: "",
            subject:"",
            email_template:""
        };
    }

    edit.heading=heading;

    app.log("Got click: ", edit);
   
   var dh=app.templates.modal({
    'title': edit.heading,
    'content': app.templates.emailfriendeditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary' data-dismiss='modal'>Close</button><button type='button' id='submitemailfrinedbutton' class='btn btn-primary'>"+button+"</button></div>"
  });
  $('#result').html( app.templates.emailfriendeditor(edit));


 //$("select").chosen();
  //$('#modal').modal('show');
}





app.getEmailFriend=function(pages = 1) {
     
        // $('.loading').show();
        var token = $('meta[name="csrf-token"]').attr('content');
       
       // formData.append("sortby",reverse_order);
       // formData.append("sorttype",column_name);
       
        $.ajax({
            type: "POST",
            url: app.location+'distributor/emailfriend/getList',
            data : app.filter,
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
              //  $("#searchResultDiv").html(data);
              // $('.loading').hide();

                app.showEmailFriendList();
                
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
    }




app.showEmailFriendList=function(){

var id=0;

   _.each(app.resp.email_friend, function(content) {
  
       id=content.id;
    }); //-

   app.editEmailFrined(id);


    
}


app.loadTemplates = function() {

	app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    
  app.templates.norecord = _.template('<div class="col-sm-12 text-center"><img src="'+app.location+'public/images/norecord.png" width="300"><h4 class="mt-4">No partner found</h4></div>');

} //- loadTemplates
