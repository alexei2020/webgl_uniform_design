// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}


if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "fonteditor",
  file: "distributor/font/create"
  },
  {
  name: "fonttable",
  file: "distributor/font/table"
  },
  {
  name: "fontrow",
  file: "distributor/font/row"
  },
  {
  name: "fontimport",
  file: "distributor/font/import"
  },
   {
  name: "fontxlstable",
  file: "distributor/font/xlstable"
  },
  {
  name: "fontxlsrow",
  file: "distributor/font/xlsrow"
  },
  {
  name: "select",
  file: "common/select"
  }
];

app.fontForm={
  id:""
};

app.filter={
  search:"", 
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};


$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {
  app.setLibListeners();

  $(document).on('click touch', '#addfontbutton', function(e) {
    e.preventDefault();
     var id = 0;
     app.editFont(id);
  });
 
  $(document).on('click touch', '#editfontbutton', function(e) {
    e.preventDefault();
     var id = $(this).attr("data-id");
     app.editFont(id);
  });

  $(document).on('click touch', '#importfontbutton', function(e) {
    e.preventDefault();
     app.importFont();
  });

  $(document).on('click touch', '#submitimportfontbutton', function(e) {
    e.preventDefault();
     app.saveImportFont();
  });

  $(document).on('click touch', '#submitfontbutton', function(e) {
    e.preventDefault();
     app.handleFont();
  });

  $(document).on("click touch","#deletefontbutton",function() {
      app.fontForm.id=$(this).attr('data-id');
      app.deleteFonthandle();
  });
   
  $(document).on('change','.fontcheckboxoption',function(e){
    e.preventDefault();
    app.fontForm.id=$(this).attr('data-id');
    app.fontStatusChange();
  });

  $(document).on("change",'input[type="file"]',function(e){
        var fileName = e.target.files[0].name;
        $("#importFontForm").find('.custom-file-label').html(fileName);
  });

  $(document).on('keyup touch', '#searchfontinput', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getFont();
  });
    
  $(document).on('click','.pagination a', function (event) {
        event.preventDefault();
        app.filter.page=$(this).attr('href').match(/page=([0-9]+)/)[1];
        app.getFont();
  });

  $(document).on('click', '#font_xls_download', function(e) {
      e.preventDefault();
      app.filter.download_status ="xls_download";     
      app.getFont();
    });
} //setListeners

app.launchFont = function() {
  app.getFont();
} //- launchUser

app.startApp = function() {
  app.log("Starting");
 app.launchFont();
} //- startApp

app.handleFont =function(){
  app.makeFont();
  var errors=app.validateFont();
   if(errors.length>0)
   {
      app.alertMessage(errors.join("<br \>"),"warning");
      return;
   }
   app.saveFont();
}

app.makeFont=function(){
  var editId=$('#id').val().trim();
  app.fontForm={
    name:$('#name').val().trim(),
    value:$('#value').val().trim(),
    status:$('#status').prop('checked'),
    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }

}
app.validateFont=function(){
  var error=[];
  if(app.fontForm.id=="")
  {
    if(app.fontForm.value==""){
      error.push("Please select font");
    }
  }
  return error;
}

app.saveFont=function(){
  $('#submitfontbutton').prop("disabled",true);
  $('#submitfontbutton').html("Saving....");

  var button="Save";
  if(app.fontForm.id<1)
  {
    var url=app.location+"distributor/font";
  }
  else
  {
    button="Save Changes";
    var url=app.location+"distributor/font/update";
  }
      
    var formData = new FormData($('#fontform')[0]);
    formData.append("_token", app.fontForm._token);
        formData.append("id", app.fontForm.id);
        formData.append("name", app.fontForm.name);
        formData.append("value",app.fontForm.value);
        formData.append("status",app.fontForm.status);
  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
        
        $('#submitfontbutton').prop("disabled",false);
        $('#submitfontbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           $('#modal').modal('hide');
           window.location.reload();
       }
      
    }); //- ajax

}
app.editFont=function(editId){
    var edit = {};
    var heading="Add Font";
    var button="Save";
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
         edit = _.findWhere(app.resp.font.data, {
            id:parseInt(editId)
        });

         button="Save Changes";
        heading="Edit Font";

    } else {
        edit = {
            id: "",
            name: "",
            status:"",
            value:""
        };
    }
    if(edit.id=="")
    {
      edit.status="active";
    }
    edit.heading=heading;
   app.log("Got click: ", edit);
  
  $('#modalContainer').html(app.templates.modal({
    'title': edit.heading,
    'content': app.templates.fonteditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary mr-3' data-dismiss='modal'>Close</button>  <button type='button' id='submitfontbutton' class='btn btn-primary'>"+button+"</button>"
  }));
  

  $("#test_font>span").css("font-family", edit.value);
  $("select").chosen();
  $('#modal').modal('show');
}


app.importFont=function(){
  $('#modalContainer').html(app.templates.modal({
    'title':'Import/Export Font',
    'content': app.templates.fontimport(),
    'footer':"<button type='button'  class='btn btn-secondary mr-3' data-dismiss='modal'>Close</button>  <button type='button' id='submitimportfontbutton' class='btn btn-primary'>Upload</button>"
  }));
  $('#modal').modal('show');

}

app.saveImportFont=function(){

  $('#submitimportfontbutton').prop("disabled",true);
  $('#submitimportfontbutton').html("Uploading....");
  var method="POST";
  var url=app.location+"distributor/font/importExcel";
  var formData = new FormData($('#importFontForm')[0]);
       
  formData.append("_token",$('meta[name="csrf-token"]').attr('content'));     
  $.ajax({
      method: method,
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
        
        $('#submitimportfontbutton').prop("disabled",false);
        $('#submitimportfontbutton').html('Upload');
    })
    .done(function(resp) {
      console.log(resp);
      if (resp.resp == 'ok') {
           $('#modal').modal('hide');

           location.reload();           
       }
      
    }); //- ajax
}

app.deleteFontbuttonCheck=function(){
  var flag=0;
  $('.checkboxoption').each(function() {
       if($(this).is(":checked"))
       {
           flag++;
       }
  });
  
  if(flag!=0)
  {
     if(flag>1)
      {
         $('#deletefontbutton').html("Delete "+flag+" items");
      }
      else
      {
         $('#deletefontbutton').html("Delete "+flag+" item");
      }
      $('#multiOperate').show();
  }
  else
  {
     $('#multiOperate').hide();
  }
}

app.deleteFonthandle=function(){
   app.deleteFont();
}

app.deleteFont=function(){
  $.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete the record',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteFont();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.fontForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}


app.dodeleteFont=function(){
     app.fontForm = {
            id: app.fontForm.id,
            _token:$('meta[name="csrf-token"]').attr('content')
        };

  $.ajax({
      method: "DELETE",
      url: app.location+"distributor/font/1",
      dataType: "json",
      data:app.fontForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           window.location.reload();
       }
        $('#multiOperate').hide();
      
    }); //- ajax

}//-ef deleteUser

app.getFont=function(pages = 1) {
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: "POST",
        url: app.location+'distributor/font/getList',
        data : app.filter,
        dataType:"json",
        success: function (resp) {
            app.resp=resp;
            if(app.filter.download_status=="xls_download")
            {
              app.downloadFontXls();  
            }
            else{
              app.showFontList();  
            }
        },
        error: function (xhr, status, error) 
        {
           // alert(xhr.responseText);
        }
    });
}

app.showFontList=function(){
    var rows = [];
    var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }
   _.each(app.resp.font.data, function(content) {
  
        content.i=i++;
        rows.push(app.templates.fontrow(content));
    }); //-

    $('#result').html(app.templates.fonttable({
         rows: rows.join(""),
         filter:app.filter
    }));
    $('#pagination').html(app.resp.pagination);
}

app.downloadFontXls=function(){
    var rows = [];
    var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }
   _.each(app.resp.font.data, function(content) {
  
        content.i=i++;
        rows.push(app.templates.fontxlsrow(content));
    }); //-
    var test =app.templates.fontxlstable({
         rows: rows.join(""),
         filter:app.filter
    });
    app.exceller(test,"Font");
    app.filter.download_status="";
}


app.fontStatusChange=function(){
    app.fontForm = {
        id: app.fontForm.id,
        _token:$('meta[name="csrf-token"]').attr('content')
    };

  $.ajax({
      method: "POST",
      url: app.location+"distributor/font/fontStatusChange",
      dataType: "json",
      data:app.fontForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
  
       }
    }); //- ajax
}


app.loadTemplates = function() {
  app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    
} //- loadTemplates

function setFontToName(font)
{
    font = font.replace(/\+/g, ' ');
    font = font.split(':');
    $('#name').val(font[0]);
}

function setFontToValue(font)
{
    font = font.replace(/\+/g, ' ');
    font = font.split(':');
    $('#value').val(font[0]);
    $("#test_font>span").css("font-family", font[0]);
}


function applyFont(font) {

    // Replace + signs with spaces for css
    font = font.replace(/\+/g, ' ');
    // Split font into family and weight
    font = font.split(':');
    var fontFamily = font[0];
    var fontWeight = font[1] || 400;
    return fontFamily;
  }

app.exceller=function(table,filename) {
  console.log(table);
    var uri = 'data:application/vnd.ms-excel;base64,',
      template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
      base64 = function(s) {
        return window.btoa(unescape(encodeURIComponent(s)))
      },
      format = function(s, c) {
        return s.replace(/{(\w+)}/g, function(m, p) {
          return c[p];
        })
      }
    var toExcel =  table;//document.getElementById(id).innerHTML;
    var ctx = {
      worksheet: name || '',
      table: toExcel
    };
    var link = document.createElement("a");
    link.download = filename+".xls";
    link.href = uri + base64(format(template, ctx))
    link.click();
}