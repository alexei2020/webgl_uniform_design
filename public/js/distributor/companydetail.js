// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "companydetaileditor",
  file: "distributor/companydetail/create"
  },
  {
  name: "sociamediaeditor",
  file: "distributor/companydetail/createsocialmedia"
  }
 /* {
  name: "select",
  file: "superadmin/companies/select"
  }*/
];

app.companiesForm={
  id:""
};
app.companySocialForm={
  id:""
};
app.filter={
  search:"",
  sortName:"id",
  sortBy:"asc",
  page:1,
  view:"companyDetail",
 _token: $('meta[name="csrf-token"]').attr('content')
};

app.bu={};




$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();


  $(document).on('click touch', '#submitcompanydetailbutton', function(e) {
    e.preventDefault();
     app.handleCompanyDetail();
  });

  $(document).on('click touch', '#submitsocialmedialbutton', function(e) {
    e.preventDefault();
     app.handleSocialMedia();
  });

  $(document).on('click touch', '.roottablink', function(e) {
    e.preventDefault();
    app.filter.view=$(this).attr("data-value");
     app.rootTabChange();
  });
  
} //setListeners

app.launchUser = function() {

  app.getCompanyDetail();
  // app.getBU();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchUser();
} //- startApp

app.handleCompanyDetail =function(){

 app.makeCompanyDetail();
     var errors=app.validateCompanyDetail();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveCompanyDetail();

}

app.makeCompanyDetail=function(){
   var editId=$('#id').val().trim();
    app.companiesForm={
    company_name:$('#company_name').val().trim(),
    email:$('#email').val().trim(),
    phone: $('#phone').val(),
    website_url: $('#website_url').val(),
    // default_lang: $('#default_lang').val(),
    time_zone: $('#time_zone').val(),
    // locale: $('#locale').val(),
    status: $('#status').prop('checked'),
    profile:$("#profile").val(),
    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
 
}




app.validateCompanyDetail =function(){
  var error=[];

 
   if(app.companiesForm.company_name=="")
  {
      error.push("Please enter company name");
  }

  else if(app.companiesForm.email=="")
  {
      error.push("Please enter email");
  }
  else if(!app.validateEmail(app.companiesForm.email))
  {
      error.push("E-mail address is not valid");
  }
  else if(app.companiesForm.phone=="")
  {
      error.push("Please enter phone number");
  }
  else if(!validateURL(app.companiesForm.website_url)=="")
  {
      error.push("Please enter valid website url");
  }
 

  

  return error;
}
function validateURL(url)
{
  url_validate = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
  if(!url_validate.test(url)){
     return true;
  }
  else{
     return false;
  }
}


app.saveCompanyDetail=function(){

$('#submitcompanydetailbutton').prop("disabled",true);
$('#submitcompanydetailbutton').html("Saving....");
 var button="Save";
if(app.companiesForm.id<1)
{
  var url=app.location+"distributor/companydetail";
}
else
{
  button="Save Changes";
  var url=app.location+"distributor/companydetail/update";
}

 var formData = new FormData($('#companyform')[0]);
 formData.append("_token", app.companiesForm._token);
        formData.append("id", app.companiesForm.id);
        formData.append("company_name", app.companiesForm.company_name);
        formData.append("email",app.companiesForm.email);
        formData.append("phone",app.companiesForm.phone);
        formData.append("website_url",app.companiesForm.website_url);
        // formData.append("default_lang", app.companiesForm.default_lang);
        formData.append("time_zone",app.companiesForm.time_zone);
        // formData.append("locale",app.companiesForm.locale);
        formData.append("status",app.companiesForm.status);
        formData.append("profile",app.companiesForm.profile);

       
  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
          /*if(response.msg.profile.length>0)
          {
            var message =["Company Logo must be an image type as jpg/jpeg or png."]; 
            response.msg.profile = message;
          }*/

         app.validationMessage(response.msg,"warning");
        $('#submitcompanydetailbutton').prop("disabled",false);
        $('#submitcompanydetailbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {


           // $('#modal').modal('hide');
          location.reload();
            // app.getForm();
       }
      
    }); //- ajax

}
app.editCompanyDetail=function(editId){

    var edit = {};
    var heading="Add New Form";
     var button="Save";
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
        edit = _.findWhere(app.resp.company_details, {
            id:parseInt(editId)
        });

        button="Save Changes";
        heading="Edit Form";
    } else {
        edit = {
            id: "",
            company_name: "",
            email: "",
            phone:"",
            website_url:"",
            // default_lang:"",
            time_zone:"",
            // locale:"",
            status:"",
            profile:"",
            profileId:"",
            profileName:""
        };
    }

    var timezone=[];

      _.each(app.resp.timezone, function(content) {
          timezone.push('<option value="'+content.id+'">'+content.name+'</option>');
    }); //-

      edit.timezone =timezone;

    edit.heading=heading;

    app.log("Got click: ", edit);
   
   var dh=app.templates.modal({
    'title': edit.heading,
    'content': app.templates.companydetaileditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary' data-dismiss='modal'>Close</button><button type='button' id='submitcompanydetailbutton' class='btn btn-primary'>"+button+"</button></div>"
  });
  $('#companyDetail').html( app.templates.companydetaileditor(edit));
  if(edit.id>0)
  {
    $("#time_zone").val(edit.time_zone);  
  }

  $("#profile").val(edit.profileId);
 
  $("select").chosen();
  //$('#modal').modal('show');
}




app.handleSocialMedia =function(){

 app.makeSocialMedia();
     var errors=app.validateSocialMedia();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveSocialMedia();

}

app.makeSocialMedia=function(){
   var editId=$('#csmId').val().trim();
    app.companySocialForm={
    social_share_html:$('#social_share_html').val(),
    pubId:$('#pubId').val(),
    status_add_script: $('#status_add_script').prop('checked'),
    id: $('#csmId').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }

}


app.validateSocialMedia =function(){
  var error=[];

 
   /*if(app.companiesForm.company_name=="")
  {
      error.push("Please enter company name");
  }

  else if(app.companiesForm.email=="")
  {
      error.push("Please enter email");
  }
  else if(!app.validateEmail(app.companiesForm.email))
  {
      error.push("E-mail address is not valid");
  }
  else if(app.companiesForm.phone=="")
  {
      error.push("Please enter phone number");
  }
  else if(!validateURL(app.companiesForm.website_url)=="")
  {
      error.push("Please enter valid website url");
  }
 */

  

  return error;
}



app.saveSocialMedia=function(){

$('#submitsocialmedialbutton').prop("disabled",true);
$('#submitsocialmedialbutton').html("Saving....");
 var button="Save";

if(app.companySocialForm.id<1)
{
  var url=app.location+"distributor/companydetail/socialStore";
}
else
{
  button="Save Changes";
  var url=app.location+"distributor/companydetail/socialUpdate";
}

 var formData = new FormData($('#companySocialform')[0]);
 formData.append("_token", app.companySocialForm._token);
        formData.append("id", app.companySocialForm.id);
        formData.append("social_share_html", app.companySocialForm.social_share_html);
       /* formData.append("pubId",app.companySocialForm.pubId);
        formData.append("status_add_script",app.companySocialForm.status_add_script);*/
       
  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
        
         app.validationMessage(response.msg,"warning");
        $('#submitsocialmedialbutton').prop("disabled",false);
        $('#submitsocialmedialbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
        $('#submitsocialmedialbutton').prop("disabled",false);
        $('#submitsocialmedialbutton').html("Save");
          // location.reload();
  
       }
      
    }); //- ajax

}
app.editSocialMedia=function(editId){

    var edit = {};
    var heading="Add New Form";
     var button="Save";
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
        edit = _.findWhere(app.resp.company_social_detail, {
            id:parseInt(editId)
        });

        button="Save Changes";
        heading="Edit Form";
    } else {
        edit = {
            id: "",
            social_share_html: "",
           /* pubId: "",
            status_add_script:"",*/
        };
    }

    edit.heading=heading;

    app.log("Got click: ", edit);
   var dh=app.templates.modal({
    'title': edit.heading,
    'content': app.templates.sociamediaeditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary' data-dismiss='modal'>Close</button><button type='button' id='submitsocialmedialbutton' class='btn btn-primary'>"+button+"</button></div>"
  });
  $('#socialMedia').html( app.templates.sociamediaeditor(edit));


 //$("select").chosen();
  //$('#modal').modal('show');
}



app.getCompanyDetail=function(pages = 1) {
     
        var token = $('meta[name="csrf-token"]').attr('content');
       
        $.ajax({
            type: "POST",
            url: app.location+'distributor/companydetail/getList',
            data : app.filter,
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
              app.rootTabChange();
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
    }




app.showCompanyDetailList=function(){

var id=0;

   _.each(app.resp.company_details, function(content) {
  
       id=content.id;
    }); //-

   app.editCompanyDetail(id);

}


app.showSocialMediaList=function(){

var id=0;

   _.each(app.resp.company_social_detail, function(content) {
    
       id=content.id;

    }); //-

   app.editSocialMedia(id);

}

app.rootTabChange=function(){
   $('.roottablink').removeClass('active');
  if(app.filter.view=="companyDetail")
  {
    app.showCompanyDetailList();
    $('#main-tab').addClass('active');
    $('#companyDetail').show(); $('#socialMedia').hide();
  
  }
  else
  {
    app.showSocialMediaList();
    $('#sm-tab').addClass('active');
   $('#companyDetail').hide(); $('#socialMedia').show();
  }
}


app.loadTemplates = function() {

	app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    
  app.templates.norecord = _.template('<div class="col-sm-12 text-center"><img src="'+app.location+'public/images/norecord.png" width="300"><h4 class="mt-4">No partner found</h4></div>');

} //- loadTemplates
