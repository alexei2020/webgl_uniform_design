// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [
  {
  name: "formrepliesdetail",
  file: "distributor/formreplies/viewdetail"
  },

 
  {
  name: "formreplytable",
  file: "distributor/formreplies/table"
  },
  {
  name: "formreplyrow",
  file: "distributor/formreplies/row"
  },

  {
  name: "select",
  file: "common/select"
  }
];


app.replyForm={
  id:""
};


app.filter={
  search:"",
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};


$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

  
 
  $(document).on('click touch', '#viewformreplybutton', function(e){
    e.preventDefault();
    var id = $(this).attr('data-id');
    app.getViewDetail(id);
  });


  $(document).on('click','#download_replies',function(e){
      e.preventDefault();
      $('#download_replies').hide();
      $('#heading').hide();
      $('.navbar-nav').hide();
      $('.navbar-toggler').hide();
      $('.bg_black').css('background-color','#f6f8f8');

      window.print(); 
      $('#download_replies').show();
      $('#heading').show();
      $('.navbar-nav').show();
      $('.navbar-toggler').show();
      $('.bg_black').css('background-color','#3d3d3e');
    })


  $(document).on('click','#imageDownload',function(e){
    e.preventDefault();

    var urls = $(this).attr('data-value').split(",");      
      for (var i = 0; i < urls.length; i++) { 
        var a = document.createElement("a");
        a.setAttribute('href', app.location+'public/images/front_back/'+urls[i]);
        a.setAttribute('download', '');
        a.setAttribute('target', '_blank');
        a.click();
      }
      
      if (urls.length == 0) {
        // clearInterval(interval);
      }
  });


  $(document).on('click','#deleteformrepliesbutton',function(e){
    e.preventDefault();
    app.replyForm.id=$(this).attr('data-id');
      app.deleteReplyhandle();
  });

 
   $(document).on('keyup touch', '#searchproductinput', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getFormReply();
  });

  $(document).on('click','#svgDownloadInquiry',function(e){
    e.preventDefault();

    var id=$(this).attr('data-id');
    var svg_snippet = $('#svg_div_'+id).html();
    save_as_svg(svg_snippet);
  });


 
} //setListeners

app.launchFormReply = function() {
  app.getFormReply();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchFormReply();
} //- startApp



app.getFormReply=function(pages = 1) {

        var token = $('meta[name="csrf-token"]').attr('content');
 
        $.ajax({
            type: "POST",
            url: app.location+'distributor/formreplies/getList',
            data : app.filter,
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
                app.showReplyList();
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
}

app.showReplyList=function(){
    var rows = [];
    var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }
   _.each(app.resp.form_replies.data, function(content) {
  
        content.i=i++;
        rows.push(app.templates.formreplyrow(content));
    }); //-

    $('#result').html(app.templates.formreplytable({
         rows: rows.join(""),
         filter:app.filter
    }));
    $('#pagination').html(app.resp.pagination);
}


app.getViewDetail=function(id) {

        var token = $('meta[name="csrf-token"]').attr('content');
 
        $.ajax({
            type: "POST",
            url: app.location+'distributor/formreplies/getViewDetail',
            data : {id:id,_token:token},
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
                app.showViewDetail();
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
}

app.showViewDetail=function(){
    var rows = [];
    $('.search_area').hide();
    $('#result').hide();
   
    _.each(app.resp.quote_mail, function(content) {
        
        var  quotation_detail = _.where(app.resp.quotation_detail, {
            quoteId:parseInt(content.id),
        });
        content.quotation_detail=quotation_detail;
     
        $('#viewDetail').html( app.templates.formrepliesdetail(content));
    }); //-

 
}

app.deleteReplyhandle=function(){
   
   app.deleteReply();
}

app.deleteReply=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete the replies',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteReply();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.replyForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}


app.dodeleteReply=function(){
   
     app.replyForm = {
            id: app.replyForm.id,
            _token:$('meta[name="csrf-token"]').attr('content')
        };

  $.ajax({
      method: "DELETE",
      url: app.location+"distributor/formreplies/1",
      dataType: "json",
      data:app.replyForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           window.location.reload();
       }
        
    }); //- ajax
}//-ef deleteUser


app.loadTemplates = function() {
  app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
} //- loadTemplates


function save_as_svg(svg_snippet){
    var svg_data = svg_snippet; //document.getElementById("svg").innerHTML //put id of your svg element here
    var full_svg =  svg_data ;
    var svgBlob = new Blob([full_svg], {type:"image/svg+xml;charset=utf-8"});
    var svgUrl = URL.createObjectURL(svgBlob);
    var downloadLink = document.createElement("a");
    downloadLink.href = svgUrl;
    downloadLink.download = generateRandomNum()+".svg";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
};

function generateRandomNum() {
  var x = Math.floor((Math.random() * 10000000000000) + 1);
  return x;
}

function replaceLink(e,url) {
  // alert(url);
     e.target.href =url;

     }