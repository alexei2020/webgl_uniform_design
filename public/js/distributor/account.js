// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [
  /*{
  name: "distributoreditor",
  file: "superadmin/distributor/create"
  },
  {
  name: "distributortable",
  file: "superadmin/distributor/table"
  },
  {
  name: "distributorrow",
  file: "superadmin/distributor/row"
  },*/
 /* {
  name: "select",
  file: "superadmin/companies/select"
  }*/
];

app.companiesForm={
  id:""
};
app.respSelect={};
app.industrySelected=[];

app.filter={
  search:"",
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};

app.bu={};




$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();


  $(document).on('click touch', '#updateAccountbutton', function(e) {
    e.preventDefault();
     app.handleAccount();
  });

  $(document).on('click touch', '#changePasswordbutton', function(e) {
    e.preventDefault();
     app.handleChangePassword();
  });  


  
} //setListeners

app.launchUser = function() {

  // app.getDistributor();
  // app.getBU();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchUser();
} //- startApp

app.handleAccount =function(){

 app.updateAccount();
     var errors=app.validateAccount();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveAccount();

}

app.updateAccount=function(){
  var editId=$('#id').val().trim();
   app.companiesForm={
    name:$('#name').val().trim(),
    email:$('#email').val().trim(),
     id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
  
}


app.validateAccount=function(){
  var error=[];
   if(app.companiesForm.name=="")
  {
      error.push("Please enter name");
  }

  else if(app.companiesForm.email=="")
  {
      error.push("Please enter email");
  }
  else if(!app.validateEmail(app.companiesForm.email))
  {
      error.push("E-mail address is not valid");
  }
 
  return error;
}





app.saveAccount=function(){

$('#updateAccountbutton').prop("disabled",true);
$('#updateAccountbutton').html("Saving....");
 var button="Save";
if(app.companiesForm.id<1)
{
  var url=app.location+"superadmin/distributor";
}
else
{
  button="Save Changes";
  var url=app.location+"superadmin/account/update";
}

 var formData = new FormData($('#companyform')[0]);
 formData.append("_token", app.companiesForm._token);
        formData.append("id", app.companiesForm.id);
        formData.append("name", app.companiesForm.name);
        formData.append("email",app.companiesForm.email);

  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
          /*if(response.msg.profile.length>0)
          {
            var message =["Company Logo must be an image type as jpg/jpeg or png."]; 
            response.msg.profile = message;
          }*/

         app.validationMessage(response.msg,"warning");
        $('#updateAccountbutton').prop("disabled",false);
        $('#updateAccountbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {

           // $('#modal').modal('hide');
           location.reload();
            // app.getDistributor();
       }
      
    }); //- ajax

}




app.handleChangePassword =function(){

 app.makeChangePassword();
     var errors=app.validatePassword();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.updateChangePassword();

}

app.makeChangePassword=function(){
  // var editId=$('#id').val().trim();
   app.companiesForm={
    current_password:$('#current_password').val().trim(),
    password:$('#password').val().trim(),
    confirm_password:$('#confirm_password').val().trim(),
    
     // id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
  
}


app.validatePassword=function(){
  var error=[];

    if(app.companiesForm.current_password=="")
    {
        error.push("Please enter current password");
    }
   else if(app.companiesForm.password=="")
    {
        error.push("Please enter password");
    }
    else if(app.companiesForm.password!="" && app.companiesForm.password.length<8)
    {
        error.push("Password must contain at least 8 characters");
    }
   else if(app.companiesForm.password != app.companiesForm.confirm_password) {
     error.push('Password and confirm password fields are not matched.');
    }
 
  return error;
}


app.updateChangePassword=function(){

$('#changePasswordbutton').prop("disabled",true);
$('#changePasswordbutton').html("Saving....");
 var button="Save";
if(app.companiesForm.id<1)
{
  var url=app.location+"distributor/account";
}
else
{
  button="Save Changes";
  var url=app.location+"distributor/account/updatePassword";
}

 var formData = new FormData($('#companyform')[0]);
 formData.append("_token", app.companiesForm._token);
        // formData.append("id", app.companiesForm.id);
        formData.append("current_password", app.companiesForm.current_password);
        formData.append("password", app.companiesForm.password);
        formData.append("confirm_password",app.companiesForm.confirm_password);


  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
       
         app.validationMessage(response.msg,"warning");
        $('#changePasswordbutton').prop("disabled",false);
        $('#changePasswordbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           location.reload();
           
       }
      
    }); //- ajax

}


app.loadTemplates = function() {

	app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    
  app.templates.norecord = _.template('<div class="col-sm-12 text-center"><img src="'+app.location+'public/images/norecord.png" width="300"><h4 class="mt-4">No partner found</h4></div>');

} //- loadTemplates
