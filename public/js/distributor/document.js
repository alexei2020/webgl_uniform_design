// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  
  name: "documenttable",
  file: "distributor/document/table"
  },
  {
  name: "documentrow",
  file: "distributor/document/row"
  },
  {
  name: "select",
  file: "common/select"
  }
];

app.documentForm={
  id:""
};

app.documentDownloadForm={
  file:"",
  name:""
}
app.filter={
  search:"",
  page:1,
  sortName:"id",
  sortBy:"desc",
 _token: $('meta[name="csrf-token"]').attr('content')
};





$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

  $(document).on('click touch', '#adddocumentbutton', function(e) {
    e.preventDefault();
     var id = 0;
     app.editDocument(id);
  });
  


  $(document).on('click touch', '#submitdocumentbutton', function(e) {
    e.preventDefault();
     app.handleDocument();
  });


    $(document).on("click touch","#deletedocumentbutton",function() {
      app.documentForm.id=$(this).attr('data-id');
      app.deleteDocumenthandle();
   });
   

   $(document).on('keyup touch', '#searchdocumentinput', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getDocument();
  });

    $(document).on('click touch', '#downloaddocumentbutton', function(e) {
    e.preventDefault();
       app.documentDownloadForm.file=$(this).attr("data-file");
        app.documentDownloadForm.name=$(this).attr("data-name");
       app.downloadDocument();
    });


  
     $(document).on('click', '.sorting', function(){
        app.filter.sortName=$(this).data('column_name')
        app.filter.sortBy=$(this).data('sorting_type');
       
          app.getDocument();  
  
  });



   $(document).on('click','.pagination a', function (event) {
        event.preventDefault();
        app.filter.page=$(this).attr('href').match(/page=([0-9]+)/)[1];
        app.getDocument();
    });
  

  
} //setListeners

app.launchDocument = function() {
  app.getDocument();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchDocument();
} //- startApp

app.handleDocument =function(){

 app.makeDocument();
     var errors=app.validateDocument();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveDocument();

}

app.makeDocument=function(){
  var editId=$('#id').val().trim();
   app.documentForm={
    name:$('#customFile').val().trim(),
    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
 

}
app.validateDocument=function(){
  var error=[];
 
    if(app.documentForm.name==""){
      error.push("Please choose file");
    }
    
  
  return error;
}

app.saveDocument=function(){
$('#submitdocumentbutton').prop("disabled",true);
$('#submitdocumentbutton').html("Saving....");

var button="Save";
// var method="POST";
if(app.documentForm.id<1)
{
  var url=app.location+"distributor/document";
}
else
{
  // method="PUT";
  button="Save Changes";
  var url=app.location+"distributor/document/update";
}
    


  var formData = new FormData($('#documentform')[0]);
  formData.append("_token", app.documentForm._token);

  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
        
        $('#submitdocumentbutton').prop("disabled",false);
        $('#submitdocumentbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
          // $('#modal').modal('hide');

           window.location.reload();
          //  app.getCategory();
       }
      
    }); //- ajax

}
app.editCategory=function(editId){
    var edit = {};
    var heading="Add New Document";
    var button="Save";

 
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
         edit = _.findWhere(app.resp.category.data, {
            id:parseInt(editId)
        });

         button="Save Changes";
        heading="Edit Document";

    } else {
        edit = {
            id: "",
            name: ""
           
        };
    }


    edit.heading=heading;
   app.log("Got click: ", edit);
  

  /*$('#modalContainer').html(app.templates.modal({
    'title': edit.heading,
    'content': app.templates.categoryeditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary mr-3' data-dismiss='modal'>Close</button>  <button type='button' id='submitcategorybutton' class='btn btn-primary'>"+button+"</button>"
  }));


  $('#modal').modal('show');*/

}




app.deleteDocumenthandle=function(){
   // app.makeDelete();
   app.deleteDocument();
}
app.makeDelete=function(){
  var rows = [];
  $('.checkboxoption').each(function() {
        if($(this).is(':checked'))
        {
           rows.push($(this).val());
        }
   }); 
   rows=rows.join(",");
   app.documentForm.id=rows;
}

app.deleteDocument=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete the record',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteDocument();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.documentForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}


app.dodeleteDocument=function(){
   
     app.documentForm = {
            id: app.documentForm.id,
            _token:$('meta[name="csrf-token"]').attr('content')
        };

  $.ajax({
      method: "DELETE",
      url: app.location+"distributor/document/1",
      dataType: "json",
      data:app.documentForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           /// app.getCategory();
           window.location.reload();
       }
      
    }); //- ajax

}//-ef deleteUser

app.getDocument=function(pages = 1) {

        var token = $('meta[name="csrf-token"]').attr('content');
 
        $.ajax({
            type: "POST",
            url: app.location+'distributor/document/getList',
            data : app.filter,
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
                app.showDocumentList();
                
                
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });


}


app.showDocumentList=function(){

    var rows = [];
    var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }

   _.each(app.resp.documents.data, function(content) {
  
        content.i=i++;
        content.file_size=app.formatSizeUnits(content.file_size);
        rows.push(app.templates.documentrow(content));
    }); //-

    $('#result').html(app.templates.documenttable({
         rows: rows.join(""),
         filter:app.filter
    }));
    $('#pagination').html(app.resp.pagination);
   app.sorting();
 $('#customFile').val('');

}



app.sorting=function(){
     app.clear_icon();
    if(app.filter.sortBy == 'asc')
      {
       $('#'+app.filter.sortName+'_icon').closest('.sorting').data('sorting_type', 'desc');
       $('#'+app.filter.sortName+'_icon').html('<span class="opacity_5 fa fa-caret-up"></span><span class="fa fa-caret-down"></span>');
      
      }
      if(app.filter.sortBy == 'desc')
      {
        $('#'+app.filter.sortName+'_icon').closest('.sorting').data('sorting_type', 'asc');
        $('#'+app.filter.sortName+'_icon').html(' <span class="fa fa-caret-up"></span><span class="fa fa-caret-down opacity_5"></span>');
      }
}
app.clear_icon=function(){
  $('#original_name_icon,#file_size_icon,#createDate_icon').html('<span class="fa fa-caret-up opacity_5"></span><span class="fa fa-caret-down opacity_5"></span>');
}



app.downloadDocument=function(){
  
var link = document.createElement("a");
    link.download = app.documentDownloadForm.name;
    link.href =  app.documentDownloadForm.file;
    link.click();

}
app.formatSizeUnits=function(bytes)
    {

   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];


}


app.loadTemplates = function() {

  app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    

} //- loadTemplates

/*

$(document).on('change','#name',function(){

    var str= $(this).val();
    var str1 = $(this).val().length;
    if(str1<1001)
    {
        str= str.toLowerCase().replace(/[^a-z0-9\s]/gi, '').replace(/\s+/g, '-');
        $('#url_slug').val(str);
         base = $('#base_url').val();
        url = base+'/'+str;

        $('#copy_full_url').text(url);
    }
    else
    {
        alert('max length 100 Character'); 
    }
});*/



/*

  app.urlMake=function(str)
  {
      var str2 = str.length;
      if(str2<1001)
      {
        str1= str.toLowerCase().replace(/[^a-z0-9\s]/gi, '').replace(/\s+/g, '-');
        $('#url_slug').val(str1);
         base = $('#base_url').val();
            $('#copy_full_url').text(base+str1);
      }
      else
      {
        alert('max length 100 Character'); 
      }
  }*/