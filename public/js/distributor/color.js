// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "coloreditor",
  file: "distributor/color/create"
  },
  {
  name: "colortable",
  file: "distributor/color/table"
  },
  {
  name: "colorrow",
  file: "distributor/color/row"
  },

  {
  name: "colorxlstable",
  file: "distributor/color/xlstable"
  },
  {
  name: "colorxlsrow",
  file: "distributor/color/xlsrow"
  },

   {
  name: "colorimport",
  file: "distributor/color/import"
  },
  {
  name: "select",
  file: "common/select"
  }
];

app.colorForm={
  id:""
};

app.filter={
  search:"",
  page:1,
  limit:20,
 _token: $('meta[name="csrf-token"]').attr('content')
};


$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

  $(document).on('click touch', '#addcolorbutton', function(e) {
    e.preventDefault();
     var id = 0;
     app.editColor(id);
  });


  $(document).on('click touch', '#importcolorbutton', function(e) {
    e.preventDefault();
     app.importColor();
  });

  $(document).on('click touch', '#submitimportcolorbutton', function(e) {
    e.preventDefault();
     app.saveImportColor();
  });
 
  $(document).on('click touch', '#editcolorbutton', function(e) {
    e.preventDefault();
     var id = $(this).attr("data-id");
     app.editColor(id);
  });

  $(document).on('click touch', '#submitcolorbutton', function(e) {
    e.preventDefault();
     app.handleColor();
  });


    $(document).on("click touch","#deletecolorbutton",function() {
      app.colorForm.id=$(this).attr('data-id');
      app.deleteColorhandle();
   });
   

   $(document).on('keyup touch', '#searchcolorinput', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getColor();
  });
    

  $(document).on('change','.colorcheckboxoption',function(e){
    e.preventDefault();
    app.colorForm.id=$(this).attr('data-id');
    app.colorStatusChange();
  });

   $(document).on('click','.pagination a', function (event) {
        event.preventDefault();
        app.filter.page=$(this).attr('href').match(/page=([0-9]+)/)[1];
        app.getColor();
    });

  $(document).on('change','#limitfilter',function(e){
      e.preventDefault();
      var limit=$(this).val();
      if(limit!="")
      {
        app.filter.limit=limit; 
      }
      else
      {
        app.filter.limit=20; 
      }
      app.getColor();
      
  });

   $(document).on("change",'input[type="file"]',function(e){
        var fileName = e.target.files[0].name;
        $("#importColorForm").find('.custom-file-label').html(fileName);
    });

    $(document).on('click', '#xls_download', function(e) {
      e.preventDefault();
      app.filter.download_status ="xls_download";   
    
      app.getColor();
    });
 
} //setListeners

app.launchCategory = function() {
  app.getColor();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchCategory();
} //- startApp

app.handleColor =function(){

 app.makeColor();
     var errors=app.validateColor();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveColor();

}

app.makeColor=function(){
  var editId=$('#id').val().trim();
   app.colorForm={
    name:$('#name').val().trim(),
    code:$('#code').val().trim(),
    status:$('#status').prop('checked'),
    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
}
app.validateColor=function(){
  var error=[];
  if(app.colorForm.id=="")
  {
    if(app.colorForm.code==""){
      error.push("Please select color");
    }  
    /*else if(app.colorForm.name==""){
      error.push("Please enter name");
    }*/
    
   /* else if(app.colorForm.status==""){
      error.push("Please select status");
    }*/
    
  }
  return error;
}

app.saveColor=function(){
  console.log(app.colorForm);
$('#submitcolorbutton').prop("disabled",true);
$('#submitcolorbutton').html("Saving....");

var button="Save";
// var method="POST";
if(app.colorForm.id<1)
{
  var url=app.location+"distributor/color";
}
else
{
  // method="PUT";
  button="Save Changes";
  var url=app.location+"distributor/color/update";
}
      
    var formData = new FormData($('#colorForm')[0]);
 formData.append("_token", app.colorForm._token);
        formData.append("id", app.colorForm.id);
        formData.append("name", app.colorForm.name);
        formData.append("code",app.colorForm.code);
        formData.append("status",app.colorForm.status);


  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
        
        $('#submitcolorbutton').prop("disabled",false);
        $('#submitcolorbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           $('#modal').modal('hide');
           window.location.reload();
  
       }
      
    }); //- ajax

}
app.editColor=function(editId){
    var edit = {};
    var heading="Add Color";
    var button="Save";

    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
         edit = _.findWhere(app.resp.colors.data, {
            id:parseInt(editId)
        });

         button="Save Changes";
        heading="Edit Color";

    } else {
        edit = {
            id: "",
            name: "",
            status:"",
            code:""
        };
    }

    if(edit.id=="")
    {
      edit.status="active";
    }
    edit.heading=heading;
   app.log("Got click: ", edit);
  

  $('#modalContainer').html(app.templates.modal({
    'title': edit.heading,
    'content': app.templates.coloreditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary mr-3' data-dismiss='modal'>Close</button>  <button type='button' id='submitcolorbutton' class='btn btn-primary'>"+button+"</button>"
  }));

  if(edit.code!="")
  {
    $("#color_view_default_toggle_color").css('background-color', edit.code);  
  }
  else{
    
  }

  $("select").chosen();
  $('#modal').modal('show');
  
}


app.importColor=function(){

    $('#modalContainer').html(app.templates.modal({
    'title':'Import/Export Color',
    'content': app.templates.colorimport(),
    'footer':"<button type='button'  class='btn btn-secondary mr-3' data-dismiss='modal'>Close</button>  <button type='button' id='submitimportcolorbutton' class='btn btn-primary'>Upload</button>"
  }));
  $('#modal').modal('show');

}



app.saveImportColor=function(){

$('#submitimportcolorbutton').prop("disabled",true);
$('#submitimportcolorbutton').html("Uploading....");

 var method="POST";
var url=app.location+"distributor/color/importExcel";

 var formData = new FormData($('#importColorForm')[0]);
       
        formData.append("_token",$('meta[name="csrf-token"]').attr('content'));
       
  $.ajax({
      method: method,
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
        
        $('#submitimportcolorbutton').prop("disabled",false);
        $('#submitimportcolorbutton').html('Upload');
    })
    .done(function(resp) {
      console.log(resp);
      if (resp.resp == 'ok') {
           $('#modal').modal('hide');

           location.reload();           
       }
      
    }); //- ajax

}


app.deleteCategorybuttonCheck=function(){
 
  var flag=0;
  $('.checkboxoption').each(function() {
       if($(this).is(":checked"))
       {
           flag++;
       }
  });
  
  if(flag!=0)
  {
     if(flag>1)
      {
         $('#deletecategorybutton').html("Delete "+flag+" items");
      }
      else
      {
         $('#deletecategorybutton').html("Delete "+flag+" item");
      }
      $('#multiOperate').show();
  }
  else
  {
     $('#multiOperate').hide();
  }
}

app.deleteColorhandle=function(){
   
   app.deleteColor();
}

app.deleteColor=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete the record',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteColor();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.colorForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}


app.dodeleteColor=function(){
   
     app.colorForm = {
            id: app.colorForm.id,
            _token:$('meta[name="csrf-token"]').attr('content')
        };

  $.ajax({
      method: "DELETE",
      url: app.location+"distributor/color/1",
      dataType: "json",
      data:app.colorForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           /// app.getCategory();
           window.location.reload();
       }
        $('#multiOperate').hide();
      
    }); //- ajax

}//-ef deleteUser

app.getColor=function(pages = 1) {

        var token = $('meta[name="csrf-token"]').attr('content');
 
        $.ajax({
            type: "POST",
            url: app.location+'distributor/color/getList',
            data : app.filter,
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
                if(app.filter.download_status=="xls_download")
                {
                  app.showColorXls();  
                }
                else{
                  app.showColorList();  
                }
                
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
}



app.showColorList=function(){

    var rows = [];
    var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }
   _.each(app.resp.colors.data, function(content) {
  
        content.i=i++;
        rows.push(app.templates.colorrow(content));
    }); //-

    $('#result').html(app.templates.colortable({
         rows: rows.join(""),
         filter:app.filter
    }));

    $('#pagination').html(app.resp.pagination);
}


app.showColorXls=function(){

    var rows = [];
    var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }
   _.each(app.resp.colors.data, function(content) {
  
        content.i=i++;
        rows.push(app.templates.colorxlsrow(content));
    }); //-

    var test =app.templates.colorxlstable({
         rows: rows.join(""),
         filter:app.filter
    });
  console.log(test);
    app.exceller(test,"Color");
    app.filter.download_status="";
}


app.colorStatusChange=function(){
    app.colorForm = {
        id: app.colorForm.id,
        // status:app.categoryForm.status,
        _token:$('meta[name="csrf-token"]').attr('content')
    };

  $.ajax({
      method: "POST",
      url: app.location+"distributor/color/colorStatusChange",
      dataType: "json",
      data:app.colorForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
            // app.getDistributor();
            // app.alertMessage("Status change successfully","success");
       }
    }); //- ajax
}


app.loadTemplates = function() {

  app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    

} //- loadTemplates


app.exceller=function(table,filename) {
  console.log(table);
    var uri = 'data:application/vnd.ms-excel;base64,',
      template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
      base64 = function(s) {
        return window.btoa(unescape(encodeURIComponent(s)))
      },
      format = function(s, c) {
        return s.replace(/{(\w+)}/g, function(m, p) {
          return c[p];
        })
      }
    var toExcel =  table;//document.getElementById(id).innerHTML;
    var ctx = {
      worksheet: name || '',
      table: toExcel
    };
    var link = document.createElement("a");
    link.download = filename+".xls";
    link.href = uri + base64(format(template, ctx))
    link.click();
  }