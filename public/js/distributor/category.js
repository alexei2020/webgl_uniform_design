// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "categoryeditor",
  file: "distributor/category/create"
  },
  {
  name: "categorytable",
  file: "distributor/category/table"
  },
  {
  name: "categoryrow",
  file: "distributor/category/row"
  },
  {
  name: "select",
  file: "common/select"
  }
];

app.categoryForm={
  id:""
};
app.categorytoggle={id:""};

app.filter={
  search:"",
  category:"",
  subcategory:"",
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};





$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

  $(document).on('click touch', '#addcategorybutton', function(e) {
    e.preventDefault();
     var id = 0;
     app.editCategory(id);
  });
  $(document).on('click touch', '#addsubcategorybutton', function(e) {
    e.preventDefault();
     var id = 0;
     app.editCategory(id);
  });

  $(document).on('click touch', '#editcategorybutton', function(e) {
    e.preventDefault();
     var id = $(this).attr("data-id");
     app.editCategory(id);
  });

  $(document).on('click touch', '#submitcategorybutton', function(e) {
    e.preventDefault();
     app.handleCategory();
  });

  $(document).on('keyup', '#name', function(e) {
      $('#url_slug').val(app.slugUrl($(this).val()));
   });


  $(document).on("click touch","#cancelcategorybutton",function() {
      app.cancelCategorybutton();
  });
    $(document).on("click touch","#deletecategorybutton",function() {
      app.categoryForm.id=$(this).attr('data-id');
      app.deleteCategoryhandle();
   });
   

   $(document).on('keyup touch', '#searchcategoryinput', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getCategory();
  });
    $(document).on('change', '#searchcategory', function(e) {
    e.preventDefault();
     app.filter.category=$(this).val();
     app.filter.subcategory="";
     app.getSubCategory();
     app.getCategory();
  });
  $(document).on('change', '#searchsubcategory', function(e) {
    e.preventDefault();
     app.filter.subcategory=$(this).val();
     app.getCategory();
  });


  $(document).on('change','#limitfilter',function(e){
      e.preventDefault();
      var limit=$(this).val();
      if(limit!="")
      {
        app.filter.limit=limit; 
      }
      else
      {
        app.filter.limit=20; 
      }
      app.getCategory();
      
  });


  $(document).on('change','.checkboxoption',function(e){
    e.preventDefault();
    app.categoryForm.id=$(this).attr('data-id');
    app.statusChange();
  });



   $(document).on('click','.pagination a', function (event) {
        event.preventDefault();
        app.filter.page=$(this).attr('href').match(/page=([0-9]+)/)[1];
        app.getCategory();
    });
   $(document).on('click','.categorylisttogglelink',function(){
        event.preventDefault();
        app.categorytoggle.id=$(this).attr('data-id');
       app.categorylisttoggle();
   });


   $(document).on('change','.uploadCategoryImage',function(e){
    e.preventDefault();
    var token ='';
        var f = $(this).prop("files")[0];
        var form_data = new FormData();

         var token=$('meta[name="csrf-token"]').attr('content');
       
        form_data.append("file", f);
        form_data.append("_token", token);
      $.ajax({
        url:app.location+"distributor/category/saveuploaddoc",
        type:"POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        
        xhr: function () {
          // $('.progress').show();
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $('.myprogress_uploadCategoryImage').text(percentComplete + '%');
                        $('.myprogress_uploadCategoryImage').css('width', percentComplete + '%');
                    }
                }, false);
                return xhr;
            },

        success:function(resp)
        {
            if(resp.success==true)
            {
                $('#image').append(resp.result.option);
                $("#image").val(resp.result.id);  
                $('select').trigger("chosen:updated");
            }            
          
        },
       error:function(resp){
        $('.uploadCategoryImage').val("");
        var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
       }
       });
      
  });

  
} //setListeners

app.launchCategory = function() {
  app.getCategory();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchCategory();
} //- startApp

app.handleCategory =function(){

 app.makeCategory();
     var errors=app.validateCategory();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveCategory();

}

app.makeCategory=function(){
  var editId=$('#id').val().trim();
   app.categoryForm={
    name:$('#name').val().trim(),
    categoryId:$('#categoryId').val().trim(),
    url_slug: $("#url_slug").val(),
    image:$("#image").val(),
    status:$('#status').prop('checked'),
    position:$("#position").val(),
    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
 

}
app.validateCategory=function(){
  var error=[];
  if(app.categoryForm.id=="")
  {
    if(app.categoryForm.name==""){
      error.push("Please enter name");
    }
    
  }
  return error;
}

app.saveCategory=function(){
  console.log(app.categoryForm);
$('#submitcategorybutton').prop("disabled",true);
$('#submitcategorybutton').html("Saving....");

var button="Save";
// var method="POST";
if(app.categoryForm.id<1)
{
  var url=app.location+"distributor/category";
}
else
{
  // method="PUT";
  button="Save Changes";
  var url=app.location+"distributor/category/update";
}
      
    var formData = new FormData($('#categoryform')[0]);
 formData.append("_token", app.categoryForm._token);
        formData.append("id", app.categoryForm.id);
        formData.append("name", app.categoryForm.name);
        formData.append("categoryId",app.categoryForm.categoryId);
        formData.append("url_slug",app.categoryForm.url_slug);
        formData.append("status",app.categoryForm.status);
        formData.append("image",app.categoryForm.image);
        formData.append("position",app.categoryForm.position);



  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
        
        $('#submitcategorybutton').prop("disabled",false);
        $('#submitcategorybutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           $('#modal').modal('hide');
           window.location.reload();
          //  app.getCategory();
       }
      
    }); //- ajax

}
app.editCategory=function(editId){
    var edit = {};
    var heading="Add New Category";
    var button="Save";
 /*   var base = "";
 var p_name = "";*/
 
 // $('#copy_full_url').text(base+p_name);
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
         edit = _.findWhere(app.resp.category.data, {
            id:parseInt(editId)
        });

         button="Save Changes";
        heading="Edit Category";

    } else {
        edit = {
            id: "",
            name: "",
            parentId:"",
            categoryId: "",
            url_slug:"",
            status:"",
            image:"",
            imageId:"",
            position:""
        };
    }

    if(edit.id=="")
    {
      edit.status="active";
    }
    edit.heading=heading;
   app.log("Got click: ", edit);
  

  $('#modalContainer').html(app.templates.modal({
    'title': edit.heading,
    'content': app.templates.categoryeditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary mr-3' data-dismiss='modal'>Close</button>  <button type='button' id='submitcategorybutton' class='btn btn-primary'>"+button+"</button>"
  }));

  if(edit.id!="")
  {
    // app.urlMake(edit.name);     
  }
  if(edit.childcategoryid<1 || edit.parentId>0 || edit.id=="")
  {
     $('#categoryParentnull').hide();
     $('#categoryParentnotnull').show();
  }
  else
  {
    $('#categoryParentnull').show();
    $('#categoryParentnotnull').hide();  
  }

  $('#categoryId').val(edit.parentId);
  $('#image').val(edit.imageId);

  $("select").chosen();
  $('#modal').modal('show');
}


app.deleteCategorybuttonCheck=function(){
 
  var flag=0;
  $('.checkboxoption').each(function() {
       if($(this).is(":checked"))
       {
           flag++;
       }
  });
  
  if(flag!=0)
  {
     if(flag>1)
      {
         $('#deletecategorybutton').html("Delete "+flag+" items");
      }
      else
      {
         $('#deletecategorybutton').html("Delete "+flag+" item");
      }
      $('#multiOperate').show();
  }
  else
  {
     $('#multiOperate').hide();
  }
}

app.cancelCategorybutton=function(){
  $('#multiOperate').hide();
  $('.checkboxoption').attr("checked",false);
}


app.deleteCategoryhandle=function(){
   // app.makeDelete();
   app.deleteCategory();
}
app.makeDelete=function(){
  var rows = [];
  $('.checkboxoption').each(function() {
        if($(this).is(':checked'))
        {
           rows.push($(this).val());
        }
   }); 
   rows=rows.join(",");
   app.categoryForm.id=rows;
}

app.deleteCategory=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete this category.',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteCategory();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.categoryForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}


app.dodeleteCategory=function(){
   
     app.categoryForm = {
            id: app.categoryForm.id,
            _token:$('meta[name="csrf-token"]').attr('content')
        };

  $.ajax({
      method: "DELETE",
      url: app.location+"distributor/category/1",
      dataType: "json",
      data:app.categoryForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           /// app.getCategory();
           window.location.reload();
       }
        $('#multiOperate').hide();
      
    }); //- ajax

}//-ef deleteUser

app.getCategory=function(pages = 1) {

        var token = $('meta[name="csrf-token"]').attr('content');
 
        $.ajax({
            type: "POST",
            url: app.location+'distributor/category/getList',
            data : app.filter,
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
                app.showCategoryList();
                
                
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
}

app.getSubCategory=function() {

        var token = $('meta[name="csrf-token"]').attr('content');
       
     
       app.subcatefilter={
        _token:token,
        id:$('#searchcategory').val()
       };
       
        $.ajax({
            type: "POST",
            url: app.location+'distributor/category/getSubCategory',
            data : app.subcatefilter,
            dataType:"json",
            success: function (resp) {
                app.subcategory=resp.subcategory;
             
                app.searchchangeCategory();
                
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
}


app.showCategoryList=function(){

  var rows = [];  var parentCate=""; var childCate="";
    var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }
      parentCate=_.where(app.resp.category.data,{parentId:null});
      childCate=_.filter(app.resp.category.data, function(v) { return v.parentId != null });
     
          _.each(parentCate, function(content) {
               content.i=i++;
              rows.push(app.templates.categoryrow(content));
          }); //-
          $('#result').html(app.templates.categorytable({
               rows: rows.join(""),
               filter:app.filter
          }));

          console.log(childCate)

          _.each(childCate, function(content) {
               content.i=i++;
               if($("#row_"+content.parentId).length)
               {
                  $("#row_"+content.parentId).after(app.templates.categoryrow(content));
               }
               else
               {
                  $("#searchResultDiv").prepend(app.templates.categoryrow(content));
               }
              
           }); //- 
 
 
          $('#pagination').html(app.resp.pagination);
          
         /* if($('#searchResultDiv').find('tr').length==0)
          {
             $('#searchResultDiv').html('<p>No categories found</p>');
              // $('#searchResultDiv').html('<div class="no_audit p-5 text-center"><img src="'+app.location+'public/images/norecord.png"><p>No record found');
          }*/

}


app.searchchangeCategory=function(){
  var busid=$('#searchbusinessunit').val();
   

  var category = ['<option value="">All Sub-Categories</option>'];

   _.each(app.subcategory, function(content) {
              category.push(app.templates.select(content));
          }); //-

   $('#searchsubcategory').html(category);
   $("select").trigger("chosen:updated");

}

app.categorylisttoggle=function(){
   if($('#row_'+app.categorytoggle.id).find('.fa-minus').length)
   {
      $('#row_'+app.categorytoggle.id).find('.categorylisttogglelink').html('<i class="fa fa-plus"></i>');
      $('.parent_'+app.categorytoggle.id).hide();
   }
   else
   {
      $('.parent_'+app.categorytoggle.id).show();
      $('#row_'+app.categorytoggle.id).find('.categorylisttogglelink').html('<i class="fa fa-minus"></i>');

   }
   //categorylisttogglelink
}


app.statusChange=function(){
    app.categoryForm = {
        id: app.categoryForm.id,
        // status:app.categoryForm.status,
        _token:$('meta[name="csrf-token"]').attr('content')
    };

  $.ajax({
      method: "POST",
      url: app.location+"distributor/category/statusChange",
      dataType: "json",
      data:app.categoryForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
       }
    }); //- ajax
}

app.loadTemplates = function() {

  app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    

} //- loadTemplates

