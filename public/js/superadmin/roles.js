// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "roleseditor",
  file: "superadmin/companies/edit"
  }
];

app.rolesForm={
  id:""
};

app.filter={
  search:"",
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};





$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

 $(document).on('click touch', '#addcompaniesbutton', function(e) {
    e.preventDefault();
     var id = 0;
     app.editCompanies(id);
  });
  $(document).on('click','.collapse1',function(){
     var href=$(this).attr('href');
     $(href).slideToggle('500');
  });

  
} //setListeners

app.launchRoles = function() {
 // app.getCompanies();
} //- launchRoles


app.startApp = function() {
  app.log("Starting");
 app.launchRoles();
} //- startApp



app.loadTemplates = function() {

	app.templates.modal = _.template("<div class='modal far-right  fade' tabindex='-1' data-backdrop='static' data-keyboard='false' role='dialog' id='modal'><div class='modal-dialog' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title'><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    
  app.templates.norecord = _.template('<div class="col-sm-12 text-center"><img src="'+app.location+'public/images/norecord.png" width="300"><h4 class="mt-4">No partner found</h4></div>');

} //- loadTemplates
