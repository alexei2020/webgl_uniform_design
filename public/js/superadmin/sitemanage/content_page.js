// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "distributoreditor",
  file: "superadmin/distributor/create"
  },
  {
  name: "distributortable",
  file: "superadmin/distributor/table"
  },
  {
  name: "distributorrow",
  file: "superadmin/distributor/row"
  },
 /* {
  name: "select",
  file: "superadmin/companies/select"
  }*/
];

app.contentPageForm={
  id:""
};
app.respSelect={};
app.industrySelected=[];

app.filter={
  search:"",
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};

app.bu={};




$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

 

  $(document).on('click touch', '#submitcontentpagebutton', function(e) {
    e.preventDefault();
     
     app.handleContentPage();
  });

 

  
} //setListeners

app.launchUser = function() {
  app.getDistributor();
  // app.getBU();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchUser();
} //- startApp

app.handleContentPage =function(){

 app.makeContentPage();
     var errors=app.validateContentPage();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveContentPage();

}

app.makeContentPage=function(){
  var editId=$('#id').val().trim();
   app.contentPageForm={
    description:CKEDITOR.instances['content_page_editor'].getData(),
    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
  
}




app.validateContentPage=function(){
  var error=[];

  return error;
}

app.saveContentPage=function(){

$('#submitcontentpagebutton').prop("disabled",true);
$('#submitcontentpagebutton').html("Saving....");
 var button="Save";
if(app.contentPageForm.id<1)
{
  var url=app.location+"superadmin/site-manage/content-page/save";
}
else
{
  button="Save Changes";
  var url=app.location+"superadmin/site-manage/content-page/update";
}

 var formData = new FormData($('#companyform')[0]);
 formData.append("_token", app.contentPageForm._token);
        formData.append("id", app.contentPageForm.id);
        formData.append("description", app.contentPageForm.description);

  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
      

         app.validationMessage(response.msg,"warning");
        $('#submitcontentpagebutton').prop("disabled",false);
        $('#submitcontentpagebutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {


           // $('#modal').modal('hide');
           location.reload();
            // app.getDistributor();
       }
      
    }); //- ajax

}



app.getDistributor=function(pages = 1) {
     
        // $('.loading').show();
        var token = $('meta[name="csrf-token"]').attr('content');
       
       // formData.append("sortby",reverse_order);
       // formData.append("sorttype",column_name);
       
        $.ajax({
            type: "POST",
            url: app.location+'superadmin/distributor/getList',
            data : app.filter,
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
              //  $("#searchResultDiv").html(data);
              // $('.loading').hide();

                app.showDistributorList();
                
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
    }




app.showDistributorList=function(){

   var rows = [];
      var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }
   _.each(app.resp.companies.data, function(content) {
  
              content.i=i++;
              rows.push(app.templates.distributorrow(content));
          }); //-

          $('#result').html(app.templates.distributortable({
               rows: rows.join(""),
               filter:app.filter
          }));
          $('#pagination').html(app.resp.pagination);

}


app.loadTemplates = function() {

	app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    
  app.templates.norecord = _.template('<div class="col-sm-12 text-center"><img src="'+app.location+'public/images/norecord.png" width="300"><h4 class="mt-4">No partner found</h4></div>');

} //- loadTemplates
