// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "distributoreditor",
  file: "superadmin/distributor/create"
  },
  {
  name: "distributortable",
  file: "superadmin/distributor/table"
  },
  {
  name: "distributorrow",
  file: "superadmin/distributor/row"
  },
 /* {
  name: "select",
  file: "superadmin/companies/select"
  }*/
];

app.privacyPolicyForm={
  id:""
};
app.respSelect={};
app.industrySelected=[];

app.filter={
  search:"",
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};

app.bu={};




$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

 

  $(document).on('click touch', '#submitprivacypolicybutton', function(e) {
    e.preventDefault();
     
     app.handleProvacyPolicy();
  });

 

  
} //setListeners

app.launchUser = function() {
  // app.getDistributor();
  // app.getBU();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchUser();
} //- startApp

app.handleProvacyPolicy =function(){

 app.makeProvacyPolicy();
     var errors=app.validateProvacyPolicy();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveProvacyPolicy();

}

app.makeProvacyPolicy=function(){
  var editId=$('#id').val().trim();
   app.privacyPolicyForm={
    description:CKEDITOR.instances['privacy_policy_editor'].getData(),
    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
  
}




app.validateProvacyPolicy=function(){
  var error=[];

  return error;
}

app.saveProvacyPolicy=function(){

$('#submitprivacypolicybutton').prop("disabled",true);
$('#submitprivacypolicybutton').html("Saving....");
 var button="Save";
if(app.privacyPolicyForm.id<1)
{
  var url=app.location+"superadmin/site-manage/privacy-policy/save";
}
else
{
  button="Save Changes";
  var url=app.location+"superadmin/site-manage/privacy-policy/update";
}

 var formData = new FormData($('#companyform')[0]);
 formData.append("_token", app.privacyPolicyForm._token);
        formData.append("id", app.privacyPolicyForm.id);
        formData.append("description", app.privacyPolicyForm.description);

  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);

         app.validationMessage(response.msg,"warning");
        $('#submitprivacypolicybutton').prop("disabled",false);
        $('#submitprivacypolicybutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {

           location.reload();
       }
      
    }); //- ajax

}




app.loadTemplates = function() {

	app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    
  app.templates.norecord = _.template('<div class="col-sm-12 text-center"><img src="'+app.location+'public/images/norecord.png" width="300"><h4 class="mt-4">No partner found</h4></div>');

} //- loadTemplates
