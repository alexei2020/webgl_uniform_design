// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "producteditor",
  file: "superadmin/product/create"
  },
  
  {
  name: "svgselectoreditor",
  file: "superadmin/product/svgselector"
  },


  {
  name: "producttable",
  file: "superadmin/product/table"
  },
  {
  name: "productrow",
  file: "superadmin/product/row"
  },

  {
  name: "stepeditor",
  file: "superadmin/product/stepcreate"
  },
  {
  name: "steptable",
  file: "superadmin/product/steptable"
  },
  {
  name: "steprow",
  file: "superadmin/product/steprow"
  },

  {
  name: "fieldeditor",
  file: "superadmin/product/fieldcreate"
  },
  {
  name: "fieldtable",
  file: "superadmin/product/fieldtable"
  },
  {
  name: "fieldrow",
  file: "superadmin/product/fieldrow"
  },

  {
  name: "designeditor",
  file: "superadmin/product/designcreate"
  },
  {
  name: "designtable",
  file: "superadmin/product/designtable"
  },
  {
  name: "designrow",
  file: "superadmin/product/designrow"
  },

  {
  name: "select",
  file: "common/select"
  }
];


app.respEdit={
  product_step:[],
  product_field:[],
  product_design:[],
  design_filed:[],
  list_image:[],
  model_3D:[],
  category:[],
  document:[],
  svg_image:[]

}

app.resp={
  product_step:[],
  product_field:[],
  product_design:[],
  design_filed:[],
  list_image:[],
  model_3D:[],
  category:[],
  document:[],

}
app.productForm={
  id:""
};
app.stepForm={
  id:""
};

app.fieldForm={
  id:""
};

app.designForm={
  id:""
};

app.designFiledForm={
  id:""
};

app.designFiled={
  id:""
};

app.stepSelected=[];
app.groupSelected=[];

app.tableDFiled="";

app.filter={
  search:"",
  distributor:"",
  view:"product_tab",
  sortName:"id",
  sortBy:"asc",
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};


$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

  $(document).on('click touch', '#addproductbutton', function(e) {
      e.preventDefault();
      var id = 0;
      app.filter.productId=id;
      $("#result").hide();
      $('.search_area').hide();
      $('#edit_product').show();
      // app.rootTabChange();
      app.getEditProduct(id);

  });

  $(document).on('click touch', '#editproductbutton', function(e) {
      e.preventDefault();
      var id = $(this).attr("data-id");
      app.filter.productId=id;
      $("#result").hide();
      $('.search_area').hide();
      $('#edit_product').show();
      // app.rootTabChange();
      app.getEditProduct(id);

  });
  
  $(document).on('click touch', '#submitproductbutton', function(e) {
      e.preventDefault();
      app.handleProduct();

  });

  $(document).on('click touch', '#deleteproductbutton', function(e) {
      e.preventDefault();
      app.productForm.id=$(this).attr('data-id');
      app.deleteProduct();

  });  

  $(document).on('change','.productcheckboxoption',function(e){
    e.preventDefault();
    app.productForm.id=$(this).attr('data-id');
    app.productStatusChange();
  });

  $(document).on('change','.stepcheckboxoption',function(e){
    e.preventDefault();
    app.stepForm.id=$(this).attr('data-id');
    app.stepStatusChange();
  });

  $(document).on('change','.designcheckboxoption',function(e){
    e.preventDefault();
    app.designForm.id=$(this).attr('data-id');
    app.designStatusChange();
  });

   $(document).on('keyup touch', '#searchproductinput', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getProduct();
  });


   $(document).on('change', '#distributorselectbox', function(e) {
    e.preventDefault();
     app.filter.distributor=$(this).val();
     app.getProduct();
  });


  $(document).on('click touch', '#selected_svg_dimension', function(e) {
    e.preventDefault();
    var svg_name = $('#select_svg').find(':selected').attr('data-name');
    if(svg_name!="")
    { 
      
        // app.productForm.list_ImageId = $('#list_ImageId').val();
        // app.getSVGDimension($('#list_ImageId').val());
        $("#select_svg_object").show();
        $("#select_svg_object").attr('data',app.location+"public/images/"+svg_name);
        setTimeout(function(){ app.setSvgDimension(); }, 50);
        $("#select_svg_object").hide();
    }
    else
    {
      alert("Please select an SVG from the dropdown list on the left.");
    }
  });

  $(document).on('keyup', '#default_toggle_color', function(e) {
    var value = $(this).val();
      $("#color_view_default_toggle_color").css('background-color', value);
   });
   

   $(document).on('keyup', '#name', function(e) {
      $('#url_slug').val(app.slugUrl($(this).val()));
   });

   $(document).on('keyup', '#design_name', function(e) {
      $('#design_slug').val(app.slugUrl($(this).val()));
   });

 $(document).on('change','#select_svg',function(e){
    e.preventDefault();
    var image_name = $(this).val();
    if(image_name!="")
    {
      app.editSvgSelector(image_name);  
    }
    
  });

  $(document).on('click',"#resetsvgselector", function(){
      $("#svg_list").find(':checkbox').prop('checked',false);
      $("#alphasvg").contents().find("svg").find('g').hide();
  });


   // Step

   $(document).on('click touch', '#addstepbutton', function(e) {
      e.preventDefault();
      var id = 0;
      app.stepForm.productId = $(this).attr('data-productId');
      app.editStep(id);
  });

    $(document).on('click touch', '#editstepbutton', function(e) {
      e.preventDefault();
      var id = $(this).attr('data-id');
      app.editStep(id);
  });

   $(document).on('click touch', '#submitstepbutton', function(e) {
      e.preventDefault();
      app.handleStep();
  });

   $(document).on('keyup touch', '#searchstepinput', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getProductStep(); 
  });
   
  $(document).on('click touch', '#deletestepbutton', function(e) {
      e.preventDefault();
      app.stepForm.id=$(this).attr('data-id');
      app.deleteStep();
  });   

// Field
  $(document).on('click touch', '#addfieldbutton', function(e) {
      e.preventDefault();
      var id = 0;
      app.fieldForm.productId = $(this).attr('data-productId');
      app.editField(id);
  });

  $(document).on('click touch', '#editfieldbutton', function(e) {
      e.preventDefault();
      var id = $(this).attr('data-id');
      app.editField(id);
  });


  $(document).on('click touch', '#submitfieldbutton', function(e) {
      e.preventDefault();
      app.handleField();
  });

  $(document).on('keyup touch', '#searchfieldinput', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getProductField();  
  });
   
  $(document).on('click touch', '#deletefieldbutton', function(e) {
      e.preventDefault();
      app.fieldForm.id=$(this).attr('data-id');
      app.deleteField();
  });   


  // Design
  $(document).on('click touch', '#adddesignbutton', function(e) {
      e.preventDefault();
      var id = 0;
      app.designForm.productId = $(this).attr('data-productId');
      app.editDesign(id);
  });

  $(document).on('click touch', '#editdesignbutton', function(e) {
      e.preventDefault();
      var id = $(this).attr('data-id');
      app.editDesign(id);

  });


  $(document).on('click touch', '#submitdesignbutton', function(e) {
      e.preventDefault();
      app.handleDesign();
  });

  $(document).on('keyup touch', '#searchdesigninput', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getProductDesign();
  });
   
  $(document).on('click touch', '#deletedesignbutton', function(e) {
      e.preventDefault();
      app.designForm.id=$(this).attr('data-id');
      app.deleteDesign();
  });   

  // Design Filed

  $(document).on("click touch",".deletedesignfiledbutton",function(e) {
       e.preventDefault();
       app.designForm.id=$("#designid").val();
      app.designForm.designFiledId=$(this).attr("data-id");
      if(app.designForm.designFiledId==undefined)
      {
        $(this).closest('.designFiled_add_more').remove();
      }
      else{
        app.deleteDesignFiled();
      }
      
  });
  
  $(document).on('click touch', '#adddesignfiledbutton', function(e) {
      e.preventDefault();
      app.addDesignFiled();

  });


  $(document).on('click touch', '.roottablink', function(e) {
    e.preventDefault();
    app.filter.view=$(this).attr("data-value");
     app.rootTabChange();
  });


  $(document).on('click', '.sorting', function(){
      app.filter.sortName=$(this).data('column_name')
      app.filter.sortBy=$(this).data('sorting_type');
      app.getProduct();  
  });

   $(document).on('click','.pagination a', function (event) {
        event.preventDefault();
        app.filter.page=$(this).attr('href').match(/page=([0-9]+)/)[1];
        app.getProduct();
    });
 
} //setListeners

app.launchProduct = function() {
  app.getProduct();
} //- launchUser


app.startApp = function() {
  console.log('ready');
  app.log("Starting");
 app.launchProduct();
} //- startApp



app.handleProduct =function(){

 app.makeProduct();
     var errors=app.validateProduct();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveProduct();

}

app.makeProduct=function(){
  var editId=$('#id').val().trim();
   app.productForm={

    name: $('#name').val(),
    categoryId: $('#categoryId').val(),
    url_slug:$('#url_slug').val(),
    description: CKEDITOR.instances['extra_description'].getData(), 
    rotation_disable:$('#rotation_disable').prop('checked'),
    list_ImageId:$('#list_ImageId').val(),
    model_3DId:$('#model_3DId').val(),
    cameras:$('#cameras').val(),
    camera_override:$('#camera_override').val(),
    x_axis:$('#x_axis').val(),
    y_axis:$('#y_axis').val(),
    z_axis:$('#z_axis').val(),
    scale:$('#scale').val(),
    override_model_position_scale:$('#override_model_position_scale').prop('checked'),
    light_brightness:$('#light_brightness').val(),
    svg_dimension_ht:$('#svg_dimension_ht').val(),
    svg_dimension_vt:$('#svg_dimension_vt').val(),
    hight_on_basker_page:$('#hight_on_basker_page').val(),
    default_toggle_color:$('#default_toggle_color').val(),
    position:$('#position').val(),
    status:$('#status').prop('checked'),

    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
 

}
app.validateProduct=function(){
  var error=[];
  if(app.productForm.id=="")
  {
    if(app.productForm.name==""){
      error.push("Please enter name");
    }
    else if(app.productForm.categoryId==""){
      error.push("Please select category");
    }
    else if(app.productForm.list_ImageId==""){
      error.push("Please select list image");
    }
    else if(app.productForm.model_3DId==""){
      error.push("Please select 3D model");
    }
    else if(app.productForm.svg_dimension_ht==""){
      error.push("SVG width is required");
    }
    else if(app.productForm.svg_dimension_vt==""){
      error.push("SVG height is required");
    }
    
  }
  return error;
}

app.saveProduct=function(){

$('#submitproductbutton').prop("disabled",true);
$('#submitproductbutton').html("Saving....");

var button="Save";
// var method="POST";
if(app.productForm.id<1)
{
  var url=app.location+"superadmin/product";
}
else
{
  // method="PUT";
  button="Save Changes";
  var url=app.location+"superadmin/product/update";
}
      
    var formData = new FormData($('#productform')[0]);
 formData.append("_token", app.productForm._token);
        formData.append("id", app.productForm.id);
        formData.append("name", app.productForm.name);
        formData.append("categoryId",app.productForm.categoryId);
        formData.append("url_slug",app.productForm.url_slug);
        formData.append("description",app.productForm.description);
        formData.append("rotation_disable",app.productForm.rotation_disable);
        formData.append("list_ImageId",app.productForm.list_ImageId);
        formData.append("model_3DId",app.productForm.model_3DId);
        formData.append("cameras",app.productForm.cameras);
        formData.append("camera_override",app.productForm.camera_override);
        formData.append("x_axis",app.productForm.x_axis);
        formData.append("y_axis",app.productForm.y_axis);
        formData.append("z_axis",app.productForm.z_axis);
        formData.append("scale",app.productForm.scale);
        formData.append("override_model_position_scale",app.productForm.override_model_position_scale);
        formData.append("light_brightness",app.productForm.light_brightness);
        formData.append("svg_dimension_ht",app.productForm.svg_dimension_ht);
        formData.append("svg_dimension_vt",app.productForm.svg_dimension_vt);
        formData.append("hight_on_basker_page",app.productForm.hight_on_basker_page);
        formData.append("default_toggle_color",app.productForm.default_toggle_color);
        formData.append("position",app.productForm.position);
        formData.append("status",app.productForm.status);
      
  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
        
        $('#submitproductbutton').prop("disabled",false);
        $('#submitproductbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           // $('#modal').modal('hide');
           // window.location.reload();

           // $('#modal').modal('hide');
           $('#id').val(resp.result.id);
           $('#submitproductbutton').prop("disabled",false);
           $('#submitproductbutton').html('Save');
           app.filter.productId=$('#id').val();
           $('#x_axis').val(resp.result.x_axis);
           $('#y_axis').val(resp.result.y_axis);
           $('#z_axis').val(resp.result.z_axis);
           $('#scale').val(resp.result.scale);
           // app.rootTabChange();
           // window.location.reload();
       }
      
    }); //- ajax

}

app.editSvgSelector= function(image){

    var edit = {};
    var heading="SVG Selector Preview";
    var button="Reset";

    var image_name=""; 
    _.each(app.respEdit.list_image, function(getp){
      if(getp.id==image)
      {
        image_name=getp.name;
      }
    });

    edit = {
        id:"",
        image: image_name,
        productId: app.filter.productId,
    };  

    edit.heading=heading;
    app.log("Got click: ", edit);
  
  $('#modalContainer').html(app.templates.modal({
    'title': edit.heading,
    'content': app.templates.svgselectoreditor(edit),
    'footer':"<button type='button' id='resetsvgselector' class='btn btn-primary'>"+button+"</button>"
  }));


  $("select").chosen();
  $('#modal').modal('show');

  setTimeout(function(){ app.svgSelectorShow(); }, 300);
}



app.editProduct=function(editId){
    var edit = {};
    var heading="Add Product";
    var button="Save";
  
 
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
         edit = _.findWhere(app.resp.product.data, {
            id:parseInt(editId)
        });

         button="Save";
        heading="Edit Product";
    } else {
        edit = {
            id: "",
            name: "",
            categoryId: "",
            url_slug:"",
             description :"",
            rotation_disable:"",
            list_ImageId:"",
            model_3DId:"",
            cameras:"",
            camera_override:"",
            x_axis:"",
            y_axis:"",
            z_axis:"",
            scale:"",
            override_model_position_scale:"",
            light_brightness:"",
            svg_dimension_ht:"",
            svg_dimension_vt:"",
            hight_on_basker_page:"",
            default_toggle_color:"",
            position:"",
            status:""
        };
    }


    var category=[]; 
  _.each(app.respEdit.category, function(getp){
        category.push(app.templates.select(getp));
      });

    var list_image=[]; 
  _.each(app.respEdit.list_image, function(getp){

        list_image.push(app.templates.select(getp));
      });

    var svg_image=[]; 

  _.each(app.respEdit.list_image, function(getp){
          if(getp.type=="svg")
          {
            // svg_image.push(app.templates.select(getp));
            svg_image.push("<option value='"+getp.id+"' data-name='"+getp.name+"'>"+getp.name+"</option>"); 
          }
        // console.log(getp);
        
      });
   
       var model_3D=[]; 
  _.each(app.respEdit.model_3D, function(getp){
        model_3D.push(app.templates.select(getp));
      });


    edit.category=category.join("");
    edit.list_image=list_image.join("");
    edit.model_3D=model_3D.join("");
    edit.svg_image=svg_image.join("");

    
    edit.heading=heading;
   app.log("Got click: ", edit);
  

   var dh=app.templates.modal({
    'title': edit.heading,
    'content': app.templates.producteditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary mr-3' data-dismiss='modal'>Close</button>  <button type='button' id='submitproductbutton' class='btn btn-primary'>"+button+"</button>"
  });

  $('#edit_product').html( app.templates.producteditor(edit));

  $("#color_view_default_toggle_color").css('background-color', edit.default_toggle_color);
    
  $("#select_svg_object").hide();

  $("#categoryId").val(edit.categoryId);
  $("#list_ImageId").val(edit.list_ImageId);
  $("#model_3DId").val(edit.model_3DId);
  
  $("select").chosen();

}


app.deleteProduct=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete this product.',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteProduct();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.productForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}


app.dodeleteProduct=function(){
   
     app.productForm = {
            id: app.productForm.id,
            _token:$('meta[name="csrf-token"]').attr('content')
        };

  $.ajax({
      method: "DELETE",
      url: app.location+"superadmin/product/1",
      dataType: "json",
      data:app.productForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           window.location.reload();
       }
        $('#multiOperate').hide();
      
    }); //- ajax

}//-ef deleteUser

app.getProduct=function(pages = 1) {

        var token = $('meta[name="csrf-token"]').attr('content');
 
        $.ajax({
            type: "POST",
            url: app.location+'superadmin/product/getList',
            data : app.filter,
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
                app.showProductList();
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
}

app.showProductList=function(){
    var rows = [];
    var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }
   _.each(app.resp.product.data, function(content) {
  
        content.i=i++;
        rows.push(app.templates.productrow(content));
    }); //-

    $('#result').html(app.templates.producttable({
         rows: rows.join(""),
         filter:app.filter
    }));
    $('#pagination').html(app.resp.pagination);
    app.sorting();
}




//Product Step 
app.handleStep =function(){
 app.makeStep();
     var errors=app.validateStep();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveStep();

}

app.makeStep=function(){
  var editId=$('#stepid').val().trim();
   app.stepForm={
    name: $('#stepname').val(),
    position: $('#stepposition').val(),
    status:$('#stepstatus').prop('checked'),
    id: $('#stepid').val().trim(),
    productId: $('#productId').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
 

}
app.validateStep=function(){
  var error=[];
  if(app.stepForm.id=="")
  {
    if(app.stepForm.name==""){
      error.push("Please enter name");
    }
  
  }
  return error;
}

app.saveStep=function(){

$('#submitstepbutton').prop("disabled",true);
$('#submitstepbutton').html("Saving....");
var button="Save";
// var method="POST";
if(app.stepForm.id<1)
{
  var url=app.location+"superadmin/product/stepstore";
}
else
{
  button="Save Changes";
  var url=app.location+"superadmin/product/stepupdate";
}
      
    var formData = new FormData($('#stepform')[0]);
 formData.append("_token", app.stepForm._token);
        formData.append("id", app.stepForm.id);
        formData.append("name", app.stepForm.name);
        formData.append("position",app.stepForm.position);
        formData.append("status",app.stepForm.status);
        formData.append("productId",app.stepForm.productId);

  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
        
        $('#submitstepbutton').prop("disabled",false);
        $('#submitstepbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           $('#modal').modal('hide');
            
            app.getProductStep();
          // window.location.reload();
       }
      
    }); //- ajax

}

app.editStep = function(editId){
     var edit = {};
    var heading="Add Step";
    var button="Save";
  
 
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
         edit = _.findWhere(app.resp.product_step, {
            id:parseInt(editId)
        });

         button="Save";
        heading="Edit Step";

    } else {
        edit = {
            id: "",
            name: "",
            productId: app.filter.productId,
            position:"",
            status:""
        };
    }


    edit.heading=heading;
   app.log("Got click: ", edit);
  
  $('#modalContainer').html(app.templates.modal({
    'title': edit.heading,
    'content': app.templates.stepeditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary mr-3' data-dismiss='modal'>Close</button>  <button type='button' id='submitstepbutton' class='btn btn-primary'>"+button+"</button>"
  }));

  $("select").chosen();
  $('#modal').modal('show');
}


app.deleteStep=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete this product step.',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteStep();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.stepForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}


app.dodeleteStep=function(){

     app.stepForm = {
            id: app.stepForm.id,
            productId:app.stepForm.productId,
            _token:$('meta[name="csrf-token"]').attr('content')
        };
           console.log(app.stepForm);
  $.ajax({
      method: "post",
      url: app.location+"superadmin/product/stepdelete",
      dataType: "json",
      data:app.stepForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {

      
            app.getProductStep();

          // window.location.reload();
       }
        
    }); //- ajax

}//-ef 




//Product Field 
app.handleField =function(){
 app.makeField();
     var errors=app.validateField();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveField();
}

app.makeField=function(){
  var editId=$('#fieldid').val().trim();
   app.fieldForm={
    name: $('#field_name').val(),
    field_step: $('#field_step').val(),
    filed_type:$('#filed_type').val(),
    field_color_group:$("#field_color_group").val(),
    id: $('#fieldid').val().trim(),
    productId: $('#productId').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
 

}
app.validateField=function(){
  var error=[];
  if(app.fieldForm.id=="")
  {
    if(app.fieldForm.name==""){
      error.push("Please enter name");
    }
  
  }
  return error;
}

app.saveField=function(){

  $('#submitfieldbutton').prop("disabled",true);
  $('#submitfieldbutton').html("Saving....");

  var button="Save";
  if(app.fieldForm.id<1)
  {
    var url=app.location+"superadmin/product/fieldstore";
  }
  else
  {
    button="Save Changes";
    var url=app.location+"superadmin/product/fieldupdate";
  }
      
    var formData = new FormData($('#fieldForm')[0]);
    formData.append("_token", app.fieldForm._token);
        formData.append("id", app.fieldForm.id);
        formData.append("name", app.fieldForm.name);
        formData.append("field_step", app.fieldForm.field_step);
        formData.append("filed_type",app.fieldForm.filed_type);
        formData.append("field_color_group",app.fieldForm.field_color_group);
        formData.append("productId",app.fieldForm.productId);

  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
        
        $('#submitfieldbutton').prop("disabled",false);
        $('#submitfieldbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           $('#modal').modal('hide');
            app.getProductField();

       }
      
    }); //- ajax

}

app.editField = function(editId){
     var edit = {};
    var heading="Add Field";
    var button="Save";
  
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
         edit = _.findWhere(app.resp.product_field, {
            id:parseInt(editId)
        });

         button="Save";
        heading="Edit Field";

    } else {
        edit = {
            id: "",
            name: "",
            productId: app.filter.productId,
            field_step:"",
            filed_type:"",
            field_color_group:""
        };
    }

    var step = ['<option value="">Select Step </option>'];
    _.each(app.respEdit.product_step, function(content) {
          step.push(app.templates.select(content));
    }); //-

    var groups = ['<option value="">Select Group </option>'];  
    _.each(app.respEdit.fc_groups, function(content) {
        if(content.type=="color"){
          groups.push(app.templates.select(content));
        }
    }); //-
    edit.step = step;
    edit.groups = groups;

    edit.heading=heading;
   app.log("Got click: ", edit);
  
  $('#modalContainer').html(app.templates.modal({
    'title': edit.heading,
    'content': app.templates.fieldeditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary mr-3' data-dismiss='modal'>Close</button>  <button type='button' id='submitfieldbutton' class='btn btn-primary'>"+button+"</button>"
  }));

    if(edit.stepId!="")
    {
      $('#field_step').val(edit.stepId);
    }

    if(edit.groupId!="")
    {
      $('#field_color_group').val(edit.groupId);
    }  

     

  $("select").chosen();
  $('#modal').modal('show');
}


app.deleteField=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete this product field.',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteField();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.fieldForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}


app.dodeleteField=function(){

     app.fieldForm = {
            id: app.fieldForm.id,
            _token:$('meta[name="csrf-token"]').attr('content')
        };
           console.log(app.fieldForm);
  $.ajax({
      method: "post",
      url: app.location+"superadmin/product/fielddelete",
      dataType: "json",
      data:app.fieldForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           
           app.getProductField();

       }
      
    }); //- ajax

}//-ef deleteUser



//Product Design 
app.handleDesign =function(){
 app.makeDesign();
     var errors=app.validateDesign();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveDesign();
}

app.makeDesign=function(){
  var editId=$('#designid').val().trim();
   app.designForm={
    name: $('#design_name').val(),
    sku: $('#sku_name').val(),
    design_slug:$('#design_slug').val(),
    // override_image: $('#override_image').val(),
    svg_snippet: $('#svg_snippet').val(),
    show_svg_defs: $('#show_svg_defs').prop('checked'),
    position: $('#design_position').val(),
    status:$("#design_status").prop('checked'),
    id: $('#designid').val().trim(),
    productId: $('#productId').val().trim(),
    profile: $('#profile').val(),
    player_status:$("#player_status").prop('checked'),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
 app.getDesignFiledChecked();

}
app.validateDesign=function(){
  var error=[];
  var productStepId=""; var design_filed_name=""; var data_group="";
   $('.productStepId').each(function() {
        if($(this).val()=="")
        {
           productStepId="empty";
        }

   }); 
  $('.design_filed_name').each(function() {
       if($(this).val()=="")
        {
           design_filed_name="empty";
        }
   }); 
  $('.data_group').each(function() {
        if($(this).val()=="")
        {
           data_group="empty";
        }
   });


  if(app.designForm.id=="")
  {
    if(app.designForm.name==""){
      error.push("Please enter name");
    }
     else if(productStepId=="empty")
    {
       error.push("Please select step");
    }
     else if(design_filed_name=="empty")
    {
       error.push("Please enter filed name");
    }
     else if(data_group=="empty")
    {
       error.push("Please select group");
    }
  
  }
   else if(productStepId=="empty")
    {
       error.push("Please select step");
    }
     else if(design_filed_name=="empty")
    {
       error.push("Please enter filed name");
    }
     else if(data_group=="empty")
    {
       error.push("Please select group");
    }
  return error;
}

app.saveDesign=function(){
    console.log(app.designForm);
    $('#submitdesignbutton').prop("disabled",true);
    $('#submitdesignbutton').html("Saving....");

    var button="Save";
    // var method="POST";
    if(app.designForm.id<1)
    {
      var url=app.location+"superadmin/product/designstore";
    }
    else
    {
      // method="PUT";
      button="Save Changes";
      var url=app.location+"superadmin/product/designupdate";
    }
      
    var formData = new FormData($('#designForm')[0]);
    formData.append("_token", app.designForm._token);
        formData.append("id", app.designForm.id);
        formData.append("name", app.designForm.name);
        formData.append('design_slug',app.designForm.design_slug);
        // formData.append("override_image", app.designForm.override_image);
        formData.append("sku",app.designForm.sku);
        formData.append("svg_snippet",app.designForm.svg_snippet);
        formData.append("show_svg_defs",app.designForm.show_svg_defs);
        formData.append("position",app.designForm.position);
        formData.append("status",app.designForm.status);
        formData.append("productId",app.designForm.productId);
         formData.append("player_status",app.designForm.player_status);

        formData.append("ids",app.designForm.ids);
        formData.append("stepId",app.designForm.stepId);
         formData.append("design_filed_name",app.designForm.design_filed_name);
        formData.append("design_key",app.designForm.design_key);
        formData.append("data_group",app.designForm.data_group);

  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.validationMessage(response.msg,"warning");
        
        $('#submitdesignbutton').prop("disabled",false);
        $('#submitdesignbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           $('#modal').modal('hide');
           app.getProductDesign();

       }
      
    }); //- ajax

}


app.getDesignFiledChecked=function(){
  var row0=[];
  var row1=[];
  var row2=[];
  var row3=[];
  var row4=[];
  $('.ids').each(function() {
      row0.push($(this).val());
        
   });
  $('.productStepId').each(function() {
        if($(this).val()!="")
        {
           row1.push($(this).val());
        }
   }); 

  $('.design_filed_name').each(function() {
        if($(this).val()!="")
        {
           row2.push($(this).val());
        }
   }); 
  $('.design_key').each(function() {
        if($(this).val()!="")
        {
           row3.push($(this).val());
        }
   }); 

  $('.data_group').each(function() {
        if($(this).val()!="")
        {
           row4.push($(this).val());
        }
   }); 
  app.designForm.ids=row0.join(",");
  app.designForm.stepId=row1.join(",");
  app.designForm.design_filed_name =row2.join(",");
  app.designForm.design_key =row3.join(",");
  app.designForm.data_group=row4.join(",");
}

app.editDesign = function(editId){
     var edit = {};
    var heading="Add Design";
    var button="Save";
  
 
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
         edit = _.findWhere(app.resp.product_design, {
            id:parseInt(editId)
        });

         button="Save";
        heading="Edit Design";

    } else {
        edit = {
            id: "",
            name: "",
            productId: app.filter.productId,
            // override_image:"",
            sku:"",
            svg_snippet:"",
            show_svg_defs:"",
            position:"",
            status:"",
            designFiledID:"",
            profile:"",
            player_status:"",
            design_slug:""
        };
    }
    // app.showDesignFiledList(editId);

    // edit.tableDFiled = app.tableDFiled;

    edit.designFiled = app.respEdit.design_filed;

    edit.heading=heading;

    var reresult = [];
     var reresult1 = [];
     var groupres=[];
    if(edit.id!="")
    {
        _.each(edit.designFiled, function(contents) {
           if(contents.designId==edit.id)
           {
              reresult.push(contents.stepId);
              groupres.push(contents.groupId);
           }
         });
        _.each(edit.designFiled, function(contents) {
           if(contents.designId==edit.id)
           {
              reresult1.push(contents);
           }
         });

    }
    edit.designFiled = reresult1;
    edit.designFiledIdCount =reresult;
    edit.designFiledId =reresult.join(",");  // Get industryId 
    edit.groupId =groupres.join(",");  // Get industryId 

 var step = ['<option value="">Select Step</option>'];
    
    _.each(app.respEdit.product_step, function(content) {
          step.push(app.templates.select(content));
      }); //-

    var group = ['<option value="">Select Group</option>'];
    
    _.each(app.respEdit.fc_groups, function(content) {
        if(content.type=="color"){
          group.push(app.templates.select(content));
        }
      }); //-
 
  edit.step=step;
   edit.group=group;

   app.log("Got click: ", edit);
  
  $('#modalContainer').html(app.templates.modal({
    'title': edit.heading,
    'content': app.templates.designeditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary mr-3' data-dismiss='modal'>Close</button>  <button type='button' id='submitdesignbutton' class='btn btn-primary'>"+button+"</button>"
  }));

  if(edit.id!="")
  {
    $('#design_slug').val(app.slugUrl(edit.name));
  }

  $.each(edit.designFiledId.split(","), function(i,e){
       $('.productStepId').eq(i).val(e);
      });

  $.each(edit.groupId.split(","), function(i,e){
       $('.data_group').eq(i).val(e);
      });

  $("select").chosen();
  $('#modal').modal('show');
}


app.deleteDesign=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete this product design.',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteDesign();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.designForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}


app.dodeleteDesign=function(){

     app.designForm = {
            id: app.designForm.id,
            _token:$('meta[name="csrf-token"]').attr('content')
        };
  $.ajax({
      method: "post",
      url: app.location+"superadmin/product/designdelete",
      dataType: "json",
      data:app.designForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
           app.getProductDesign();
       }
      
    }); //- ajax

}//-ef 


app.deleteDesignFiled=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete the record',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteDesignFiled();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.designForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}


app.dodeleteDesignFiled=function(){

     app.designForm = {
            id: app.designForm.id,
            designFiledId:app.designForm.designFiledId,
            _token:$('meta[name="csrf-token"]').attr('content')
        };
  $.ajax({
      method: "post",
      url: app.location+"superadmin/product/designfileddelete",
      dataType: "json",
      data:app.designForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
         $('#df_'+app.designForm.designFiledId).remove();  
       }
       
    }); //- ajax

}//-ef 


app.addDesignFiled = function(){

    var step = []; var stepSelected=[];
    var group=[]; var groupSelected=[];

    _.each(app.respEdit.product_step, function(content) {
          if(app.stepSelected.includes(""+content.id+"")<1){
          
            step.push(app.templates.select(content));
          }
      });
    _.each(app.respEdit.fc_groups, function(content) {
          if(app.groupSelected.includes(""+content.id+"")<1){
            if(content.type=="color"){
              group.push(app.templates.select(content));
            }
          }
      });


    var html =  '<div class="designFiled_add_more d-flex"> <div class="mr-1 mb-4">';
       html+=    '<select class="form-control chosen-select productStepId" id=""><option value="">Select Step</option>';
      html+=step;
      html+=    '</select>'; 
       html+=  ' <input type="hidden" id="ids" value="" class="form-control ids" name="">';
       html+=  '</div>';

      html+= '<div class="mr-1 mb-4">';
      html+='<input type="text " id="" placeholder="Enter Name" class="form-control design_filed_name" >';

      html+=  '</div>';
      html+= '<div class="mr-1 mb-4">';
      html+=  ' <input type="text" id="" placeholder="Enter Key" class="form-control design_key" >';
      html+=  '</div>';
      
      html+= '<div class="mr-1 mb-4">';
      html+=    '<select class="form-control chosen-select data_group" id=""><option value="">Select Group</option>';
      html+=group;
      html+=    '</select>'; 
      html+= '</div>';

      html+= '<div class="ml-1 mb-4">';
      html+= '<a href="javascript:;" id="" class="deletedesignfiledbutton pt-2 d-block text-muted"><i class="fas fa-trash"></i></a></div> </div>';

    $('#designFiledResult').append(html);
    $("select").chosen();
    $("select").trigger("chosen:updated");
}

app.getEditProduct=function(id){

   var token = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
          type: "POST",
          url: app.location+'superadmin/product/editProduct',
          data : app.filter,
          dataType:"json",
          success: function (resp) {
              app.respEdit=resp;

              app.resp.product_step=resp.product_step;
              app.resp.product_field=resp.product_field;
              app.resp.product_design=resp.product_design;
              app.resp.design_filed=resp.design_filed;

              app.editProduct(id);
              app.rootTabChange();
              app.showProductStepList();
              app.showProductFieldList();
              app.showProductDesignList();
          },
          error: function (xhr, status, error) 
          {
             // alert(xhr.responseText);
          }
      });
}

app.getProduct_detail=function(pages = 1) {
      var token = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
          type: "POST",
          url: app.location+'superadmin/product/getDetailList',
          data : app.filter,
          dataType:"json",
          success: function (resp) {
              app.resp=resp;
          },
          error: function (xhr, status, error) 
          {
             // alert(xhr.responseText);
          }
      });
}


app.getProductStep=function(){

  var token = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
          type: "POST",
          url: app.location+'superadmin/product/getProductStep',
          data : app.filter,
          dataType:"json",
          success: function (resp) {
              app.resp.product_step=resp.product_step;
              app.showProductStepList();
             
          },
          error: function (xhr, status, error) 
          {
             // alert(xhr.responseText);
          }
      });
}


app.showProductStepList=function(){
    var rows = [];
    var i=1;
   
   _.each(app.resp.product_step, function(content) {
  
        content.i=i++;
        rows.push(app.templates.steprow(content));
    }); //-

    $('#step_result').html(app.templates.steptable({
         rows: rows.join(""),
         filter:app.filter
    }));
}


app.getProductField=function(){

  var token = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
          type: "POST",
          url: app.location+'superadmin/product/getProductField',
          data : app.filter,
          dataType:"json",
          success: function (resp) {
              app.resp.product_field=resp.product_field;
              app.showProductFieldList();
             
          },
          error: function (xhr, status, error) 
          {
             // alert(xhr.responseText);
          }
      });
}


app.showProductFieldList=function(){
    var rows = [];
    var i=1;
   
   _.each(app.resp.product_field, function(content) {
  
        content.i=i++;
        rows.push(app.templates.fieldrow(content));
    }); //-

    $('#field_result').html(app.templates.fieldtable({
         rows: rows.join(""),
         filter:app.filter
    }));
}


app.getProductDesign=function(){

  var token = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
          type: "POST",
          url: app.location+'superadmin/product/getProductDesign',
          data : app.filter,
          dataType:"json",
          success: function (resp) {
              app.resp.product_design=resp.product_design;
              app.resp.design_filed=resp.design_filed;
              app.showProductDesignList();
             
          },
          error: function (xhr, status, error) 
          {
             // alert(xhr.responseText);
          }
      });
}

app.showProductDesignList=function(){
    var rows = [];
    var i=1;
   
   _.each(app.resp.product_design, function(content) {
  
        content.i=i++;
        rows.push(app.templates.designrow(content));
    }); //-

    $('#design_result').html(app.templates.designtable({
         rows: rows.join(""),
         filter:app.filter
    }));
}

app.showDesignFiledList=function(id){
    var rows = [];
    var i=1;
  
   _.each(app.respEdit.design_filed, function(content) {
        if(content.designId==id){
          content.i=i++;
          rows.push(app.templates.designfiledrow(content));
        }
    }); //-
 
    var test = app.templates.designfiledtable({
         rows: rows.join(""),
         filter:app.filter
    });
    app.tableDFiled = test;
}


app.rootTabChange=function(){
   $('.roottablink').removeClass('active');
  if(app.filter.view=="product_tab")
  {
    $('#Product-tab').addClass('active');
    $('#product_area').show(); $('#step_area').hide();
    $('#field_area').hide();$('#design_area').hide();    
    $('#nav-Product').addClass('active show');
  }
  else if(app.filter.view=="product_step_tab")
  {
    $('#Steps-tab').addClass('active');
    $('#product_area').hide(); $('#step_area').show();
    $('#field_area').hide();$('#design_area').hide();
    $('#nav-Steps').addClass('active show');
    app.showProductStepList();
  }
  else if(app.filter.view=="product_field_tab")
  {
    $('#field-tab').addClass('active');
    $('#product_area').hide(); $('#step_area').hide();
    $('#field_area').show();$('#design_area').hide();
    $('#nav-field').addClass('active show');
    app.showProductFieldList();
  }
  else if(app.filter.view=="product_design_tab")
  {
    $('#Designs-tab').addClass('active');
    $('#product_area').hide(); $('#step_area').hide();
    $('#field_area').hide();$('#design_area').show();
    $('#nav-Designs').addClass('active show'); 
    app.showProductDesignList();
  }
}


app.getSVGDimension=function() {
  app.productForm = {
            // id:app.productForm.id,
            list_imageId: app.productForm.list_ImageId,
            _token:$('meta[name="csrf-token"]').attr('content')
        };
  $.ajax({
      method: "post",
      url: app.location+"superadmin/product/getsvgdimension",
      dataType: "json",
      data:app.productForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
        $("#svg_dimension_ht").val(resp.result[0]);
        $("#svg_dimension_vt").val(resp.result[1]);
      }
       
    }); //- ajax
}


app.svgSelectorShow=function()
{
    $("#alphasvg").contents().find("svg").find('g').hide();

    var htm='<ul class="svg_ul">';
    $("#alphasvg").contents().find("svg").find('g').each(function(){
        var id = $(this).attr('id');
        if(id!=undefined)
        {
            htm +='<li id="g#_'+id+'"><input type="checkbox" id="'+id+'"  /> g#'+id+'</li>';
        }
        
    });
    htm +='</ul>';
    
    $("#svg_list").append(htm);

    $("input:checkbox").click(function() {
        if($(this).is(":checked"))
        {
            $("#alphasvg").contents().find("svg").find('g').closest("#"+$(this).attr('id')).show();    
        }
        else if(!$(this).is(":checked"))
        {
            $("#alphasvg").contents().find("svg").find('g').closest("#"+$(this).attr('id')).hide();    
        }
    }); 
}


app.productStatusChange=function(){
  app.productForm = {
        id: app.productForm.id,
        // status:app.categoryForm.status,
        _token:$('meta[name="csrf-token"]').attr('content')
    };

  $.ajax({
      method: "POST",
      url: app.location+"superadmin/product/productStatusChange",
      dataType: "json",
      data:app.productForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
            // app.getDistributor();
            // app.alertMessage("Status change successfully","success");
       }
    }); //- ajax
}

app.stepStatusChange=function(){
  app.stepForm = {
        id: app.stepForm.id,
        // status:app.categoryForm.status,
        _token:$('meta[name="csrf-token"]').attr('content')
    };

  $.ajax({
      method: "POST",
      url: app.location+"superadmin/product/stepStatusChange",
      dataType: "json",
      data:app.stepForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
            // app.getDistributor();
            // app.alertMessage("Status change successfully","success");
            app.getProductStep();
       }
    }); //- ajax
}

app.designStatusChange=function(){
  app.designForm = {
        id: app.designForm.id,
        // status:app.categoryForm.status,
        _token:$('meta[name="csrf-token"]').attr('content')
    };

  $.ajax({
      method: "POST",
      url: app.location+"superadmin/product/designStatusChange",
      dataType: "json",
      data:app.designForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
            // app.getDistributor();
            // app.alertMessage("Status change successfully","success");
            app.getProductDesign();
       }
    }); //- ajax
}



app.sorting=function(){
     app.clear_icon();
    if(app.filter.sortBy == 'asc')
      {
       $('#'+app.filter.sortName+'_icon').closest('.sorting').data('sorting_type', 'desc');
       $('#'+app.filter.sortName+'_icon').html('<span class="opacity_5 fa fa-caret-up"></span><span class="fa fa-caret-down"></span>');
      
      }
      if(app.filter.sortBy == 'desc')
      {
        $('#'+app.filter.sortName+'_icon').closest('.sorting').data('sorting_type', 'asc');
        $('#'+app.filter.sortName+'_icon').html(' <span class="fa fa-caret-up"></span><span class="fa fa-caret-down opacity_5"></span>');
      }
     

}
app.clear_icon=function(){
  $('#distributor_name_icon,#name_icon,#category_icon,#url_slug_icon,#status_icon,#position_icon').html('<span class="fa fa-caret-up opacity_5"></span><span class="fa fa-caret-down opacity_5"></span>');
}

app.loadTemplates = function() {

  app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");

} //- loadTemplates


app.getWordsRemainingForDesc=function(){
    $("#tiny_textarea").attr("data-placeholder","");
    
    $('#tiny_textarea').html($('#extra_description').val());
    
    $('#word_count').html($('#extra_description').val());
  
    $("#word_count > p").each(function () {
          $(this).replaceWith($(this).text() + " ");
    });

    var init_word = $.trim($("#word_count").html()).split(' ').filter(function(v){return v!==''}).length;
    $('#description_word_left').text(100-init_word);
    
    $(document).on('keyup propertychange paste','#tiny_textarea',function(){
        var html_text = $('#tiny_textarea').html();
          $('#extra_description').text(html_text);
          $('#word_count').html(html_text);
          $("#word_count > p").each(function () {
              $(this).replaceWith($(this).text() + " ");
          });
        
        init_word='';
        var words = $.trim($('#word_count').html()).split(' ').filter(function(v){return v!==''}).length;

        if (words > 100) {
          // var trimmed = $('#word_count').html().split(/\s+/, 100).join(" ");
          var trimmed = $.trim($('#word_count').html()).split(/\s+/, 100).filter(function(v){return v!==''}).join(" ");

         /* $('#word_count').html(trimmed + " ");
          $('#tiny_textarea').html(trimmed);*/
          
          return false;
        }
        else {
          $('#description_word_left').text(100-words);
        }

    });

}

app.setSvgDimension = function()
{
  // alert($("#select_svg_object").contents().find("svg").attr('width'));
      $("#svg_dimension_ht").val($("#select_svg_object").contents().find("svg").attr('width'));
      $("#svg_dimension_vt").val($("#select_svg_object").contents().find("svg").attr('height'));
}