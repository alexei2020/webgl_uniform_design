// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "distributoreditor",
  file: "superadmin/distributor/create"
  },
  {
  name: "distributortable",
  file: "superadmin/distributor/table"
  },
  {
  name: "distributorrow",
  file: "superadmin/distributor/row"
  },
 /* {
  name: "select",
  file: "superadmin/companies/select"
  }*/
];

app.companiesForm={
  id:""
};
app.respSelect={};
app.industrySelected=[];

app.filter={
  search:"",
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};

app.bu={};




$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

 $(document).on('click touch', '#adddistributorbutton', function(e) {
    e.preventDefault();
     var id = 0;
     app.editDistributor(id);
  });
  $(document).on('click touch', '#editdistributorbutton', function(e) {
    e.preventDefault();
     var id = $(this).attr("data-id");

     app.editDistributor(id);
  });

  $(document).on('click touch', '#submitdistributorbutton', function(e) {
    e.preventDefault();
     app.handleDistributor();
  });

  $(document).on('click touch', '#deletedistributorbutton', function(e) {
    e.preventDefault();
     app.companiesForm.id=$(this).attr("data-id");
     app.deleteDistributor();
  });

   $(document).on('keyup touch', '#search', function(e) {
    e.preventDefault();
     app.filter.search=$(this).val();
     app.getDistributor();
  });

 

   $(document).on('click','.pagination a', function (event) {
        event.preventDefault();
        app.filter.page=$(this).attr('href').match(/page=([0-9]+)/)[1];
        app.getDistributor();
    });

  
} //setListeners

app.launchUser = function() {

  app.getDistributor();
  // app.getBU();
} //- launchUser


app.startApp = function() {
  app.log("Starting");
 app.launchUser();
} //- startApp

app.handleDistributor =function(){

 app.makeDistributor();
     var errors=app.validateDistributor();
     
     if(errors.length>0)
     {
        app.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app.saveDistributor();

}

app.makeDistributor=function(){
  var editId=$('#id').val().trim();
   app.companiesForm={
    name:$('#name').val().trim(),
    email:$('#email').val().trim(),
    profile: $('#profile').val(),
    mobile: $('#mobile').val(),
    id: $('#id').val().trim(),
    _token:$('meta[name="csrf-token"]').attr('content')
  }
  if(editId=="")
  {
    app.companiesForm.password=$('#password').val().trim();
    app.companiesForm.confirmpassword=$('#confirm_password').val().trim();
  }



}




app.validateDistributor=function(){
  var error=[];

 

   if(app.companiesForm.name=="")
  {
      error.push("Please enter name");
  }

  else if(app.companiesForm.email=="")
  {
      error.push("Please enter email");
  }
  else if(!app.validateEmail(app.companiesForm.email))
  {
      error.push("E-mail address is not valid");
  }
  else if(app.companiesForm.mobile=="")
  {
      error.push("Please enter mobile number");
  }

  else if(app.companiesForm.id=="")
  {
     if(app.companiesForm.password=="")
    {
        error.push("Please enter password");
    }
    else if(app.companiesForm.password!="" && app.companiesForm.password.length<8)
    {
        error.push("Password must contain at least 8 characters");
    }
   else if(app.companiesForm.password != app.companiesForm.confirmpassword) {
     error.push('Password and confirm password fields are not matched.');
    }
    
  }
 


  return error;
}

app.saveDistributor=function(){

$('#submitdistributorbutton').prop("disabled",true);
$('#submitdistributorbutton').html("Saving....");
 var button="Save";
if(app.companiesForm.id<1)
{
  var url=app.location+"superadmin/distributor";
}
else
{
  button="Save Changes";
  var url=app.location+"superadmin/distributor/update";
}

 var formData = new FormData($('#companyform')[0]);
 formData.append("_token", app.companiesForm._token);
        formData.append("id", app.companiesForm.id);
        formData.append("name", app.companiesForm.name);
        formData.append("email",app.companiesForm.email);
        formData.append("mobile",app.companiesForm.mobile);

        if(app.companiesForm.id=="")
        {
           formData.append("password",app.companiesForm.password);
           formData.append("confirmpassword",app.companiesForm.confirmpassword);
        }
  $.ajax({
      method: "POST",
      url: url,
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
          /*if(response.msg.profile.length>0)
          {
            var message =["Company Logo must be an image type as jpg/jpeg or png."]; 
            response.msg.profile = message;
          }*/

         app.validationMessage(response.msg,"warning");
        $('#submitdistributorbutton').prop("disabled",false);
        $('#submitdistributorbutton').html(button);
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {


           // $('#modal').modal('hide');
           location.reload();
            // app.getDistributor();
       }
      
    }); //- ajax

}
app.editDistributor=function(editId){
    var edit = {};
    var heading="Add New Distributor";
     var button="Save";
    if (_.isNumber(parseInt(editId)) && parseInt(editId) > 0) {
        edit = _.findWhere(app.resp.companies.data, {
            id:parseInt(editId)
        });

        button="Save Changes";
        heading="Edit Distributor";
    } else {
        edit = {
            id: "",
            name: "",
            email: "",
            profile:"",
            mobile:"",
        };
    }

    edit.heading=heading;

    app.log("Got click: ", edit);
   
  $('#modalContainer').html(app.templates.modal({
    'title': edit.heading,
    'content': app.templates.distributoreditor(edit),
    'footer':"<button type='button'  class='btn btn-secondary' data-dismiss='modal'>Close</button><button type='button' id='submitdistributorbutton' class='btn btn-primary'>"+button+"</button></div>"
  }));


 $("select").chosen();
  $('#modal').modal('show');
}

app.deleteDistributor=function(){
  
$.confirm({
      title: 'Are you sure?',
      content: 'Please confirm if you want to delete the record',
      type: 'red',
      // theme:'supervan',
      closeIcon:true,
      animation:'scale',
      buttons: {   
          ok: {
              text: "Confirm!",
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function(){
                   console.log('the user clicked confirm');
                   app.dodeleteDistributor();
              }
          },
          cancel:{
            text: "Cancel",
            action:function(){
             app.companiesForm.id="";
             console.log('the user clicked cancel');
          }
        }
        
      }
  });
}

app.dodeleteDistributor=function(){

     app.companiesForm = {
            id: app.companiesForm.id,
            _token:$('meta[name="csrf-token"]').attr('content')
        };

  $.ajax({
      method: "DELETE",
      url: app.location+"superadmin/distributor/1",
      dataType: "json",
      data:app.companiesForm
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
            app.getDistributor();
       }
      
    }); //- ajax

}//-ef deleteUser




app.getDistributor=function(pages = 1) {
     
        // $('.loading').show();
        var token = $('meta[name="csrf-token"]').attr('content');
       
       // formData.append("sortby",reverse_order);
       // formData.append("sorttype",column_name);
       
        $.ajax({
            type: "POST",
            url: app.location+'superadmin/distributor/getList',
            data : app.filter,
            dataType:"json",
            success: function (resp) {
                app.resp=resp;
              //  $("#searchResultDiv").html(data);
              // $('.loading').hide();

                app.showDistributorList();
                
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
    }




app.showDistributorList=function(){

   var rows = [];
      var i=1;
    if(app.filter.page>1)
    {
         i=(app.filter.page-1)*20+1;
    }
   _.each(app.resp.companies.data, function(content) {
  
              content.i=i++;
              rows.push(app.templates.distributorrow(content));
          }); //-

          $('#result').html(app.templates.distributortable({
               rows: rows.join(""),
               filter:app.filter
          }));
          $('#pagination').html(app.resp.pagination);

}


app.loadTemplates = function() {

	app.templates.modal = _.template("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-hidden='true' data-keyboard='false'><div class='modal-dialog modal-dialog-slideout' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' ><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><%= footer %></div></div></div></div>");
    
  app.templates.norecord = _.template('<div class="col-sm-12 text-center"><img src="'+app.location+'public/images/norecord.png" width="300"><h4 class="mt-4">No partner found</h4></div>');

} //- loadTemplates
