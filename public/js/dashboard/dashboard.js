
// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}




if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [
 
];

app.respSelect={};

app.actionplanreport={
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content'),
};


app.filter={
  companyId:"",
  businessunit:"",
  department:"",
  unit:"",
  auditstatus:"",
  auditname:"",
  auditor:"",
  page:1,
 _token: $('meta[name="csrf-token"]').attr('content')
};

$(document).ready(function() {
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();
  app.getDashboardChart();
} //setListeners

app.launchDashboard = function() {

  app.getDashboardChart();
} //- launchUnit


app.startApp = function() {
  app.log("Starting");
 app.launchDashboard();

} //- startApp




app.getDashboardChart=function() {

    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: "POST",
        url: app.location+'dashboard/getDashboardChart',
        data : app.filter,
        dataType:"json",
        success: function (resp) {
          console.log(resp.data);
            app.checklistCompletion = resp.data.checklistCompletion;
            app.latestAuditScoreChart = resp.data.latestAuditScoreChart;
            app.latestSiteRating = resp.data.latestSiteRating;
            app.timeTakenPerAudit = resp.data.timeTakenPerAudit;
            app.actionPointsByRisk = resp.data.actionPointsByRisk;
            app.actionPointByCategory = resp.data.actionPointByCategory;
            app.overdueActionPointByRisk = resp.data.overdueActionPointByRisk;
            app.overdueActionPointByCategory = resp.data.overdueActionPointByCategory;
            app.highRiskWithoutActionPoint = resp.data.highRiskWithoutActionPoint;
            
            app.chartLatestSiteRating();
            app.chartlatestAuditScore();
            app.chartTimeTakenPerAudit();
            app.chartCheckListCompletion();
            app.chartActionPlanByRisk();
            app.chartActionPlanByCategory();

            app.chartOverDueActionPlanByRisk();
            app.chartOverdueActionPlanByCategory();
            app.chartHighRiskWithoutActionPlans();
        },
        error: function (xhr, status, error) 
        {
           // alert(xhr.responseText);
        }
    });
}

app.chartLatestSiteRating = function () {

var chart5 = new CanvasJS.Chart("chart_latestSiteRating", {
        animationEnabled: true,
        // exportEnabled: true,
        title:{
          horizontalAlign: "center",
          fontSize: 18,
          padding: 2,

          fontWeight: "normal",
          fontFamily: "tahoma",
          valueFormatString: "#,##,###.#"

        },
        
        legend: {
          verticalAlign: "top",
          horizontalAlign: "right",
          // dockInsidePlotArea: false,
          // fontSize: 12
          cursor: "pointer",
          fontSize: 12,
          itemclick: explodePie
        },
        data: [{
          type: "doughnut",
          startAngle: 90,
          radius: "75%",
            indexLabelFontSize: 13,
            showInLegend: true,
            indexLabel: "{name} - {labely}",
            dataPoints: [
            { y: app.latestSiteRating.diamond, name: "Diamond",labely:app.latestSiteRating.diamond, color: "#B9F2FF" },
            { y: app.latestSiteRating.gold, name: "Gold",labely:app.latestSiteRating.gold, color: "#ffd700" },
            { y: app.latestSiteRating.silver, name: "Silver",labely:app.latestSiteRating.silver, color: "#C0C0C0"},
            { y: app.latestSiteRating.bronze, name: "Bronze",labely:app.latestSiteRating.bronze, color: "#CD7F32" },
            { y: app.latestSiteRating.needsImprovement, name: "Needs Improvement",labely:app.latestSiteRating.needsImprovement, color: "#FF0000"},
            ]
          }]
        });
     chart5.render();

}

function explodePie (e) {
  console.log(e);
  if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
    e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
  } else {
    e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
  }
  e.chart.render();

}


app.chartlatestAuditScore=function() {
  
  var chart1 = new CanvasJS.Chart("chart_latestAuditScore", {
          animationEnabled: true,
          // width:canvasWidth,
          theme: "light1",
          title:{
           // text: "Budget by category",
            fontSize: 18,
            padding: 10,
            fontWeight: "normal",
            fontFamily: "tahoma",
          },
           
          legend: {
             horizontalAlign: "right", // left, center ,right 
             verticalAlign: "top",  // top, center, bottom
            fontSize: 12,

           },
             axisX:{
          // interval: 1,
          title:"Site Name",
          titleFontSize: 14,
          titleFontWeight: "bold",

        },
          axisY: {
            title: "Score (In %)",
            titleFontSize: 14,
             maximum: 100,
            // suffix: "%",
            
            titleFontWeight: "bold",
            gridThickness: 1,
            gridColor: "rgba(0,0,0,.08)",
            valueFormatString: " #,##,###.#"
          },
          toolTip:{   
             animationEnabled: true,
            content: "{label}: {y}%"      
          },
          data: [{        
            type: "column",  
            showInLegend: true, 
            legendMarkerColor: "black",
            showInLegend: false,
            dataPoints:app.latestAuditScoreChart,
            
          }]
        });
     chart1.render();

}


app.chartTimeTakenPerAudit=function() {
    var max = [];
    for (var i = 0; i < app.timeTakenPerAudit.length; i++) {
      max.push(app.timeTakenPerAudit[i]['y']);
    }
    var interval=1;
    if(_.max(max)>5)
    {
      interval="";
    }

  var chart1 = new CanvasJS.Chart("chart_timeTakenPerAudit", {
          animationEnabled: true,
          theme: "light1",
          title:{
           // text: "Budget by category",
            fontSize: 18,
            padding: 10,
            fontWeight: "normal",
            fontFamily: "tahoma",
          },
            toolTip:{
           content:"{labelfull}: {y}" ,
         },
          legend: {
             horizontalAlign: "right", // left, center ,right 
             verticalAlign: "top",  // top, center, bottom
            fontSize: 12,

           },
          axisX:{
            interval: 1,
             title: "Audit Name",
            titleFontSize: 14,
            titleFontWeight: "bold",
          },
          axisY: {
            interval:interval,
            title: "Days",
            titleFontSize: 14,
            titleFontWeight: "bold",
            gridThickness: 1,
            gridColor: "rgba(0,0,0,.08)",
            valueFormatString: " #,##,###.#"
          },
          data: [{        
            type: "column",  
            legendMarkerColor: "black",
            showInLegend: false,
            dataPoints:app.timeTakenPerAudit,    
          }]
        });
     chart1.render();

}

app.chartCheckListCompletion=function(){
  var chart1 = new CanvasJS.Chart("chart_checkListCompletion", {
        animationEnabled: true,
          title:{
         // text:"Project budgets by Progress Status",
          fontSize: 18,
          padding: 10,
          fontWeight: "normal",
          fontFamily: "tahoma",
          valueFormatString: " #,##,###.#"

        },

        axisX:{
        //  interval: 1,
          gridThickness: 1,
          gridColor: "rgba(0,0,0,.08)",
           title: "Audit Name",
            titleFontSize: 14,
            titleFontWeight: "bold",

        },
        axisY:{
          // suffix:"%",
          interlacedColor: "rgba(1,77,101,.02)",
          gridColor: "rgba(0,0,0,.08)",
          titleFontSize: 14,
          valueFormatString: "#,##,###.#",
          title: "Checklist Score (In %)",        
          // titleFontColor: "red",
          titleFontWeight: "bold",
          legendMarkerColor: "grey",
          maximum: 100,

        },
        toolTip:{   
           animationEnabled: true,
            content: "{label}: {y}%"      
          },
        data: [{
          type: "bar",
          color: "rgba(192,0,0,.7)",
          dataPoints:app.checklistCompletion,
        }]
        });
     chart1.render();
}

app.chartActionPlanByRisk=function(){
      
  var chart1 = new CanvasJS.Chart("chart_actionPointsByRisk", {
        animationEnabled: true,
        title:{
          horizontalAlign: "center",
          fontSize: 18,
          padding: 2,
          fontWeight: "normal",
          fontFamily: "tahoma",
          valueFormatString: "#,##,###.#"

        },
        
        legend: {
          verticalAlign: "top",
          horizontalAlign: "right",
          dockInsidePlotArea: false,
          fontSize: 12,
          itemclick: explodePie
        },
        data: [{
          type: "doughnut",
          startAngle: 60,
            indexLabelFontSize: 13,
            showInLegend: true,
            radius: "75%",
            indexLabel: "{name} - {labely}",
             dataPoints: [
              { y: app.actionPointsByRisk.high, name: "High", labely:app.actionPointsByRisk.high, color: "#B03060" },
              { y: app.actionPointsByRisk.medium, name: "Medium", labely:app.actionPointsByRisk.medium, color: "#FFA500" },
              { y: app.actionPointsByRisk.low, name: "Low", labely:app.actionPointsByRisk.low, color: "#0000ff"},
            ]
          }]
        });
     chart1.render();
}

app.chartActionPlanByCategory=function(){
  var chart1 = new CanvasJS.Chart("chart_actionPointsByCategory", {
        animationEnabled: true,
        title:{
          horizontalAlign: "center",
          fontSize: 18,
          padding: 2,
          fontWeight: "normal",
          fontFamily: "tahoma",
          valueFormatString: "#,##,###.#"

        },
        
        legend: {
          verticalAlign: "top",
          horizontalAlign: "right",
          dockInsidePlotArea: false,
          fontSize: 12,
          itemclick:explodePie
        },
        data: [{
          type: "doughnut",
          startAngle: 60,
            indexLabelFontSize: 13,
            radius: "75%",
            showInLegend: true,
            indexLabel: "{y} - {name}",
             dataPoints: app.actionPointByCategory,
          }]
        });
     chart1.render();
}


app.chartOverDueActionPlanByRisk=function(){
      
  var chart1 = new CanvasJS.Chart("chart_overDueActionPointsByRisk", {
        animationEnabled: true,
        title:{
          horizontalAlign: "center",
          fontSize: 18,
          padding: 2,
          fontWeight: "normal",
          fontFamily: "tahoma",
          valueFormatString: "#,##,###.#"

        },
        
        legend: {
          verticalAlign: "top",
          horizontalAlign: "right",
          dockInsidePlotArea: false,
          fontSize: 12,
          itemclick: explodePie
        },
        data: [{
          type: "doughnut",
          startAngle: 60,
            indexLabelFontSize: 13,
            showInLegend: true,
            radius: "75%",
            indexLabel: "{name} - {labely}",
             dataPoints: [
              { y: app.overdueActionPointByRisk.high, name: "High", labely:app.overdueActionPointByRisk.high, color: "#B03060" },
              { y: app.overdueActionPointByRisk.medium, name: "Medium", labely:app.overdueActionPointByRisk.medium, color: "#FFA500" },
              { y: app.overdueActionPointByRisk.low, name: "Low", labely:app.overdueActionPointByRisk.low, color: "#0000ff"},
            ]
          }]
        });
     chart1.render();
}


app.chartOverdueActionPlanByCategory=function(){
      
  var chart1 = new CanvasJS.Chart("chart_overDueActionPointsByCategory", {
        animationEnabled: true,
        title:{
          horizontalAlign: "center",
          fontSize: 18,
          padding: 2,
          fontWeight: "normal",
          fontFamily: "tahoma",
          valueFormatString: "#,##,###.#"

        },
        
        legend: {
          verticalAlign: "top",
          horizontalAlign: "right",
          dockInsidePlotArea: false,
          fontSize: 12,
          itemclick:explodePie
        },
        data: [{
          type: "doughnut",
          startAngle: 60,
            indexLabelFontSize: 13,
            radius: "75%",
            showInLegend: true,
            indexLabel: "{y} - {name}",
             dataPoints: app.overdueActionPointByCategory,
          }]
        });
     chart1.render();
}


app.chartHighRiskWithoutActionPlans=function(){
      
  var chart1 = new CanvasJS.Chart("chart_highRiskWithoutActionPlans", {
        animationEnabled: true,
        title:{
          horizontalAlign: "center",
          fontSize: 18,
          padding: 2,
          fontWeight: "normal",
          fontFamily: "tahoma",
          valueFormatString: "#,##,###.#"

        },
        
        legend: {
          verticalAlign: "top",
          horizontalAlign: "right",
          dockInsidePlotArea: false,
          fontSize: 12,
          itemclick:explodePie
        },
        data: [{
          type: "doughnut",
          startAngle: 60,
            indexLabelFontSize: 13,
            radius: "75%",
            showInLegend: true,
            indexLabel: "{y} - {name}",
             dataPoints: app.highRiskWithoutActionPoint,
          }]
        });
     chart1.render();
}



app.loadTemplates = function() {

	app.templates.modal = _.template("<div class='modal far-right' tabindex='-1' role='dialog' id='modal'><div class='modal-dialog' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title'><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div  class='modal-footer'><%= footer %></div></div></div></div>");
    

} //- loadTemplates

