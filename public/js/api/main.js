// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app360)) {
  alert("System Library failed to load [SYSLD01]. Please report this error to support@nthriveeducation.com");
  //the app360.object is defined in lib.js
}

var scene;

if (app360.dev) {
  app360.log("DEV MODE ON");
}

app360.files = [

  {
  name: "distributordashboard",
  file: "api/dashboard"
  },
  {
    name: "productsrow",
    file: "api/dashboard/row"
  },

  {
    name: "productdesignrow",
    file: "api/dashboard/productdesignrow"
  },

  {
    name: "stepcustomiserow",
    file: "api/dashboard/stepcustomiserow"
  },
  {
    name: "fieldcustomiserow",
    file: "api/dashboard/fieldcustomiserow"
  },

  {
    name: "productcartindex",
    file: "api/dashboard/productcartindex"
  },
   {
    name: "productcarttable",
    file: "api/dashboard/productcarttable"
  },
   {
    name: "productcartrow",
    file: "api/dashboard/productcartrow"
  },

  {
    name: "emailformcreate",
    file: "api/dashboard/emailformcreate"
  },

  {
    name: "cartproductdetail",
    file: "api/dashboard/cartproductdetail"
  },

  {
    name: "getquoteeditor",
    file: "api/dashboard/getquoteeditor"
  },
  
  {
    name: "commonleftmenu",
    file: "api/common/leftmenu"
  }
];



app360.drawArray=[{}];

app360.scaleData={};

app360.filter={
  distributorId:$('#distributor-api').attr('data-distributor'),
  productId:"",
  productUrl_slug:"",
  categoryId:"",
  categoryUrl_slug:"",
  subcategoryId:"",
  subcategoryUrl_slug:"",
  productDesignId:"",
  design_slug:"",
  search:"",
  sortName:"id",
  orderId:0,
  sortBy:"asc",
  page:1,
  _token:''
};


app360.emailForm={};
app360.quoteForm={};

app360.common={
  view:"front",
  viewValue:1
}

app360.camera={
  x:0 ,
  y:0,
  z:0
}
app360.obj_doc={};

// Product Order Data

app360.productStep={}

app360.productOrderData={}

app360.cartProductData={
  id:"",
}


app360.playerData={};
app360.productAction={};

$(document).ready(function() {
  app360.setListeners();
  app360.init(function() {
    app360.startapp();
  });
});

app360.setListeners = function() {

  app360.setLibListeners();


   $(document).on('click','.ub-pagination a', function (event) {
        event.preventDefault();
        app360.filter.page=$(this).attr('href').match(/page=([0-9]+)/)[1];
        app360.getProduct(1);
    });

    $(window).on('popstate', function(event) {
        app360.getDashboard();
        // app360.activeUrl();
     });

    $(document).on('click',".ub-main_categories>li>a",function(){
      $(this).toggleClass("open");
      $(this).next(".ub-sub_categories").slideToggle("fast");
    });

    $(document).on('change', '#select_sorting_id', function(e){
      e.preventDefault();
      app360.filter.sortName = $(this).val();
      app360.getProduct(1);
    });

     $(document).on('click touch', "#ub-category_list>.ub-main_categories>li", function(e){
      e.preventDefault();
        $("#ub-category_list>.ub-main_categories>li").find('#a_cate').removeClass("active");
        $(this).find('#a_cate').addClass("active");
        app360.filter.categoryId = $(this).find('#a_cate').attr('data-id');
        app360.filter.categoryUrl_slug = $(this).find('#a_cate').attr('data-value');
        window.history.pushState("data","Title","#/category/"+app360.filter.categoryUrl_slug);
         app360.filter.subcategoryId="";
         app360.filter.subcategoryUrl_slug="";
         app360.getProduct(1);
    });

    $(document).on('click touch',".ub-sub_categories>li",function(e){
      e.preventDefault();
        app360.filter.subcategoryId="";
        app360.filter.subcategoryUrl_slug="";
        $(".ub-sub_categories>li").find('#a_subcate').removeClass("active");
        $(this).find('#a_subcate').addClass("active");
        app360.filter.subcategoryId = $(this).find('#a_subcate').attr('data-id');
        app360.filter.subcategoryUrl_slug = $(this).find('#a_subcate').attr('data-value'); 
        app360.getProduct(1);
    });

    $(document).on('click', "#productdetailbutton", function(e){
      e.preventDefault();
      $('.ub-loader').show();
      app360.filter.productId=$(this).attr('data-id');
      app360.filter.productUrl_slug=$(this).attr('data-value');
      
      window.history.pushState("data","Title","#/product/"+app360.filter.productUrl_slug);
      app360.getProductDesign();

    });


 
    $(document).on('click touch', '#ub-productdesignbutton', function(e){
      e.preventDefault();
      $('.ub-loader').show();
      app360.filter.productDesignId = $(this).attr('data-id');
      app360.filter.productId = $(this).attr('data-productId');
      app360.filter.design_slug = $(this).attr('data-design_slug');
      window.history.pushState("data","Title","#/product/"+app360.filter.productUrl_slug+"/"+app360.filter.design_slug);
      app360.getProductCustomiseList();
    });

    $(document).on('click','.ub-view_orientation',function(){
      if(app360.common.view=="front")
      {

      }
      app360.viewChange();
   
    })
      
    $(document).on('click','#ub-next_button>a',function(){
      $("#"+$(this).attr('data-value')).click();
    });

    $(document).on('click','#ub-previous_button>a',function(){
      $("#"+$(this).attr('data-value')).click();
    });

    $(document).on('click','#ub-finish_button>a',function(e){
      e.preventDefault();
      
      if(app360.filter.orderId!=null && app360.filter.orderId!=0)
      {
        app360.updateProductOrder();
      }
      else{
        app360.setDefaultDesignColor();
        app360.addProductOrder();  
      }        
    });

    $(document).on('click', '.ub-color_swatch',function(e){
        e.preventDefault();
      $('.ub-color_swatch').removeClass('active');      
      $(this).addClass('active');
      
      var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');

      var fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
      app360.productStep.color_value=$(this).attr('data-value');
        var positionX_value = $(this).parent().closest('#panelField_'+fieldId).find('#position_x').val();
        var positionY_value = $(this).parent().closest('#panelField_'+fieldId).find('#position_y').val();
        app360.productStep.fieldId=fieldId;
        app360.productStep.positionX_value=positionX_value;
        app360.productStep.positionY_value=positionY_value;
      app360.productStep.stepId=stepId;
     
        var design_key = $(this).parent().closest('#Step_'+stepId).attr('data-designkey');
        if(design_key!="")
        {
          /*$.each(design_key.split(","), function(i,e){
            $("#ub-texture>svg").find("#"+e).attr('fill',app360.productStep.color_value);
          });*/
          // setTimeout(function(){
            if ($("#ub-texture>svg").find("#"+design_key).length!=0)
            {
              $("#ub-texture>svg").find("#"+design_key).children().attr('fill',app360.productStep.color_value);
              $("#ub-texture>svg").find("#"+design_key).attr('fill',app360.productStep.color_value);
              $("#ub-texture>svg").find("#"+design_key).css({ fill: app360.productStep.color_value });
              // app360.makeProductCart();
            }

          // },500);
          
        } 
         app360.makeProductCart();
    })


    $(document).on('change','#position_x',function(){
      var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
      var fieldId =fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
        app360.productStep.fieldId=fieldId;

        var checkImage = $('#dd_'+fieldId).find('.deleteLogoButton').attr('data-id'); 
        if(checkImage=="")
        {
          app360.productStep.image_value="";
        }
        app360.productStep.positionX_value=$(this).val();
        app360.productStep.stepId=stepId;
        app360.makeProductCart();
    });

    $(document).on('change','#position_y',function(){
      var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
        // app360.productStep.fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
        var fieldId =fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
        var checkImage = $('#dd_'+fieldId).find('.deleteLogoButton').attr('data-id'); 
        if(checkImage=="")
        {
          app360.productStep.image_value="";
        }
        app360.productStep.fieldId=fieldId;
        app360.productStep.positionY_value=$(this).val();
        app360.productStep.stepId=stepId;
        app360.makeProductCart();
    });

    $(document).on('change','#ub-fieldtext',function(){
      var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
        var fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
        var positionX_value=$(this).parent().closest('#Step_'+stepId).find('#position_x').val();
        var positionY_value=$(this).parent().closest('#Step_'+stepId).find('#position_y').val();
        var text_value=$(this).val();
        app360.productStep.fieldId=fieldId;
        app360.productStep.positionX_value=positionX_value;
        app360.productStep.positionY_value=positionY_value;
        app360.productStep.color_value="";
        app360.productStep.text_value=text_value;
        app360.productStep.stepId=stepId;
        app360.makeProductCart();
        // app360.createButtonForTextUploaded(app360.productStep);
    });


    $(document).on('change','#fontSizeId',function(){
        var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
        var fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
        var fontsize_value=$(this).val();
        var text_value=$(this).parent().closest('#panelField_'+fieldId).find('#ub-fieldtext').val();
        var positionX_value = $(this).parent().closest('#panelField_'+fieldId).find('#position_x').val();
        var positionY_value = $(this).parent().closest('#panelField_'+fieldId).find('#position_y').val();
        app360.productStep.positionX_value=positionX_value;
        app360.productStep.positionY_value=positionY_value;
        app360.productStep.text_value=text_value;
        app360.productStep.fieldId=fieldId;
        app360.productStep.fontsize_value=fontsize_value;
        app360.productStep.stepId=stepId;
        console.log(fontsize_value);
        app360.makeProductCart();
     
    });

    $(document).on('change','#fontName',function(){
        var fontname = $(this).val();
        var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
        var fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
        var positionX_value = $(this).parent().closest('#panelField_'+fieldId).find('#position_x').val();
        var positionY_value = $(this).parent().closest('#panelField_'+fieldId).find('#position_y').val();
        app360.productStep.fieldId=fieldId;
        app360.productStep.positionX_value=positionX_value;
        app360.productStep.positionY_value=positionY_value;
        app360.productStep.fontname_value=$(this).val();
        app360.productStep.stepId=stepId;
        app360.makeProductCart(); 
    });

    $(document).on('click','#resetLogoValue',function(){
       var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
          var fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
          app360.productStep.fieldId=fieldId;
          $(this).parent().closest('#Step_'+stepId).find('#position_x').val(250);
          $(this).parent().closest('#Step_'+stepId).find('#position_y').val(400);
          $(this).parent().closest('#Step_'+stepId).find('#height_value').val(50);
          $(this).parent().closest('#Step_'+stepId).find('#width_value').val(50);

          app360.productStep.positionX_value=$(this).parent().closest('#Step_'+stepId).find('#position_x').val();
          app360.productStep.positionY_value=$(this).parent().closest('#Step_'+stepId).find('#position_y').val();
          app360.productStep.height_value=$(this).parent().closest('#Step_'+stepId).find('#height_value').val();
          app360.productStep.width_value=$(this).parent().closest('#Step_'+stepId).find('#width_value').val();
          app360.makeProductCart();

    });


    $(document).on('click','#changeDesignButton', function(e){
        var productUrl_slug = $(this).attr('data-project_slug');
        window.history.pushState("data","Title","#/product/"+productUrl_slug);
        $("#ub-ProductCustomiseSection").hide();
        $("#ub-product_section").show();    
        app360.getDashboard();
        // app360.activeUrl();
    });

    $(document).on('click','#editCustomiseButton',function(){
      var productUrl_slug = $(this).attr('data-project_slug');
        var productDesignId = $(this).attr('data-productDesignId');
        var orderId =$(this).attr('data-orderId');
        var design_slug = $(this).attr('data-design_slug');
        var id = $(this).attr('data-id');
        app360.filter.productUrl_slug = productUrl_slug;
        app360.filter.productDesignId = productDesignId;
        app360.filter.orderId = orderId;
        app360.filter.design_slug = design_slug;
        app360.productAction.actionStatus="edit_productCustomise";
        window.history.pushState("data","Title","#/product/"+app360.filter.productUrl_slug+"/"+app360.filter.design_slug+"?token="+app360.filter.orderId);
        // app360.resp.proDesign={};
        app360.getDashboard(); 
     
    });

    $(document).on('click','#deletecartpoduct',function(){
        app360.cartProductData.id=$(this).attr('data-id');
        app360.deleteCartProduct();
    });


    $(document).on('click','#ub-viewCartBasket',function(){
       window.history.pushState("data","Title","#/final/");
        app360.activeUrl();
    });


    $(document).on('click','#textEraser',function(){
      var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
      $(this).parent().closest('#Step_'+stepId).find('#ub-fieldtext').val("");
      $(this).parent().closest('#Step_'+stepId).find('#ub-fieldtext').focus();
        app360.productStep.fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
        app360.productStep.text_value=$(this).val();
        app360.productStep.stepId=stepId;
        app360.makeProductCart();
        // app360.removeCreateButtonsForImage();
      
    });

    $(document).on('click','#addplayerrow',function(e){
      e.preventDefault();
      app360.addPlayerRow();
    });

    $(document).on('change','.ub-player_name',function(e){
      e.preventDefault();
      app360.getPlayerData();
    });

    $(document).on('change','.ub-player_number',function(e){
      e.preventDefault();
      app360.getPlayerData();
    });

    $(document).on('change','.ub-player_size',function(e){
      e.preventDefault();
      app360.getPlayerData();
    });

    $(document).on('click','.ub-player_delete', function(e){
      e.preventDefault();
      var id = $(this).attr('data-id');
      if(id!="" && id!=undefined)
      {
        app360.deletePlayer(id);
      }
      else
      {
        $(this).closest('.ub-player_row').remove();
        app360.getPlayerData();  
      }
      
    });

    
    $(document).on("change",'#uploadLogo',function(e){
     
        var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
          var fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
          app360.productStep.fieldId=fieldId;

            // height x 80/400, Width x 80/400
          
          app360.productStep.positionX_value=$(this).parent().closest('#Step_'+stepId).find('#position_x').val();
          app360.productStep.positionY_value=$(this).parent().closest('#Step_'+stepId).find('#position_y').val();
          app360.productStep.height_value=$(this).parent().closest('#Step_'+stepId).find('#height_value').val();
          app360.productStep.width_value=$(this).parent().closest('#Step_'+stepId).find('#width_value').val();
          app360.productStep.color_value="";
          app360.productStep.stepId=stepId;

        var token ='';
        var imageSize =$(this).prop("files")[0].size;


        var imageName = $(this).prop("files")[0].name;
        var extension =  imageName.split('.').pop();
        if(extension=="bmp" || extension=="png" || extension=="jpg" || extension=="jpeg")
        {
            if(imageSize>= 5000000)
            {
              alert("Logo size not be greater than 5mb");
            }
            else{
               $('.ub-loader').show();
              var f = $(this).prop("files")[0]
              var form_data = new FormData();
              
              form_data.append("file", f);
              form_data.append("_token", token);
              $.ajax({
                url:app360.location+"api/dashboard/uploadlogo",
                type:"POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
              
                success:function(resp)
                {
                  // console.log(resp);
                  $('.ub-loader').hide();
                  // setTimeout(function(){
                      $('#dd_'+fieldId).find('img').attr('src',app360.location+'public/images/design_logo/'+resp.result.imageName);
                      $('#dd_'+fieldId).find('.deleteLogoButton').attr('data-id',resp.result.imageName);
                      $('#dd_'+fieldId).show();
                      
                      $('#dd_'+fieldId).find('img').attr('data-image_value',resp.result.imageName);                      
                     
/*                    app360.productStep.image_value=resp.result.imageName;
                    app360.makeProductCart();*/
                  // },300);
                }
              });
            }
        }
        else{
          alert("Please upload image format: bmp, jpg, jpeg, png.");
          $(this).val('');
        }
        
      
    });
   
    $(document).on('change','#width_value',function(e){
      e.preventDefault();
        var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
        var fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
        var width=$(this).val();
        app360.productStep.fieldId=fieldId;
        app360.productStep.width_value=width;

        var positionX_value = $(this).parent().closest('#panelField_'+fieldId).find('#position_x').val();
        var positionY_value = $(this).parent().closest('#panelField_'+fieldId).find('#position_y').val();
        app360.productStep.positionX_value=positionX_value;
        app360.productStep.positionY_value=positionY_value;

        var checkImage = $('#dd_'+fieldId).find('.deleteLogoButton').attr('data-id'); 

        if(checkImage!="" && checkImage!=null)
        {
          app360.productStep.image_value=checkImage; 
        }
        else{
          app360.productStep.image_value="";
        }

        var height =width * 480 / 400;
        app360.productStep.height_value=height;
        $(this).parent().closest('#Step_'+stepId).find('#height_value').val(height);
        app360.productStep.stepId=stepId;
        app360.makeProductCart();
    });

    $(document).on('change','#height_value',function(e){
      e.preventDefault();
       var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
        app360.productStep.fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
        app360.productStep.height_value=$(this).val();
        app360.productStep.stepId=stepId;
        app360.makeProductCart();
    });


    $(document).on('change','#height_check_disable',function(e){
      e.preventDefault();
      var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
      app360.productStep.fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
       var checked = $(this).prop('checked');
        app360.productStep.height_check_disable=checked;
        app360.productStep.stepId=stepId;
       if($(this).is(':checked'))
      {
        $(this).parent().closest('#Step_'+stepId).find('#height_value').prop('disabled', true);
        $(this).parent().closest('#Step_'+stepId).find('#width_value').prop('disabled', true);
        $(this).parent().closest('#Step_'+stepId).find('#position_x').prop('disabled', true);
        $(this).parent().closest('#Step_'+stepId).find('#position_y').prop('disabled', true);
      }
      else{
        $(this).parent().closest('#Step_'+stepId).find('#height_value').prop('disabled', false);
        $(this).parent().closest('#Step_'+stepId).find('#width_value').prop('disabled', false);
        $(this).parent().closest('#Step_'+stepId).find('#position_x').prop('disabled', false);
        $(this).parent().closest('#Step_'+stepId).find('#position_y').prop('disabled', false);

      }
       app360.makeProductCart();
        
    });


    $(document).on('click','.deleteLogoButton',function(e){
      e.preventDefault();
      app360.productStep={};
        var stepId=$(".ub-product_customize_point").find('.active').attr('data-id');
        var fieldId=$(this).parent().closest('#Step_'+stepId).attr('data-id');
        app360.productStep.fieldId=fieldId;
        app360.productStep.positionX_value=$(this).parent().closest('#Step_'+stepId).find('#position_x').val();
        app360.productStep.positionY_value=$(this).parent().closest('#Step_'+stepId).find('#position_y').val();
        app360.productStep.color_value="";
        app360.productStep.stepId=stepId;
      var imageName =$(this).attr('data-id');

      app360.deleteProductLogo(imageName);

    });

    
    $(document).on('click','#emailTofriendButton',function(e){
      e.preventDefault();
     $('#emailtofriendModal').show();
   });

    $(document).on('click','#model_close_button',function(){
      $('#emailtofriendModal').hide();
    })

    $(document).on('click','#sendemailbutton',function(e){
      e.preventDefault();
      app360.emailForm.name=$('#yourname').val();
      app360.emailForm.email=$('#friendemail').val();
      app360.emailToFriend();
      
    });

    $(document).on('click','#shareButton',function(e){
      e.preventDefault();
      $("#copylink").val(window.location.href);
      $('#sharepopupModal').show();
    });

    $(document).on('click','#sharemodel_close_button',function(e){
      e.preventDefault();
      $('#sharepopupModal').hide();
    })

    $(document).on('click','#printButton',function(e){
      e.preventDefault();
      $('#breadcrumb2').hide();
      $('#action_button_tab').hide();
      $('#action_button_tab1').hide();
      $('.not_print').hide();
      window.print(); 
      $('#breadcrumb2').show();
      $('#action_button_tab').show();
      $('#action_button_tab1').show();
      $('.not_print').show();
    })

    $(document).on('click','#viewDetailButton',function(e){
      e.preventDefault();
      var cartorderId=$(this).attr('data-orderId');
       window.history.pushState("data","Title","#/final/"+cartorderId+"/viewdetail");
      app360.getCartProductDetail(cartorderId);
    });

    $(document).on('click','#getQuoteButton',function(e){
      e.preventDefault();
      app360.getEditQuote();
    });

    $(document).on('click','#quoteSubmitButton',function(e){
      e.preventDefault();
       app360.handleQuote();
    });
    
    $(document).on('click','#copyButton',function(e){
      e.preventDefault();
      var id =$('#copylink').attr('id');
      app360.copyContent(id)
      $('#show_text').html('Copied!');
      $( "#show_text").fadeIn("slow");
      setTimeout(function(){
        $( "#show_text").fadeOut("slow");
      },2000);
    });



    $(document).on('click', '.ub-change_design',function(e){
      e.preventDefault();
        var hash = window.location.hash;
        var arr =hash.split('/');
        if(arr[2]!="")
        {
          window.history.pushState("data","Title","#/product/"+arr[2]);
            // $("#ub-ProductCustomiseSection").hide();
            // $("#ub-product_section").show();    
            app360.filter.orderId="";
            app360.productAction.actionStatus="add_productCustomise";
            // window.location.reload();
            app360.getDashboard();
        }
    });

} //setListeners



app360.launchUser = function() {

  app360.getDashboard();
} //- launchUser


app360.startapp= function() {
  app360.log("Starting");
  app360.launchUser();
} //- startApp


app360.getDashboard=function(){
  $('.ub-loader').show();
      app360.getCompanyFeedCodeStatus();
}


app360.getDashboardCheck=function()
{
    app360.activeUrl();
    $('#UniformBuilder').html(app360.templates.distributordashboard());
    
    var res = window.location.pathname.split("/");

    if($('#ub-product_previewlink').length) 
    {   var flag = 0;
        for (var i = 0; i < res.length; i++) {
          if(res[i]=="distributor"){
            flag=1;
          }
        }
        if(flag==1){
          $('#ub-product_previewlink').attr('href',app360.location+"distributor/preview/"+app360.filter.distributorId);
        }
        else{
          $('#ub-product_previewlink').attr('href',window.location.href.split('#')[0]); 
        }

    }
}



app360.getCompanyFeedCodeStatus=function() {
  $('.ub-loader').show();
    var token = ""; //$('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: "POST",
        url: app360.location+'api/dashboard/getCompanyFeedCodeStatus',
        data : app360.filter,
        dataType:"json",
        success: function (resp) {
          $('.ub-loader').hide();
          app360.resp=resp;

          app360.showFeedCodeStatusList();
        },
        error: function (xhr, status, error) 
        {
           // alert(xhr.responseText);
        }
    });
}

app360.showFeedCodeStatusList=function()
{
  if(app360.resp.checkFeedCodeStatus.length>0)
  {
      _.each(app360.resp.checkFeedCodeStatus, function(content){
        var res = window.location.pathname.split("/");
        var flag = 0;
        for (var i = 0; i < res.length; i++) {            
          if(res[i]=="distributor"){
              flag=1;
          }
        }
        if(flag==0)
        {
          if(content.status=="active"){
            app360.getDashboardCheck();
            
          }
          else{
            var htm = '<div id="ub-pageNotAvailable">Web page not available.</div>';
            $('#UniformBuilder').append(htm);
            
          }
        }
        else{
            app360.getDashboardCheck();
        }
      });
  }
  else{
      app360.getDashboardCheck();
  }
  
}
  

app360.getCategory=function() {
  $('.ub-loader').show();
    var token = ""; //$('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: "POST",
        url: app360.location+'api/dashboard/getList',
        data : app360.filter,
        dataType:"json",
        success: function (resp) {
          $('.ub-loader').hide();
          app360.resp=resp;
          app360.showCategoryList();
        },
        error: function (xhr, status, error) 
        {
           // alert(xhr.responseText);
        }
    });
}


app360.activeUrl=function(){
  var hash = window.location.hash;
    var arr =hash.split('/');
    if(arr[0]=="")
    {

      app360.getCategory();
      app360.getProduct(1);
    }
    else if(arr[1]=="final")
    {
        app360.activeProductCartSlug(arr);
    }
    else
    {
        if(arr.length>3)
        {
          if(arr[3]!="")
          {
            app360.activeProductDesignSlug(arr);
          }
          else
          {
            if(arr[1]=="product")
            {
               app360.getCategory();
              app360.activeProductSlug(arr);
            }
          }
        }
        else if(arr.length==3)
        {
          if(arr[1]=="product")
          {
             app360.getCategory();
            app360.activeProductSlug(arr);
          }
          else if(arr[1]=="category"){
            app360.activeCategorySlug(arr);
          }
        }
    }
}

app360.activeCategorySlug=function(arr){
    $(this).find(".ub-main_categories>li>a").addClass('active');
    app360.filter.categoryUrl_slug = arr[2];
    app360.getCategory();
    app360.getProduct(1);
}    


app360.activeProductSlug=function(arr){

    app360.filter.productUrl_slug=arr[2];
    app360.getProductDesign();
}

app360.activeProductDesignSlug=function(arr){
    app360.filter.productUrl_slug=arr[2];
    var test =arr[3].split('?');
    // app360.filter.productDesignId=test[0];
      app360.filter.design_slug=test[0];
    var orderId =  app360.getParameterByName('token');
    app360.filter.orderId = orderId;
    if(app360.filter.orderId!=null)
    {
      app360.productAction.actionStatus="edit_productCustomise";
      app360.getEditProductCustomiseList();
    }
    else{
      app360.productAction.actionStatus="add_productCustomise";
      app360.getProductCustomiseList();
    }
}

app360.activeProductCartSlug=function(arr){
  if(arr[1]=="final")
    {
      if(arr[2]!="")
      {   
          app360.getProductCartList();  
          window.history.pushState("data","Title","#/final/"+arr[2]+"/viewdetail");
          app360.getCartProductDetail(arr[2]);
      }
      else{
        app360.getProductCartList();  
      }
      
    }
    else{
      app360.productCartDetail();  
    }
    
}

app360.getProduct=function(pages=1){
        $('.ub-loader').show();

    var token = "";//$('meta[name="csrf-token"]').attr('content');
      $.ajax({
          type: "POST",
          url: app360.location+'api/dashboard/getProductList',
          data : app360.filter,
          dataType:"json",
          success: function (resp) {
              app360.resp=resp;
              app360.showProductList();
          },
          error: function (xhr, status, error) 
          {
             // alert(xhr.responseText);
          }
      });
}

app360.showProductList=function(){
  var rows=[];
  _.each(app360.resp.products.data, function(content){
    if(content.product_status=="active" && content.deleted==0){ 
      rows.push(app360.templates.productsrow(content));
    }
  });
  $("#ub-productDesignAreaShow").hide();
  $("#ub-productAreaShow").show();
  $('#ub-productList').html(rows);
  $('#ub-pagination').html(app360.resp.pagination);
  $('.ub-loader').hide();
}


app360.showCategoryList=function(){

    var rows = [];
    var newCategoryresult=[];
    var newSubCategoryresult="";
    var newParentCategory=[];
    var cresult=[];
    var subcatList="";
    _.each(app360.resp.category, function(content){

            subcatList = _.where(app360.resp.totalCategory, {
              parentId:content.categoryId,
            });
            content.subcatList = subcatList;

           
            rows.push(app360.templates.commonleftmenu(content));
           
        });
    $('#ub-category_list').html(rows);
}


app360.getProductDesign=function() {
    
      var token = ""; //$('meta[name="csrf-token"]').attr('content');
      $.ajax({
          type: "POST",
          url: app360.location+'api/dashboard/getProductDesignList',
          data : app360.filter,
          dataType:"json",
          success: function (resp) {
            $('.ub-loader').hide();
              app360.resp=resp;
              app360.showProductDesignList();
          },
          error: function (xhr, status, error) 
          {
             // alert(xhr.responseText);
          }
      });
}

app360.showProductDesignList=function(){

  var rows=[];
    _.each(app360.resp.product_design, function(content){
        rows.push(app360.templates.productdesignrow(content));
    });

    $("#ub-productAreaShow").hide();
    $('#ub-productDesignList').html(rows);
    $("#ub-productDesignAreaShow").show();
    _.each(app360.resp.product_design, function(content){
        if(content.prox_axis!=null){ app360.camera.xd = content.prox_axis; }
      if(content.proy_axis!=null){ app360.camera.yd = content.proy_axis; }
      if(content.proz_axis!=null){ app360.camera.zd = content.proz_axis; }
      
      if(content.pro_scale!=null){ 
        app360.scaleData.scaled = content.pro_scale; 
      } else{ 
        app360.scaleData.scaled=1; 
      }
      init2dynamic(app360.location+'public/images/'+content.obj_doc,content.id);
        
    });  

}


app360.captureProductDesignImage=function(id){

  html2canvas(document.querySelector("#webgl_"+id)).then(function(canvas) {
    var successCount=0;
     var strMime = "image/png";
     var imgData = canvas.toDataURL(strMime);
     // console.log(imgData);
     

    $('#webgl_'+id).show();
     // $("#designImage_"+id).attr("src",imgData);
     // $("#designImage_"+id).show();
    /* 
      $.ajax({
        url: app360.location+'api/dashboard/captureProductDesignImageUpload',
        data: {
          imgBase64: imgData,
          id:id,
          _token:''
        },
        type: 'post',
        success: function (response) 
        {  
             successCount=Number(successCount)+1;
            if(successCount==1)
            {
                
            }
            
        }
      });*/
  });
      
}


app360.getProductCustomiseList=function() {
  $('.loader').show();
    var token =""; //$('meta[name="csrf-token"]').attr('content');
  
      $.ajax({
          type: "POST",
          url: app360.location+'api/dashboard/getProductCustomiseList',
          data : app360.filter,
          dataType:"json",
          success: function (resp) {
             $('.ub-loader').hide();
              app360.resp=resp;
              app360.showStepCustomiseList();

          },
          error: function (xhr, status, error) 
          {
             // alert(xhr.responseText);
          }
      });
}

app360.showStepCustomiseList=function(){
  // console.log(app360.resp.proDesign);
  $('.ub-ViewBasket').hide();
  $('#rightContentHide').hide();
  var svg_snippet_text="";
    var rows=[];
    var i=0;
    var product_name="";
    var product_description="";
    var obj_doc = "";
    _.each(app360.resp.product_step, function(content){
        i++;
        content.i= i;
        obj_doc = content.obj_doc;
         product_name = content.product_name;
         product_description = content.description; 
        rows.push(app360.templates.stepcustomiserow(content));

    });

    $("#ub-product_section").hide();
    $("#ub-ProductCustomiseSection").show();
    $("#ub-product_name").html(product_name);
    if(product_description!="" && product_description!=null)
    {
      $('#ub-product_info').show();
      $("#ub-product_description").html(product_description);
    }
    $('.ub-product_customize_point').html(rows);

    // Share Link Here
    if(app360.resp.social_media.length>0)
    {
      if(app360.resp.social_media[0].social_share_html!="" && app360.resp.social_media[0].social_share_html!=null)
      {
        $('#ub-share_social_media').html("Share to  "+app360.resp.social_media[0].social_share_html);
      } 
    }
    

  // Create Player Step Start 
     var htm = "";
    j=i+1;
    htm +='<a href="javascript:;" id="tab_Step_'+j+'" data-id="players"   class="ub-product_customize_step ub-tablinks" datalinktitle="Step_'+j+'" >';
    htm +='<span class="circle">'+j+'</span>';
    htm +='<span class="txt">Roster</span>';
    htm +='</a>';
    var flag=0;
    if(app360.resp.proDesign[0]!=undefined)
    {
      if(app360.resp.proDesign[0].player_status=="active")
      {
        flag=1;
      }
    }
    

    if(flag==1)
    {
      $('.ub-product_customize_point').append(htm);  
    }
    
  // Player Step End


    _.each(app360.resp.proDesign, function(design_cont){
      if(design_cont.x_axis!=null){ app360.camera.x = design_cont.x_axis; }
      if(design_cont.y_axis!=null){ app360.camera.y = design_cont.y_axis; }
      if(design_cont.z_axis!=null){ app360.camera.z = design_cont.z_axis; }
      if(design_cont.scale!=null){ 
        app360.scaleData.scale = design_cont.scale; 
      } else{ 
        app360.scaleData.scale=1; 
      }
      
      if(design_cont.rotation_disable=='true'){ 
        app360.common.rotation_disable = false; 
      } else{ 
        app360.common.rotation_disable = true; 
      }

      if(design_cont.light_brightness!=null){ 
        app360.common.light_brightness = design_cont.light_brightness; 
      } else{ 
        app360.common.light_brightness=0.5; 
      }

      if(design_cont.default_toggle_color!="")
      {
        app360.common.default_toggle_color=design_cont.default_toggle_color;
      }
      else{
        app360.common.default_toggle_color="";
      }
      if(design_cont.svg_viewBox!=null)
      {
        svg_viewBox="viewBox='"+design_cont.svg_viewBox+"'";
      }
      else{
        svg_viewBox=""; 
      }
        $("#ub-texture").html('<svg data-elems="Body" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="'+design_cont.svg_width+'" height="'+design_cont.svg_height+'" version="1.0" '+svg_viewBox+'  style="position: absolute;">'+design_cont.svg_snippet+'</svg>');
        $("#ub-product_design_name").html(design_cont.name);
    });
          
    app360.obj_doc.filename=obj_doc;
    setTimeout(function(){  getSvg(); },2000);
    setTimeout(function(){
      // getSvg();
       scene = init2(app360.location+'public/images/'+app360.obj_doc.filename);
    },3000);
    
    app360.activeStep();
    app360.loadCustomFont();
}


app360.showFieldCustomiseList=function(stepId){
    var rows=[];
    var i=0;
    var design_filed = app360.resp.design_filed;
    var product_field = app360.resp.product_field;
    var fieldList = design_filed.concat(product_field);
    var fieldMobileView = [];

    _.each(fieldList, function(content){

        if(content.stepId==stepId)
        {
          if(content.designId!="")
          {
            designId=content.designId;
          }
          else{
            designId="";
          }

          if(content.design_key!="")
          {
            design_key=content.design_key;
          }
          else{
            design_key="";
          }
          if(content.fontGroupId!="")
          {
            fontGroupId=content.fontGroupId;
          }
          else{
            fontGroupId="";
          }

          if(content.filed_type!="")
          {
            filed_type=content.filed_type;
          }
          else{
            filed_type="";
          }
          content.filed_type=filed_type;
          // content.fontname_value=content.fontname_value;
          content.fontGroupId=fontGroupId;
          content.designId=designId;
          content.design_key=design_key;
          content.colorgroup = content.colorGup;
          content.fontgroup = content.fontGup;   
          
          fieldMobileView.push('<a href="javascript:;" class="field_mobile_view" data-fieldId="'+content.id+'"  data-show="field_'+content.id+'" >'+content.name+'</a>');
    
          rows.push(app360.templates.fieldcustomiserow(content));
        }

    });
    
    if(stepId=="players")
    {
      $('.field_mobile_view').hide();
        var playerCount=app360.resp.playerList.length;

        var size = []; 
        var sizeSelected=[];
          _.each(app360.resp.size_list,function(k){
            size.push(k);
          });
         
        content={};
        content.id="";
        content.stepId=stepId;
        content.design_key="";
        content.playerCount=playerCount;
        content.playerList=app360.resp.playerList;
        content.size=size;
        // fieldMobileView.push('<a href="javascript:;" class="field_mobile_view" data-fieldId="players"  data-show="field_players" >Players Detail</a>');
    
        $('#ub-field_customise').html(app360.templates.fieldcustomiserow(content));
        // $('.ub-product_mobile_show>div').html(fieldMobileView);
    }
    else
    {
        $("#ub-field_customise").html(rows);  
        $('.ub-product_mobile_show>div').html(fieldMobileView);
    }
    app360.drawArray=app360.resp.product_field; 
      
    if( app360.productAction.actionStatus=="edit_productCustomise")
    {      
      if(app360.common.viewValue==-1.9)
      {
        app360.common.viewValue=1;
        app360.common.view="front";
      }
    }
     app360.toggleField();
     app360.setObjImageValues(); 
}


app360.loadCustomFont=function(){

    ttfFile=_.filter(app360.resp.fonts, function(x) {

        return x.font_file != null;
      });
    var htm = "<style>";
    _.each(ttfFile,function(k){
      htm +="@font-face {font-family: "+k.name+"; src: url('"+app360.location+"public/images/fonts/"+k.font_file+"') format('truetype')}\n";
    });
    htm +="</style>";
    $('#UniformBuilder').prepend(htm);
}


app360.setObjImageValues=function(){
    _.each(app360.resp.design_filed, function(content){
      if( app360.productAction.actionStatus=="edit_productCustomise")
      {  
        if(content.color_value!=null && content.color_value!="")
        {
          if ($("#ub-texture>svg").find("#"+content.design_key).length!=0)
          {
            $("#ub-texture>svg").find("#"+content.design_key).attr('fill',content.color_value);
            $("#ub-texture>svg").find("#"+content.design_key).children().attr('fill',content.color_value);
            $("#ub-texture>svg").find("#"+content.design_key).css({ fill: content.color_value });
          }
        }
      }
      else if(app360.common.default_toggle_color!="" && app360.common.default_toggle_color!=null)
      {
        if( app360.productAction.actionStatus=="add_productCustomise")
        {
          if(content.color_value==null)
          {
            if ($("#ub-texture>svg").find("#"+content.design_key).length!=0)
            {
              $("#ub-texture>svg").find("#"+content.design_key).children().attr('fill',app360.common.default_toggle_color);
              $("#ub-texture>svg").find("#"+content.design_key).attr('fill',app360.common.default_toggle_color);
              $("#ub-texture>svg").find("#"+content.design_key).css({ fill: app360.common.default_toggle_color });
            }
          }
        }
      }
    });
}

app360.activeField=function(fieldId){
  _.each(app360.resp.design_filed, function(content){
      if(content.id==fieldId)
      {
           $('.ub-color_swatch').removeClass('active');
          setTimeout(function(){
            $('.ub-color_swatch').filter('[data-value="'+content.color_value+'"]').addClass('active');
          },300);
      }
  });
  _.each(app360.resp.product_field, function(content){
      if(content.id==fieldId)
      {
        if(content.filed_type=="image" && content.image_value!=null && content.image_value!="")
        {  
            $('#dd_'+content.id).find('img').attr('src',app360.location+'public/images/design_logo/'+content.image_value);
            $('#dd_'+content.id).find('.deleteLogoButton').attr('data-id',content.image_value);
            $('#dd_'+content.id).show();

            $('.ub-color_swatch').removeClass('active');
            setTimeout(function(){
              $('.ub-color_swatch').filter('[data-value="'+content.color_value+'"]').addClass('active');
            },500);

            app360.productStep.image_value=content.image_value;
            app360.productStep.positionX_value=content.positionX_value;
            app360.productStep.positionY_value=content.positionY_value;
            app360.productStep.width_value=content.width_value;
            app360.productStep.height_value=content.height_value;
            app360.productStep.color_value=content.color_value;
             app360.productStep.height_check_disable=content.height_check_disable;
             // app360.productStep.width_check_disable=content.width_check_disable;
        }
        else if(content.filed_type=="text")
        {
          $('.ub-color_swatch').removeClass('active');
          setTimeout(function(){
            $('.ub-color_swatch').filter('[data-value="'+content.color_value+'"]').addClass('active');
          },500);
            app360.productStep.text_value=content.text_value;
            app360.productStep.fontsize_value=content.fontsize_value;
            app360.productStep.positionY_value=content.positionY_value;
            app360.productStep.positionX_value=content.positionX_value;
            app360.productStep.color_value=content.color_value;
            app360.productStep.fontname_value=content.fontname_value;
        }
      }
       
  });

}

app360.getEditProductCustomiseList=function() {
     var token = "";//$('meta[name="csrf-token"]').attr('content');  
      $.ajax({
          type: "POST",
          url: app360.location+'api/dashboard/getEditProductCustomiseList',
          data : app360.filter,
          dataType:"json",
          success: function (resp) {
              app360.resp = resp;
              $('.ub-loader').hide();
              app360.showStepCustomiseList();
          },
          error: function (xhr, status, error) 
          {
             // alert(xhr.responseText);
          }
      });
}


app360.activeStep=function(){

    $(".ub-product_customize_step:first").addClass('active');
    var id= $(".ub-product_customize_step:first").attr('data-id');
    var datalinktitle= $(".ub-product_customize_step:first").attr('id');
    // app360.productStep.id=id;

    $(document).on('click',".ub-product_customize_step", function(){

        $('.ub-product_customize_step').removeClass('active');
        $('.ub-product_customize_step').removeClass('done');
        $(this).addClass('active');
        id = $(this).attr('data-id');
        // app360.productStep.id=id;
        datalinktitle = $(this).attr('id');

        var count =$(this).find('.circle').text();
        for (var i = count-1; i > 0; i--) {
          $('#tab_Step_'+i).addClass('done');  
        } 
        app360.stepSwitchByButton(datalinktitle);
        app360.showFieldCustomiseList(id);
        
    });
    app360.stepSwitchByButton(datalinktitle);
    app360.showFieldCustomiseList(id);
}

app360.toggleField=function(){
    $('.ub-accordion:first').addClass('active');
    $('.ub-panel:first').css('display','block');

    $('.field_mobile_view:first').addClass('active');
    $('.ub-panel:first').css('display','block');
    
    var fieldId=$('.ub-accordion:first').attr('data-fieldId');
    $(".ub-accordion").click(function(){
      
      if($(this).attr('class')=="ub-accordion active")
      {
        $(this).removeClass("active");
        $(this).next(".ub-panel").slideUp();
      }
      else
      {

        $('.field_mobile_view').removeClass('active');
        $(".ub-accordion").removeClass("active");        
        $(".ub-accordion").next(".ub-panel").slideUp();
        $(this).addClass("active");
        $(this).next(".ub-panel").slideDown("slow");
        fieldId=$(this).attr('data-fieldId');

        $(".ub-product_mobile_show").find(".field_mobile_view[data-show='field_" + fieldId+ "']").addClass('active');
        app360.activeField(fieldId);
      }
    });

    $(".field_mobile_view").click(function(){
        $('.ub-panel').hide();
        $('#'+$(this).attr('data-show')).next('.ub-panel').show();
        $('.ub-accordion').removeClass('active');
        $('#'+$(this).attr('data-show')).addClass('active')
        $(".field_mobile_view").removeClass("active");        
        $(".field_mobile_view").next(".ub-panel").slideUp();
        $(this).addClass("active");
        fieldId=$(this).attr('data-fieldId');
        app360.activeField(fieldId);

   
    });
    app360.activeField(fieldId);
}


app360.stepSwitchByButton =function(id){

  var step_length = $(".ub-product_customize_step").length;
  var j = $("#"+id).find('.circle').html();
  if(j==1)
  {
    if(j==step_length)
    {
      $("#ub-next_button").hide();
      $('#ub-previous_button').hide();
      $("#ub-finish_button").show();
    }
    else{
      nb=j; nb++;
      $("#ub-previous_button").hide();
      $("#ub-finish_button").hide();
      $('#ub-next_button').removeClass('w-50').addClass('w-100');
      $('#ub-next_button').show();
      $("#ub-next_button>a").attr("data-value","tab_Step_"+nb); 
    }
 
  }
  else if(j==step_length)
  {
    pb=j; pb--;
    $("#ub-next_button").hide();
    $("#ub-finish_button").show();
    $('#ub-previous_button').show();
    $("#ub-previous_button>a").attr("data-value","tab_Step_"+pb); 
  }
  else{
    nb=pb=j; nb++; pb--;
    $("#ub-finish_button").hide();
    $('#ub-next_button').removeClass('w-100').addClass('w-50');
    $('#ub-next_button').show();
    $('#ub-previous_button').show();
    
    $("#ub-previous_button>a").attr("data-value","tab_Step_"+pb); 
    $("#ub-next_button>a").attr("data-value","tab_Step_"+nb); 
  }
}


app360.addProductOrder=function(){
$('.ub-loader').show();
  $('#ub-finish_button>a').prop("disabled",true);
  $('#ub-finish_button>a').html("Finish....");

  var updated_snippet= $('#ub-texture').find('svg').html();
  app360.productOrderData = {
          distributorId:app360.filter.distributorId,
          productUrl_slug:app360.filter.productUrl_slug,
          design_slug:app360.filter.design_slug,
          productDesignId:app360.filter.productDesignId,
          design_filed:app360.resp.design_filed,
          product_field:app360.resp.product_field,
          player_name:app360.playerData.player_name,
          player_number:app360.playerData.player_number,
          player_size:app360.playerData.player_size,
          ids:app360.playerData.ids,
          updated_snippet:updated_snippet,
            _token:''
        };

   $.ajax({
      method: "POST",
      url: app360.location+"api/dashboard/addProductOrder",
      dataType: "json",
      data:app360.productOrderData
    })
    .fail(function(resp) {
          $('#ub-finish_button>a').prop("disabled",false);
        $('#ub-finish_button>a').html("Finish");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {

        app360.captureImage(resp.result);
       }
       
    }); //- ajax

}


app360.updateProductOrder=function(){

  var orderId =  app360.getParameterByName('token');
  $('.ub-loader').show();
  app360.filter.orderId=orderId;
  $('#ub-finish_button>a').prop("disabled",true);
  $('#ub-finish_button>a').html("Finish....");
  app360.productOrderData = {
        id:app360.filter.orderId,
          distributorId:app360.filter.distributorId,
          productUrl_slug:app360.filter.productUrl_slug,
          design_slug:app360.filter.design_slug,
          productDesignId:app360.filter.productDesignId,
          design_filed:app360.resp.design_filed,
          product_field:app360.resp.product_field,
          player_name:app360.playerData.player_name,
          player_number:app360.playerData.player_number,
          player_size:app360.playerData.player_size,
          ids:app360.playerData.ids,
            _token:''
        };
   $.ajax({
      method: "POST",
      url: app360.location+"api/dashboard/updateProductOrder",
      dataType: "json",
      data:app360.productOrderData
    })
    .fail(function(resp) {
      $('#ub-finish_button>a').prop("disabled",false);
        $('#ub-finish_button>a').html("Finish");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
        app360.captureImage(app360.filter.orderId);
       }
       
    }); //- ajax

}

app360.getProductCartList=function() {
      $('.ub-loader').show();
        var token =""; // $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            type: "POST",
            url: app360.location+'api/dashboard/getProductCartList',
            data : app360.filter,
            dataType:"json",
            success: function (resp) {
                app360.resp=resp;
                $('.ub-loader').hide();
                app360.showProductCartList();
                 // $(".texture").hide();
                
            },
            error: function (xhr, status, error) 
            {
               // alert(xhr.responseText);
            }
        });
}

app360.showProductCartList=function(){

    var rows = [];
    var i=1;
    var totalCount=0;
    $("#ub-product_section").hide();
    $("#ub-ProductCustomiseSection").show();
    
    $("#ub-PCustomise_div").hide();   

    $("#ub-Product_cart_div").html(app360.templates.productcartindex());
   _.each(app360.resp.product_order, function(content) {
              content.i=i++;

              PlayerSize=_.filter(app360.resp.player_setting, function(x) {
                  return x.productOrderId == parseInt(content.id);
              });
              var totalSizeCounteachRow=PlayerSize.length;
               data = _.countBy(PlayerSize, 'player_size')

              var sizeCount = _.map(data, function(value, key){
                  return {
                      size: key,
                      count: value,
                  };
              });
              _.each(sizeCount, function(low_content){
                  totalCount+=Number(low_content.count);
               });
           
              content.sizeCount=sizeCount;
              content.totalSizeCounteachRow=totalSizeCounteachRow;
              
              rows.push(app360.templates.productcartrow(content));
          }); //-
    $('#ub-cart_table').html(app360.templates.productcarttable({
         rows: rows.join(""),
         filter:app360.filter
    }));
    $('#totalCountProduct').html(totalCount);

    if($('#norecord_link').length) 
    {
      $('#norecord_link').attr('href',app360.location+"/distributor/preview/"+app360.filter.distributorId);
    }
}

app360.productCartDetail=function(){
    app360.getProductCartList();
    window.history.pushState("data","Title","#/final/");
}

app360.deleteCartProduct=function(){
     app360.cartProductData = {
            id: app360.cartProductData.id,
            _token:''
        };
  $.ajax({
      method: "POST",
      url: app360.location+"api/dashboard/deleteCartProduct",
      dataType: "json",
      data:app360.cartProductData
    })
    .fail(function(resp) {
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
            app360.getProductCartList();
       }
      
    }); //- ajax

}//-ef deleteUser


app360.makeProductCart=function(){
  var image_type=[];
    var design_filed = app360.resp.design_filed;
    var product_field = app360.resp.product_field;
    _.each(product_field, function(content){
      if(app360.productStep.fieldId==content.id)
      {
          if(app360.productStep.color_value!=undefined && app360.productStep.color_value!=""){
            content.color_value = app360.productStep.color_value;
          }
          else if(content.color_value==null){

            content.color_value = "#FFFFFF"; 
          }
          if(app360.productStep.text_value!=undefined){
            content.text_value = app360.productStep.text_value;
          }
          if(app360.productStep.image_value!=undefined ){

            content.image_value = app360.productStep.image_value;
          }
          if(app360.productStep.fontsize_value!=undefined){
            content.fontsize_value = app360.productStep.fontsize_value;  
          }
          else if(content.fontsize_value==null){
            content.fontsize_value = "16"; 
          }
          if(app360.productStep.fontname_value!=undefined){
            content.fontname_value = app360.productStep.fontname_value;  
          }
          else if(content.fontname_value==null){
            content.fontname_value = "Arial"; 
          }
          if(app360.productStep.positionX_value!=undefined){
            content.positionX_value = app360.productStep.positionX_value;  
          }
          if(app360.productStep.positionY_value!=undefined){
            content.positionY_value = app360.productStep.positionY_value;  
          }
          if(app360.productStep.height_value!=undefined){
            content.height_value = app360.productStep.height_value;  
          }
          if(app360.productStep.width_value!=undefined){
            content.width_value = app360.productStep.width_value;  
          }
          if(app360.productStep.height_check_disable!=undefined){
            content.height_check_disable = app360.productStep.height_check_disable;  
          }
          if(app360.productStep.width_value!=undefined){
            content.width_value = app360.productStep.width_value;  
          }
      }
    });
   
    _.each(design_filed, function(content){
      if(app360.productStep.fieldId==content.id && app360.productStep.stepId==content.stepId)
      {
          if(app360.productStep.color_value!=undefined){
            content.color_value = app360.productStep.color_value;
          }
      }
    });
      app360.drawArray=product_field; 
      getSvg();
      $('.ub-loader').show();
      setTimeout(function(){
        // $('.ub-loader').hide();
         updatedscreen();
      },150);
}


app360.viewChange=function(){
  if(app360.common.view=="front")
  {
     app360.common.view="back";
     app360.common.viewValue=-1.9;
     $('.ub-view_orientation') .html('View Back');
  }
  else
  {
     app360.common.view="front";
     app360.common.viewValue=1;
     $('.ub-view_orientation') .html('View Front');
  }
  scene = updatedscreen();
}

app360.captureImage=function(id){
    $('.ub-loader').show();
    var successCount=0;
    
    
    renderer = new THREE.WebGLRenderer({antialias: true, preserveDrawingBuffer: true,alpha:true});
    renderer.setSize(400,480 );
    renderer.setClearColor( 0xffffff, 0);
    document.getElementById('webgl').appendChild(renderer.domElement);
    window.addEventListener( 'resize', windowResize, false );
 
    camera.position.x = app360.camera.x; //200;
    camera.position.y = 0; //200;
    camera.position.z = app360.camera.z; //200;

    mesh.rotation.y =app360.common.viewValue;
    setTimeout(function(){
      update(renderer, scene, camera, controls);
    },100);

    // document.getElementById('img_val').src = renderer.domElement.toDataURL("image/png");
    // return false;
    
    setTimeout(function(){
      if(app360.common.viewValue==1)
      {
         type="front";
      }
      else{
        type="back";
      }
      var strMime = "image/png";
        imgData = renderer.domElement.toDataURL(strMime);
        $.ajax({
          url: app360.location+'api/dashboard/canvasImageUpload',
          data: {
            imgBase64: imgData,
            productOrderId:id,
            type:type,
            _token:''
          },
          type: 'post',
          success: function (response) 
          {  
            successCount=Number(successCount)+1;
            if(successCount==2)
            {
              $('.ub-loader').hide();
              app360.productCartDetail();
            }
          }
        });
      
        if(app360.common.viewValue==1)
        {
          app360.common.viewValue=-1.9;
        }
        else{
          app360.common.viewValue=1;
        } 
        getSvg();
        updatedscreen();
    },500);
   
    setTimeout(function(){

      if(app360.common.viewValue==1)
      {
         type="front";
      }
      else{
        type="back";
      }
        var strMime = "image/png";
        imgData = renderer.domElement.toDataURL(strMime);
        $('.ub-loader').show();
        $.ajax({
            url: app360.location+'api/dashboard/canvasImageUpload',
            data: {
              imgBase64: imgData,
              productOrderId:id,
              type:type,
              _token:''
            },
            type: 'post',
            success: function (response) 
            {  
               successCount=Number(successCount)+1;
               if(successCount==2)
               {
                  $('.ub-loader').hide();
                  app360.productCartDetail();
               }
            }
        });

     },1000);
}

app360.addPlayerRow=function(){
    var size = ['<option>Select Size</option>']; var sizeSelected=[];
    _.each(app360.resp.size_list,function(k){
      size.push('<option value="'+k+'">'+k+'</option>');
    });
    
    var htm="";
    htm +='<div style="display: flex;" class="ub-player_row" id="">';
    htm +='<div class="w-40"><div class="form-group mb-1"><input type="text" class="form-control ub-player_name" placeholder="Enter Player Name" /></div></div>';
    htm +='<div class="w-25"><div class="form-group mb-1"><input type="text" onkeypress="return checkOnlyDigits(event)" class="form-control ub-player_number" placeholder="Enter Number" /></div></div>';        
    htm +='<div class="w-20"><div class="form-group mb-1"><select class="form-control chosen-select ub-player_size">'; 
    htm +=size;
    htm +='</select></div></div>';        
    htm +='<div class="w-15 ub-delet_nm text-right pull-right">';                
    htm +='<a href="javascript:;" class="ub-player_delete"><img src="'+app360.location+"/"+'public/images/api/trash.svg" alt="trash" class="img-svg" /></a>';
    htm +='</div></div>';           
            
    $('#playerrow_result').append(htm);
    $("select").trigger("chosen:updated"); 
}

app360.getPlayerData=function(){
  var row0=[];
  var row1=[];
  var row2=[];
  var row3=[];
    $('.ids').each(function() {
        row0.push($(this).val());     
     }); 
    $('.ub-player_name').each(function() {
        if($(this).val()!="")
        {
           row1.push($(this).val());
        }
    }); 
    $('.ub-player_number').each(function() {
        if($(this).val()!="")
        {
           row2.push($(this).val());
        }
    }); 
    $('.ub-player_size').each(function() {
        if($(this).val()!="")
        {
           row3.push($(this).val());
        }
    }); 
    app360.playerData.ids=row0.join(",");
    app360.playerData.player_name=row1.join(",");
    app360.playerData.player_number =row2.join(",");
    app360.playerData.player_size =row3.join(",");
}

app360.deletePlayer=function(id){
  $.ajax({
      method: "POST",
      url: app360.location+"api/dashboard/deletePlayer",
      dataType: "json",
      data:{
        id:id,
        _token:''
      }
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app360.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
      // console.log(resp);
      if (resp.resp == 'ok') {
         $('#row_'+id).remove(); 
       }
    }); //- ajax

}//-ef deleteUser


app360.deleteProductLogo=function(imageName){

  $.ajax({
      method: "POST",
      url: app360.location+"api/dashboard/deleteProductLogo",
      dataType: "json",
      data:{
        imageName:imageName,
        _token:''
      }
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app360.alertMessage(response.msg,"warning");
    })
    .done(function(resp) {
  
      if (resp.resp == 'ok') {
        app360.productStep.image_value="";
        
        $('#dd_'+app360.productStep.fieldId).hide(); 
        $('#dd_'+app360.productStep.fieldId).parent().find('#uploadLogo').val(''); 
        app360.makeProductCart();
        // app360.removeCreateButtonsForImage();
       
       }
      
    }); //- ajax

}//-ef deleteUser




app360.emailToFriend=function(){
  $('#sendemailbutton').prop("disabled",true);
  $('#sendemailbutton').html("Send EMail....");
  app360.emailForm = {
          send_url:window.location.href,
          main_url:app360.location,
          name:app360.emailForm.name,
          email:app360.emailForm.email,
            _token:''
        };
  var formData = new FormData($('#sendemailtofriendform')[0]);
        formData.append("_token", app360.emailForm._token);
        formData.append("email", app360.emailForm.email);
        formData.append("name", app360.emailForm.name);
        formData.append("send_url",app360.emailForm.send_url);
        formData.append("main_url",app360.emailForm.main_url);
   $.ajax({
      method: "POST",
      url: app360.location+"api/dashboard/emailtofriend",
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         $('#sendemailbutton').prop("disabled",false);
        $('#sendemailbutton').html("Send Email");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
          $('#emailtofriendModal').hide();
          app360.resetemailModal();
       }
       
    }); //- ajax
}


app360.resetemailModal=function(){
    $('#yourname').val('');
    $('#friendemail').val('');
    $('#sendemailbutton').prop("disabled",false);
    $('#sendemailbutton').html("Send Email");
}


app360.getCartProductDetail=function(id){
  $('.ub-loader').show();
    var token = '';
      $.ajax({
          type: "POST",
          url: app360.location+'api/dashboard/getCartProductDetail',
          data : {id:id,_token:token},
          dataType:"json",
          success: function (resp) {
              app360.resp=resp;
              $('.ub-loader').hide();
              app360.showCartProductDetail();
          },
          error: function (xhr, status, error) 
          {
             // alert(xhr.responseText);
          }
      });
}

app360.showCartProductDetail=function(){

    var rows = [];

    $("#ub-CartList").hide();

    _.each(app360.resp.product_order_detail, function(content) {
        teamMemberDetail=_.filter(app360.resp.player_setting_Detail, function(x) {
            return x.productOrderId == parseInt(content.id);
        });
        data = _.countBy(teamMemberDetail, 'player_size')
        var sizeCount = _.map(data, function(value, key){
          return {
              size: key,
              count: value,
          };
        });
        var field_data=[];
        _.each(app360.resp.field_basic_dtl, function(field_cont) {
            if(field_cont.productOrderId==content.id)
            {
              field_data.push({"name":field_cont.field_name,"color":field_cont.color,"type":field_cont.filed_type,"color_name":field_cont.color_name,'fontsize':field_cont.fontsize});  
            }
        });

        var design_data=[];
        _.each(app360.resp.design_basic_dtl, function(design_cont) {
            if(design_cont.productOrderId==content.id)
            {
              design_data.push({"name":design_cont.design_name,"color":design_cont.color,"color_name":design_cont.color_name});  
            }
        });
        content.field_data=field_data;
        content.design_data=design_data;
        content.teamMemberDetail=teamMemberDetail;
        content.sizeCount=sizeCount;
        $("#ub-CartProductDetail").html(app360.templates.cartproductdetail(content));
    });
    $('#ub-CartProductDetail').show();
}

app360.getEditQuote=function(){
   $("#ub-CartList").hide();
   $("#ub-getAQuotePage").html(app360.templates.getquoteeditor());
}

app360.handleQuote =function(){
 app360.makeQuote();
     var errors=app360.validateQuote();
     
     if(errors.length>0)
     {
        app360.alertMessage(errors.join("<br \>"),"warning");
        return;
     }
     app360.saveQuote();

}

app360.makeQuote=function(){

   app360.quoteForm={
    distributorId:app360.filter.distributorId,
    email:$('#email').val(),
    message:$('#message').val(),
    company_name:$('#company_name').val(),
    mobile:$('#mobile').val(),
    _token:'',
  }


}
app360.validateQuote=function(){
  var error=[];
 
  if(app360.quoteForm.email=="")
  {
    alert("Please enter email");
      error.push("Please enter email");
  }
  else if(!app360.validateEmail(app360.quoteForm.email))
  {
      error.push("E-mail address is not valid");
      alert('E-mail address is not valid');
  }

    // console.log(error);
  
  return error;
}



app360.saveQuote=function(){
  $('.ub-loader').show();
  $('#quoteSubmitButton').prop("disabled",true);
  $('#quoteSubmitButton').html("Submit....");
  app360.quoteForm = {
          message:app360.quoteForm.message,
          mobile:app360.quoteForm.mobile,
          company_name:app360.quoteForm.company_name,
          email:app360.quoteForm.email,
          distributorId:app360.quoteForm.distributorId,
            _token:''
        };
  var formData = new FormData($('#getQuoteForm')[0]);
        formData.append("_token", app360.quoteForm._token);
        formData.append("email", app360.quoteForm.email);
        formData.append("mobile", app360.quoteForm.mobile);
        formData.append("company_name",app360.quoteForm.company_name);
        formData.append("message",app360.quoteForm.message);
        formData.append('distributorId',app360.quoteForm.distributorId);
   $.ajax({
      method: "POST",
      url: app360.location+"api/dashboard/saveQuote",
      dataType: "json",
      contentType: false,
      cache: false,           
      processData:false,
      data : formData ,
    })
    .fail(function(resp) {
         var response = $.parseJSON(resp.responseText);
         app360.alertMessage(response.msg,"warning");
        $('#quoteSubmitButton').prop("disabled",false);
        $('#quoteSubmitButton').html("Submit");
    })
    .done(function(resp) {
      if (resp.resp == 'ok') {
        $('.ub-loader').hide();
          app360.activeUrl();
       }
       
    }); //- ajax
}

app360.setDefaultDesignColor=function(){
  _.each(app360.resp.design_filed, function(content){
        if(content.color_value==null || content.color_value==""){
          if ($("#ub-texture>svg").find("#"+content.design_key).length!=0)
          {
            content.color_value = $("#ub-texture>svg").find("#"+content.design_key).attr('fill');
            // $("#ub-texture>svg").find("#"+design_key).css({ fill: app360.productStep.color_value });
          }
        }
        if(content.color_value==undefined)
        {
          content.color_value=null;
        }
    });
}

app360.loadTemplates = function() {

  app360.templates.modal = _.template("<div class='modal far-right' tabindex='-1' role='dialog' id='modal'><div class='modal-dialog' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title'><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div  class='modal-footer'><%= footer %></div></div></div></div>");
    
  app360.templates.norecord = _.template('<div class="col-sm-12 text-center"><img src="'+app360.location+'public/images/norecord.png" width="300"><h4 class="mt-4">No partner found</h4></div>');

} //- loadTemplates


app360.getParameterByName=function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}


function checkOnlyDigits(e) 
{
      e = e ? e : window.event;
      var charCode = e.which ? e.which : e.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {  
        return false;
      } else {
        return true;
      }
}


 app360.copyContent=function(id) {
    let textarea = document.getElementById(id);
    textarea.select();
    document.execCommand("copy");
  }

  function replaceLink(e,url) {
  // alert(url);
     e.target.href =url;

  }



function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  // console.log(ev);
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {

  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  var imageName = $('#'+data).attr('data-image_value');
  // ev.target.appendChild(document.getElementById(data));

  // var img = document.createElement("img");
  // img.src=document.getElementById(data).src;
  // img.id=data;  
  // img.width=app360.productStep.width_value; img.height=app360.productStep.height_value;
  app360.productStep.image_value=imageName;

    // app360.createButtonForLogo(app360.productStep);

  app360.makeProductCart();
  // ev.target.appendChild(htm);
}

app360.createButtonForLogo=function(logoDetail)
{

    var dragDropBtnGroup = document.createElement("DIV");            
          dragDropBtnGroup.id="dragDropBtnGroup_"+logoDetail.fieldId;
          dragDropBtnGroup.className = "image"+logoDetail.fieldId;
          dragDropBtnGroup.className += " buttonGroupDrag";
          document.getElementById('webgl').appendChild(dragDropBtnGroup);


    var deleteImageBtn = document.createElement("BUTTON");
      deleteImageBtn.innerHTML = "Delete";
      deleteImageBtn.class="dragDropBtnChange_"+logoDetail.fieldId;
      deleteImageBtn.id=logoDetail.fieldId;
      deleteImageBtn.setAttribute('data-image_value',logoDetail.image_value);
      deleteImageBtn.setAttribute("style","width:40px; font-size:8px;margin-right:2px;");
      deleteImageBtn.setAttribute('data-stepId',logoDetail.stepId);
      dragDropBtnGroup.appendChild(deleteImageBtn);
      deleteImageBtn.addEventListener("click", onButtonClickToDeleteImage, false);

    var sizeIncreaseBtn = document.createElement("BUTTON");
      sizeIncreaseBtn.innerHTML = "+";
      sizeIncreaseBtn.class="dragDropBtnChange_"+logoDetail.fieldId;
      sizeIncreaseBtn.id=logoDetail.fieldId;
      sizeIncreaseBtn.setAttribute('data-image_value',logoDetail.image_value);
      sizeIncreaseBtn.setAttribute("style","width:40px; font-size:8px;margin-right:2px;");
      sizeIncreaseBtn.setAttribute('data-image_size',logoDetail.width_value);
      sizeIncreaseBtn.setAttribute('data-stepId',logoDetail.stepId);
      dragDropBtnGroup.appendChild(sizeIncreaseBtn);
      sizeIncreaseBtn.addEventListener("click", onButtonClickToIncreaseSize, false);


    var sizeDecreaseBtn = document.createElement("BUTTON");
      sizeDecreaseBtn.innerHTML = "=";
      sizeDecreaseBtn.class="dragDropBtnChange_"+logoDetail.fieldId;
      sizeDecreaseBtn.id=logoDetail.fieldId;
      sizeDecreaseBtn.setAttribute('data-image_value',logoDetail.image_value);
      sizeDecreaseBtn.setAttribute("style","width:40px; font-size:8px;margin-right:2px;");
      sizeDecreaseBtn.setAttribute('data-image_size',logoDetail.width_value);
      sizeDecreaseBtn.setAttribute('data-stepId',logoDetail.stepId);
      dragDropBtnGroup.appendChild(sizeDecreaseBtn);
      sizeDecreaseBtn.addEventListener("click", onButtonClickToDecreaseSize, false);


    var rotateClockWiseBtn = document.createElement("BUTTON");
      rotateClockWiseBtn.innerHTML = "Rotate Right";
      rotateClockWiseBtn.class="dragDropBtnChange_"+logoDetail.fieldId;
      rotateClockWiseBtn.id=logoDetail.fieldId;
      rotateClockWiseBtn.setAttribute('data-image_value',logoDetail.image_value);
      rotateClockWiseBtn.setAttribute("style","width:40px; font-size:8px;margin-right:2px;");
      rotateClockWiseBtn.setAttribute('data-image_size',logoDetail.width_value);
      rotateClockWiseBtn.setAttribute('data-stepId',logoDetail.stepId);
      dragDropBtnGroup.appendChild(rotateClockWiseBtn);
      rotateClockWiseBtn.addEventListener("click", onButtonClickToRotateClockWise, false);


    var rotateAntiClockWiseBtn = document.createElement("BUTTON");
      rotateAntiClockWiseBtn.innerHTML = "Rotate Left";
      rotateAntiClockWiseBtn.class="dragDropBtnChange_"+logoDetail.fieldId;
      rotateAntiClockWiseBtn.id=logoDetail.fieldId;
      rotateAntiClockWiseBtn.setAttribute('data-image_value',logoDetail.image_value);
      rotateAntiClockWiseBtn.setAttribute("style","width:40px; font-size:8px;margin-right:2px;");
      rotateAntiClockWiseBtn.setAttribute('data-image_size',logoDetail.width_value);
      rotateAntiClockWiseBtn.setAttribute('data-stepId',logoDetail.stepId);
      dragDropBtnGroup.appendChild(rotateAntiClockWiseBtn);
      rotateAntiClockWiseBtn.addEventListener("click", onButtonClickToRotateAntiClockWise, false);
  
}

app360.removeCreateButtonsForImage=function(){
  if($('#webgl').find('#dragDropBtnGroup_'+app360.productStep.fieldId).find('button').length>0)
  {
    $('#webgl').find('#dragDropBtnGroup_'+app360.productStep.fieldId).remove();
  }
}


app360.createButtonForTextUploaded=function(textDetail)
{
    var dragDropTextBtnGroup = document.createElement("DIV");            
        dragDropTextBtnGroup.id="dragDropBtnGroup_"+textDetail.fieldId;
        dragDropTextBtnGroup.className = "text"+textDetail.fieldId;
        dragDropTextBtnGroup.className += " buttonGroupDrag";
        document.getElementById('webgl').appendChild(dragDropTextBtnGroup);

                  
    var deleteTextBtn = document.createElement("BUTTON");
        deleteTextBtn.innerHTML = "Delete";
        deleteTextBtn.class="dragDropBtnChange_"+textDetail.fieldId;
        deleteTextBtn.id=textDetail.fieldId;
        deleteTextBtn.setAttribute("style","width:40px; font-size:8px;margin-right:2px;");
        deleteTextBtn.setAttribute('data-stepId',textDetail.stepId);
        dragDropTextBtnGroup.appendChild(deleteTextBtn);
        deleteTextBtn.addEventListener("click", onButtonClickToDeleteText, false);


    var increaseTextSizeBtn = document.createElement("BUTTON");
        increaseTextSizeBtn.innerHTML = "+";
        increaseTextSizeBtn.class="dragDropBtnChange_"+textDetail.fieldId;
        increaseTextSizeBtn.id=textDetail.fieldId;
        increaseTextSizeBtn.setAttribute("style","width:40px; font-size:8px;margin-right:2px;");
        increaseTextSizeBtn.setAttribute('data-stepId',textDetail.stepId);
        dragDropTextBtnGroup.appendChild(increaseTextSizeBtn);
        increaseTextSizeBtn.addEventListener("click", onButtonClickToIncreaseFontSize, false);

    var decreaseTextSizeBtn = document.createElement("BUTTON");
        decreaseTextSizeBtn.innerHTML = "-";
        decreaseTextSizeBtn.class="dragDropBtnChange_"+textDetail.fieldId;
        decreaseTextSizeBtn.id=textDetail.fieldId;
        decreaseTextSizeBtn.setAttribute("style","width:40px; font-size:8px;margin-right:2px;");
        decreaseTextSizeBtn.setAttribute('data-stepId',textDetail.stepId);
        dragDropTextBtnGroup.appendChild(decreaseTextSizeBtn);
        decreaseTextSizeBtn.addEventListener("click", onButtonClickToDecreaseFontSize, false);

    var rotateClockWiseTextBtn = document.createElement("BUTTON");
        rotateClockWiseTextBtn.innerHTML = "Rotate Right";
        rotateClockWiseTextBtn.class="dragDropBtnChange_"+textDetail.fieldId;
        rotateClockWiseTextBtn.id=textDetail.fieldId;
        rotateClockWiseTextBtn.setAttribute("style","width:40px; font-size:8px;margin-right:2px;");
        rotateClockWiseTextBtn.setAttribute('data-stepId',textDetail.stepId);
        dragDropTextBtnGroup.appendChild(rotateClockWiseTextBtn);
        rotateClockWiseTextBtn.addEventListener("click", onButtonClickToRotateClockWiseText, false);

    var rotateAntiClockWiseTextBtn = document.createElement("BUTTON");
        rotateAntiClockWiseTextBtn.innerHTML = "Rotate Left";
        rotateAntiClockWiseTextBtn.class="dragDropBtnChange_"+textDetail.fieldId;
        rotateAntiClockWiseTextBtn.id=textDetail.fieldId;
        rotateAntiClockWiseTextBtn.setAttribute("style","width:40px; font-size:8px;margin-right:2px;");
        rotateAntiClockWiseTextBtn.setAttribute('data-stepId',textDetail.stepId);
        dragDropTextBtnGroup.appendChild(rotateAntiClockWiseTextBtn);
        rotateAntiClockWiseTextBtn.addEventListener("click", onButtonClickToRotateAntiClockWiseText, false);
}