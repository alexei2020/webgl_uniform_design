/* global $,_ *,Rollbar */

// THIS FILE MUST BE LOADED FIRST
var app360 = {
  dev: false,
  adminuser: false,
  templates: {}


}

if (!window.console) {
  window.console = {
    log: function() {}
  };
}

//dev sniffer
if (window.location.href.match(/imenso|localhost|temp/i)) {
  app360.dev = true;

}



//location sniffer
if(location.hostname=="uniformbuilder.com.au"){
  app360.location = 'https://uniformbuilder.com.au/api/';
}
else if (location.hostname=="imenso.co") {
  app360.location = 'https://imenso.co/dev/360Uniform/';
}
else if (window.location.href.match(/360Uniform/i)) { // local url of mahesh-pc
  app360.location = 'http://localhost/360Uniform/';
  // app360.location = 'http://192.168.1.120/360Uniform/';
}
else if (window.location.href.match(/360Uniform/i)) { // local url of mahesh-pc
  app360.location = 'https://imenso.co/dev/360Uniform/';
}


 app360.location =url_api;
app360.setLibListeners = function() {


   $(document).on('click touch','.close',function(e){
      e.preventDefault();
    $(this).closest('.alert').remove();
   });


} //setListeners



app360.loadAssets = function(cb) {

  var count = 0;
  _.each(app360.files, function(f) {
    app360.log("Loading: ", f.file);
    $.get(app360.location + f.file, function(html) {
      app360.templates[f.name] = _.template(html);
      count++;
      if (count == app360.files.length) {
        cb();
      }
    }).fail(function(resp) {
      app360.warn("Got error from ajax call load", resp);
      alert("There was an error  connecting to the server [02]");
    });

  }); //-each



} //loadAssets

app360.init = function(cb) {
  app360.log("loading config: ");



  app360.loadAssets(function() {
    app360.loadTemplates();


    app360.log("Init complete");
    if (_.isFunction(cb)) {
      cb();
    }
  });

} //init


app360.quoteattr=function(s, preserveCR) {
    preserveCR = preserveCR ? '&#13;' : '\n';
    return ('' + s) /* Forces the conversion to string. */
        .replace(/&/g, '&amp;') /* This MUST be the 1st replacement. */
        .replace(/'/g, '&apos;') /* The 4 other predefined entities, required. */
        .replace(/"/g, '&quot;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        /*
        You may add other replacements here for HTML only
        (but it's not necessary).
        Or for XML, only if the named entities are defined in its DTD.
        */
        .replace(/\r\n/g, preserveCR) /* Must be before the next replacement. */
        .replace(/[\r\n]/g, preserveCR);
        ;
}

app360.getHash = function() {

  if (location.hash.length > 1) {
    var pat = /#([A-z]*)=?(.*)/;
    var hash = pat.exec(location.hash);
  }

}; //- getHash

app360.validationMessage=function(msg,type)
{
  var errorString="";
   if($.isPlainObject(msg)){
     $.each(msg, function (key, val) {
             errorString += '<br \>' + val;
          });
  }
  else
  {
     errorString=msg;
  }
  if(errorString==undefined)
  {
    // location.reload();
  }
  else
  {
    app360.alertMessage(errorString,"warning");
  }
}

app360.alertMessage=function(msg,type){
  var randId=Math.random().toString(36).substring(7);
   msg='<a href="#" id="'+randId+'" class="close" aria-label="close">&times;</a>'+msg;
   msg="<div class='alert alert-"+type+"'>"+msg+"</div>";
  $(".error_section").prepend(msg);
   $('#'+randId).closest(".alert-"+type).show();
  setTimeout(function(){
    $('#'+randId).closest(".alert-"+type).remove();
  },5000);
}

//loader start
 app360.loader= function(){
  $.ajaxSetup({
    beforeSend: function(){
          var progressBar = $('#loader');
        if (progressBar.length == 0) {
          $('body').append('<div id="loader" class="loader"><dt></dt><dd></dd></div>');
         }
       $('#loader').animate({
          width:'30%'
         },200);
       },
        complete: function() {

         $('#loader').animate({
          width: '101%'

        }, 50);
         $('#loader').delay(200);
           $('#loader').fadeOut({
              complete: function() {
                  $('#loader').remove();
              }
          }, 200);

       },
      success: function(resp) {

       /* if (resp.error == true && resp.msg == 'timeout') {
          window.location.href=app360.location+"logout.html";
        }*/
      },
      error: function() {
          $('#ytLoad').remove();
      }
  });
}
app360.loader();

$(window).resize(function() {
    scrollMenuOnZoom();
});
  scrollMenuOnZoom();

function viewPassword()
{
   var passwordInput = document.getElementById('password');
  var passStatus = document.getElementById('pass-status');
  if (passwordInput.type == 'password')
  {
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash show_password';
  }
  else
  {
    passwordInput.type='password';
    passStatus.className='fa fa-eye show_password';
  }
}

app360.validateEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


app360.log = function() {
  if (app360.dev) {
    switch (arguments.length) { // yes this is janky, blame IE
      case 1:
        console.log(arguments[0]);
        break;
      case 2:
        console.log(arguments[0], arguments[1]);
        break;
      case 3:
        console.log(arguments[0], arguments[1], arguments[2]);
        break;
      case 4:
        console.log(arguments[0], arguments[1], arguments[2], arguments[3]);
        break;
    }
  }
}; //- app360.log

app360.warn = function() {

  switch (arguments.length) {
    case 1:
      console.log(arguments[0]);
      break;
    case 2:
      console.log(arguments[0], arguments[1]);
      break;
    case 3:
      console.log(arguments[0], arguments[1], arguments[2]);
      break;
  }

  if (typeof Rollbar != "undefined") {
    if (app360.location == 'nthrive') {
      switch (arguments.length) {
        case 1:
          Rollbar.warn(arguments[0]);
          break;
        case 2:
          Rollbar.warn(arguments[0], arguments[1]);
          break;
        case 3:
          Rollbar.warn(arguments[0], arguments[1], arguments[2]);
          break;
        case 4:
          Rollbar.warn(arguments[0], arguments[1], arguments[2], arguments[3]);
          break;
      }
    }
  }


}; //- app360.warn


function check(e,value){
    //Check Charater
        var unicode=e.charCode? e.charCode : e.keyCode;
        if (value.indexOf(".") != -1)if( unicode == 46 )return false;
        if (unicode!=8)if((unicode<48||unicode>57)&&unicode!=46)return false;

    }

    function checkLength(that,value){
    //Suppose u want 3 number of character
      if( value <= 100){ return true; }
      else{
          var str = that.value;
          str = str.substring(0, str.length - 1);
          that.value = str;
      }
    }

     function check_float(e,value){
    //Check Charater
        var unicode=e.charCode? e.charCode : e.keyCode;
        if (value.indexOf(".") != -1)if( unicode == 46 )return false;
        if (unicode!=8)if((unicode<48||unicode>57)&&unicode!=46)return false;
        if (value>5) {return false;}
    }


    function scrollMenuOnZoom()
    {

        var browserZoomLevel = Math.round(window.devicePixelRatio * 100);
//alert(screen.width+" == "+window.innerWidth);
    // $('#main_navBar').height();

    if($('#main_navBar').height()+100 <window.innerHeight){
              $("#main_navBar").css("height","");
              $("#main_navBar").css("overflow-y","");
        } else {
            var totalwidth = Math.round((screen.width/browserZoomLevel)*100);
            var diff = Math.round((screen.width/browserZoomLevel)*10);
            $("#main_navBar").css("height",(window.innerHeight-90)+"px");
            $("#main_navBar").css("overflow-y","auto");
        }
       /* if(screen.width == window.innerWidth){
              $("#main_navBar").css("height","");
              $("#main_navBar").css("overflow-y","");
        } else if(screen.width > window.innerWidth){
            var totalwidth = Math.round((screen.width/browserZoomLevel)*100);
            var diff = Math.round((screen.width/browserZoomLevel)*10);
            $("#main_navBar").css("height",(window.innerHeight-90)+"px");
            $("#main_navBar").css("overflow-y","auto");
        } */

    }
