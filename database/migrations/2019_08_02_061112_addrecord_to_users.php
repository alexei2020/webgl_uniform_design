<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddrecordToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
         DB::table('users')->insert([
                'name' => 'superadmin',
                'email' => 'sadmin@gmail.com',
                'password' => '$2y$10$UyIiorZ92TZvJwUX26SCq.Dt1qk1Tcz9LdwMSp7UH0IO2LQkmXkTW',
                'status'=>'active'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::table('users')->where(['email' => 'sadmin@gmail.com'])->delete();
    }
}
