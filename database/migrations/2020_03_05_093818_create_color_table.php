<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('color', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('distributorId')->unsigned();
            $table->string('name');
            $table->string('value')->nullable();
            $table->string('thumb')->nullable();
            $table->enum('status',['true','false']);
            $table->timestamps();
            $table->foreign('distributorId')->references('id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('color');
    }
}
