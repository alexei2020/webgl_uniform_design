<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignFiledTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('design_filed', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('designId')->unsigned();
            $table->bigInteger('stepId')->unsigned();
            $table->string('name');
            $table->string('design_key')->nullable();
            $table->bigInteger('groupId')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('designId')->references('id')->on('product_design');
            $table->foreign('stepId')->references('id')->on('product_step');
            $table->foreign('groupId')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('design_filed');
    }
}
