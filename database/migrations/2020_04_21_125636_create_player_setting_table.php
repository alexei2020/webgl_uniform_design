<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('productOrderId')->unsigned()->nullable();
            // $table->bigInteger('designId')->unsigned()->nullable();
            $table->string('player_name')->nullable();
            $table->string('player_number')->nullable();
            $table->string('player_size')->nullable();
            $table->timestamps();

            $table->foreign('productOrderId')->references('id')->on('product_order');
            // $table->foreign('designId')->references('id')->on('product_design');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_setting');
    }
}
