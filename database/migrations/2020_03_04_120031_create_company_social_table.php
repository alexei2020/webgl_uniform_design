<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanySocialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_social', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->bigInteger('companyId')->unsigned()->;
            $table->bigInteger('distributorId')->unsigned();
            $table->text('social_share_html')->nullable();
            $table->text('pubId')->nullable();
            $table->enum('status_add_script',['true','false']);
            $table->foreign('distributorId')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_social');
    }
}
