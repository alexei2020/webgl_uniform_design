<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('distributorId')->unsigned();
            $table->string('company_name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('website_url');
            $table->string('default_lang');
            $table->string('time_zone');
            $table->string('locale');
            $table->enum('status',['active','deactive']);
            $table->string('profile');

            $table->foreign('distributorId')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
