<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFontFileInFont extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('font', function (Blueprint $table) {
            $table->string('font_file')->nullable();
            $table->string('font_file_original_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('font', function (Blueprint $table) {
            $table->dropColumn('font_file');
            $table->dropColumn('font_file_original_name');
            
        });
    }
}
