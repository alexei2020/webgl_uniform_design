<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('quoteId')->unsigned();
            $table->bigInteger('productOrderId')->unsigned();
            $table->timestamps();

            $table->foreign('quoteId')->references('id')->on('get_quote');
            $table->foreign('productOrderId')->references('id')->on('product_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_order');
    }
}
