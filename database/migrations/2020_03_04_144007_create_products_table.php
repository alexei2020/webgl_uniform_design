<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('distributorId')->unsigned();
            $table->bigInteger('categoryId')->unsigned();
            $table->bigInteger('list_ImageId')->unsigned();
            $table->bigInteger('model_3DId')->unsigned();
            $table->string('name');
            $table->string('url_slug');
            $table->enum('rotation_disable',['true','false']);
            $table->string('cameras')->nullable();
            $table->string('camera_override')->nullable();
            $table->string('x_axis')->nullable();
            $table->string('y_axis')->nullable();
            $table->string('z_axis')->nullable();
            $table->string('scale')->nullable();
            $table->enum('override_model_position_scale',['true','false']);
            $table->string('light_brightness')->nullable();
            $table->string('svg_dimension_ht')->nullable();
            $table->string('svg_dimension_vt')->nullable();
            $table->string('hight_on_basker_page')->nullable();
            $table->string('default_toggle_color')->nullable();
            $table->string('position')->nullable();
            $table->enum('status',['active','deactive']);

            $table->timestamps();

            $table->foreign('distributorId')->references('id')->on('users');
            $table->foreign('categoryId')->references('id')->on('category');
            $table->foreign('list_ImageId')->references('id')->on('documents');
            $table->foreign('model_3DId')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
