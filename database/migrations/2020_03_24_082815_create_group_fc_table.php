<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupFcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_fc', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('groupId')->unsigned();
            $table->bigInteger('colorId')->unsigned()->nullable();
            $table->bigInteger('fontId')->unsigned()->nullable();
            $table->string('type')->nullable();
            $table->timestamps();

            $table->foreign('groupId')->references('id')->on('groups');
            $table->foreign('colorId')->references('id')->on('color');
            $table->foreign('fontId')->references('id')->on('font');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_fc');
    }
}
