<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailFriendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_friend', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->bigInteger('distributorId')->unsigned();
            $table->string('from_email');
          
            $table->string('from_name');
            $table->string('subject');
           
            $table->text('email_template');
           
            $table->timestamps();
            $table->foreign('distributorId')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_friend');
    }
}
