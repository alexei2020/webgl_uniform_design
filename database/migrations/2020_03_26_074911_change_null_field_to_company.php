<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNullFieldToCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company', function (Blueprint $table) {
            DB::statement('ALTER TABLE company MODIFY website_url VARCHAR(255)  NULL DEFAULT NULL;');
            DB::statement('ALTER TABLE company MODIFY default_lang VARCHAR(255)  NULL DEFAULT NULL;');
            DB::statement('ALTER TABLE company MODIFY time_zone VARCHAR(255)  NULL DEFAULT NULL;');
            DB::statement('ALTER TABLE company MODIFY locale VARCHAR(255)  NULL DEFAULT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company', function (Blueprint $table) {
            //
        });
    }
}
