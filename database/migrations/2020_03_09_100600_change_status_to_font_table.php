<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatusToFontTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('font', function (Blueprint $table) {
             DB::statement('ALTER TABLE font CHANGE status status ENUM("active","deactive") CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('font', function (Blueprint $table) {
            //
        });
    }
}
