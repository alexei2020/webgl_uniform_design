<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHeightWidthInProductOrderField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_order_field', function (Blueprint $table) {
            $table->text('height_value')->after('position_z')->nullable();
            $table->text('width_value')->after('position_z')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_order_field', function (Blueprint $table) {
            $table->dropColumn('height_value');
            $table->dropColumn('width_value');
        });
    }
}
