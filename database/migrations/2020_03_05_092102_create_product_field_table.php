<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_field', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('distributorId')->unsigned();
            $table->bigInteger('productId')->unsigned();
            $table->bigInteger('stepId')->unsigned();
            $table->string('name');
            $table->string('filed_type');
            $table->bigInteger('groupId')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('distributorId')->references('id')->on('users');
            $table->foreign('productId')->references('id')->on('products');
            $table->foreign('stepId')->references('id')->on('product_step');
            $table->foreign('groupId')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_field');
    }
}
