<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatusToProductStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_step', function (Blueprint $table) {
            DB::statement('ALTER TABLE product_step CHANGE step_status status ENUM("active","deactive") CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_step', function (Blueprint $table) {
            //
        });
    }
}
