<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderFrontBackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_front_back', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('productOrderId')->unsigned()->nullable();
            $table->enum('type',['front','back']);
            $table->string('image');
            $table->timestamps();

            $table->foreign('productOrderId')->references('id')->on('product_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_front_back');
    }
}
