<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDesignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_design', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('distributorId')->unsigned();
            $table->bigInteger('productId')->unsigned();
            $table->string('name');
            $table->string('sku')->nullable();
            // $table->bigInteger('overrideImageId')->unsigned()->nullable();
            $table->text('svg_snippet')->nullable();
            $table->enum('show_svg_defs',['true','false']);
            $table->string('position')->nullable();
            $table->enum('status',['active','deactive']);
            $table->timestamps();
            $table->foreign('distributorId')->references('id')->on('users');
            $table->foreign('productId')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_design');
    }
}
