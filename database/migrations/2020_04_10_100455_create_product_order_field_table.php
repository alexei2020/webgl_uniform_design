<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOrderFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_order_field', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('productOrderId')->unsigned();
            $table->bigInteger('stepId')->unsigned()->nullable();
            $table->bigInteger('fieldId')->unsigned()->nullable();
            $table->bigInteger('designId')->unsigned()->nullable();
            $table->bigInteger('designfieldId')->unsigned()->nullable();
            $table->string('color')->nullable();
            $table->string('fontsize')->nullable();
            $table->string('image')->nullable();
            $table->string('text')->nullable();
            $table->string('position_x')->nullable();
            $table->string('position_y')->nullable();
            $table->string('position_z')->nullable();
            $table->timestamps();

            $table->foreign('productOrderId')->references('id')->on('product_order');
            $table->foreign('stepId')->references('id')->on('product_step');
            $table->foreign('fieldId')->references('id')->on('product_field');
            $table->foreign('designId')->references('id')->on('product_design');
            $table->foreign('designfieldId')->references('id')->on('design_filed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_order_field');
    }
}
