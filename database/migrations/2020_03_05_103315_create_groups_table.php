<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('distributorId')->unsigned();
            $table->bigInteger('colorId')->unsigned()->nullable();
            $table->bigInteger('fontId')->unsigned()->nullable();
            $table->string('name');
            $table->enum('type',['color','font']);
            $table->timestamps();
            $table->foreign('distributorId')->references('id')->on('users');
            $table->foreign('colorId')->references('id')->on('color');
            $table->foreign('fontId')->references('id')->on('font');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
