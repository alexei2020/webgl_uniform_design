<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_step', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('distributorId')->unsigned();
            $table->bigInteger('productId')->unsigned();
            $table->string('name');
            $table->string('position')->nullable();
            $table->enum('step_status',['active','deactive']);
            $table->timestamps();
            $table->foreign('distributorId')->references('id')->on('users');
            $table->foreign('productId')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_step');
    }
}
