<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFontGroupIdInProductFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_field', function (Blueprint $table) {
            $table->bigInteger('fontGroupId')->after('groupId')->unsigned()->nullable();
            $table->foreign('fontGroupId')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_field', function (Blueprint $table) {
            $table->dropColumn('fontGroupId');
        });
    }
}
