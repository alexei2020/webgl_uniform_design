<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_form', function (Blueprint $table) {
           $table->bigIncrements('id');
            $table->bigInteger('distributorId')->unsigned();
            $table->string('name');
            $table->enum('notification',['false','true']);
            $table->enum('type',['Get a quote']);
            $table->string('email');
            $table->string('email_subject');
            $table->string('form_code');
            $table->string('submit_button_code');
            $table->text('intro_text');
            $table->text('footer_text');
            $table->enum('status',['active','deactive']);
            $table->timestamps();
            $table->foreign('distributorId')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('quote_form');
    }
}
