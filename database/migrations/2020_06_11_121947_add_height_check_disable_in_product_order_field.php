<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHeightCheckDisableInProductOrderField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_order_field', function (Blueprint $table) {
            $table->enum('height_check_disable', ['false','true'])->nullable();
            $table->enum('width_check_disable', ['false','true'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_order_field', function (Blueprint $table) {
            $table->dropColumn('height_check_disable');
            $table->dropColumn('width_check_disable');
        });
    }
}
