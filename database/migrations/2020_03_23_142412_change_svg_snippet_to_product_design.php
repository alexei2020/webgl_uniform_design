<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSvgSnippetToProductDesign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_design', function (Blueprint $table) {
            DB::statement('ALTER TABLE product_design MODIFY svg_snippet LONGTEXT  NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_design', function (Blueprint $table) {
            //
        });
    }
}
